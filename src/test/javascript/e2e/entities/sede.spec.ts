import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Sede e2e test', () => {

    let navBarPage: NavBarPage;
    let sedeDialogPage: SedeDialogPage;
    let sedeComponentsPage: SedeComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Sedes', () => {
        navBarPage.goToEntity('sede');
        sedeComponentsPage = new SedeComponentsPage();
        expect(sedeComponentsPage.getTitle())
            .toMatch(/Sedes/);

    });

    it('should load create Sede dialog', () => {
        sedeComponentsPage.clickOnCreateButton();
        sedeDialogPage = new SedeDialogPage();
        expect(sedeDialogPage.getModalTitle())
            .toMatch(/Create or edit a Sede/);
        sedeDialogPage.close();
    });

    it('should create and save Sedes', () => {
        sedeComponentsPage.clickOnCreateButton();
        sedeDialogPage.setNombreSedeInput('nombreSede');
        expect(sedeDialogPage.getNombreSedeInput()).toMatch('nombreSede');
        sedeDialogPage.setDireccionSedeInput('direccionSede');
        expect(sedeDialogPage.getDireccionSedeInput()).toMatch('direccionSede');
        sedeDialogPage.getEstadoSedeInput().isSelected().then((selected) => {
            if (selected) {
                sedeDialogPage.getEstadoSedeInput().click();
                expect(sedeDialogPage.getEstadoSedeInput().isSelected()).toBeFalsy();
            } else {
                sedeDialogPage.getEstadoSedeInput().click();
                expect(sedeDialogPage.getEstadoSedeInput().isSelected()).toBeTruthy();
            }
        });
        sedeDialogPage.gimnasioSelectLastOption();
        sedeDialogPage.save();
        expect(sedeDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class SedeComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-sede div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class SedeDialogPage {
    modalTitle = element(by.css('h4#mySedeLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nombreSedeInput = element(by.css('input#field_nombreSede'));
    direccionSedeInput = element(by.css('input#field_direccionSede'));
    estadoSedeInput = element(by.css('input#field_estadoSede'));
    gimnasioSelect = element(by.css('select#field_gimnasio'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setNombreSedeInput = function(nombreSede) {
        this.nombreSedeInput.sendKeys(nombreSede);
    };

    getNombreSedeInput = function() {
        return this.nombreSedeInput.getAttribute('value');
    };

    setDireccionSedeInput = function(direccionSede) {
        this.direccionSedeInput.sendKeys(direccionSede);
    };

    getDireccionSedeInput = function() {
        return this.direccionSedeInput.getAttribute('value');
    };

    getEstadoSedeInput = function() {
        return this.estadoSedeInput;
    };
    gimnasioSelectLastOption = function() {
        this.gimnasioSelect.all(by.tagName('option')).last().click();
    };

    gimnasioSelectOption = function(option) {
        this.gimnasioSelect.sendKeys(option);
    };

    getGimnasioSelect = function() {
        return this.gimnasioSelect;
    };

    getGimnasioSelectedOption = function() {
        return this.gimnasioSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
