import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Rol e2e test', () => {

    let navBarPage: NavBarPage;
    let rolDialogPage: RolDialogPage;
    let rolComponentsPage: RolComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Rols', () => {
        navBarPage.goToEntity('rol');
        rolComponentsPage = new RolComponentsPage();
        expect(rolComponentsPage.getTitle())
            .toMatch(/Rols/);

    });

    it('should load create Rol dialog', () => {
        rolComponentsPage.clickOnCreateButton();
        rolDialogPage = new RolDialogPage();
        expect(rolDialogPage.getModalTitle())
            .toMatch(/Create or edit a Rol/);
        rolDialogPage.close();
    });

    it('should create and save Rols', () => {
        rolComponentsPage.clickOnCreateButton();
        rolDialogPage.setNombreRolInput('nombreRol');
        expect(rolDialogPage.getNombreRolInput()).toMatch('nombreRol');
        rolDialogPage.getEstadoRolInput().isSelected().then((selected) => {
            if (selected) {
                rolDialogPage.getEstadoRolInput().click();
                expect(rolDialogPage.getEstadoRolInput().isSelected()).toBeFalsy();
            } else {
                rolDialogPage.getEstadoRolInput().click();
                expect(rolDialogPage.getEstadoRolInput().isSelected()).toBeTruthy();
            }
        });
        // rolDialogPage.rolXPermisoSelectLastOption();
        rolDialogPage.save();
        expect(rolDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class RolComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-rol div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class RolDialogPage {
    modalTitle = element(by.css('h4#myRolLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nombreRolInput = element(by.css('input#field_nombreRol'));
    estadoRolInput = element(by.css('input#field_estadoRol'));
    rolXPermisoSelect = element(by.css('select#field_rolXPermiso'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setNombreRolInput = function(nombreRol) {
        this.nombreRolInput.sendKeys(nombreRol);
    };

    getNombreRolInput = function() {
        return this.nombreRolInput.getAttribute('value');
    };

    getEstadoRolInput = function() {
        return this.estadoRolInput;
    };
    rolXPermisoSelectLastOption = function() {
        this.rolXPermisoSelect.all(by.tagName('option')).last().click();
    };

    rolXPermisoSelectOption = function(option) {
        this.rolXPermisoSelect.sendKeys(option);
    };

    getRolXPermisoSelect = function() {
        return this.rolXPermisoSelect;
    };

    getRolXPermisoSelectedOption = function() {
        return this.rolXPermisoSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
