import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Invitacion e2e test', () => {

    let navBarPage: NavBarPage;
    let invitacionDialogPage: InvitacionDialogPage;
    let invitacionComponentsPage: InvitacionComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Invitacions', () => {
        navBarPage.goToEntity('invitacion');
        invitacionComponentsPage = new InvitacionComponentsPage();
        expect(invitacionComponentsPage.getTitle())
            .toMatch(/Invitacions/);

    });

    it('should load create Invitacion dialog', () => {
        invitacionComponentsPage.clickOnCreateButton();
        invitacionDialogPage = new InvitacionDialogPage();
        expect(invitacionDialogPage.getModalTitle())
            .toMatch(/Create or edit a Invitacion/);
        invitacionDialogPage.close();
    });

    it('should create and save Invitacions', () => {
        invitacionComponentsPage.clickOnCreateButton();
        invitacionDialogPage.setHorarioInvitacionInput(12310020012301);
        expect(invitacionDialogPage.getHorarioInvitacionInput()).toMatch('2001-12-31T02:30');
        invitacionDialogPage.getEstadoInvitacionInput().isSelected().then((selected) => {
            if (selected) {
                invitacionDialogPage.getEstadoInvitacionInput().click();
                expect(invitacionDialogPage.getEstadoInvitacionInput().isSelected()).toBeFalsy();
            } else {
                invitacionDialogPage.getEstadoInvitacionInput().click();
                expect(invitacionDialogPage.getEstadoInvitacionInput().isSelected()).toBeTruthy();
            }
        });
        invitacionDialogPage.usuarioSelectLastOption();
        invitacionDialogPage.aceptaInvitacionSelectLastOption();
        invitacionDialogPage.save();
        expect(invitacionDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class InvitacionComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-invitacion div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class InvitacionDialogPage {
    modalTitle = element(by.css('h4#myInvitacionLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    horarioInvitacionInput = element(by.css('input#field_horarioInvitacion'));
    estadoInvitacionInput = element(by.css('input#field_estadoInvitacion'));
    usuarioSelect = element(by.css('select#field_usuario'));
    aceptaInvitacionSelect = element(by.css('select#field_aceptaInvitacion'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setHorarioInvitacionInput = function(horarioInvitacion) {
        this.horarioInvitacionInput.sendKeys(horarioInvitacion);
    };

    getHorarioInvitacionInput = function() {
        return this.horarioInvitacionInput.getAttribute('value');
    };

    getEstadoInvitacionInput = function() {
        return this.estadoInvitacionInput;
    };
    usuarioSelectLastOption = function() {
        this.usuarioSelect.all(by.tagName('option')).last().click();
    };

    usuarioSelectOption = function(option) {
        this.usuarioSelect.sendKeys(option);
    };

    getUsuarioSelect = function() {
        return this.usuarioSelect;
    };

    getUsuarioSelectedOption = function() {
        return this.usuarioSelect.element(by.css('option:checked')).getText();
    };

    aceptaInvitacionSelectLastOption = function() {
        this.aceptaInvitacionSelect.all(by.tagName('option')).last().click();
    };

    aceptaInvitacionSelectOption = function(option) {
        this.aceptaInvitacionSelect.sendKeys(option);
    };

    getAceptaInvitacionSelect = function() {
        return this.aceptaInvitacionSelect;
    };

    getAceptaInvitacionSelectedOption = function() {
        return this.aceptaInvitacionSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
