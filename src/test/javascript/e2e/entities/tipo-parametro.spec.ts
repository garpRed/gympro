import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('TipoParametro e2e test', () => {

    let navBarPage: NavBarPage;
    let tipoParametroDialogPage: TipoParametroDialogPage;
    let tipoParametroComponentsPage: TipoParametroComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load TipoParametros', () => {
        navBarPage.goToEntity('tipo-parametro');
        tipoParametroComponentsPage = new TipoParametroComponentsPage();
        expect(tipoParametroComponentsPage.getTitle())
            .toMatch(/Tipo Parametros/);

    });

    it('should load create TipoParametro dialog', () => {
        tipoParametroComponentsPage.clickOnCreateButton();
        tipoParametroDialogPage = new TipoParametroDialogPage();
        expect(tipoParametroDialogPage.getModalTitle())
            .toMatch(/Create or edit a Tipo Parametro/);
        tipoParametroDialogPage.close();
    });

    it('should create and save TipoParametros', () => {
        tipoParametroComponentsPage.clickOnCreateButton();
        tipoParametroDialogPage.setNombreTipoParametroInput('nombreTipoParametro');
        expect(tipoParametroDialogPage.getNombreTipoParametroInput()).toMatch('nombreTipoParametro');
        tipoParametroDialogPage.getEstadoTipoParametroInput().isSelected().then((selected) => {
            if (selected) {
                tipoParametroDialogPage.getEstadoTipoParametroInput().click();
                expect(tipoParametroDialogPage.getEstadoTipoParametroInput().isSelected()).toBeFalsy();
            } else {
                tipoParametroDialogPage.getEstadoTipoParametroInput().click();
                expect(tipoParametroDialogPage.getEstadoTipoParametroInput().isSelected()).toBeTruthy();
            }
        });
        tipoParametroDialogPage.save();
        expect(tipoParametroDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class TipoParametroComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-tipo-parametro div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class TipoParametroDialogPage {
    modalTitle = element(by.css('h4#myTipoParametroLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nombreTipoParametroInput = element(by.css('input#field_nombreTipoParametro'));
    estadoTipoParametroInput = element(by.css('input#field_estadoTipoParametro'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setNombreTipoParametroInput = function(nombreTipoParametro) {
        this.nombreTipoParametroInput.sendKeys(nombreTipoParametro);
    };

    getNombreTipoParametroInput = function() {
        return this.nombreTipoParametroInput.getAttribute('value');
    };

    getEstadoTipoParametroInput = function() {
        return this.estadoTipoParametroInput;
    };
    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
