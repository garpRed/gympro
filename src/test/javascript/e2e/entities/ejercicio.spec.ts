import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Ejercicio e2e test', () => {

    let navBarPage: NavBarPage;
    let ejercicioDialogPage: EjercicioDialogPage;
    let ejercicioComponentsPage: EjercicioComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Ejercicios', () => {
        navBarPage.goToEntity('ejercicio');
        ejercicioComponentsPage = new EjercicioComponentsPage();
        expect(ejercicioComponentsPage.getTitle())
            .toMatch(/Ejercicios/);

    });

    it('should load create Ejercicio dialog', () => {
        ejercicioComponentsPage.clickOnCreateButton();
        ejercicioDialogPage = new EjercicioDialogPage();
        expect(ejercicioDialogPage.getModalTitle())
            .toMatch(/Create or edit a Ejercicio/);
        ejercicioDialogPage.close();
    });

    it('should create and save Ejercicios', () => {
        ejercicioComponentsPage.clickOnCreateButton();
        ejercicioDialogPage.setNombreEjercicioInput('nombreEjercicio');
        expect(ejercicioDialogPage.getNombreEjercicioInput()).toMatch('nombreEjercicio');
        ejercicioDialogPage.getEstadoEjercicioInput().isSelected().then((selected) => {
            if (selected) {
                ejercicioDialogPage.getEstadoEjercicioInput().click();
                expect(ejercicioDialogPage.getEstadoEjercicioInput().isSelected()).toBeFalsy();
            } else {
                ejercicioDialogPage.getEstadoEjercicioInput().click();
                expect(ejercicioDialogPage.getEstadoEjercicioInput().isSelected()).toBeTruthy();
            }
        });
        ejercicioDialogPage.sedeSelectLastOption();
        // ejercicioDialogPage.ejercicioActivosSelectLastOption();
        // ejercicioDialogPage.ejercicioMusculosSelectLastOption();
        ejercicioDialogPage.save();
        expect(ejercicioDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class EjercicioComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-ejercicio div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class EjercicioDialogPage {
    modalTitle = element(by.css('h4#myEjercicioLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nombreEjercicioInput = element(by.css('input#field_nombreEjercicio'));
    estadoEjercicioInput = element(by.css('input#field_estadoEjercicio'));
    sedeSelect = element(by.css('select#field_sede'));
    ejercicioActivosSelect = element(by.css('select#field_ejercicioActivos'));
    ejercicioMusculosSelect = element(by.css('select#field_ejercicioMusculos'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setNombreEjercicioInput = function(nombreEjercicio) {
        this.nombreEjercicioInput.sendKeys(nombreEjercicio);
    };

    getNombreEjercicioInput = function() {
        return this.nombreEjercicioInput.getAttribute('value');
    };

    getEstadoEjercicioInput = function() {
        return this.estadoEjercicioInput;
    };
    sedeSelectLastOption = function() {
        this.sedeSelect.all(by.tagName('option')).last().click();
    };

    sedeSelectOption = function(option) {
        this.sedeSelect.sendKeys(option);
    };

    getSedeSelect = function() {
        return this.sedeSelect;
    };

    getSedeSelectedOption = function() {
        return this.sedeSelect.element(by.css('option:checked')).getText();
    };

    ejercicioActivosSelectLastOption = function() {
        this.ejercicioActivosSelect.all(by.tagName('option')).last().click();
    };

    ejercicioActivosSelectOption = function(option) {
        this.ejercicioActivosSelect.sendKeys(option);
    };

    getEjercicioActivosSelect = function() {
        return this.ejercicioActivosSelect;
    };

    getEjercicioActivosSelectedOption = function() {
        return this.ejercicioActivosSelect.element(by.css('option:checked')).getText();
    };

    ejercicioMusculosSelectLastOption = function() {
        this.ejercicioMusculosSelect.all(by.tagName('option')).last().click();
    };

    ejercicioMusculosSelectOption = function(option) {
        this.ejercicioMusculosSelect.sendKeys(option);
    };

    getEjercicioMusculosSelect = function() {
        return this.ejercicioMusculosSelect;
    };

    getEjercicioMusculosSelectedOption = function() {
        return this.ejercicioMusculosSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
