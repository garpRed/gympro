import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Permiso e2e test', () => {

    let navBarPage: NavBarPage;
    let permisoDialogPage: PermisoDialogPage;
    let permisoComponentsPage: PermisoComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Permisos', () => {
        navBarPage.goToEntity('permiso');
        permisoComponentsPage = new PermisoComponentsPage();
        expect(permisoComponentsPage.getTitle())
            .toMatch(/Permisos/);

    });

    it('should load create Permiso dialog', () => {
        permisoComponentsPage.clickOnCreateButton();
        permisoDialogPage = new PermisoDialogPage();
        expect(permisoDialogPage.getModalTitle())
            .toMatch(/Create or edit a Permiso/);
        permisoDialogPage.close();
    });

    it('should create and save Permisos', () => {
        permisoComponentsPage.clickOnCreateButton();
        permisoDialogPage.setNombrePermisoInput('nombrePermiso');
        expect(permisoDialogPage.getNombrePermisoInput()).toMatch('nombrePermiso');
        permisoDialogPage.getEstadoPermisoInput().isSelected().then((selected) => {
            if (selected) {
                permisoDialogPage.getEstadoPermisoInput().click();
                expect(permisoDialogPage.getEstadoPermisoInput().isSelected()).toBeFalsy();
            } else {
                permisoDialogPage.getEstadoPermisoInput().click();
                expect(permisoDialogPage.getEstadoPermisoInput().isSelected()).toBeTruthy();
            }
        });
        permisoDialogPage.save();
        expect(permisoDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class PermisoComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-permiso div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class PermisoDialogPage {
    modalTitle = element(by.css('h4#myPermisoLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nombrePermisoInput = element(by.css('input#field_nombrePermiso'));
    estadoPermisoInput = element(by.css('input#field_estadoPermiso'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setNombrePermisoInput = function(nombrePermiso) {
        this.nombrePermisoInput.sendKeys(nombrePermiso);
    };

    getNombrePermisoInput = function() {
        return this.nombrePermisoInput.getAttribute('value');
    };

    getEstadoPermisoInput = function() {
        return this.estadoPermisoInput;
    };
    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
