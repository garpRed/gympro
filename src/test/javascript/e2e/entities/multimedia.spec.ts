import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Multimedia e2e test', () => {

    let navBarPage: NavBarPage;
    let multimediaDialogPage: MultimediaDialogPage;
    let multimediaComponentsPage: MultimediaComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Multimedias', () => {
        navBarPage.goToEntity('multimedia');
        multimediaComponentsPage = new MultimediaComponentsPage();
        expect(multimediaComponentsPage.getTitle())
            .toMatch(/Multimedias/);

    });

    it('should load create Multimedia dialog', () => {
        multimediaComponentsPage.clickOnCreateButton();
        multimediaDialogPage = new MultimediaDialogPage();
        expect(multimediaDialogPage.getModalTitle())
            .toMatch(/Create or edit a Multimedia/);
        multimediaDialogPage.close();
    });

    it('should create and save Multimedias', () => {
        multimediaComponentsPage.clickOnCreateButton();
        multimediaDialogPage.setNombreMultimediaInput('nombreMultimedia');
        expect(multimediaDialogPage.getNombreMultimediaInput()).toMatch('nombreMultimedia');
        multimediaDialogPage.setPropositoMultimediaInput('5');
        expect(multimediaDialogPage.getPropositoMultimediaInput()).toMatch('5');
        multimediaDialogPage.setEncodeInput('encode');
        expect(multimediaDialogPage.getEncodeInput()).toMatch('encode');
        multimediaDialogPage.tipoMultimediaSelectLastOption();
        multimediaDialogPage.save();
        expect(multimediaDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class MultimediaComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-multimedia div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class MultimediaDialogPage {
    modalTitle = element(by.css('h4#myMultimediaLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nombreMultimediaInput = element(by.css('input#field_nombreMultimedia'));
    propositoMultimediaInput = element(by.css('input#field_propositoMultimedia'));
    encodeInput = element(by.css('input#field_encode'));
    tipoMultimediaSelect = element(by.css('select#field_tipoMultimedia'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setNombreMultimediaInput = function(nombreMultimedia) {
        this.nombreMultimediaInput.sendKeys(nombreMultimedia);
    };

    getNombreMultimediaInput = function() {
        return this.nombreMultimediaInput.getAttribute('value');
    };

    setPropositoMultimediaInput = function(propositoMultimedia) {
        this.propositoMultimediaInput.sendKeys(propositoMultimedia);
    };

    getPropositoMultimediaInput = function() {
        return this.propositoMultimediaInput.getAttribute('value');
    };

    setEncodeInput = function(encode) {
        this.encodeInput.sendKeys(encode);
    };

    getEncodeInput = function() {
        return this.encodeInput.getAttribute('value');
    };

    tipoMultimediaSelectLastOption = function() {
        this.tipoMultimediaSelect.all(by.tagName('option')).last().click();
    };

    tipoMultimediaSelectOption = function(option) {
        this.tipoMultimediaSelect.sendKeys(option);
    };

    getTipoMultimediaSelect = function() {
        return this.tipoMultimediaSelect;
    };

    getTipoMultimediaSelectedOption = function() {
        return this.tipoMultimediaSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
