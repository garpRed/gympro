import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Musculo e2e test', () => {

    let navBarPage: NavBarPage;
    let musculoDialogPage: MusculoDialogPage;
    let musculoComponentsPage: MusculoComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Musculos', () => {
        navBarPage.goToEntity('musculo');
        musculoComponentsPage = new MusculoComponentsPage();
        expect(musculoComponentsPage.getTitle())
            .toMatch(/Musculos/);

    });

    it('should load create Musculo dialog', () => {
        musculoComponentsPage.clickOnCreateButton();
        musculoDialogPage = new MusculoDialogPage();
        expect(musculoDialogPage.getModalTitle())
            .toMatch(/Create or edit a Musculo/);
        musculoDialogPage.close();
    });

    it('should create and save Musculos', () => {
        musculoComponentsPage.clickOnCreateButton();
        musculoDialogPage.setNombreMusculoInput('nombreMusculo');
        expect(musculoDialogPage.getNombreMusculoInput()).toMatch('nombreMusculo');
        musculoDialogPage.getEstadoMusculoInput().isSelected().then((selected) => {
            if (selected) {
                musculoDialogPage.getEstadoMusculoInput().click();
                expect(musculoDialogPage.getEstadoMusculoInput().isSelected()).toBeFalsy();
            } else {
                musculoDialogPage.getEstadoMusculoInput().click();
                expect(musculoDialogPage.getEstadoMusculoInput().isSelected()).toBeTruthy();
            }
        });
        musculoDialogPage.zonaMusculoSelectLastOption();
        musculoDialogPage.save();
        expect(musculoDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class MusculoComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-musculo div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class MusculoDialogPage {
    modalTitle = element(by.css('h4#myMusculoLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nombreMusculoInput = element(by.css('input#field_nombreMusculo'));
    estadoMusculoInput = element(by.css('input#field_estadoMusculo'));
    zonaMusculoSelect = element(by.css('select#field_zonaMusculo'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setNombreMusculoInput = function(nombreMusculo) {
        this.nombreMusculoInput.sendKeys(nombreMusculo);
    };

    getNombreMusculoInput = function() {
        return this.nombreMusculoInput.getAttribute('value');
    };

    getEstadoMusculoInput = function() {
        return this.estadoMusculoInput;
    };
    zonaMusculoSelectLastOption = function() {
        this.zonaMusculoSelect.all(by.tagName('option')).last().click();
    };

    zonaMusculoSelectOption = function(option) {
        this.zonaMusculoSelect.sendKeys(option);
    };

    getZonaMusculoSelect = function() {
        return this.zonaMusculoSelect;
    };

    getZonaMusculoSelectedOption = function() {
        return this.zonaMusculoSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
