import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Solicitud e2e test', () => {

    let navBarPage: NavBarPage;
    let solicitudDialogPage: SolicitudDialogPage;
    let solicitudComponentsPage: SolicitudComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Solicituds', () => {
        navBarPage.goToEntity('solicitud');
        solicitudComponentsPage = new SolicitudComponentsPage();
        expect(solicitudComponentsPage.getTitle())
            .toMatch(/Solicituds/);

    });

    it('should load create Solicitud dialog', () => {
        solicitudComponentsPage.clickOnCreateButton();
        solicitudDialogPage = new SolicitudDialogPage();
        expect(solicitudDialogPage.getModalTitle())
            .toMatch(/Create or edit a Solicitud/);
        solicitudDialogPage.close();
    });

    it('should create and save Solicituds', () => {
        solicitudComponentsPage.clickOnCreateButton();
        solicitudDialogPage.setDescripcionSolicitudInput('descripcionSolicitud');
        expect(solicitudDialogPage.getDescripcionSolicitudInput()).toMatch('descripcionSolicitud');
        solicitudDialogPage.setFechaSolicitudInput(12310020012301);
        expect(solicitudDialogPage.getFechaSolicitudInput()).toMatch('2001-12-31T02:30');
        solicitudDialogPage.setHoraInicioInput(12310020012301);
        expect(solicitudDialogPage.getHoraInicioInput()).toMatch('2001-12-31T02:30');
        solicitudDialogPage.setHoraFinInput(12310020012301);
        expect(solicitudDialogPage.getHoraFinInput()).toMatch('2001-12-31T02:30');
        solicitudDialogPage.usuarioSelectLastOption();
        solicitudDialogPage.entrenadorSolicitudSelectLastOption();
        solicitudDialogPage.tipoSolicitudSelectLastOption();
        solicitudDialogPage.save();
        expect(solicitudDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class SolicitudComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-solicitud div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class SolicitudDialogPage {
    modalTitle = element(by.css('h4#mySolicitudLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    descripcionSolicitudInput = element(by.css('input#field_descripcionSolicitud'));
    fechaSolicitudInput = element(by.css('input#field_fechaSolicitud'));
    horaInicioInput = element(by.css('input#field_horaInicio'));
    horaFinInput = element(by.css('input#field_horaFin'));
    usuarioSelect = element(by.css('select#field_usuario'));
    entrenadorSolicitudSelect = element(by.css('select#field_entrenadorSolicitud'));
    tipoSolicitudSelect = element(by.css('select#field_tipoSolicitud'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setDescripcionSolicitudInput = function(descripcionSolicitud) {
        this.descripcionSolicitudInput.sendKeys(descripcionSolicitud);
    };

    getDescripcionSolicitudInput = function() {
        return this.descripcionSolicitudInput.getAttribute('value');
    };

    setFechaSolicitudInput = function(fechaSolicitud) {
        this.fechaSolicitudInput.sendKeys(fechaSolicitud);
    };

    getFechaSolicitudInput = function() {
        return this.fechaSolicitudInput.getAttribute('value');
    };

    setHoraInicioInput = function(horaInicio) {
        this.horaInicioInput.sendKeys(horaInicio);
    };

    getHoraInicioInput = function() {
        return this.horaInicioInput.getAttribute('value');
    };

    setHoraFinInput = function(horaFin) {
        this.horaFinInput.sendKeys(horaFin);
    };

    getHoraFinInput = function() {
        return this.horaFinInput.getAttribute('value');
    };

    usuarioSelectLastOption = function() {
        this.usuarioSelect.all(by.tagName('option')).last().click();
    };

    usuarioSelectOption = function(option) {
        this.usuarioSelect.sendKeys(option);
    };

    getUsuarioSelect = function() {
        return this.usuarioSelect;
    };

    getUsuarioSelectedOption = function() {
        return this.usuarioSelect.element(by.css('option:checked')).getText();
    };

    entrenadorSolicitudSelectLastOption = function() {
        this.entrenadorSolicitudSelect.all(by.tagName('option')).last().click();
    };

    entrenadorSolicitudSelectOption = function(option) {
        this.entrenadorSolicitudSelect.sendKeys(option);
    };

    getEntrenadorSolicitudSelect = function() {
        return this.entrenadorSolicitudSelect;
    };

    getEntrenadorSolicitudSelectedOption = function() {
        return this.entrenadorSolicitudSelect.element(by.css('option:checked')).getText();
    };

    tipoSolicitudSelectLastOption = function() {
        this.tipoSolicitudSelect.all(by.tagName('option')).last().click();
    };

    tipoSolicitudSelectOption = function(option) {
        this.tipoSolicitudSelect.sendKeys(option);
    };

    getTipoSolicitudSelect = function() {
        return this.tipoSolicitudSelect;
    };

    getTipoSolicitudSelectedOption = function() {
        return this.tipoSolicitudSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
