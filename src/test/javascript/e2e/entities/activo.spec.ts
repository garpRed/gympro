import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Activo e2e test', () => {

    let navBarPage: NavBarPage;
    let activoDialogPage: ActivoDialogPage;
    let activoComponentsPage: ActivoComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Activos', () => {
        navBarPage.goToEntity('activo');
        activoComponentsPage = new ActivoComponentsPage();
        expect(activoComponentsPage.getTitle())
            .toMatch(/Activos/);

    });

    it('should load create Activo dialog', () => {
        activoComponentsPage.clickOnCreateButton();
        activoDialogPage = new ActivoDialogPage();
        expect(activoDialogPage.getModalTitle())
            .toMatch(/Create or edit a Activo/);
        activoDialogPage.close();
    });

    it('should create and save Activos', () => {
        activoComponentsPage.clickOnCreateButton();
        activoDialogPage.setNombreActivoInput('nombreActivo');
        expect(activoDialogPage.getNombreActivoInput()).toMatch('nombreActivo');
        activoDialogPage.setCantActivosInput('5');
        expect(activoDialogPage.getCantActivosInput()).toMatch('5');
        activoDialogPage.getEstadoActivoInput().isSelected().then((selected) => {
            if (selected) {
                activoDialogPage.getEstadoActivoInput().click();
                expect(activoDialogPage.getEstadoActivoInput().isSelected()).toBeFalsy();
            } else {
                activoDialogPage.getEstadoActivoInput().click();
                expect(activoDialogPage.getEstadoActivoInput().isSelected()).toBeTruthy();
            }
        });
        activoDialogPage.sedeSelectLastOption();
        activoDialogPage.save();
        expect(activoDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ActivoComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-activo div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class ActivoDialogPage {
    modalTitle = element(by.css('h4#myActivoLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nombreActivoInput = element(by.css('input#field_nombreActivo'));
    cantActivosInput = element(by.css('input#field_cantActivos'));
    estadoActivoInput = element(by.css('input#field_estadoActivo'));
    sedeSelect = element(by.css('select#field_sede'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setNombreActivoInput = function(nombreActivo) {
        this.nombreActivoInput.sendKeys(nombreActivo);
    };

    getNombreActivoInput = function() {
        return this.nombreActivoInput.getAttribute('value');
    };

    setCantActivosInput = function(cantActivos) {
        this.cantActivosInput.sendKeys(cantActivos);
    };

    getCantActivosInput = function() {
        return this.cantActivosInput.getAttribute('value');
    };

    getEstadoActivoInput = function() {
        return this.estadoActivoInput;
    };
    sedeSelectLastOption = function() {
        this.sedeSelect.all(by.tagName('option')).last().click();
    };

    sedeSelectOption = function(option) {
        this.sedeSelect.sendKeys(option);
    };

    getSedeSelect = function() {
        return this.sedeSelect;
    };

    getSedeSelectedOption = function() {
        return this.sedeSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
