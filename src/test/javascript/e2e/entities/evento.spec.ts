import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Evento e2e test', () => {

    let navBarPage: NavBarPage;
    let eventoDialogPage: EventoDialogPage;
    let eventoComponentsPage: EventoComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Eventos', () => {
        navBarPage.goToEntity('evento');
        eventoComponentsPage = new EventoComponentsPage();
        expect(eventoComponentsPage.getTitle())
            .toMatch(/Eventos/);

    });

    it('should load create Evento dialog', () => {
        eventoComponentsPage.clickOnCreateButton();
        eventoDialogPage = new EventoDialogPage();
        expect(eventoDialogPage.getModalTitle())
            .toMatch(/Create or edit a Evento/);
        eventoDialogPage.close();
    });

    it('should create and save Eventos', () => {
        eventoComponentsPage.clickOnCreateButton();
        eventoDialogPage.setNombreEventoInput('nombreEvento');
        expect(eventoDialogPage.getNombreEventoInput()).toMatch('nombreEvento');
        eventoDialogPage.setDescripcionEventoInput('descripcionEvento');
        expect(eventoDialogPage.getDescripcionEventoInput()).toMatch('descripcionEvento');
        eventoDialogPage.setFechaInicioInput(12310020012301);
        expect(eventoDialogPage.getFechaInicioInput()).toMatch('2001-12-31T02:30');
        eventoDialogPage.setFechaFinInput(12310020012301);
        expect(eventoDialogPage.getFechaFinInput()).toMatch('2001-12-31T02:30');
        eventoDialogPage.setHoraInicioEventoInput(12310020012301);
        expect(eventoDialogPage.getHoraInicioEventoInput()).toMatch('2001-12-31T02:30');
        eventoDialogPage.setHoraFinEventoInput(12310020012301);
        expect(eventoDialogPage.getHoraFinEventoInput()).toMatch('2001-12-31T02:30');
        eventoDialogPage.getEstadoEventoInput().isSelected().then((selected) => {
            if (selected) {
                eventoDialogPage.getEstadoEventoInput().click();
                expect(eventoDialogPage.getEstadoEventoInput().isSelected()).toBeFalsy();
            } else {
                eventoDialogPage.getEstadoEventoInput().click();
                expect(eventoDialogPage.getEstadoEventoInput().isSelected()).toBeTruthy();
            }
        });
        eventoDialogPage.save();
        expect(eventoDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class EventoComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-evento div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class EventoDialogPage {
    modalTitle = element(by.css('h4#myEventoLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nombreEventoInput = element(by.css('input#field_nombreEvento'));
    descripcionEventoInput = element(by.css('input#field_descripcionEvento'));
    fechaInicioInput = element(by.css('input#field_fechaInicio'));
    fechaFinInput = element(by.css('input#field_fechaFin'));
    horaInicioEventoInput = element(by.css('input#field_horaInicioEvento'));
    horaFinEventoInput = element(by.css('input#field_horaFinEvento'));
    estadoEventoInput = element(by.css('input#field_estadoEvento'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setNombreEventoInput = function(nombreEvento) {
        this.nombreEventoInput.sendKeys(nombreEvento);
    };

    getNombreEventoInput = function() {
        return this.nombreEventoInput.getAttribute('value');
    };

    setDescripcionEventoInput = function(descripcionEvento) {
        this.descripcionEventoInput.sendKeys(descripcionEvento);
    };

    getDescripcionEventoInput = function() {
        return this.descripcionEventoInput.getAttribute('value');
    };

    setFechaInicioInput = function(fechaInicio) {
        this.fechaInicioInput.sendKeys(fechaInicio);
    };

    getFechaInicioInput = function() {
        return this.fechaInicioInput.getAttribute('value');
    };

    setFechaFinInput = function(fechaFin) {
        this.fechaFinInput.sendKeys(fechaFin);
    };

    getFechaFinInput = function() {
        return this.fechaFinInput.getAttribute('value');
    };

    setHoraInicioEventoInput = function(horaInicioEvento) {
        this.horaInicioEventoInput.sendKeys(horaInicioEvento);
    };

    getHoraInicioEventoInput = function() {
        return this.horaInicioEventoInput.getAttribute('value');
    };

    setHoraFinEventoInput = function(horaFinEvento) {
        this.horaFinEventoInput.sendKeys(horaFinEvento);
    };

    getHoraFinEventoInput = function() {
        return this.horaFinEventoInput.getAttribute('value');
    };

    getEstadoEventoInput = function() {
        return this.estadoEventoInput;
    };
    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
