import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('RegistroActividadRutina e2e test', () => {

    let navBarPage: NavBarPage;
    let registroActividadRutinaDialogPage: RegistroActividadRutinaDialogPage;
    let registroActividadRutinaComponentsPage: RegistroActividadRutinaComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load RegistroActividadRutinas', () => {
        navBarPage.goToEntity('registro-actividad-rutina');
        registroActividadRutinaComponentsPage = new RegistroActividadRutinaComponentsPage();
        expect(registroActividadRutinaComponentsPage.getTitle())
            .toMatch(/Registro Actividad Rutinas/);

    });

    it('should load create RegistroActividadRutina dialog', () => {
        registroActividadRutinaComponentsPage.clickOnCreateButton();
        registroActividadRutinaDialogPage = new RegistroActividadRutinaDialogPage();
        expect(registroActividadRutinaDialogPage.getModalTitle())
            .toMatch(/Create or edit a Registro Actividad Rutina/);
        registroActividadRutinaDialogPage.close();
    });

    it('should create and save RegistroActividadRutinas', () => {
        registroActividadRutinaComponentsPage.clickOnCreateButton();
        registroActividadRutinaDialogPage.setFechaAsistenciaInput(12310020012301);
        expect(registroActividadRutinaDialogPage.getFechaAsistenciaInput()).toMatch('2001-12-31T02:30');
        registroActividadRutinaDialogPage.setDiaAsistenciaInput('5');
        expect(registroActividadRutinaDialogPage.getDiaAsistenciaInput()).toMatch('5');
        registroActividadRutinaDialogPage.getEstadoRegistroActividadInput().isSelected().then((selected) => {
            if (selected) {
                registroActividadRutinaDialogPage.getEstadoRegistroActividadInput().click();
                expect(registroActividadRutinaDialogPage.getEstadoRegistroActividadInput().isSelected()).toBeFalsy();
            } else {
                registroActividadRutinaDialogPage.getEstadoRegistroActividadInput().click();
                expect(registroActividadRutinaDialogPage.getEstadoRegistroActividadInput().isSelected()).toBeTruthy();
            }
        });
        registroActividadRutinaDialogPage.usuarioSelectLastOption();
        registroActividadRutinaDialogPage.subRutinaSelectLastOption();
        registroActividadRutinaDialogPage.save();
        expect(registroActividadRutinaDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class RegistroActividadRutinaComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-registro-actividad-rutina div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class RegistroActividadRutinaDialogPage {
    modalTitle = element(by.css('h4#myRegistroActividadRutinaLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    fechaAsistenciaInput = element(by.css('input#field_fechaAsistencia'));
    diaAsistenciaInput = element(by.css('input#field_diaAsistencia'));
    estadoRegistroActividadInput = element(by.css('input#field_estadoRegistroActividad'));
    usuarioSelect = element(by.css('select#field_usuario'));
    subRutinaSelect = element(by.css('select#field_subRutina'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setFechaAsistenciaInput = function(fechaAsistencia) {
        this.fechaAsistenciaInput.sendKeys(fechaAsistencia);
    };

    getFechaAsistenciaInput = function() {
        return this.fechaAsistenciaInput.getAttribute('value');
    };

    setDiaAsistenciaInput = function(diaAsistencia) {
        this.diaAsistenciaInput.sendKeys(diaAsistencia);
    };

    getDiaAsistenciaInput = function() {
        return this.diaAsistenciaInput.getAttribute('value');
    };

    getEstadoRegistroActividadInput = function() {
        return this.estadoRegistroActividadInput;
    };
    usuarioSelectLastOption = function() {
        this.usuarioSelect.all(by.tagName('option')).last().click();
    };

    usuarioSelectOption = function(option) {
        this.usuarioSelect.sendKeys(option);
    };

    getUsuarioSelect = function() {
        return this.usuarioSelect;
    };

    getUsuarioSelectedOption = function() {
        return this.usuarioSelect.element(by.css('option:checked')).getText();
    };

    subRutinaSelectLastOption = function() {
        this.subRutinaSelect.all(by.tagName('option')).last().click();
    };

    subRutinaSelectOption = function(option) {
        this.subRutinaSelect.sendKeys(option);
    };

    getSubRutinaSelect = function() {
        return this.subRutinaSelect;
    };

    getSubRutinaSelectedOption = function() {
        return this.subRutinaSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
