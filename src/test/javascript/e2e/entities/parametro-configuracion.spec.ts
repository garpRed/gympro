import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('ParametroConfiguracion e2e test', () => {

    let navBarPage: NavBarPage;
    let parametroConfiguracionDialogPage: ParametroConfiguracionDialogPage;
    let parametroConfiguracionComponentsPage: ParametroConfiguracionComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load ParametroConfiguracions', () => {
        navBarPage.goToEntity('parametro-configuracion');
        parametroConfiguracionComponentsPage = new ParametroConfiguracionComponentsPage();
        expect(parametroConfiguracionComponentsPage.getTitle())
            .toMatch(/Parametro Configuracions/);

    });

    it('should load create ParametroConfiguracion dialog', () => {
        parametroConfiguracionComponentsPage.clickOnCreateButton();
        parametroConfiguracionDialogPage = new ParametroConfiguracionDialogPage();
        expect(parametroConfiguracionDialogPage.getModalTitle())
            .toMatch(/Create or edit a Parametro Configuracion/);
        parametroConfiguracionDialogPage.close();
    });

    it('should create and save ParametroConfiguracions', () => {
        parametroConfiguracionComponentsPage.clickOnCreateButton();
        parametroConfiguracionDialogPage.setNombreParametroInput('nombreParametro');
        expect(parametroConfiguracionDialogPage.getNombreParametroInput()).toMatch('nombreParametro');
        parametroConfiguracionDialogPage.setValorParametroInput('valorParametro');
        expect(parametroConfiguracionDialogPage.getValorParametroInput()).toMatch('valorParametro');
        parametroConfiguracionDialogPage.setTipoDeDatoInput('tipoDeDato');
        expect(parametroConfiguracionDialogPage.getTipoDeDatoInput()).toMatch('tipoDeDato');
        parametroConfiguracionDialogPage.getEstadoParametroInput().isSelected().then((selected) => {
            if (selected) {
                parametroConfiguracionDialogPage.getEstadoParametroInput().click();
                expect(parametroConfiguracionDialogPage.getEstadoParametroInput().isSelected()).toBeFalsy();
            } else {
                parametroConfiguracionDialogPage.getEstadoParametroInput().click();
                expect(parametroConfiguracionDialogPage.getEstadoParametroInput().isSelected()).toBeTruthy();
            }
        });
        parametroConfiguracionDialogPage.tipoParametroSelectLastOption();
        parametroConfiguracionDialogPage.save();
        expect(parametroConfiguracionDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ParametroConfiguracionComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-parametro-configuracion div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class ParametroConfiguracionDialogPage {
    modalTitle = element(by.css('h4#myParametroConfiguracionLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nombreParametroInput = element(by.css('input#field_nombreParametro'));
    valorParametroInput = element(by.css('input#field_valorParametro'));
    tipoDeDatoInput = element(by.css('input#field_tipoDeDato'));
    estadoParametroInput = element(by.css('input#field_estadoParametro'));
    tipoParametroSelect = element(by.css('select#field_tipoParametro'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setNombreParametroInput = function(nombreParametro) {
        this.nombreParametroInput.sendKeys(nombreParametro);
    };

    getNombreParametroInput = function() {
        return this.nombreParametroInput.getAttribute('value');
    };

    setValorParametroInput = function(valorParametro) {
        this.valorParametroInput.sendKeys(valorParametro);
    };

    getValorParametroInput = function() {
        return this.valorParametroInput.getAttribute('value');
    };

    setTipoDeDatoInput = function(tipoDeDato) {
        this.tipoDeDatoInput.sendKeys(tipoDeDato);
    };

    getTipoDeDatoInput = function() {
        return this.tipoDeDatoInput.getAttribute('value');
    };

    getEstadoParametroInput = function() {
        return this.estadoParametroInput;
    };
    tipoParametroSelectLastOption = function() {
        this.tipoParametroSelect.all(by.tagName('option')).last().click();
    };

    tipoParametroSelectOption = function(option) {
        this.tipoParametroSelect.sendKeys(option);
    };

    getTipoParametroSelect = function() {
        return this.tipoParametroSelect;
    };

    getTipoParametroSelectedOption = function() {
        return this.tipoParametroSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
