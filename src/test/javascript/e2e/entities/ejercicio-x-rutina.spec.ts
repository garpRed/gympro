import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('EjercicioXRutina e2e test', () => {

    let navBarPage: NavBarPage;
    let ejercicioXRutinaDialogPage: EjercicioXRutinaDialogPage;
    let ejercicioXRutinaComponentsPage: EjercicioXRutinaComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load EjercicioXRutinas', () => {
        navBarPage.goToEntity('ejercicio-x-rutina');
        ejercicioXRutinaComponentsPage = new EjercicioXRutinaComponentsPage();
        expect(ejercicioXRutinaComponentsPage.getTitle())
            .toMatch(/Ejercicio X Rutinas/);

    });

    it('should load create EjercicioXRutina dialog', () => {
        ejercicioXRutinaComponentsPage.clickOnCreateButton();
        ejercicioXRutinaDialogPage = new EjercicioXRutinaDialogPage();
        expect(ejercicioXRutinaDialogPage.getModalTitle())
            .toMatch(/Create or edit a Ejercicio X Rutina/);
        ejercicioXRutinaDialogPage.close();
    });

    it('should create and save EjercicioXRutinas', () => {
        ejercicioXRutinaComponentsPage.clickOnCreateButton();
        ejercicioXRutinaDialogPage.setPesoInput('5');
        expect(ejercicioXRutinaDialogPage.getPesoInput()).toMatch('5');
        ejercicioXRutinaDialogPage.setRepsInput('5');
        expect(ejercicioXRutinaDialogPage.getRepsInput()).toMatch('5');
        ejercicioXRutinaDialogPage.setSetsInput('5');
        expect(ejercicioXRutinaDialogPage.getSetsInput()).toMatch('5');
        ejercicioXRutinaDialogPage.setAjusteInput('ajuste');
        expect(ejercicioXRutinaDialogPage.getAjusteInput()).toMatch('ajuste');
        ejercicioXRutinaDialogPage.setTiempoInput('tiempo');
        expect(ejercicioXRutinaDialogPage.getTiempoInput()).toMatch('tiempo');
        ejercicioXRutinaDialogPage.setCommentarioInput('commentario');
        expect(ejercicioXRutinaDialogPage.getCommentarioInput()).toMatch('commentario');
        ejercicioXRutinaDialogPage.getEstadoEjercicioRutinaInput().isSelected().then((selected) => {
            if (selected) {
                ejercicioXRutinaDialogPage.getEstadoEjercicioRutinaInput().click();
                expect(ejercicioXRutinaDialogPage.getEstadoEjercicioRutinaInput().isSelected()).toBeFalsy();
            } else {
                ejercicioXRutinaDialogPage.getEstadoEjercicioRutinaInput().click();
                expect(ejercicioXRutinaDialogPage.getEstadoEjercicioRutinaInput().isSelected()).toBeTruthy();
            }
        });
        ejercicioXRutinaDialogPage.ejercicioXRutinaSelectLastOption();
        ejercicioXRutinaDialogPage.save();
        expect(ejercicioXRutinaDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class EjercicioXRutinaComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-ejercicio-x-rutina div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class EjercicioXRutinaDialogPage {
    modalTitle = element(by.css('h4#myEjercicioXRutinaLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    pesoInput = element(by.css('input#field_peso'));
    repsInput = element(by.css('input#field_reps'));
    setsInput = element(by.css('input#field_sets'));
    ajusteInput = element(by.css('input#field_ajuste'));
    tiempoInput = element(by.css('input#field_tiempo'));
    commentarioInput = element(by.css('input#field_commentario'));
    estadoEjercicioRutinaInput = element(by.css('input#field_estadoEjercicioRutina'));
    ejercicioXRutinaSelect = element(by.css('select#field_ejercicioXRutina'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setPesoInput = function(peso) {
        this.pesoInput.sendKeys(peso);
    };

    getPesoInput = function() {
        return this.pesoInput.getAttribute('value');
    };

    setRepsInput = function(reps) {
        this.repsInput.sendKeys(reps);
    };

    getRepsInput = function() {
        return this.repsInput.getAttribute('value');
    };

    setSetsInput = function(sets) {
        this.setsInput.sendKeys(sets);
    };

    getSetsInput = function() {
        return this.setsInput.getAttribute('value');
    };

    setAjusteInput = function(ajuste) {
        this.ajusteInput.sendKeys(ajuste);
    };

    getAjusteInput = function() {
        return this.ajusteInput.getAttribute('value');
    };

    setTiempoInput = function(tiempo) {
        this.tiempoInput.sendKeys(tiempo);
    };

    getTiempoInput = function() {
        return this.tiempoInput.getAttribute('value');
    };

    setCommentarioInput = function(commentario) {
        this.commentarioInput.sendKeys(commentario);
    };

    getCommentarioInput = function() {
        return this.commentarioInput.getAttribute('value');
    };

    getEstadoEjercicioRutinaInput = function() {
        return this.estadoEjercicioRutinaInput;
    };
    ejercicioXRutinaSelectLastOption = function() {
        this.ejercicioXRutinaSelect.all(by.tagName('option')).last().click();
    };

    ejercicioXRutinaSelectOption = function(option) {
        this.ejercicioXRutinaSelect.sendKeys(option);
    };

    getEjercicioXRutinaSelect = function() {
        return this.ejercicioXRutinaSelect;
    };

    getEjercicioXRutinaSelectedOption = function() {
        return this.ejercicioXRutinaSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
