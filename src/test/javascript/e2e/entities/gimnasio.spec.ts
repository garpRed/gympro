import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Gimnasio e2e test', () => {

    let navBarPage: NavBarPage;
    let gimnasioDialogPage: GimnasioDialogPage;
    let gimnasioComponentsPage: GimnasioComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Gimnasios', () => {
        navBarPage.goToEntity('gimnasio');
        gimnasioComponentsPage = new GimnasioComponentsPage();
        expect(gimnasioComponentsPage.getTitle())
            .toMatch(/Gimnasios/);

    });

    it('should load create Gimnasio dialog', () => {
        gimnasioComponentsPage.clickOnCreateButton();
        gimnasioDialogPage = new GimnasioDialogPage();
        expect(gimnasioDialogPage.getModalTitle())
            .toMatch(/Create or edit a Gimnasio/);
        gimnasioDialogPage.close();
    });

    it('should create and save Gimnasios', () => {
        gimnasioComponentsPage.clickOnCreateButton();
        gimnasioDialogPage.setNombreGimnasioInput('nombreGimnasio');
        expect(gimnasioDialogPage.getNombreGimnasioInput()).toMatch('nombreGimnasio');
        gimnasioDialogPage.getEstadoGimnasioInput().isSelected().then((selected) => {
            if (selected) {
                gimnasioDialogPage.getEstadoGimnasioInput().click();
                expect(gimnasioDialogPage.getEstadoGimnasioInput().isSelected()).toBeFalsy();
            } else {
                gimnasioDialogPage.getEstadoGimnasioInput().click();
                expect(gimnasioDialogPage.getEstadoGimnasioInput().isSelected()).toBeTruthy();
            }
        });
        gimnasioDialogPage.save();
        expect(gimnasioDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class GimnasioComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-gimnasio div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class GimnasioDialogPage {
    modalTitle = element(by.css('h4#myGimnasioLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nombreGimnasioInput = element(by.css('input#field_nombreGimnasio'));
    estadoGimnasioInput = element(by.css('input#field_estadoGimnasio'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setNombreGimnasioInput = function(nombreGimnasio) {
        this.nombreGimnasioInput.sendKeys(nombreGimnasio);
    };

    getNombreGimnasioInput = function() {
        return this.nombreGimnasioInput.getAttribute('value');
    };

    getEstadoGimnasioInput = function() {
        return this.estadoGimnasioInput;
    };
    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
