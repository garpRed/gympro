import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Rutina e2e test', () => {

    let navBarPage: NavBarPage;
    let rutinaDialogPage: RutinaDialogPage;
    let rutinaComponentsPage: RutinaComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Rutinas', () => {
        navBarPage.goToEntity('rutina');
        rutinaComponentsPage = new RutinaComponentsPage();
        expect(rutinaComponentsPage.getTitle())
            .toMatch(/Rutinas/);

    });

    it('should load create Rutina dialog', () => {
        rutinaComponentsPage.clickOnCreateButton();
        rutinaDialogPage = new RutinaDialogPage();
        expect(rutinaDialogPage.getModalTitle())
            .toMatch(/Create or edit a Rutina/);
        rutinaDialogPage.close();
    });

    it('should create and save Rutinas', () => {
        rutinaComponentsPage.clickOnCreateButton();
        rutinaDialogPage.setUsuarioCreadorRutinaInput('5');
        expect(rutinaDialogPage.getUsuarioCreadorRutinaInput()).toMatch('5');
        rutinaDialogPage.setNombreRutinaInput('nombreRutina');
        expect(rutinaDialogPage.getNombreRutinaInput()).toMatch('nombreRutina');
        rutinaDialogPage.setCantDiasRutinaInput('5');
        expect(rutinaDialogPage.getCantDiasRutinaInput()).toMatch('5');
        rutinaDialogPage.getEstadoRutinaInput().isSelected().then((selected) => {
            if (selected) {
                rutinaDialogPage.getEstadoRutinaInput().click();
                expect(rutinaDialogPage.getEstadoRutinaInput().isSelected()).toBeFalsy();
            } else {
                rutinaDialogPage.getEstadoRutinaInput().click();
                expect(rutinaDialogPage.getEstadoRutinaInput().isSelected()).toBeTruthy();
            }
        });
        rutinaDialogPage.save();
        expect(rutinaDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class RutinaComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-rutina div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class RutinaDialogPage {
    modalTitle = element(by.css('h4#myRutinaLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    usuarioCreadorRutinaInput = element(by.css('input#field_usuarioCreadorRutina'));
    nombreRutinaInput = element(by.css('input#field_nombreRutina'));
    cantDiasRutinaInput = element(by.css('input#field_cantDiasRutina'));
    estadoRutinaInput = element(by.css('input#field_estadoRutina'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setUsuarioCreadorRutinaInput = function(usuarioCreadorRutina) {
        this.usuarioCreadorRutinaInput.sendKeys(usuarioCreadorRutina);
    };

    getUsuarioCreadorRutinaInput = function() {
        return this.usuarioCreadorRutinaInput.getAttribute('value');
    };

    setNombreRutinaInput = function(nombreRutina) {
        this.nombreRutinaInput.sendKeys(nombreRutina);
    };

    getNombreRutinaInput = function() {
        return this.nombreRutinaInput.getAttribute('value');
    };

    setCantDiasRutinaInput = function(cantDiasRutina) {
        this.cantDiasRutinaInput.sendKeys(cantDiasRutina);
    };

    getCantDiasRutinaInput = function() {
        return this.cantDiasRutinaInput.getAttribute('value');
    };

    getEstadoRutinaInput = function() {
        return this.estadoRutinaInput;
    };
    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
