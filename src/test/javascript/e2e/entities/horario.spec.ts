import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Horario e2e test', () => {

    let navBarPage: NavBarPage;
    let horarioDialogPage: HorarioDialogPage;
    let horarioComponentsPage: HorarioComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Horarios', () => {
        navBarPage.goToEntity('horario');
        horarioComponentsPage = new HorarioComponentsPage();
        expect(horarioComponentsPage.getTitle())
            .toMatch(/Horarios/);

    });

    it('should load create Horario dialog', () => {
        horarioComponentsPage.clickOnCreateButton();
        horarioDialogPage = new HorarioDialogPage();
        expect(horarioDialogPage.getModalTitle())
            .toMatch(/Create or edit a Horario/);
        horarioDialogPage.close();
    });

    it('should create and save Horarios', () => {
        horarioComponentsPage.clickOnCreateButton();
        horarioDialogPage.setDiaEntrenadorInput('diaEntrenador');
        expect(horarioDialogPage.getDiaEntrenadorInput()).toMatch('diaEntrenador');
        horarioDialogPage.getDisponibilidadEntrenadorInput().isSelected().then((selected) => {
            if (selected) {
                horarioDialogPage.getDisponibilidadEntrenadorInput().click();
                expect(horarioDialogPage.getDisponibilidadEntrenadorInput().isSelected()).toBeFalsy();
            } else {
                horarioDialogPage.getDisponibilidadEntrenadorInput().click();
                expect(horarioDialogPage.getDisponibilidadEntrenadorInput().isSelected()).toBeTruthy();
            }
        });
        horarioDialogPage.setHorarioInicioHorarioInput(12310020012301);
        expect(horarioDialogPage.getHorarioInicioHorarioInput()).toMatch('2001-12-31T02:30');
        horarioDialogPage.setHorarioFinHorarioInput(12310020012301);
        expect(horarioDialogPage.getHorarioFinHorarioInput()).toMatch('2001-12-31T02:30');
        horarioDialogPage.getEstadoHorarioInput().isSelected().then((selected) => {
            if (selected) {
                horarioDialogPage.getEstadoHorarioInput().click();
                expect(horarioDialogPage.getEstadoHorarioInput().isSelected()).toBeFalsy();
            } else {
                horarioDialogPage.getEstadoHorarioInput().click();
                expect(horarioDialogPage.getEstadoHorarioInput().isSelected()).toBeTruthy();
            }
        });
        horarioDialogPage.horarioUsuarioSelectLastOption();
        // horarioDialogPage.horariosParametroSelectLastOption();
        horarioDialogPage.save();
        expect(horarioDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class HorarioComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-horario div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class HorarioDialogPage {
    modalTitle = element(by.css('h4#myHorarioLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    diaEntrenadorInput = element(by.css('input#field_diaEntrenador'));
    disponibilidadEntrenadorInput = element(by.css('input#field_disponibilidadEntrenador'));
    horarioInicioHorarioInput = element(by.css('input#field_horarioInicioHorario'));
    horarioFinHorarioInput = element(by.css('input#field_horarioFinHorario'));
    estadoHorarioInput = element(by.css('input#field_estadoHorario'));
    horarioUsuarioSelect = element(by.css('select#field_horarioUsuario'));
    horariosParametroSelect = element(by.css('select#field_horariosParametro'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setDiaEntrenadorInput = function(diaEntrenador) {
        this.diaEntrenadorInput.sendKeys(diaEntrenador);
    };

    getDiaEntrenadorInput = function() {
        return this.diaEntrenadorInput.getAttribute('value');
    };

    getDisponibilidadEntrenadorInput = function() {
        return this.disponibilidadEntrenadorInput;
    };
    setHorarioInicioHorarioInput = function(horarioInicioHorario) {
        this.horarioInicioHorarioInput.sendKeys(horarioInicioHorario);
    };

    getHorarioInicioHorarioInput = function() {
        return this.horarioInicioHorarioInput.getAttribute('value');
    };

    setHorarioFinHorarioInput = function(horarioFinHorario) {
        this.horarioFinHorarioInput.sendKeys(horarioFinHorario);
    };

    getHorarioFinHorarioInput = function() {
        return this.horarioFinHorarioInput.getAttribute('value');
    };

    getEstadoHorarioInput = function() {
        return this.estadoHorarioInput;
    };
    horarioUsuarioSelectLastOption = function() {
        this.horarioUsuarioSelect.all(by.tagName('option')).last().click();
    };

    horarioUsuarioSelectOption = function(option) {
        this.horarioUsuarioSelect.sendKeys(option);
    };

    getHorarioUsuarioSelect = function() {
        return this.horarioUsuarioSelect;
    };

    getHorarioUsuarioSelectedOption = function() {
        return this.horarioUsuarioSelect.element(by.css('option:checked')).getText();
    };

    horariosParametroSelectLastOption = function() {
        this.horariosParametroSelect.all(by.tagName('option')).last().click();
    };

    horariosParametroSelectOption = function(option) {
        this.horariosParametroSelect.sendKeys(option);
    };

    getHorariosParametroSelect = function() {
        return this.horariosParametroSelect;
    };

    getHorariosParametroSelectedOption = function() {
        return this.horariosParametroSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
