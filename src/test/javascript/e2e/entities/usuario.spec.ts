import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Usuario e2e test', () => {

    let navBarPage: NavBarPage;
    let usuarioDialogPage: UsuarioDialogPage;
    let usuarioComponentsPage: UsuarioComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Usuarios', () => {
        navBarPage.goToEntity('usuario');
        usuarioComponentsPage = new UsuarioComponentsPage();
        expect(usuarioComponentsPage.getTitle())
            .toMatch(/Usuarios/);

    });

    it('should load create Usuario dialog', () => {
        usuarioComponentsPage.clickOnCreateButton();
        usuarioDialogPage = new UsuarioDialogPage();
        expect(usuarioDialogPage.getModalTitle())
            .toMatch(/Create or edit a Usuario/);
        usuarioDialogPage.close();
    });

    it('should create and save Usuarios', () => {
        usuarioComponentsPage.clickOnCreateButton();
        usuarioDialogPage.setNombreUsuarioInput('nombreUsuario');
        expect(usuarioDialogPage.getNombreUsuarioInput()).toMatch('nombreUsuario');
        usuarioDialogPage.setApellidoUsuarioInput('apellidoUsuario');
        expect(usuarioDialogPage.getApellidoUsuarioInput()).toMatch('apellidoUsuario');
        usuarioDialogPage.setDireccionInput('direccion');
        expect(usuarioDialogPage.getDireccionInput()).toMatch('direccion');
        usuarioDialogPage.setFechaNacimientoInput(12310020012301);
        expect(usuarioDialogPage.getFechaNacimientoInput()).toMatch('2001-12-31T02:30');
        usuarioDialogPage.setTelefonoInput('telefono');
        expect(usuarioDialogPage.getTelefonoInput()).toMatch('telefono');
        usuarioDialogPage.setIdentificacionInput('identificacion');
        expect(usuarioDialogPage.getIdentificacionInput()).toMatch('identificacion');
        usuarioDialogPage.setCorreoElectronicoInput('correoElectronico');
        expect(usuarioDialogPage.getCorreoElectronicoInput()).toMatch('correoElectronico');
        usuarioDialogPage.setDiaPagoInput('5');
        expect(usuarioDialogPage.getDiaPagoInput()).toMatch('5');
        usuarioDialogPage.getLesionInput().isSelected().then((selected) => {
            if (selected) {
                usuarioDialogPage.getLesionInput().click();
                expect(usuarioDialogPage.getLesionInput().isSelected()).toBeFalsy();
            } else {
                usuarioDialogPage.getLesionInput().click();
                expect(usuarioDialogPage.getLesionInput().isSelected()).toBeTruthy();
            }
        });
        usuarioDialogPage.setDescLesionInput('descLesion');
        expect(usuarioDialogPage.getDescLesionInput()).toMatch('descLesion');
        usuarioDialogPage.getEstadoInput().isSelected().then((selected) => {
            if (selected) {
                usuarioDialogPage.getEstadoInput().click();
                expect(usuarioDialogPage.getEstadoInput().isSelected()).toBeFalsy();
            } else {
                usuarioDialogPage.getEstadoInput().click();
                expect(usuarioDialogPage.getEstadoInput().isSelected()).toBeTruthy();
            }
        });
        usuarioDialogPage.setContrasennaInput('contrasenna');
        expect(usuarioDialogPage.getContrasennaInput()).toMatch('contrasenna');
        usuarioDialogPage.userSelectLastOption();
        usuarioDialogPage.sedeSelectLastOption();
        usuarioDialogPage.rolSelectLastOption();
        usuarioDialogPage.usuarioSelectLastOption();
        usuarioDialogPage.rutinaSelectLastOption();
        usuarioDialogPage.save();
        expect(usuarioDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class UsuarioComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-usuario div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class UsuarioDialogPage {
    modalTitle = element(by.css('h4#myUsuarioLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nombreUsuarioInput = element(by.css('input#field_nombreUsuario'));
    apellidoUsuarioInput = element(by.css('input#field_apellidoUsuario'));
    direccionInput = element(by.css('input#field_direccion'));
    fechaNacimientoInput = element(by.css('input#field_fechaNacimiento'));
    telefonoInput = element(by.css('input#field_telefono'));
    identificacionInput = element(by.css('input#field_identificacion'));
    correoElectronicoInput = element(by.css('input#field_correoElectronico'));
    diaPagoInput = element(by.css('input#field_diaPago'));
    lesionInput = element(by.css('input#field_lesion'));
    descLesionInput = element(by.css('input#field_descLesion'));
    estadoInput = element(by.css('input#field_estado'));
    contrasennaInput = element(by.css('input#field_contrasenna'));
    userSelect = element(by.css('select#field_user'));
    sedeSelect = element(by.css('select#field_sede'));
    rolSelect = element(by.css('select#field_rol'));
    usuarioSelect = element(by.css('select#field_usuario'));
    rutinaSelect = element(by.css('select#field_rutina'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setNombreUsuarioInput = function(nombreUsuario) {
        this.nombreUsuarioInput.sendKeys(nombreUsuario);
    };

    getNombreUsuarioInput = function() {
        return this.nombreUsuarioInput.getAttribute('value');
    };

    setApellidoUsuarioInput = function(apellidoUsuario) {
        this.apellidoUsuarioInput.sendKeys(apellidoUsuario);
    };

    getApellidoUsuarioInput = function() {
        return this.apellidoUsuarioInput.getAttribute('value');
    };

    setDireccionInput = function(direccion) {
        this.direccionInput.sendKeys(direccion);
    };

    getDireccionInput = function() {
        return this.direccionInput.getAttribute('value');
    };

    setFechaNacimientoInput = function(fechaNacimiento) {
        this.fechaNacimientoInput.sendKeys(fechaNacimiento);
    };

    getFechaNacimientoInput = function() {
        return this.fechaNacimientoInput.getAttribute('value');
    };

    setTelefonoInput = function(telefono) {
        this.telefonoInput.sendKeys(telefono);
    };

    getTelefonoInput = function() {
        return this.telefonoInput.getAttribute('value');
    };

    setIdentificacionInput = function(identificacion) {
        this.identificacionInput.sendKeys(identificacion);
    };

    getIdentificacionInput = function() {
        return this.identificacionInput.getAttribute('value');
    };

    setCorreoElectronicoInput = function(correoElectronico) {
        this.correoElectronicoInput.sendKeys(correoElectronico);
    };

    getCorreoElectronicoInput = function() {
        return this.correoElectronicoInput.getAttribute('value');
    };

    setDiaPagoInput = function(diaPago) {
        this.diaPagoInput.sendKeys(diaPago);
    };

    getDiaPagoInput = function() {
        return this.diaPagoInput.getAttribute('value');
    };

    getLesionInput = function() {
        return this.lesionInput;
    };
    setDescLesionInput = function(descLesion) {
        this.descLesionInput.sendKeys(descLesion);
    };

    getDescLesionInput = function() {
        return this.descLesionInput.getAttribute('value');
    };

    getEstadoInput = function() {
        return this.estadoInput;
    };
    setContrasennaInput = function(contrasenna) {
        this.contrasennaInput.sendKeys(contrasenna);
    };

    getContrasennaInput = function() {
        return this.contrasennaInput.getAttribute('value');
    };

    userSelectLastOption = function() {
        this.userSelect.all(by.tagName('option')).last().click();
    };

    userSelectOption = function(option) {
        this.userSelect.sendKeys(option);
    };

    getUserSelect = function() {
        return this.userSelect;
    };

    getUserSelectedOption = function() {
        return this.userSelect.element(by.css('option:checked')).getText();
    };

    sedeSelectLastOption = function() {
        this.sedeSelect.all(by.tagName('option')).last().click();
    };

    sedeSelectOption = function(option) {
        this.sedeSelect.sendKeys(option);
    };

    getSedeSelect = function() {
        return this.sedeSelect;
    };

    getSedeSelectedOption = function() {
        return this.sedeSelect.element(by.css('option:checked')).getText();
    };

    rolSelectLastOption = function() {
        this.rolSelect.all(by.tagName('option')).last().click();
    };

    rolSelectOption = function(option) {
        this.rolSelect.sendKeys(option);
    };

    getRolSelect = function() {
        return this.rolSelect;
    };

    getRolSelectedOption = function() {
        return this.rolSelect.element(by.css('option:checked')).getText();
    };

    usuarioSelectLastOption = function() {
        this.usuarioSelect.all(by.tagName('option')).last().click();
    };

    usuarioSelectOption = function(option) {
        this.usuarioSelect.sendKeys(option);
    };

    getUsuarioSelect = function() {
        return this.usuarioSelect;
    };

    getUsuarioSelectedOption = function() {
        return this.usuarioSelect.element(by.css('option:checked')).getText();
    };

    rutinaSelectLastOption = function() {
        this.rutinaSelect.all(by.tagName('option')).last().click();
    };

    rutinaSelectOption = function(option) {
        this.rutinaSelect.sendKeys(option);
    };

    getRutinaSelect = function() {
        return this.rutinaSelect;
    };

    getRutinaSelectedOption = function() {
        return this.rutinaSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
