import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('SubRutina e2e test', () => {

    let navBarPage: NavBarPage;
    let subRutinaDialogPage: SubRutinaDialogPage;
    let subRutinaComponentsPage: SubRutinaComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load SubRutinas', () => {
        navBarPage.goToEntity('sub-rutina');
        subRutinaComponentsPage = new SubRutinaComponentsPage();
        expect(subRutinaComponentsPage.getTitle())
            .toMatch(/Sub Rutinas/);

    });

    it('should load create SubRutina dialog', () => {
        subRutinaComponentsPage.clickOnCreateButton();
        subRutinaDialogPage = new SubRutinaDialogPage();
        expect(subRutinaDialogPage.getModalTitle())
            .toMatch(/Create or edit a Sub Rutina/);
        subRutinaDialogPage.close();
    });

    it('should create and save SubRutinas', () => {
        subRutinaComponentsPage.clickOnCreateButton();
        subRutinaDialogPage.setNombreSubRutinaInput('nombreSubRutina');
        expect(subRutinaDialogPage.getNombreSubRutinaInput()).toMatch('nombreSubRutina');
        subRutinaDialogPage.setCantSeriesInput('5');
        expect(subRutinaDialogPage.getCantSeriesInput()).toMatch('5');
        subRutinaDialogPage.setCantRepeticionesInput('5');
        expect(subRutinaDialogPage.getCantRepeticionesInput()).toMatch('5');
        subRutinaDialogPage.setComentarioSubRutinaInput('comentarioSubRutina');
        expect(subRutinaDialogPage.getComentarioSubRutinaInput()).toMatch('comentarioSubRutina');
        subRutinaDialogPage.getEstadoSubrutinaInput().isSelected().then((selected) => {
            if (selected) {
                subRutinaDialogPage.getEstadoSubrutinaInput().click();
                expect(subRutinaDialogPage.getEstadoSubrutinaInput().isSelected()).toBeFalsy();
            } else {
                subRutinaDialogPage.getEstadoSubrutinaInput().click();
                expect(subRutinaDialogPage.getEstadoSubrutinaInput().isSelected()).toBeTruthy();
            }
        });
        subRutinaDialogPage.rutinaSelectLastOption();
        // subRutinaDialogPage.subrutinaEjerciciosSelectLastOption();
        subRutinaDialogPage.save();
        expect(subRutinaDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class SubRutinaComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-sub-rutina div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getText();
    }
}

export class SubRutinaDialogPage {
    modalTitle = element(by.css('h4#mySubRutinaLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nombreSubRutinaInput = element(by.css('input#field_nombreSubRutina'));
    cantSeriesInput = element(by.css('input#field_cantSeries'));
    cantRepeticionesInput = element(by.css('input#field_cantRepeticiones'));
    comentarioSubRutinaInput = element(by.css('input#field_comentarioSubRutina'));
    estadoSubrutinaInput = element(by.css('input#field_estadoSubrutina'));
    rutinaSelect = element(by.css('select#field_rutina'));
    subrutinaEjerciciosSelect = element(by.css('select#field_subrutinaEjercicios'));

    getModalTitle() {
        return this.modalTitle.getText();
    }

    setNombreSubRutinaInput = function(nombreSubRutina) {
        this.nombreSubRutinaInput.sendKeys(nombreSubRutina);
    };

    getNombreSubRutinaInput = function() {
        return this.nombreSubRutinaInput.getAttribute('value');
    };

    setCantSeriesInput = function(cantSeries) {
        this.cantSeriesInput.sendKeys(cantSeries);
    };

    getCantSeriesInput = function() {
        return this.cantSeriesInput.getAttribute('value');
    };

    setCantRepeticionesInput = function(cantRepeticiones) {
        this.cantRepeticionesInput.sendKeys(cantRepeticiones);
    };

    getCantRepeticionesInput = function() {
        return this.cantRepeticionesInput.getAttribute('value');
    };

    setComentarioSubRutinaInput = function(comentarioSubRutina) {
        this.comentarioSubRutinaInput.sendKeys(comentarioSubRutina);
    };

    getComentarioSubRutinaInput = function() {
        return this.comentarioSubRutinaInput.getAttribute('value');
    };

    getEstadoSubrutinaInput = function() {
        return this.estadoSubrutinaInput;
    };
    rutinaSelectLastOption = function() {
        this.rutinaSelect.all(by.tagName('option')).last().click();
    };

    rutinaSelectOption = function(option) {
        this.rutinaSelect.sendKeys(option);
    };

    getRutinaSelect = function() {
        return this.rutinaSelect;
    };

    getRutinaSelectedOption = function() {
        return this.rutinaSelect.element(by.css('option:checked')).getText();
    };

    subrutinaEjerciciosSelectLastOption = function() {
        this.subrutinaEjerciciosSelect.all(by.tagName('option')).last().click();
    };

    subrutinaEjerciciosSelectOption = function(option) {
        this.subrutinaEjerciciosSelect.sendKeys(option);
    };

    getSubrutinaEjerciciosSelect = function() {
        return this.subrutinaEjerciciosSelect;
    };

    getSubrutinaEjerciciosSelectedOption = function() {
        return this.subrutinaEjerciciosSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
