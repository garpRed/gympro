/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TestGymTestModule } from '../../../test.module';
import { ActivoDetailComponent } from '../../../../../../main/webapp/app/entities/activo/activo-detail.component';
import { ActivoService } from '../../../../../../main/webapp/app/entities/activo/activo.service';
import { Activo } from '../../../../../../main/webapp/app/entities/activo/activo.model';

describe('Component Tests', () => {

    describe('Activo Management Detail Component', () => {
        let comp: ActivoDetailComponent;
        let fixture: ComponentFixture<ActivoDetailComponent>;
        let service: ActivoService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [ActivoDetailComponent],
                providers: [
                    ActivoService
                ]
            })
            .overrideTemplate(ActivoDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ActivoDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ActivoService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Activo(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.activo).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
