/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestGymTestModule } from '../../../test.module';
import { MultimediaComponent } from '../../../../../../main/webapp/app/entities/multimedia/multimedia.component';
import { MultimediaService } from '../../../../../../main/webapp/app/entities/multimedia/multimedia.service';
import { Multimedia } from '../../../../../../main/webapp/app/entities/multimedia/multimedia.model';

describe('Component Tests', () => {

    describe('Multimedia Management Component', () => {
        let comp: MultimediaComponent;
        let fixture: ComponentFixture<MultimediaComponent>;
        let service: MultimediaService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [MultimediaComponent],
                providers: [
                    MultimediaService
                ]
            })
            .overrideTemplate(MultimediaComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(MultimediaComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MultimediaService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Multimedia(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.multimedias[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
