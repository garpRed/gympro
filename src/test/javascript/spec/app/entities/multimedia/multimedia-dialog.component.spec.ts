/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { TestGymTestModule } from '../../../test.module';
import { MultimediaDialogComponent } from '../../../../../../main/webapp/app/entities/multimedia/multimedia-dialog.component';
import { MultimediaService } from '../../../../../../main/webapp/app/entities/multimedia/multimedia.service';
import { Multimedia } from '../../../../../../main/webapp/app/entities/multimedia/multimedia.model';
import { ParametroConfiguracionService } from '../../../../../../main/webapp/app/entities/parametro-configuracion';

describe('Component Tests', () => {

    describe('Multimedia Management Dialog Component', () => {
        let comp: MultimediaDialogComponent;
        let fixture: ComponentFixture<MultimediaDialogComponent>;
        let service: MultimediaService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [MultimediaDialogComponent],
                providers: [
                    ParametroConfiguracionService,
                    MultimediaService
                ]
            })
            .overrideTemplate(MultimediaDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(MultimediaDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MultimediaService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Multimedia(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.multimedia = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'multimediaListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Multimedia();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.multimedia = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'multimediaListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
