/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TestGymTestModule } from '../../../test.module';
import { MultimediaDetailComponent } from '../../../../../../main/webapp/app/entities/multimedia/multimedia-detail.component';
import { MultimediaService } from '../../../../../../main/webapp/app/entities/multimedia/multimedia.service';
import { Multimedia } from '../../../../../../main/webapp/app/entities/multimedia/multimedia.model';

describe('Component Tests', () => {

    describe('Multimedia Management Detail Component', () => {
        let comp: MultimediaDetailComponent;
        let fixture: ComponentFixture<MultimediaDetailComponent>;
        let service: MultimediaService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [MultimediaDetailComponent],
                providers: [
                    MultimediaService
                ]
            })
            .overrideTemplate(MultimediaDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(MultimediaDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MultimediaService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Multimedia(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.multimedia).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
