/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TestGymTestModule } from '../../../test.module';
import { TipoParametroDetailComponent } from '../../../../../../main/webapp/app/entities/tipo-parametro/tipo-parametro-detail.component';
import { TipoParametroService } from '../../../../../../main/webapp/app/entities/tipo-parametro/tipo-parametro.service';
import { TipoParametro } from '../../../../../../main/webapp/app/entities/tipo-parametro/tipo-parametro.model';

describe('Component Tests', () => {

    describe('TipoParametro Management Detail Component', () => {
        let comp: TipoParametroDetailComponent;
        let fixture: ComponentFixture<TipoParametroDetailComponent>;
        let service: TipoParametroService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [TipoParametroDetailComponent],
                providers: [
                    TipoParametroService
                ]
            })
            .overrideTemplate(TipoParametroDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TipoParametroDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TipoParametroService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new TipoParametro(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.tipoParametro).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
