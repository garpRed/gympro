/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestGymTestModule } from '../../../test.module';
import { TipoParametroComponent } from '../../../../../../main/webapp/app/entities/tipo-parametro/tipo-parametro.component';
import { TipoParametroService } from '../../../../../../main/webapp/app/entities/tipo-parametro/tipo-parametro.service';
import { TipoParametro } from '../../../../../../main/webapp/app/entities/tipo-parametro/tipo-parametro.model';

describe('Component Tests', () => {

    describe('TipoParametro Management Component', () => {
        let comp: TipoParametroComponent;
        let fixture: ComponentFixture<TipoParametroComponent>;
        let service: TipoParametroService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [TipoParametroComponent],
                providers: [
                    TipoParametroService
                ]
            })
            .overrideTemplate(TipoParametroComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TipoParametroComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TipoParametroService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new TipoParametro(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.tipoParametros[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
