/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TestGymTestModule } from '../../../test.module';
import { HorarioDetailComponent } from '../../../../../../main/webapp/app/entities/horario/horario-detail.component';
import { HorarioService } from '../../../../../../main/webapp/app/entities/horario/horario.service';
import { Horario } from '../../../../../../main/webapp/app/entities/horario/horario.model';

describe('Component Tests', () => {

    describe('Horario Management Detail Component', () => {
        let comp: HorarioDetailComponent;
        let fixture: ComponentFixture<HorarioDetailComponent>;
        let service: HorarioService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [HorarioDetailComponent],
                providers: [
                    HorarioService
                ]
            })
            .overrideTemplate(HorarioDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(HorarioDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(HorarioService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Horario(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.horario).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
