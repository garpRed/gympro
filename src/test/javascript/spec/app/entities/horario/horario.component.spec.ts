/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestGymTestModule } from '../../../test.module';
import { HorarioComponent } from '../../../../../../main/webapp/app/entities/horario/horario.component';
import { HorarioService } from '../../../../../../main/webapp/app/entities/horario/horario.service';
import { Horario } from '../../../../../../main/webapp/app/entities/horario/horario.model';

describe('Component Tests', () => {

    describe('Horario Management Component', () => {
        let comp: HorarioComponent;
        let fixture: ComponentFixture<HorarioComponent>;
        let service: HorarioService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [HorarioComponent],
                providers: [
                    HorarioService
                ]
            })
            .overrideTemplate(HorarioComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(HorarioComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(HorarioService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Horario(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.horarios[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
