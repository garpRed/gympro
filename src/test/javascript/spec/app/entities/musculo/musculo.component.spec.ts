/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestGymTestModule } from '../../../test.module';
import { MusculoComponent } from '../../../../../../main/webapp/app/entities/musculo/musculo.component';
import { MusculoService } from '../../../../../../main/webapp/app/entities/musculo/musculo.service';
import { Musculo } from '../../../../../../main/webapp/app/entities/musculo/musculo.model';

describe('Component Tests', () => {

    describe('Musculo Management Component', () => {
        let comp: MusculoComponent;
        let fixture: ComponentFixture<MusculoComponent>;
        let service: MusculoService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [MusculoComponent],
                providers: [
                    MusculoService
                ]
            })
            .overrideTemplate(MusculoComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(MusculoComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MusculoService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Musculo(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.musculos[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
