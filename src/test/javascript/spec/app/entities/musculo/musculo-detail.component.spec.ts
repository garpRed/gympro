/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TestGymTestModule } from '../../../test.module';
import { MusculoDetailComponent } from '../../../../../../main/webapp/app/entities/musculo/musculo-detail.component';
import { MusculoService } from '../../../../../../main/webapp/app/entities/musculo/musculo.service';
import { Musculo } from '../../../../../../main/webapp/app/entities/musculo/musculo.model';

describe('Component Tests', () => {

    describe('Musculo Management Detail Component', () => {
        let comp: MusculoDetailComponent;
        let fixture: ComponentFixture<MusculoDetailComponent>;
        let service: MusculoService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [MusculoDetailComponent],
                providers: [
                    MusculoService
                ]
            })
            .overrideTemplate(MusculoDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(MusculoDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(MusculoService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Musculo(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.musculo).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
