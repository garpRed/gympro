/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestGymTestModule } from '../../../test.module';
import { EjercicioXRutinaComponent } from '../../../../../../main/webapp/app/entities/ejercicio-x-rutina/ejercicio-x-rutina.component';
import { EjercicioXRutinaService } from '../../../../../../main/webapp/app/entities/ejercicio-x-rutina/ejercicio-x-rutina.service';
import { EjercicioXRutina } from '../../../../../../main/webapp/app/entities/ejercicio-x-rutina/ejercicio-x-rutina.model';

describe('Component Tests', () => {

    describe('EjercicioXRutina Management Component', () => {
        let comp: EjercicioXRutinaComponent;
        let fixture: ComponentFixture<EjercicioXRutinaComponent>;
        let service: EjercicioXRutinaService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [EjercicioXRutinaComponent],
                providers: [
                    EjercicioXRutinaService
                ]
            })
            .overrideTemplate(EjercicioXRutinaComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(EjercicioXRutinaComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EjercicioXRutinaService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new EjercicioXRutina(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.ejercicioXRutinas[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
