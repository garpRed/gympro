/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { TestGymTestModule } from '../../../test.module';
import { EjercicioXRutinaDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/ejercicio-x-rutina/ejercicio-x-rutina-delete-dialog.component';
import { EjercicioXRutinaService } from '../../../../../../main/webapp/app/entities/ejercicio-x-rutina/ejercicio-x-rutina.service';

describe('Component Tests', () => {

    describe('EjercicioXRutina Management Delete Component', () => {
        let comp: EjercicioXRutinaDeleteDialogComponent;
        let fixture: ComponentFixture<EjercicioXRutinaDeleteDialogComponent>;
        let service: EjercicioXRutinaService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [EjercicioXRutinaDeleteDialogComponent],
                providers: [
                    EjercicioXRutinaService
                ]
            })
            .overrideTemplate(EjercicioXRutinaDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(EjercicioXRutinaDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EjercicioXRutinaService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
