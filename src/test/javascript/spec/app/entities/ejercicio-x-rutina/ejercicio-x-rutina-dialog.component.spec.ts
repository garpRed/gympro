/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { TestGymTestModule } from '../../../test.module';
import { EjercicioXRutinaDialogComponent } from '../../../../../../main/webapp/app/entities/ejercicio-x-rutina/ejercicio-x-rutina-dialog.component';
import { EjercicioXRutinaService } from '../../../../../../main/webapp/app/entities/ejercicio-x-rutina/ejercicio-x-rutina.service';
import { EjercicioXRutina } from '../../../../../../main/webapp/app/entities/ejercicio-x-rutina/ejercicio-x-rutina.model';
import { SubRutinaService } from '../../../../../../main/webapp/app/entities/sub-rutina';

describe('Component Tests', () => {

    describe('EjercicioXRutina Management Dialog Component', () => {
        let comp: EjercicioXRutinaDialogComponent;
        let fixture: ComponentFixture<EjercicioXRutinaDialogComponent>;
        let service: EjercicioXRutinaService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [EjercicioXRutinaDialogComponent],
                providers: [
                    SubRutinaService,
                    EjercicioXRutinaService
                ]
            })
            .overrideTemplate(EjercicioXRutinaDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(EjercicioXRutinaDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EjercicioXRutinaService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new EjercicioXRutina(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.ejercicioXRutina = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'ejercicioXRutinaListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new EjercicioXRutina();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.ejercicioXRutina = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'ejercicioXRutinaListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
