/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TestGymTestModule } from '../../../test.module';
import { EjercicioXRutinaDetailComponent } from '../../../../../../main/webapp/app/entities/ejercicio-x-rutina/ejercicio-x-rutina-detail.component';
import { EjercicioXRutinaService } from '../../../../../../main/webapp/app/entities/ejercicio-x-rutina/ejercicio-x-rutina.service';
import { EjercicioXRutina } from '../../../../../../main/webapp/app/entities/ejercicio-x-rutina/ejercicio-x-rutina.model';

describe('Component Tests', () => {

    describe('EjercicioXRutina Management Detail Component', () => {
        let comp: EjercicioXRutinaDetailComponent;
        let fixture: ComponentFixture<EjercicioXRutinaDetailComponent>;
        let service: EjercicioXRutinaService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [EjercicioXRutinaDetailComponent],
                providers: [
                    EjercicioXRutinaService
                ]
            })
            .overrideTemplate(EjercicioXRutinaDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(EjercicioXRutinaDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EjercicioXRutinaService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new EjercicioXRutina(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.ejercicioXRutina).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
