/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestGymTestModule } from '../../../test.module';
import { EjercicioComponent } from '../../../../../../main/webapp/app/entities/ejercicio/ejercicio.component';
import { EjercicioService } from '../../../../../../main/webapp/app/entities/ejercicio/ejercicio.service';
import { Ejercicio } from '../../../../../../main/webapp/app/entities/ejercicio/ejercicio.model';

describe('Component Tests', () => {

    describe('Ejercicio Management Component', () => {
        let comp: EjercicioComponent;
        let fixture: ComponentFixture<EjercicioComponent>;
        let service: EjercicioService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [EjercicioComponent],
                providers: [
                    EjercicioService
                ]
            })
            .overrideTemplate(EjercicioComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(EjercicioComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EjercicioService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Ejercicio(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.ejercicios[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
