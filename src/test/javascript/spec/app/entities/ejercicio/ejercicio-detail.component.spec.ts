/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TestGymTestModule } from '../../../test.module';
import { EjercicioDetailComponent } from '../../../../../../main/webapp/app/entities/ejercicio/ejercicio-detail.component';
import { EjercicioService } from '../../../../../../main/webapp/app/entities/ejercicio/ejercicio.service';
import { Ejercicio } from '../../../../../../main/webapp/app/entities/ejercicio/ejercicio.model';

describe('Component Tests', () => {

    describe('Ejercicio Management Detail Component', () => {
        let comp: EjercicioDetailComponent;
        let fixture: ComponentFixture<EjercicioDetailComponent>;
        let service: EjercicioService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [EjercicioDetailComponent],
                providers: [
                    EjercicioService
                ]
            })
            .overrideTemplate(EjercicioDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(EjercicioDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EjercicioService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Ejercicio(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.ejercicio).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
