/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { TestGymTestModule } from '../../../test.module';
import { EjercicioDialogComponent } from '../../../../../../main/webapp/app/entities/ejercicio/ejercicio-dialog.component';
import { EjercicioService } from '../../../../../../main/webapp/app/entities/ejercicio/ejercicio.service';
import { Ejercicio } from '../../../../../../main/webapp/app/entities/ejercicio/ejercicio.model';
import { SedeService } from '../../../../../../main/webapp/app/entities/sede';
import { ActivoService } from '../../../../../../main/webapp/app/entities/activo';
import { MusculoService } from '../../../../../../main/webapp/app/entities/musculo';

describe('Component Tests', () => {

    describe('Ejercicio Management Dialog Component', () => {
        let comp: EjercicioDialogComponent;
        let fixture: ComponentFixture<EjercicioDialogComponent>;
        let service: EjercicioService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [EjercicioDialogComponent],
                providers: [
                    SedeService,
                    ActivoService,
                    MusculoService,
                    EjercicioService
                ]
            })
            .overrideTemplate(EjercicioDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(EjercicioDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EjercicioService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Ejercicio(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.ejercicio = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'ejercicioListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Ejercicio();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.ejercicio = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'ejercicioListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
