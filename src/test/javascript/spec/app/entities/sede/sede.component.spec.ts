/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestGymTestModule } from '../../../test.module';
import { SedeComponent } from '../../../../../../main/webapp/app/entities/sede/sede.component';
import { SedeService } from '../../../../../../main/webapp/app/entities/sede/sede.service';
import { Sede } from '../../../../../../main/webapp/app/entities/sede/sede.model';

describe('Component Tests', () => {

    describe('Sede Management Component', () => {
        let comp: SedeComponent;
        let fixture: ComponentFixture<SedeComponent>;
        let service: SedeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [SedeComponent],
                providers: [
                    SedeService
                ]
            })
            .overrideTemplate(SedeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SedeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SedeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Sede(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.sedes[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
