/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TestGymTestModule } from '../../../test.module';
import { SedeDetailComponent } from '../../../../../../main/webapp/app/entities/sede/sede-detail.component';
import { SedeService } from '../../../../../../main/webapp/app/entities/sede/sede.service';
import { Sede } from '../../../../../../main/webapp/app/entities/sede/sede.model';

describe('Component Tests', () => {

    describe('Sede Management Detail Component', () => {
        let comp: SedeDetailComponent;
        let fixture: ComponentFixture<SedeDetailComponent>;
        let service: SedeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [SedeDetailComponent],
                providers: [
                    SedeService
                ]
            })
            .overrideTemplate(SedeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SedeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SedeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Sede(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.sede).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
