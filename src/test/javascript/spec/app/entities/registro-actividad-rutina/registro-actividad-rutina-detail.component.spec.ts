/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TestGymTestModule } from '../../../test.module';
import { RegistroActividadRutinaDetailComponent } from '../../../../../../main/webapp/app/entities/registro-actividad-rutina/registro-actividad-rutina-detail.component';
import { RegistroActividadRutinaService } from '../../../../../../main/webapp/app/entities/registro-actividad-rutina/registro-actividad-rutina.service';
import { RegistroActividadRutina } from '../../../../../../main/webapp/app/entities/registro-actividad-rutina/registro-actividad-rutina.model';

describe('Component Tests', () => {

    describe('RegistroActividadRutina Management Detail Component', () => {
        let comp: RegistroActividadRutinaDetailComponent;
        let fixture: ComponentFixture<RegistroActividadRutinaDetailComponent>;
        let service: RegistroActividadRutinaService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [RegistroActividadRutinaDetailComponent],
                providers: [
                    RegistroActividadRutinaService
                ]
            })
            .overrideTemplate(RegistroActividadRutinaDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RegistroActividadRutinaDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RegistroActividadRutinaService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new RegistroActividadRutina(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.registroActividadRutina).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
