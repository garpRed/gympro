/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestGymTestModule } from '../../../test.module';
import { RegistroActividadRutinaComponent } from '../../../../../../main/webapp/app/entities/registro-actividad-rutina/registro-actividad-rutina.component';
import { RegistroActividadRutinaService } from '../../../../../../main/webapp/app/entities/registro-actividad-rutina/registro-actividad-rutina.service';
import { RegistroActividadRutina } from '../../../../../../main/webapp/app/entities/registro-actividad-rutina/registro-actividad-rutina.model';

describe('Component Tests', () => {

    describe('RegistroActividadRutina Management Component', () => {
        let comp: RegistroActividadRutinaComponent;
        let fixture: ComponentFixture<RegistroActividadRutinaComponent>;
        let service: RegistroActividadRutinaService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [RegistroActividadRutinaComponent],
                providers: [
                    RegistroActividadRutinaService
                ]
            })
            .overrideTemplate(RegistroActividadRutinaComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RegistroActividadRutinaComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RegistroActividadRutinaService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new RegistroActividadRutina(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.registroActividadRutinas[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
