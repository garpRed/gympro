/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { TestGymTestModule } from '../../../test.module';
import { RegistroActividadRutinaDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/registro-actividad-rutina/registro-actividad-rutina-delete-dialog.component';
import { RegistroActividadRutinaService } from '../../../../../../main/webapp/app/entities/registro-actividad-rutina/registro-actividad-rutina.service';

describe('Component Tests', () => {

    describe('RegistroActividadRutina Management Delete Component', () => {
        let comp: RegistroActividadRutinaDeleteDialogComponent;
        let fixture: ComponentFixture<RegistroActividadRutinaDeleteDialogComponent>;
        let service: RegistroActividadRutinaService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [RegistroActividadRutinaDeleteDialogComponent],
                providers: [
                    RegistroActividadRutinaService
                ]
            })
            .overrideTemplate(RegistroActividadRutinaDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RegistroActividadRutinaDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RegistroActividadRutinaService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
