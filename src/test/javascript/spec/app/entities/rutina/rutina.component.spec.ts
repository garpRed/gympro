/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestGymTestModule } from '../../../test.module';
import { RutinaComponent } from '../../../../../../main/webapp/app/entities/rutina/rutina.component';
import { RutinaService } from '../../../../../../main/webapp/app/entities/rutina/rutina.service';
import { Rutina } from '../../../../../../main/webapp/app/entities/rutina/rutina.model';

describe('Component Tests', () => {

    describe('Rutina Management Component', () => {
        let comp: RutinaComponent;
        let fixture: ComponentFixture<RutinaComponent>;
        let service: RutinaService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [RutinaComponent],
                providers: [
                    RutinaService
                ]
            })
            .overrideTemplate(RutinaComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RutinaComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RutinaService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Rutina(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.rutinas[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
