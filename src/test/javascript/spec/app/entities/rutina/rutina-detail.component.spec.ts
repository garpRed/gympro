/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TestGymTestModule } from '../../../test.module';
import { RutinaDetailComponent } from '../../../../../../main/webapp/app/entities/rutina/rutina-detail.component';
import { RutinaService } from '../../../../../../main/webapp/app/entities/rutina/rutina.service';
import { Rutina } from '../../../../../../main/webapp/app/entities/rutina/rutina.model';

describe('Component Tests', () => {

    describe('Rutina Management Detail Component', () => {
        let comp: RutinaDetailComponent;
        let fixture: ComponentFixture<RutinaDetailComponent>;
        let service: RutinaService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [RutinaDetailComponent],
                providers: [
                    RutinaService
                ]
            })
            .overrideTemplate(RutinaDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RutinaDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RutinaService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Rutina(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.rutina).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
