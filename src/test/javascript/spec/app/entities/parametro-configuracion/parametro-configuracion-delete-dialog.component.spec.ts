/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { TestGymTestModule } from '../../../test.module';
import { ParametroConfiguracionDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/parametro-configuracion/parametro-configuracion-delete-dialog.component';
import { ParametroConfiguracionService } from '../../../../../../main/webapp/app/entities/parametro-configuracion/parametro-configuracion.service';

describe('Component Tests', () => {

    describe('ParametroConfiguracion Management Delete Component', () => {
        let comp: ParametroConfiguracionDeleteDialogComponent;
        let fixture: ComponentFixture<ParametroConfiguracionDeleteDialogComponent>;
        let service: ParametroConfiguracionService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [ParametroConfiguracionDeleteDialogComponent],
                providers: [
                    ParametroConfiguracionService
                ]
            })
            .overrideTemplate(ParametroConfiguracionDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ParametroConfiguracionDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ParametroConfiguracionService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
