/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TestGymTestModule } from '../../../test.module';
import { ParametroConfiguracionDetailComponent } from '../../../../../../main/webapp/app/entities/parametro-configuracion/parametro-configuracion-detail.component';
import { ParametroConfiguracionService } from '../../../../../../main/webapp/app/entities/parametro-configuracion/parametro-configuracion.service';
import { ParametroConfiguracion } from '../../../../../../main/webapp/app/entities/parametro-configuracion/parametro-configuracion.model';

describe('Component Tests', () => {

    describe('ParametroConfiguracion Management Detail Component', () => {
        let comp: ParametroConfiguracionDetailComponent;
        let fixture: ComponentFixture<ParametroConfiguracionDetailComponent>;
        let service: ParametroConfiguracionService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [ParametroConfiguracionDetailComponent],
                providers: [
                    ParametroConfiguracionService
                ]
            })
            .overrideTemplate(ParametroConfiguracionDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ParametroConfiguracionDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ParametroConfiguracionService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new ParametroConfiguracion(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.parametroConfiguracion).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
