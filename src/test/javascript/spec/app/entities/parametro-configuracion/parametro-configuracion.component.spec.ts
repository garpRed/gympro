/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestGymTestModule } from '../../../test.module';
import { ParametroConfiguracionComponent } from '../../../../../../main/webapp/app/entities/parametro-configuracion/parametro-configuracion.component';
import { ParametroConfiguracionService } from '../../../../../../main/webapp/app/entities/parametro-configuracion/parametro-configuracion.service';
import { ParametroConfiguracion } from '../../../../../../main/webapp/app/entities/parametro-configuracion/parametro-configuracion.model';

describe('Component Tests', () => {

    describe('ParametroConfiguracion Management Component', () => {
        let comp: ParametroConfiguracionComponent;
        let fixture: ComponentFixture<ParametroConfiguracionComponent>;
        let service: ParametroConfiguracionService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [ParametroConfiguracionComponent],
                providers: [
                    ParametroConfiguracionService
                ]
            })
            .overrideTemplate(ParametroConfiguracionComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ParametroConfiguracionComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ParametroConfiguracionService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new ParametroConfiguracion(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.parametroConfiguracions[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
