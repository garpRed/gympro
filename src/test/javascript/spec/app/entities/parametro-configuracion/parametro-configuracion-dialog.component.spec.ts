/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { TestGymTestModule } from '../../../test.module';
import { ParametroConfiguracionDialogComponent } from '../../../../../../main/webapp/app/entities/parametro-configuracion/parametro-configuracion-dialog.component';
import { ParametroConfiguracionService } from '../../../../../../main/webapp/app/entities/parametro-configuracion/parametro-configuracion.service';
import { ParametroConfiguracion } from '../../../../../../main/webapp/app/entities/parametro-configuracion/parametro-configuracion.model';
import { TipoParametroService } from '../../../../../../main/webapp/app/entities/tipo-parametro';
import { MultimediaService } from '../../../../../../main/webapp/app/entities/multimedia';
import { MusculoService } from '../../../../../../main/webapp/app/entities/musculo';

describe('Component Tests', () => {

    describe('ParametroConfiguracion Management Dialog Component', () => {
        let comp: ParametroConfiguracionDialogComponent;
        let fixture: ComponentFixture<ParametroConfiguracionDialogComponent>;
        let service: ParametroConfiguracionService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [ParametroConfiguracionDialogComponent],
                providers: [
                    TipoParametroService,
                    MultimediaService,
                    MusculoService,
                    ParametroConfiguracionService
                ]
            })
            .overrideTemplate(ParametroConfiguracionDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ParametroConfiguracionDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ParametroConfiguracionService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ParametroConfiguracion(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.parametroConfiguracion = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'parametroConfiguracionListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new ParametroConfiguracion();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.parametroConfiguracion = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'parametroConfiguracionListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
