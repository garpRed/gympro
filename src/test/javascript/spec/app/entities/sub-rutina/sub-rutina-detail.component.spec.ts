/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TestGymTestModule } from '../../../test.module';
import { SubRutinaDetailComponent } from '../../../../../../main/webapp/app/entities/sub-rutina/sub-rutina-detail.component';
import { SubRutinaService } from '../../../../../../main/webapp/app/entities/sub-rutina/sub-rutina.service';
import { SubRutina } from '../../../../../../main/webapp/app/entities/sub-rutina/sub-rutina.model';

describe('Component Tests', () => {

    describe('SubRutina Management Detail Component', () => {
        let comp: SubRutinaDetailComponent;
        let fixture: ComponentFixture<SubRutinaDetailComponent>;
        let service: SubRutinaService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [SubRutinaDetailComponent],
                providers: [
                    SubRutinaService
                ]
            })
            .overrideTemplate(SubRutinaDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SubRutinaDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SubRutinaService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new SubRutina(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.subRutina).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
