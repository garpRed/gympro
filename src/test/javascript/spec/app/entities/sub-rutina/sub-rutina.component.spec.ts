/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestGymTestModule } from '../../../test.module';
import { SubRutinaComponent } from '../../../../../../main/webapp/app/entities/sub-rutina/sub-rutina.component';
import { SubRutinaService } from '../../../../../../main/webapp/app/entities/sub-rutina/sub-rutina.service';
import { SubRutina } from '../../../../../../main/webapp/app/entities/sub-rutina/sub-rutina.model';

describe('Component Tests', () => {

    describe('SubRutina Management Component', () => {
        let comp: SubRutinaComponent;
        let fixture: ComponentFixture<SubRutinaComponent>;
        let service: SubRutinaService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [SubRutinaComponent],
                providers: [
                    SubRutinaService
                ]
            })
            .overrideTemplate(SubRutinaComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SubRutinaComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SubRutinaService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new SubRutina(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.subRutinas[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
