/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { TestGymTestModule } from '../../../test.module';
import { SubRutinaDialogComponent } from '../../../../../../main/webapp/app/entities/sub-rutina/sub-rutina-dialog.component';
import { SubRutinaService } from '../../../../../../main/webapp/app/entities/sub-rutina/sub-rutina.service';
import { SubRutina } from '../../../../../../main/webapp/app/entities/sub-rutina/sub-rutina.model';
import { RutinaService } from '../../../../../../main/webapp/app/entities/rutina';
import { EjercicioService } from '../../../../../../main/webapp/app/entities/ejercicio';

describe('Component Tests', () => {

    describe('SubRutina Management Dialog Component', () => {
        let comp: SubRutinaDialogComponent;
        let fixture: ComponentFixture<SubRutinaDialogComponent>;
        let service: SubRutinaService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [SubRutinaDialogComponent],
                providers: [
                    RutinaService,
                    EjercicioService,
                    SubRutinaService
                ]
            })
            .overrideTemplate(SubRutinaDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SubRutinaDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SubRutinaService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new SubRutina(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.subRutina = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'subRutinaListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new SubRutina();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.subRutina = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'subRutinaListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
