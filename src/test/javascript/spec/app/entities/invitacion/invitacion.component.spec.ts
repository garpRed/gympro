/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestGymTestModule } from '../../../test.module';
import { InvitacionComponent } from '../../../../../../main/webapp/app/entities/invitacion/invitacion.component';
import { InvitacionService } from '../../../../../../main/webapp/app/entities/invitacion/invitacion.service';
import { Invitacion } from '../../../../../../main/webapp/app/entities/invitacion/invitacion.model';

describe('Component Tests', () => {

    describe('Invitacion Management Component', () => {
        let comp: InvitacionComponent;
        let fixture: ComponentFixture<InvitacionComponent>;
        let service: InvitacionService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [InvitacionComponent],
                providers: [
                    InvitacionService
                ]
            })
            .overrideTemplate(InvitacionComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(InvitacionComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(InvitacionService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Invitacion(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.invitacions[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
