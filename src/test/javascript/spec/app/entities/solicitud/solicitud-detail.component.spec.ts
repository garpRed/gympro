/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TestGymTestModule } from '../../../test.module';
import { SolicitudDetailComponent } from '../../../../../../main/webapp/app/entities/solicitud/solicitud-detail.component';
import { SolicitudService } from '../../../../../../main/webapp/app/entities/solicitud/solicitud.service';
import { Solicitud } from '../../../../../../main/webapp/app/entities/solicitud/solicitud.model';

describe('Component Tests', () => {

    describe('Solicitud Management Detail Component', () => {
        let comp: SolicitudDetailComponent;
        let fixture: ComponentFixture<SolicitudDetailComponent>;
        let service: SolicitudService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [SolicitudDetailComponent],
                providers: [
                    SolicitudService
                ]
            })
            .overrideTemplate(SolicitudDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SolicitudDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SolicitudService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Solicitud(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.solicitud).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
