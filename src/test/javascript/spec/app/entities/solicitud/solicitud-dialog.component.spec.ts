/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { TestGymTestModule } from '../../../test.module';
import { SolicitudDialogComponent } from '../../../../../../main/webapp/app/entities/solicitud/solicitud-dialog.component';
import { SolicitudService } from '../../../../../../main/webapp/app/entities/solicitud/solicitud.service';
import { Solicitud } from '../../../../../../main/webapp/app/entities/solicitud/solicitud.model';
import { UsuarioService } from '../../../../../../main/webapp/app/entities/usuario';
import { ParametroConfiguracionService } from '../../../../../../main/webapp/app/entities/parametro-configuracion';

describe('Component Tests', () => {

    describe('Solicitud Management Dialog Component', () => {
        let comp: SolicitudDialogComponent;
        let fixture: ComponentFixture<SolicitudDialogComponent>;
        let service: SolicitudService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [SolicitudDialogComponent],
                providers: [
                    UsuarioService,
                    ParametroConfiguracionService,
                    SolicitudService
                ]
            })
            .overrideTemplate(SolicitudDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SolicitudDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SolicitudService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Solicitud(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.solicitud = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'solicitudListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Solicitud();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.solicitud = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'solicitudListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
