/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestGymTestModule } from '../../../test.module';
import { SolicitudComponent } from '../../../../../../main/webapp/app/entities/solicitud/solicitud.component';
import { SolicitudService } from '../../../../../../main/webapp/app/entities/solicitud/solicitud.service';
import { Solicitud } from '../../../../../../main/webapp/app/entities/solicitud/solicitud.model';

describe('Component Tests', () => {

    describe('Solicitud Management Component', () => {
        let comp: SolicitudComponent;
        let fixture: ComponentFixture<SolicitudComponent>;
        let service: SolicitudService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [SolicitudComponent],
                providers: [
                    SolicitudService
                ]
            })
            .overrideTemplate(SolicitudComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SolicitudComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SolicitudService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Solicitud(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.solicituds[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
