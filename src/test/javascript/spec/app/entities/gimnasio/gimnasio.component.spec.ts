/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TestGymTestModule } from '../../../test.module';
import { GimnasioComponent } from '../../../../../../main/webapp/app/entities/gimnasio/gimnasio.component';
import { GimnasioService } from '../../../../../../main/webapp/app/entities/gimnasio/gimnasio.service';
import { Gimnasio } from '../../../../../../main/webapp/app/entities/gimnasio/gimnasio.model';

describe('Component Tests', () => {

    describe('Gimnasio Management Component', () => {
        let comp: GimnasioComponent;
        let fixture: ComponentFixture<GimnasioComponent>;
        let service: GimnasioService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [GimnasioComponent],
                providers: [
                    GimnasioService
                ]
            })
            .overrideTemplate(GimnasioComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(GimnasioComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(GimnasioService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Gimnasio(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.gimnasios[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
