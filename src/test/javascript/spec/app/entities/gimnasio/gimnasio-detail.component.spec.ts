/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TestGymTestModule } from '../../../test.module';
import { GimnasioDetailComponent } from '../../../../../../main/webapp/app/entities/gimnasio/gimnasio-detail.component';
import { GimnasioService } from '../../../../../../main/webapp/app/entities/gimnasio/gimnasio.service';
import { Gimnasio } from '../../../../../../main/webapp/app/entities/gimnasio/gimnasio.model';

describe('Component Tests', () => {

    describe('Gimnasio Management Detail Component', () => {
        let comp: GimnasioDetailComponent;
        let fixture: ComponentFixture<GimnasioDetailComponent>;
        let service: GimnasioService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TestGymTestModule],
                declarations: [GimnasioDetailComponent],
                providers: [
                    GimnasioService
                ]
            })
            .overrideTemplate(GimnasioDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(GimnasioDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(GimnasioService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Gimnasio(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.gimnasio).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
