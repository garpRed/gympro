package com.test.gymtest.web.rest;

import com.test.gymtest.TestGymApp;

import com.test.gymtest.domain.EjercicioXRutina;
import com.test.gymtest.repository.EjercicioXRutinaRepository;
import com.test.gymtest.service.EjercicioXRutinaService;
import com.test.gymtest.service.dto.EjercicioXRutinaDTO;
import com.test.gymtest.service.mapper.EjercicioXRutinaMapper;
import com.test.gymtest.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.test.gymtest.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EjercicioXRutinaResource REST controller.
 *
 * @see EjercicioXRutinaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestGymApp.class)
public class EjercicioXRutinaResourceIntTest {

    private static final Integer DEFAULT_PESO = 500;
    private static final Integer UPDATED_PESO = 499;

    private static final Integer DEFAULT_REPS = 500;
    private static final Integer UPDATED_REPS = 499;

    private static final Integer DEFAULT_SETS = 500;
    private static final Integer UPDATED_SETS = 499;

    private static final String DEFAULT_AJUSTE = "AAAAAAAAAA";
    private static final String UPDATED_AJUSTE = "BBBBBBBBBB";

    private static final String DEFAULT_TIEMPO = "AAAAAAAAAA";
    private static final String UPDATED_TIEMPO = "BBBBBBBBBB";

    private static final String DEFAULT_COMMENTARIO = "AAAAAAAAAA";
    private static final String UPDATED_COMMENTARIO = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ESTADO_EJERCICIO_RUTINA = false;
    private static final Boolean UPDATED_ESTADO_EJERCICIO_RUTINA = true;

    @Autowired
    private EjercicioXRutinaRepository ejercicioXRutinaRepository;

    @Autowired
    private EjercicioXRutinaMapper ejercicioXRutinaMapper;

    @Autowired
    private EjercicioXRutinaService ejercicioXRutinaService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restEjercicioXRutinaMockMvc;

    private EjercicioXRutina ejercicioXRutina;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EjercicioXRutinaResource ejercicioXRutinaResource = new EjercicioXRutinaResource(ejercicioXRutinaService);
        this.restEjercicioXRutinaMockMvc = MockMvcBuilders.standaloneSetup(ejercicioXRutinaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EjercicioXRutina createEntity(EntityManager em) {
        EjercicioXRutina ejercicioXRutina = new EjercicioXRutina()
            .peso(DEFAULT_PESO)
            .reps(DEFAULT_REPS)
            .sets(DEFAULT_SETS)
            .ajuste(DEFAULT_AJUSTE)
            .tiempo(DEFAULT_TIEMPO)
            .commentario(DEFAULT_COMMENTARIO)
            .estadoEjercicioRutina(DEFAULT_ESTADO_EJERCICIO_RUTINA);
        return ejercicioXRutina;
    }

    @Before
    public void initTest() {
        ejercicioXRutina = createEntity(em);
    }

    @Test
    @Transactional
    public void createEjercicioXRutina() throws Exception {
        int databaseSizeBeforeCreate = ejercicioXRutinaRepository.findAll().size();

        // Create the EjercicioXRutina
        EjercicioXRutinaDTO ejercicioXRutinaDTO = ejercicioXRutinaMapper.toDto(ejercicioXRutina);
        restEjercicioXRutinaMockMvc.perform(post("/api/ejercicio-x-rutinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ejercicioXRutinaDTO)))
            .andExpect(status().isCreated());

        // Validate the EjercicioXRutina in the database
        List<EjercicioXRutina> ejercicioXRutinaList = ejercicioXRutinaRepository.findAll();
        assertThat(ejercicioXRutinaList).hasSize(databaseSizeBeforeCreate + 1);
        EjercicioXRutina testEjercicioXRutina = ejercicioXRutinaList.get(ejercicioXRutinaList.size() - 1);
        assertThat(testEjercicioXRutina.getPeso()).isEqualTo(DEFAULT_PESO);
        assertThat(testEjercicioXRutina.getReps()).isEqualTo(DEFAULT_REPS);
        assertThat(testEjercicioXRutina.getSets()).isEqualTo(DEFAULT_SETS);
        assertThat(testEjercicioXRutina.getAjuste()).isEqualTo(DEFAULT_AJUSTE);
        assertThat(testEjercicioXRutina.getTiempo()).isEqualTo(DEFAULT_TIEMPO);
        assertThat(testEjercicioXRutina.getCommentario()).isEqualTo(DEFAULT_COMMENTARIO);
        assertThat(testEjercicioXRutina.isEstadoEjercicioRutina()).isEqualTo(DEFAULT_ESTADO_EJERCICIO_RUTINA);
    }

    @Test
    @Transactional
    public void createEjercicioXRutinaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ejercicioXRutinaRepository.findAll().size();

        // Create the EjercicioXRutina with an existing ID
        ejercicioXRutina.setId(1L);
        EjercicioXRutinaDTO ejercicioXRutinaDTO = ejercicioXRutinaMapper.toDto(ejercicioXRutina);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEjercicioXRutinaMockMvc.perform(post("/api/ejercicio-x-rutinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ejercicioXRutinaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EjercicioXRutina in the database
        List<EjercicioXRutina> ejercicioXRutinaList = ejercicioXRutinaRepository.findAll();
        assertThat(ejercicioXRutinaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkEstadoEjercicioRutinaIsRequired() throws Exception {
        int databaseSizeBeforeTest = ejercicioXRutinaRepository.findAll().size();
        // set the field null
        ejercicioXRutina.setEstadoEjercicioRutina(null);

        // Create the EjercicioXRutina, which fails.
        EjercicioXRutinaDTO ejercicioXRutinaDTO = ejercicioXRutinaMapper.toDto(ejercicioXRutina);

        restEjercicioXRutinaMockMvc.perform(post("/api/ejercicio-x-rutinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ejercicioXRutinaDTO)))
            .andExpect(status().isBadRequest());

        List<EjercicioXRutina> ejercicioXRutinaList = ejercicioXRutinaRepository.findAll();
        assertThat(ejercicioXRutinaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEjercicioXRutinas() throws Exception {
        // Initialize the database
        ejercicioXRutinaRepository.saveAndFlush(ejercicioXRutina);

        // Get all the ejercicioXRutinaList
        restEjercicioXRutinaMockMvc.perform(get("/api/ejercicio-x-rutinas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ejercicioXRutina.getId().intValue())))
            .andExpect(jsonPath("$.[*].peso").value(hasItem(DEFAULT_PESO)))
            .andExpect(jsonPath("$.[*].reps").value(hasItem(DEFAULT_REPS)))
            .andExpect(jsonPath("$.[*].sets").value(hasItem(DEFAULT_SETS)))
            .andExpect(jsonPath("$.[*].ajuste").value(hasItem(DEFAULT_AJUSTE.toString())))
            .andExpect(jsonPath("$.[*].tiempo").value(hasItem(DEFAULT_TIEMPO.toString())))
            .andExpect(jsonPath("$.[*].commentario").value(hasItem(DEFAULT_COMMENTARIO.toString())))
            .andExpect(jsonPath("$.[*].estadoEjercicioRutina").value(hasItem(DEFAULT_ESTADO_EJERCICIO_RUTINA.booleanValue())));
    }

    @Test
    @Transactional
    public void getEjercicioXRutina() throws Exception {
        // Initialize the database
        ejercicioXRutinaRepository.saveAndFlush(ejercicioXRutina);

        // Get the ejercicioXRutina
        restEjercicioXRutinaMockMvc.perform(get("/api/ejercicio-x-rutinas/{id}", ejercicioXRutina.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(ejercicioXRutina.getId().intValue()))
            .andExpect(jsonPath("$.peso").value(DEFAULT_PESO))
            .andExpect(jsonPath("$.reps").value(DEFAULT_REPS))
            .andExpect(jsonPath("$.sets").value(DEFAULT_SETS))
            .andExpect(jsonPath("$.ajuste").value(DEFAULT_AJUSTE.toString()))
            .andExpect(jsonPath("$.tiempo").value(DEFAULT_TIEMPO.toString()))
            .andExpect(jsonPath("$.commentario").value(DEFAULT_COMMENTARIO.toString()))
            .andExpect(jsonPath("$.estadoEjercicioRutina").value(DEFAULT_ESTADO_EJERCICIO_RUTINA.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingEjercicioXRutina() throws Exception {
        // Get the ejercicioXRutina
        restEjercicioXRutinaMockMvc.perform(get("/api/ejercicio-x-rutinas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEjercicioXRutina() throws Exception {
        // Initialize the database
        ejercicioXRutinaRepository.saveAndFlush(ejercicioXRutina);
        int databaseSizeBeforeUpdate = ejercicioXRutinaRepository.findAll().size();

        // Update the ejercicioXRutina
        EjercicioXRutina updatedEjercicioXRutina = ejercicioXRutinaRepository.findOne(ejercicioXRutina.getId());
        // Disconnect from session so that the updates on updatedEjercicioXRutina are not directly saved in db
        em.detach(updatedEjercicioXRutina);
        updatedEjercicioXRutina
            .peso(UPDATED_PESO)
            .reps(UPDATED_REPS)
            .sets(UPDATED_SETS)
            .ajuste(UPDATED_AJUSTE)
            .tiempo(UPDATED_TIEMPO)
            .commentario(UPDATED_COMMENTARIO)
            .estadoEjercicioRutina(UPDATED_ESTADO_EJERCICIO_RUTINA);
        EjercicioXRutinaDTO ejercicioXRutinaDTO = ejercicioXRutinaMapper.toDto(updatedEjercicioXRutina);

        restEjercicioXRutinaMockMvc.perform(put("/api/ejercicio-x-rutinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ejercicioXRutinaDTO)))
            .andExpect(status().isOk());

        // Validate the EjercicioXRutina in the database
        List<EjercicioXRutina> ejercicioXRutinaList = ejercicioXRutinaRepository.findAll();
        assertThat(ejercicioXRutinaList).hasSize(databaseSizeBeforeUpdate);
        EjercicioXRutina testEjercicioXRutina = ejercicioXRutinaList.get(ejercicioXRutinaList.size() - 1);
        assertThat(testEjercicioXRutina.getPeso()).isEqualTo(UPDATED_PESO);
        assertThat(testEjercicioXRutina.getReps()).isEqualTo(UPDATED_REPS);
        assertThat(testEjercicioXRutina.getSets()).isEqualTo(UPDATED_SETS);
        assertThat(testEjercicioXRutina.getAjuste()).isEqualTo(UPDATED_AJUSTE);
        assertThat(testEjercicioXRutina.getTiempo()).isEqualTo(UPDATED_TIEMPO);
        assertThat(testEjercicioXRutina.getCommentario()).isEqualTo(UPDATED_COMMENTARIO);
        assertThat(testEjercicioXRutina.isEstadoEjercicioRutina()).isEqualTo(UPDATED_ESTADO_EJERCICIO_RUTINA);
    }

    @Test
    @Transactional
    public void updateNonExistingEjercicioXRutina() throws Exception {
        int databaseSizeBeforeUpdate = ejercicioXRutinaRepository.findAll().size();

        // Create the EjercicioXRutina
        EjercicioXRutinaDTO ejercicioXRutinaDTO = ejercicioXRutinaMapper.toDto(ejercicioXRutina);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restEjercicioXRutinaMockMvc.perform(put("/api/ejercicio-x-rutinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ejercicioXRutinaDTO)))
            .andExpect(status().isCreated());

        // Validate the EjercicioXRutina in the database
        List<EjercicioXRutina> ejercicioXRutinaList = ejercicioXRutinaRepository.findAll();
        assertThat(ejercicioXRutinaList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteEjercicioXRutina() throws Exception {
        // Initialize the database
        ejercicioXRutinaRepository.saveAndFlush(ejercicioXRutina);
        int databaseSizeBeforeDelete = ejercicioXRutinaRepository.findAll().size();

        // Get the ejercicioXRutina
        restEjercicioXRutinaMockMvc.perform(delete("/api/ejercicio-x-rutinas/{id}", ejercicioXRutina.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<EjercicioXRutina> ejercicioXRutinaList = ejercicioXRutinaRepository.findAll();
        assertThat(ejercicioXRutinaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EjercicioXRutina.class);
        EjercicioXRutina ejercicioXRutina1 = new EjercicioXRutina();
        ejercicioXRutina1.setId(1L);
        EjercicioXRutina ejercicioXRutina2 = new EjercicioXRutina();
        ejercicioXRutina2.setId(ejercicioXRutina1.getId());
        assertThat(ejercicioXRutina1).isEqualTo(ejercicioXRutina2);
        ejercicioXRutina2.setId(2L);
        assertThat(ejercicioXRutina1).isNotEqualTo(ejercicioXRutina2);
        ejercicioXRutina1.setId(null);
        assertThat(ejercicioXRutina1).isNotEqualTo(ejercicioXRutina2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EjercicioXRutinaDTO.class);
        EjercicioXRutinaDTO ejercicioXRutinaDTO1 = new EjercicioXRutinaDTO();
        ejercicioXRutinaDTO1.setId(1L);
        EjercicioXRutinaDTO ejercicioXRutinaDTO2 = new EjercicioXRutinaDTO();
        assertThat(ejercicioXRutinaDTO1).isNotEqualTo(ejercicioXRutinaDTO2);
        ejercicioXRutinaDTO2.setId(ejercicioXRutinaDTO1.getId());
        assertThat(ejercicioXRutinaDTO1).isEqualTo(ejercicioXRutinaDTO2);
        ejercicioXRutinaDTO2.setId(2L);
        assertThat(ejercicioXRutinaDTO1).isNotEqualTo(ejercicioXRutinaDTO2);
        ejercicioXRutinaDTO1.setId(null);
        assertThat(ejercicioXRutinaDTO1).isNotEqualTo(ejercicioXRutinaDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(ejercicioXRutinaMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(ejercicioXRutinaMapper.fromId(null)).isNull();
    }
}
