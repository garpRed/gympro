package com.test.gymtest.web.rest;

import com.test.gymtest.TestGymApp;

import com.test.gymtest.domain.RegistroActividadRutina;
import com.test.gymtest.repository.RegistroActividadRutinaRepository;
import com.test.gymtest.service.RegistroActividadRutinaService;
import com.test.gymtest.service.dto.RegistroActividadRutinaDTO;
import com.test.gymtest.service.mapper.RegistroActividadRutinaMapper;
import com.test.gymtest.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.test.gymtest.web.rest.TestUtil.sameInstant;
import static com.test.gymtest.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RegistroActividadRutinaResource REST controller.
 *
 * @see RegistroActividadRutinaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestGymApp.class)
public class RegistroActividadRutinaResourceIntTest {

    private static final ZonedDateTime DEFAULT_FECHA_ASISTENCIA = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_FECHA_ASISTENCIA = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Integer DEFAULT_DIA_ASISTENCIA = 1;
    private static final Integer UPDATED_DIA_ASISTENCIA = 2;

    private static final Boolean DEFAULT_ESTADO_REGISTRO_ACTIVIDAD = false;
    private static final Boolean UPDATED_ESTADO_REGISTRO_ACTIVIDAD = true;

    @Autowired
    private RegistroActividadRutinaRepository registroActividadRutinaRepository;

    @Autowired
    private RegistroActividadRutinaMapper registroActividadRutinaMapper;

    @Autowired
    private RegistroActividadRutinaService registroActividadRutinaService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRegistroActividadRutinaMockMvc;

    private RegistroActividadRutina registroActividadRutina;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RegistroActividadRutinaResource registroActividadRutinaResource = new RegistroActividadRutinaResource(registroActividadRutinaService);
        this.restRegistroActividadRutinaMockMvc = MockMvcBuilders.standaloneSetup(registroActividadRutinaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegistroActividadRutina createEntity(EntityManager em) {
        RegistroActividadRutina registroActividadRutina = new RegistroActividadRutina()
            .fechaAsistencia(DEFAULT_FECHA_ASISTENCIA)
            .diaAsistencia(DEFAULT_DIA_ASISTENCIA)
            .estadoRegistroActividad(DEFAULT_ESTADO_REGISTRO_ACTIVIDAD);
        return registroActividadRutina;
    }

    @Before
    public void initTest() {
        registroActividadRutina = createEntity(em);
    }

    @Test
    @Transactional
    public void createRegistroActividadRutina() throws Exception {
        int databaseSizeBeforeCreate = registroActividadRutinaRepository.findAll().size();

        // Create the RegistroActividadRutina
        RegistroActividadRutinaDTO registroActividadRutinaDTO = registroActividadRutinaMapper.toDto(registroActividadRutina);
        restRegistroActividadRutinaMockMvc.perform(post("/api/registro-actividad-rutinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(registroActividadRutinaDTO)))
            .andExpect(status().isCreated());

        // Validate the RegistroActividadRutina in the database
        List<RegistroActividadRutina> registroActividadRutinaList = registroActividadRutinaRepository.findAll();
        assertThat(registroActividadRutinaList).hasSize(databaseSizeBeforeCreate + 1);
        RegistroActividadRutina testRegistroActividadRutina = registroActividadRutinaList.get(registroActividadRutinaList.size() - 1);
        assertThat(testRegistroActividadRutina.getFechaAsistencia()).isEqualTo(DEFAULT_FECHA_ASISTENCIA);
        assertThat(testRegistroActividadRutina.getDiaAsistencia()).isEqualTo(DEFAULT_DIA_ASISTENCIA);
        assertThat(testRegistroActividadRutina.isEstadoRegistroActividad()).isEqualTo(DEFAULT_ESTADO_REGISTRO_ACTIVIDAD);
    }

    @Test
    @Transactional
    public void createRegistroActividadRutinaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = registroActividadRutinaRepository.findAll().size();

        // Create the RegistroActividadRutina with an existing ID
        registroActividadRutina.setId(1L);
        RegistroActividadRutinaDTO registroActividadRutinaDTO = registroActividadRutinaMapper.toDto(registroActividadRutina);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegistroActividadRutinaMockMvc.perform(post("/api/registro-actividad-rutinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(registroActividadRutinaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RegistroActividadRutina in the database
        List<RegistroActividadRutina> registroActividadRutinaList = registroActividadRutinaRepository.findAll();
        assertThat(registroActividadRutinaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkFechaAsistenciaIsRequired() throws Exception {
        int databaseSizeBeforeTest = registroActividadRutinaRepository.findAll().size();
        // set the field null
        registroActividadRutina.setFechaAsistencia(null);

        // Create the RegistroActividadRutina, which fails.
        RegistroActividadRutinaDTO registroActividadRutinaDTO = registroActividadRutinaMapper.toDto(registroActividadRutina);

        restRegistroActividadRutinaMockMvc.perform(post("/api/registro-actividad-rutinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(registroActividadRutinaDTO)))
            .andExpect(status().isBadRequest());

        List<RegistroActividadRutina> registroActividadRutinaList = registroActividadRutinaRepository.findAll();
        assertThat(registroActividadRutinaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDiaAsistenciaIsRequired() throws Exception {
        int databaseSizeBeforeTest = registroActividadRutinaRepository.findAll().size();
        // set the field null
        registroActividadRutina.setDiaAsistencia(null);

        // Create the RegistroActividadRutina, which fails.
        RegistroActividadRutinaDTO registroActividadRutinaDTO = registroActividadRutinaMapper.toDto(registroActividadRutina);

        restRegistroActividadRutinaMockMvc.perform(post("/api/registro-actividad-rutinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(registroActividadRutinaDTO)))
            .andExpect(status().isBadRequest());

        List<RegistroActividadRutina> registroActividadRutinaList = registroActividadRutinaRepository.findAll();
        assertThat(registroActividadRutinaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEstadoRegistroActividadIsRequired() throws Exception {
        int databaseSizeBeforeTest = registroActividadRutinaRepository.findAll().size();
        // set the field null
        registroActividadRutina.setEstadoRegistroActividad(null);

        // Create the RegistroActividadRutina, which fails.
        RegistroActividadRutinaDTO registroActividadRutinaDTO = registroActividadRutinaMapper.toDto(registroActividadRutina);

        restRegistroActividadRutinaMockMvc.perform(post("/api/registro-actividad-rutinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(registroActividadRutinaDTO)))
            .andExpect(status().isBadRequest());

        List<RegistroActividadRutina> registroActividadRutinaList = registroActividadRutinaRepository.findAll();
        assertThat(registroActividadRutinaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRegistroActividadRutinas() throws Exception {
        // Initialize the database
        registroActividadRutinaRepository.saveAndFlush(registroActividadRutina);

        // Get all the registroActividadRutinaList
        restRegistroActividadRutinaMockMvc.perform(get("/api/registro-actividad-rutinas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(registroActividadRutina.getId().intValue())))
            .andExpect(jsonPath("$.[*].fechaAsistencia").value(hasItem(sameInstant(DEFAULT_FECHA_ASISTENCIA))))
            .andExpect(jsonPath("$.[*].diaAsistencia").value(hasItem(DEFAULT_DIA_ASISTENCIA)))
            .andExpect(jsonPath("$.[*].estadoRegistroActividad").value(hasItem(DEFAULT_ESTADO_REGISTRO_ACTIVIDAD.booleanValue())));
    }

    @Test
    @Transactional
    public void getRegistroActividadRutina() throws Exception {
        // Initialize the database
        registroActividadRutinaRepository.saveAndFlush(registroActividadRutina);

        // Get the registroActividadRutina
        restRegistroActividadRutinaMockMvc.perform(get("/api/registro-actividad-rutinas/{id}", registroActividadRutina.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(registroActividadRutina.getId().intValue()))
            .andExpect(jsonPath("$.fechaAsistencia").value(sameInstant(DEFAULT_FECHA_ASISTENCIA)))
            .andExpect(jsonPath("$.diaAsistencia").value(DEFAULT_DIA_ASISTENCIA))
            .andExpect(jsonPath("$.estadoRegistroActividad").value(DEFAULT_ESTADO_REGISTRO_ACTIVIDAD.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingRegistroActividadRutina() throws Exception {
        // Get the registroActividadRutina
        restRegistroActividadRutinaMockMvc.perform(get("/api/registro-actividad-rutinas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRegistroActividadRutina() throws Exception {
        // Initialize the database
        registroActividadRutinaRepository.saveAndFlush(registroActividadRutina);
        int databaseSizeBeforeUpdate = registroActividadRutinaRepository.findAll().size();

        // Update the registroActividadRutina
        RegistroActividadRutina updatedRegistroActividadRutina = registroActividadRutinaRepository.findOne(registroActividadRutina.getId());
        // Disconnect from session so that the updates on updatedRegistroActividadRutina are not directly saved in db
        em.detach(updatedRegistroActividadRutina);
        updatedRegistroActividadRutina
            .fechaAsistencia(UPDATED_FECHA_ASISTENCIA)
            .diaAsistencia(UPDATED_DIA_ASISTENCIA)
            .estadoRegistroActividad(UPDATED_ESTADO_REGISTRO_ACTIVIDAD);
        RegistroActividadRutinaDTO registroActividadRutinaDTO = registroActividadRutinaMapper.toDto(updatedRegistroActividadRutina);

        restRegistroActividadRutinaMockMvc.perform(put("/api/registro-actividad-rutinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(registroActividadRutinaDTO)))
            .andExpect(status().isOk());

        // Validate the RegistroActividadRutina in the database
        List<RegistroActividadRutina> registroActividadRutinaList = registroActividadRutinaRepository.findAll();
        assertThat(registroActividadRutinaList).hasSize(databaseSizeBeforeUpdate);
        RegistroActividadRutina testRegistroActividadRutina = registroActividadRutinaList.get(registroActividadRutinaList.size() - 1);
        assertThat(testRegistroActividadRutina.getFechaAsistencia()).isEqualTo(UPDATED_FECHA_ASISTENCIA);
        assertThat(testRegistroActividadRutina.getDiaAsistencia()).isEqualTo(UPDATED_DIA_ASISTENCIA);
        assertThat(testRegistroActividadRutina.isEstadoRegistroActividad()).isEqualTo(UPDATED_ESTADO_REGISTRO_ACTIVIDAD);
    }

    @Test
    @Transactional
    public void updateNonExistingRegistroActividadRutina() throws Exception {
        int databaseSizeBeforeUpdate = registroActividadRutinaRepository.findAll().size();

        // Create the RegistroActividadRutina
        RegistroActividadRutinaDTO registroActividadRutinaDTO = registroActividadRutinaMapper.toDto(registroActividadRutina);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restRegistroActividadRutinaMockMvc.perform(put("/api/registro-actividad-rutinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(registroActividadRutinaDTO)))
            .andExpect(status().isCreated());

        // Validate the RegistroActividadRutina in the database
        List<RegistroActividadRutina> registroActividadRutinaList = registroActividadRutinaRepository.findAll();
        assertThat(registroActividadRutinaList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteRegistroActividadRutina() throws Exception {
        // Initialize the database
        registroActividadRutinaRepository.saveAndFlush(registroActividadRutina);
        int databaseSizeBeforeDelete = registroActividadRutinaRepository.findAll().size();

        // Get the registroActividadRutina
        restRegistroActividadRutinaMockMvc.perform(delete("/api/registro-actividad-rutinas/{id}", registroActividadRutina.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<RegistroActividadRutina> registroActividadRutinaList = registroActividadRutinaRepository.findAll();
        assertThat(registroActividadRutinaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegistroActividadRutina.class);
        RegistroActividadRutina registroActividadRutina1 = new RegistroActividadRutina();
        registroActividadRutina1.setId(1L);
        RegistroActividadRutina registroActividadRutina2 = new RegistroActividadRutina();
        registroActividadRutina2.setId(registroActividadRutina1.getId());
        assertThat(registroActividadRutina1).isEqualTo(registroActividadRutina2);
        registroActividadRutina2.setId(2L);
        assertThat(registroActividadRutina1).isNotEqualTo(registroActividadRutina2);
        registroActividadRutina1.setId(null);
        assertThat(registroActividadRutina1).isNotEqualTo(registroActividadRutina2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegistroActividadRutinaDTO.class);
        RegistroActividadRutinaDTO registroActividadRutinaDTO1 = new RegistroActividadRutinaDTO();
        registroActividadRutinaDTO1.setId(1L);
        RegistroActividadRutinaDTO registroActividadRutinaDTO2 = new RegistroActividadRutinaDTO();
        assertThat(registroActividadRutinaDTO1).isNotEqualTo(registroActividadRutinaDTO2);
        registroActividadRutinaDTO2.setId(registroActividadRutinaDTO1.getId());
        assertThat(registroActividadRutinaDTO1).isEqualTo(registroActividadRutinaDTO2);
        registroActividadRutinaDTO2.setId(2L);
        assertThat(registroActividadRutinaDTO1).isNotEqualTo(registroActividadRutinaDTO2);
        registroActividadRutinaDTO1.setId(null);
        assertThat(registroActividadRutinaDTO1).isNotEqualTo(registroActividadRutinaDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(registroActividadRutinaMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(registroActividadRutinaMapper.fromId(null)).isNull();
    }
}
