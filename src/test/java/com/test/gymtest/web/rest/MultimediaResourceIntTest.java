package com.test.gymtest.web.rest;

import com.test.gymtest.TestGymApp;

import com.test.gymtest.domain.Multimedia;
import com.test.gymtest.repository.MultimediaRepository;
import com.test.gymtest.service.MultimediaService;
import com.test.gymtest.service.dto.MultimediaDTO;
import com.test.gymtest.service.mapper.MultimediaMapper;
import com.test.gymtest.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.test.gymtest.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MultimediaResource REST controller.
 *
 * @see MultimediaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestGymApp.class)
public class MultimediaResourceIntTest {

    private static final String DEFAULT_NOMBRE_MULTIMEDIA = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE_MULTIMEDIA = "BBBBBBBBBB";

    private static final Integer DEFAULT_PROPOSITO_MULTIMEDIA = 1;
    private static final Integer UPDATED_PROPOSITO_MULTIMEDIA = 2;

    private static final String DEFAULT_ENCODE = "AAAAAAAAAA";
    private static final String UPDATED_ENCODE = "BBBBBBBBBB";

    @Autowired
    private MultimediaRepository multimediaRepository;

    @Autowired
    private MultimediaMapper multimediaMapper;

    @Autowired
    private MultimediaService multimediaService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restMultimediaMockMvc;

    private Multimedia multimedia;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MultimediaResource multimediaResource = new MultimediaResource(multimediaService);
        this.restMultimediaMockMvc = MockMvcBuilders.standaloneSetup(multimediaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Multimedia createEntity(EntityManager em) {
        Multimedia multimedia = new Multimedia()
            .nombreMultimedia(DEFAULT_NOMBRE_MULTIMEDIA)
            .propositoMultimedia(DEFAULT_PROPOSITO_MULTIMEDIA)
            .encode(DEFAULT_ENCODE);
        return multimedia;
    }

    @Before
    public void initTest() {
        multimedia = createEntity(em);
    }

    @Test
    @Transactional
    public void createMultimedia() throws Exception {
        int databaseSizeBeforeCreate = multimediaRepository.findAll().size();

        // Create the Multimedia
        MultimediaDTO multimediaDTO = multimediaMapper.toDto(multimedia);
        restMultimediaMockMvc.perform(post("/api/multimedias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(multimediaDTO)))
            .andExpect(status().isCreated());

        // Validate the Multimedia in the database
        List<Multimedia> multimediaList = multimediaRepository.findAll();
        assertThat(multimediaList).hasSize(databaseSizeBeforeCreate + 1);
        Multimedia testMultimedia = multimediaList.get(multimediaList.size() - 1);
        assertThat(testMultimedia.getNombreMultimedia()).isEqualTo(DEFAULT_NOMBRE_MULTIMEDIA);
        assertThat(testMultimedia.getPropositoMultimedia()).isEqualTo(DEFAULT_PROPOSITO_MULTIMEDIA);
        assertThat(testMultimedia.getEncode()).isEqualTo(DEFAULT_ENCODE);
    }

    @Test
    @Transactional
    public void createMultimediaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = multimediaRepository.findAll().size();

        // Create the Multimedia with an existing ID
        multimedia.setId(1L);
        MultimediaDTO multimediaDTO = multimediaMapper.toDto(multimedia);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMultimediaMockMvc.perform(post("/api/multimedias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(multimediaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Multimedia in the database
        List<Multimedia> multimediaList = multimediaRepository.findAll();
        assertThat(multimediaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNombreMultimediaIsRequired() throws Exception {
        int databaseSizeBeforeTest = multimediaRepository.findAll().size();
        // set the field null
        multimedia.setNombreMultimedia(null);

        // Create the Multimedia, which fails.
        MultimediaDTO multimediaDTO = multimediaMapper.toDto(multimedia);

        restMultimediaMockMvc.perform(post("/api/multimedias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(multimediaDTO)))
            .andExpect(status().isBadRequest());

        List<Multimedia> multimediaList = multimediaRepository.findAll();
        assertThat(multimediaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEncodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = multimediaRepository.findAll().size();
        // set the field null
        multimedia.setEncode(null);

        // Create the Multimedia, which fails.
        MultimediaDTO multimediaDTO = multimediaMapper.toDto(multimedia);

        restMultimediaMockMvc.perform(post("/api/multimedias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(multimediaDTO)))
            .andExpect(status().isBadRequest());

        List<Multimedia> multimediaList = multimediaRepository.findAll();
        assertThat(multimediaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMultimedias() throws Exception {
        // Initialize the database
        multimediaRepository.saveAndFlush(multimedia);

        // Get all the multimediaList
        restMultimediaMockMvc.perform(get("/api/multimedias?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(multimedia.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombreMultimedia").value(hasItem(DEFAULT_NOMBRE_MULTIMEDIA.toString())))
            .andExpect(jsonPath("$.[*].propositoMultimedia").value(hasItem(DEFAULT_PROPOSITO_MULTIMEDIA)))
            .andExpect(jsonPath("$.[*].encode").value(hasItem(DEFAULT_ENCODE.toString())));
    }

    @Test
    @Transactional
    public void getMultimedia() throws Exception {
        // Initialize the database
        multimediaRepository.saveAndFlush(multimedia);

        // Get the multimedia
        restMultimediaMockMvc.perform(get("/api/multimedias/{id}", multimedia.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(multimedia.getId().intValue()))
            .andExpect(jsonPath("$.nombreMultimedia").value(DEFAULT_NOMBRE_MULTIMEDIA.toString()))
            .andExpect(jsonPath("$.propositoMultimedia").value(DEFAULT_PROPOSITO_MULTIMEDIA))
            .andExpect(jsonPath("$.encode").value(DEFAULT_ENCODE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMultimedia() throws Exception {
        // Get the multimedia
        restMultimediaMockMvc.perform(get("/api/multimedias/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMultimedia() throws Exception {
        // Initialize the database
        multimediaRepository.saveAndFlush(multimedia);
        int databaseSizeBeforeUpdate = multimediaRepository.findAll().size();

        // Update the multimedia
        Multimedia updatedMultimedia = multimediaRepository.findOne(multimedia.getId());
        // Disconnect from session so that the updates on updatedMultimedia are not directly saved in db
        em.detach(updatedMultimedia);
        updatedMultimedia
            .nombreMultimedia(UPDATED_NOMBRE_MULTIMEDIA)
            .propositoMultimedia(UPDATED_PROPOSITO_MULTIMEDIA)
            .encode(UPDATED_ENCODE);
        MultimediaDTO multimediaDTO = multimediaMapper.toDto(updatedMultimedia);

        restMultimediaMockMvc.perform(put("/api/multimedias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(multimediaDTO)))
            .andExpect(status().isOk());

        // Validate the Multimedia in the database
        List<Multimedia> multimediaList = multimediaRepository.findAll();
        assertThat(multimediaList).hasSize(databaseSizeBeforeUpdate);
        Multimedia testMultimedia = multimediaList.get(multimediaList.size() - 1);
        assertThat(testMultimedia.getNombreMultimedia()).isEqualTo(UPDATED_NOMBRE_MULTIMEDIA);
        assertThat(testMultimedia.getPropositoMultimedia()).isEqualTo(UPDATED_PROPOSITO_MULTIMEDIA);
        assertThat(testMultimedia.getEncode()).isEqualTo(UPDATED_ENCODE);
    }

    @Test
    @Transactional
    public void updateNonExistingMultimedia() throws Exception {
        int databaseSizeBeforeUpdate = multimediaRepository.findAll().size();

        // Create the Multimedia
        MultimediaDTO multimediaDTO = multimediaMapper.toDto(multimedia);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restMultimediaMockMvc.perform(put("/api/multimedias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(multimediaDTO)))
            .andExpect(status().isCreated());

        // Validate the Multimedia in the database
        List<Multimedia> multimediaList = multimediaRepository.findAll();
        assertThat(multimediaList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteMultimedia() throws Exception {
        // Initialize the database
        multimediaRepository.saveAndFlush(multimedia);
        int databaseSizeBeforeDelete = multimediaRepository.findAll().size();

        // Get the multimedia
        restMultimediaMockMvc.perform(delete("/api/multimedias/{id}", multimedia.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Multimedia> multimediaList = multimediaRepository.findAll();
        assertThat(multimediaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Multimedia.class);
        Multimedia multimedia1 = new Multimedia();
        multimedia1.setId(1L);
        Multimedia multimedia2 = new Multimedia();
        multimedia2.setId(multimedia1.getId());
        assertThat(multimedia1).isEqualTo(multimedia2);
        multimedia2.setId(2L);
        assertThat(multimedia1).isNotEqualTo(multimedia2);
        multimedia1.setId(null);
        assertThat(multimedia1).isNotEqualTo(multimedia2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MultimediaDTO.class);
        MultimediaDTO multimediaDTO1 = new MultimediaDTO();
        multimediaDTO1.setId(1L);
        MultimediaDTO multimediaDTO2 = new MultimediaDTO();
        assertThat(multimediaDTO1).isNotEqualTo(multimediaDTO2);
        multimediaDTO2.setId(multimediaDTO1.getId());
        assertThat(multimediaDTO1).isEqualTo(multimediaDTO2);
        multimediaDTO2.setId(2L);
        assertThat(multimediaDTO1).isNotEqualTo(multimediaDTO2);
        multimediaDTO1.setId(null);
        assertThat(multimediaDTO1).isNotEqualTo(multimediaDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(multimediaMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(multimediaMapper.fromId(null)).isNull();
    }
}
