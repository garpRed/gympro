package com.test.gymtest.web.rest;

import com.test.gymtest.TestGymApp;

import com.test.gymtest.domain.SubRutina;
import com.test.gymtest.repository.SubRutinaRepository;
import com.test.gymtest.service.SubRutinaService;
import com.test.gymtest.service.dto.SubRutinaDTO;
import com.test.gymtest.service.mapper.SubRutinaMapper;
import com.test.gymtest.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.test.gymtest.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SubRutinaResource REST controller.
 *
 * @see SubRutinaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestGymApp.class)
public class SubRutinaResourceIntTest {

    private static final String DEFAULT_NOMBRE_SUB_RUTINA = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE_SUB_RUTINA = "BBBBBBBBBB";

    private static final Integer DEFAULT_CANT_SERIES = 1;
    private static final Integer UPDATED_CANT_SERIES = 2;

    private static final Integer DEFAULT_CANT_REPETICIONES = 1;
    private static final Integer UPDATED_CANT_REPETICIONES = 2;

    private static final String DEFAULT_COMENTARIO_SUB_RUTINA = "AAAAAAAAAA";
    private static final String UPDATED_COMENTARIO_SUB_RUTINA = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ESTADO_SUBRUTINA = false;
    private static final Boolean UPDATED_ESTADO_SUBRUTINA = true;

    @Autowired
    private SubRutinaRepository subRutinaRepository;

    @Autowired
    private SubRutinaMapper subRutinaMapper;

    @Autowired
    private SubRutinaService subRutinaService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSubRutinaMockMvc;

    private SubRutina subRutina;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SubRutinaResource subRutinaResource = new SubRutinaResource(subRutinaService);
        this.restSubRutinaMockMvc = MockMvcBuilders.standaloneSetup(subRutinaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SubRutina createEntity(EntityManager em) {
        SubRutina subRutina = new SubRutina()
            .nombreSubRutina(DEFAULT_NOMBRE_SUB_RUTINA)
            .cantSeries(DEFAULT_CANT_SERIES)
            .cantRepeticiones(DEFAULT_CANT_REPETICIONES)
            .comentarioSubRutina(DEFAULT_COMENTARIO_SUB_RUTINA)
            .estadoSubrutina(DEFAULT_ESTADO_SUBRUTINA);
        return subRutina;
    }

    @Before
    public void initTest() {
        subRutina = createEntity(em);
    }

    @Test
    @Transactional
    public void createSubRutina() throws Exception {
        int databaseSizeBeforeCreate = subRutinaRepository.findAll().size();

        // Create the SubRutina
        SubRutinaDTO subRutinaDTO = subRutinaMapper.toDto(subRutina);
        restSubRutinaMockMvc.perform(post("/api/sub-rutinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subRutinaDTO)))
            .andExpect(status().isCreated());

        // Validate the SubRutina in the database
        List<SubRutina> subRutinaList = subRutinaRepository.findAll();
        assertThat(subRutinaList).hasSize(databaseSizeBeforeCreate + 1);
        SubRutina testSubRutina = subRutinaList.get(subRutinaList.size() - 1);
        assertThat(testSubRutina.getNombreSubRutina()).isEqualTo(DEFAULT_NOMBRE_SUB_RUTINA);
        assertThat(testSubRutina.getCantSeries()).isEqualTo(DEFAULT_CANT_SERIES);
        assertThat(testSubRutina.getCantRepeticiones()).isEqualTo(DEFAULT_CANT_REPETICIONES);
        assertThat(testSubRutina.getComentarioSubRutina()).isEqualTo(DEFAULT_COMENTARIO_SUB_RUTINA);
        assertThat(testSubRutina.isEstadoSubrutina()).isEqualTo(DEFAULT_ESTADO_SUBRUTINA);
    }

    @Test
    @Transactional
    public void createSubRutinaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = subRutinaRepository.findAll().size();

        // Create the SubRutina with an existing ID
        subRutina.setId(1L);
        SubRutinaDTO subRutinaDTO = subRutinaMapper.toDto(subRutina);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSubRutinaMockMvc.perform(post("/api/sub-rutinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subRutinaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SubRutina in the database
        List<SubRutina> subRutinaList = subRutinaRepository.findAll();
        assertThat(subRutinaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNombreSubRutinaIsRequired() throws Exception {
        int databaseSizeBeforeTest = subRutinaRepository.findAll().size();
        // set the field null
        subRutina.setNombreSubRutina(null);

        // Create the SubRutina, which fails.
        SubRutinaDTO subRutinaDTO = subRutinaMapper.toDto(subRutina);

        restSubRutinaMockMvc.perform(post("/api/sub-rutinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subRutinaDTO)))
            .andExpect(status().isBadRequest());

        List<SubRutina> subRutinaList = subRutinaRepository.findAll();
        assertThat(subRutinaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEstadoSubrutinaIsRequired() throws Exception {
        int databaseSizeBeforeTest = subRutinaRepository.findAll().size();
        // set the field null
        subRutina.setEstadoSubrutina(null);

        // Create the SubRutina, which fails.
        SubRutinaDTO subRutinaDTO = subRutinaMapper.toDto(subRutina);

        restSubRutinaMockMvc.perform(post("/api/sub-rutinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subRutinaDTO)))
            .andExpect(status().isBadRequest());

        List<SubRutina> subRutinaList = subRutinaRepository.findAll();
        assertThat(subRutinaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSubRutinas() throws Exception {
        // Initialize the database
        subRutinaRepository.saveAndFlush(subRutina);

        // Get all the subRutinaList
        restSubRutinaMockMvc.perform(get("/api/sub-rutinas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(subRutina.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombreSubRutina").value(hasItem(DEFAULT_NOMBRE_SUB_RUTINA.toString())))
            .andExpect(jsonPath("$.[*].cantSeries").value(hasItem(DEFAULT_CANT_SERIES)))
            .andExpect(jsonPath("$.[*].cantRepeticiones").value(hasItem(DEFAULT_CANT_REPETICIONES)))
            .andExpect(jsonPath("$.[*].comentarioSubRutina").value(hasItem(DEFAULT_COMENTARIO_SUB_RUTINA.toString())))
            .andExpect(jsonPath("$.[*].estadoSubrutina").value(hasItem(DEFAULT_ESTADO_SUBRUTINA.booleanValue())));
    }

    @Test
    @Transactional
    public void getSubRutina() throws Exception {
        // Initialize the database
        subRutinaRepository.saveAndFlush(subRutina);

        // Get the subRutina
        restSubRutinaMockMvc.perform(get("/api/sub-rutinas/{id}", subRutina.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(subRutina.getId().intValue()))
            .andExpect(jsonPath("$.nombreSubRutina").value(DEFAULT_NOMBRE_SUB_RUTINA.toString()))
            .andExpect(jsonPath("$.cantSeries").value(DEFAULT_CANT_SERIES))
            .andExpect(jsonPath("$.cantRepeticiones").value(DEFAULT_CANT_REPETICIONES))
            .andExpect(jsonPath("$.comentarioSubRutina").value(DEFAULT_COMENTARIO_SUB_RUTINA.toString()))
            .andExpect(jsonPath("$.estadoSubrutina").value(DEFAULT_ESTADO_SUBRUTINA.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingSubRutina() throws Exception {
        // Get the subRutina
        restSubRutinaMockMvc.perform(get("/api/sub-rutinas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSubRutina() throws Exception {
        // Initialize the database
        subRutinaRepository.saveAndFlush(subRutina);
        int databaseSizeBeforeUpdate = subRutinaRepository.findAll().size();

        // Update the subRutina
        SubRutina updatedSubRutina = subRutinaRepository.findOne(subRutina.getId());
        // Disconnect from session so that the updates on updatedSubRutina are not directly saved in db
        em.detach(updatedSubRutina);
        updatedSubRutina
            .nombreSubRutina(UPDATED_NOMBRE_SUB_RUTINA)
            .cantSeries(UPDATED_CANT_SERIES)
            .cantRepeticiones(UPDATED_CANT_REPETICIONES)
            .comentarioSubRutina(UPDATED_COMENTARIO_SUB_RUTINA)
            .estadoSubrutina(UPDATED_ESTADO_SUBRUTINA);
        SubRutinaDTO subRutinaDTO = subRutinaMapper.toDto(updatedSubRutina);

        restSubRutinaMockMvc.perform(put("/api/sub-rutinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subRutinaDTO)))
            .andExpect(status().isOk());

        // Validate the SubRutina in the database
        List<SubRutina> subRutinaList = subRutinaRepository.findAll();
        assertThat(subRutinaList).hasSize(databaseSizeBeforeUpdate);
        SubRutina testSubRutina = subRutinaList.get(subRutinaList.size() - 1);
        assertThat(testSubRutina.getNombreSubRutina()).isEqualTo(UPDATED_NOMBRE_SUB_RUTINA);
        assertThat(testSubRutina.getCantSeries()).isEqualTo(UPDATED_CANT_SERIES);
        assertThat(testSubRutina.getCantRepeticiones()).isEqualTo(UPDATED_CANT_REPETICIONES);
        assertThat(testSubRutina.getComentarioSubRutina()).isEqualTo(UPDATED_COMENTARIO_SUB_RUTINA);
        assertThat(testSubRutina.isEstadoSubrutina()).isEqualTo(UPDATED_ESTADO_SUBRUTINA);
    }

    @Test
    @Transactional
    public void updateNonExistingSubRutina() throws Exception {
        int databaseSizeBeforeUpdate = subRutinaRepository.findAll().size();

        // Create the SubRutina
        SubRutinaDTO subRutinaDTO = subRutinaMapper.toDto(subRutina);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSubRutinaMockMvc.perform(put("/api/sub-rutinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subRutinaDTO)))
            .andExpect(status().isCreated());

        // Validate the SubRutina in the database
        List<SubRutina> subRutinaList = subRutinaRepository.findAll();
        assertThat(subRutinaList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSubRutina() throws Exception {
        // Initialize the database
        subRutinaRepository.saveAndFlush(subRutina);
        int databaseSizeBeforeDelete = subRutinaRepository.findAll().size();

        // Get the subRutina
        restSubRutinaMockMvc.perform(delete("/api/sub-rutinas/{id}", subRutina.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SubRutina> subRutinaList = subRutinaRepository.findAll();
        assertThat(subRutinaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubRutina.class);
        SubRutina subRutina1 = new SubRutina();
        subRutina1.setId(1L);
        SubRutina subRutina2 = new SubRutina();
        subRutina2.setId(subRutina1.getId());
        assertThat(subRutina1).isEqualTo(subRutina2);
        subRutina2.setId(2L);
        assertThat(subRutina1).isNotEqualTo(subRutina2);
        subRutina1.setId(null);
        assertThat(subRutina1).isNotEqualTo(subRutina2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubRutinaDTO.class);
        SubRutinaDTO subRutinaDTO1 = new SubRutinaDTO();
        subRutinaDTO1.setId(1L);
        SubRutinaDTO subRutinaDTO2 = new SubRutinaDTO();
        assertThat(subRutinaDTO1).isNotEqualTo(subRutinaDTO2);
        subRutinaDTO2.setId(subRutinaDTO1.getId());
        assertThat(subRutinaDTO1).isEqualTo(subRutinaDTO2);
        subRutinaDTO2.setId(2L);
        assertThat(subRutinaDTO1).isNotEqualTo(subRutinaDTO2);
        subRutinaDTO1.setId(null);
        assertThat(subRutinaDTO1).isNotEqualTo(subRutinaDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(subRutinaMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(subRutinaMapper.fromId(null)).isNull();
    }
}
