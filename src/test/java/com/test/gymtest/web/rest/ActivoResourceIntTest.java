package com.test.gymtest.web.rest;

import com.test.gymtest.TestGymApp;

import com.test.gymtest.domain.Activo;
import com.test.gymtest.repository.ActivoRepository;
import com.test.gymtest.service.ActivoService;
import com.test.gymtest.service.dto.ActivoDTO;
import com.test.gymtest.service.mapper.ActivoMapper;
import com.test.gymtest.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.test.gymtest.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ActivoResource REST controller.
 *
 * @see ActivoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestGymApp.class)
public class ActivoResourceIntTest {

    private static final String DEFAULT_NOMBRE_ACTIVO = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE_ACTIVO = "BBBBBBBBBB";

    private static final Integer DEFAULT_CANT_ACTIVOS = 500;
    private static final Integer UPDATED_CANT_ACTIVOS = 499;

    private static final Boolean DEFAULT_ESTADO_ACTIVO = false;
    private static final Boolean UPDATED_ESTADO_ACTIVO = true;

    @Autowired
    private ActivoRepository activoRepository;

    @Autowired
    private ActivoMapper activoMapper;

    @Autowired
    private ActivoService activoService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restActivoMockMvc;

    private Activo activo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ActivoResource activoResource = new ActivoResource(activoService);
        this.restActivoMockMvc = MockMvcBuilders.standaloneSetup(activoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Activo createEntity(EntityManager em) {
        Activo activo = new Activo()
            .nombreActivo(DEFAULT_NOMBRE_ACTIVO)
            .cantActivos(DEFAULT_CANT_ACTIVOS)
            .estadoActivo(DEFAULT_ESTADO_ACTIVO);
        return activo;
    }

    @Before
    public void initTest() {
        activo = createEntity(em);
    }

    @Test
    @Transactional
    public void createActivo() throws Exception {
        int databaseSizeBeforeCreate = activoRepository.findAll().size();

        // Create the Activo
        ActivoDTO activoDTO = activoMapper.toDto(activo);
        restActivoMockMvc.perform(post("/api/activos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(activoDTO)))
            .andExpect(status().isCreated());

        // Validate the Activo in the database
        List<Activo> activoList = activoRepository.findAll();
        assertThat(activoList).hasSize(databaseSizeBeforeCreate + 1);
        Activo testActivo = activoList.get(activoList.size() - 1);
        assertThat(testActivo.getNombreActivo()).isEqualTo(DEFAULT_NOMBRE_ACTIVO);
        assertThat(testActivo.getCantActivos()).isEqualTo(DEFAULT_CANT_ACTIVOS);
        assertThat(testActivo.isEstadoActivo()).isEqualTo(DEFAULT_ESTADO_ACTIVO);
    }

    @Test
    @Transactional
    public void createActivoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = activoRepository.findAll().size();

        // Create the Activo with an existing ID
        activo.setId(1L);
        ActivoDTO activoDTO = activoMapper.toDto(activo);

        // An entity with an existing ID cannot be created, so this API call must fail
        restActivoMockMvc.perform(post("/api/activos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(activoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Activo in the database
        List<Activo> activoList = activoRepository.findAll();
        assertThat(activoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNombreActivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = activoRepository.findAll().size();
        // set the field null
        activo.setNombreActivo(null);

        // Create the Activo, which fails.
        ActivoDTO activoDTO = activoMapper.toDto(activo);

        restActivoMockMvc.perform(post("/api/activos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(activoDTO)))
            .andExpect(status().isBadRequest());

        List<Activo> activoList = activoRepository.findAll();
        assertThat(activoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCantActivosIsRequired() throws Exception {
        int databaseSizeBeforeTest = activoRepository.findAll().size();
        // set the field null
        activo.setCantActivos(null);

        // Create the Activo, which fails.
        ActivoDTO activoDTO = activoMapper.toDto(activo);

        restActivoMockMvc.perform(post("/api/activos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(activoDTO)))
            .andExpect(status().isBadRequest());

        List<Activo> activoList = activoRepository.findAll();
        assertThat(activoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEstadoActivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = activoRepository.findAll().size();
        // set the field null
        activo.setEstadoActivo(null);

        // Create the Activo, which fails.
        ActivoDTO activoDTO = activoMapper.toDto(activo);

        restActivoMockMvc.perform(post("/api/activos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(activoDTO)))
            .andExpect(status().isBadRequest());

        List<Activo> activoList = activoRepository.findAll();
        assertThat(activoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllActivos() throws Exception {
        // Initialize the database
        activoRepository.saveAndFlush(activo);

        // Get all the activoList
        restActivoMockMvc.perform(get("/api/activos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(activo.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombreActivo").value(hasItem(DEFAULT_NOMBRE_ACTIVO.toString())))
            .andExpect(jsonPath("$.[*].cantActivos").value(hasItem(DEFAULT_CANT_ACTIVOS)))
            .andExpect(jsonPath("$.[*].estadoActivo").value(hasItem(DEFAULT_ESTADO_ACTIVO.booleanValue())));
    }

    @Test
    @Transactional
    public void getActivo() throws Exception {
        // Initialize the database
        activoRepository.saveAndFlush(activo);

        // Get the activo
        restActivoMockMvc.perform(get("/api/activos/{id}", activo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(activo.getId().intValue()))
            .andExpect(jsonPath("$.nombreActivo").value(DEFAULT_NOMBRE_ACTIVO.toString()))
            .andExpect(jsonPath("$.cantActivos").value(DEFAULT_CANT_ACTIVOS))
            .andExpect(jsonPath("$.estadoActivo").value(DEFAULT_ESTADO_ACTIVO.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingActivo() throws Exception {
        // Get the activo
        restActivoMockMvc.perform(get("/api/activos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateActivo() throws Exception {
        // Initialize the database
        activoRepository.saveAndFlush(activo);
        int databaseSizeBeforeUpdate = activoRepository.findAll().size();

        // Update the activo
        Activo updatedActivo = activoRepository.findOne(activo.getId());
        // Disconnect from session so that the updates on updatedActivo are not directly saved in db
        em.detach(updatedActivo);
        updatedActivo
            .nombreActivo(UPDATED_NOMBRE_ACTIVO)
            .cantActivos(UPDATED_CANT_ACTIVOS)
            .estadoActivo(UPDATED_ESTADO_ACTIVO);
        ActivoDTO activoDTO = activoMapper.toDto(updatedActivo);

        restActivoMockMvc.perform(put("/api/activos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(activoDTO)))
            .andExpect(status().isOk());

        // Validate the Activo in the database
        List<Activo> activoList = activoRepository.findAll();
        assertThat(activoList).hasSize(databaseSizeBeforeUpdate);
        Activo testActivo = activoList.get(activoList.size() - 1);
        assertThat(testActivo.getNombreActivo()).isEqualTo(UPDATED_NOMBRE_ACTIVO);
        assertThat(testActivo.getCantActivos()).isEqualTo(UPDATED_CANT_ACTIVOS);
        assertThat(testActivo.isEstadoActivo()).isEqualTo(UPDATED_ESTADO_ACTIVO);
    }

    @Test
    @Transactional
    public void updateNonExistingActivo() throws Exception {
        int databaseSizeBeforeUpdate = activoRepository.findAll().size();

        // Create the Activo
        ActivoDTO activoDTO = activoMapper.toDto(activo);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restActivoMockMvc.perform(put("/api/activos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(activoDTO)))
            .andExpect(status().isCreated());

        // Validate the Activo in the database
        List<Activo> activoList = activoRepository.findAll();
        assertThat(activoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteActivo() throws Exception {
        // Initialize the database
        activoRepository.saveAndFlush(activo);
        int databaseSizeBeforeDelete = activoRepository.findAll().size();

        // Get the activo
        restActivoMockMvc.perform(delete("/api/activos/{id}", activo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Activo> activoList = activoRepository.findAll();
        assertThat(activoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Activo.class);
        Activo activo1 = new Activo();
        activo1.setId(1L);
        Activo activo2 = new Activo();
        activo2.setId(activo1.getId());
        assertThat(activo1).isEqualTo(activo2);
        activo2.setId(2L);
        assertThat(activo1).isNotEqualTo(activo2);
        activo1.setId(null);
        assertThat(activo1).isNotEqualTo(activo2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ActivoDTO.class);
        ActivoDTO activoDTO1 = new ActivoDTO();
        activoDTO1.setId(1L);
        ActivoDTO activoDTO2 = new ActivoDTO();
        assertThat(activoDTO1).isNotEqualTo(activoDTO2);
        activoDTO2.setId(activoDTO1.getId());
        assertThat(activoDTO1).isEqualTo(activoDTO2);
        activoDTO2.setId(2L);
        assertThat(activoDTO1).isNotEqualTo(activoDTO2);
        activoDTO1.setId(null);
        assertThat(activoDTO1).isNotEqualTo(activoDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(activoMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(activoMapper.fromId(null)).isNull();
    }
}
