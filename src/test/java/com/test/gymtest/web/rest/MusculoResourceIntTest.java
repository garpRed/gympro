package com.test.gymtest.web.rest;

import com.test.gymtest.TestGymApp;

import com.test.gymtest.domain.Musculo;
import com.test.gymtest.repository.MusculoRepository;
import com.test.gymtest.service.MusculoService;
import com.test.gymtest.service.dto.MusculoDTO;
import com.test.gymtest.service.mapper.MusculoMapper;
import com.test.gymtest.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.test.gymtest.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the MusculoResource REST controller.
 *
 * @see MusculoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestGymApp.class)
public class MusculoResourceIntTest {

    private static final String DEFAULT_NOMBRE_MUSCULO = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE_MUSCULO = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ESTADO_MUSCULO = false;
    private static final Boolean UPDATED_ESTADO_MUSCULO = true;

    @Autowired
    private MusculoRepository musculoRepository;

    @Autowired
    private MusculoMapper musculoMapper;

    @Autowired
    private MusculoService musculoService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restMusculoMockMvc;

    private Musculo musculo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MusculoResource musculoResource = new MusculoResource(musculoService);
        this.restMusculoMockMvc = MockMvcBuilders.standaloneSetup(musculoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Musculo createEntity(EntityManager em) {
        Musculo musculo = new Musculo()
            .nombreMusculo(DEFAULT_NOMBRE_MUSCULO)
            .estadoMusculo(DEFAULT_ESTADO_MUSCULO);
        return musculo;
    }

    @Before
    public void initTest() {
        musculo = createEntity(em);
    }

    @Test
    @Transactional
    public void createMusculo() throws Exception {
        int databaseSizeBeforeCreate = musculoRepository.findAll().size();

        // Create the Musculo
        MusculoDTO musculoDTO = musculoMapper.toDto(musculo);
        restMusculoMockMvc.perform(post("/api/musculos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(musculoDTO)))
            .andExpect(status().isCreated());

        // Validate the Musculo in the database
        List<Musculo> musculoList = musculoRepository.findAll();
        assertThat(musculoList).hasSize(databaseSizeBeforeCreate + 1);
        Musculo testMusculo = musculoList.get(musculoList.size() - 1);
        assertThat(testMusculo.getNombreMusculo()).isEqualTo(DEFAULT_NOMBRE_MUSCULO);
        assertThat(testMusculo.isEstadoMusculo()).isEqualTo(DEFAULT_ESTADO_MUSCULO);
    }

    @Test
    @Transactional
    public void createMusculoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = musculoRepository.findAll().size();

        // Create the Musculo with an existing ID
        musculo.setId(1L);
        MusculoDTO musculoDTO = musculoMapper.toDto(musculo);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMusculoMockMvc.perform(post("/api/musculos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(musculoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Musculo in the database
        List<Musculo> musculoList = musculoRepository.findAll();
        assertThat(musculoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNombreMusculoIsRequired() throws Exception {
        int databaseSizeBeforeTest = musculoRepository.findAll().size();
        // set the field null
        musculo.setNombreMusculo(null);

        // Create the Musculo, which fails.
        MusculoDTO musculoDTO = musculoMapper.toDto(musculo);

        restMusculoMockMvc.perform(post("/api/musculos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(musculoDTO)))
            .andExpect(status().isBadRequest());

        List<Musculo> musculoList = musculoRepository.findAll();
        assertThat(musculoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEstadoMusculoIsRequired() throws Exception {
        int databaseSizeBeforeTest = musculoRepository.findAll().size();
        // set the field null
        musculo.setEstadoMusculo(null);

        // Create the Musculo, which fails.
        MusculoDTO musculoDTO = musculoMapper.toDto(musculo);

        restMusculoMockMvc.perform(post("/api/musculos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(musculoDTO)))
            .andExpect(status().isBadRequest());

        List<Musculo> musculoList = musculoRepository.findAll();
        assertThat(musculoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMusculos() throws Exception {
        // Initialize the database
        musculoRepository.saveAndFlush(musculo);

        // Get all the musculoList
        restMusculoMockMvc.perform(get("/api/musculos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(musculo.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombreMusculo").value(hasItem(DEFAULT_NOMBRE_MUSCULO.toString())))
            .andExpect(jsonPath("$.[*].estadoMusculo").value(hasItem(DEFAULT_ESTADO_MUSCULO.booleanValue())));
    }

    @Test
    @Transactional
    public void getMusculo() throws Exception {
        // Initialize the database
        musculoRepository.saveAndFlush(musculo);

        // Get the musculo
        restMusculoMockMvc.perform(get("/api/musculos/{id}", musculo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(musculo.getId().intValue()))
            .andExpect(jsonPath("$.nombreMusculo").value(DEFAULT_NOMBRE_MUSCULO.toString()))
            .andExpect(jsonPath("$.estadoMusculo").value(DEFAULT_ESTADO_MUSCULO.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingMusculo() throws Exception {
        // Get the musculo
        restMusculoMockMvc.perform(get("/api/musculos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMusculo() throws Exception {
        // Initialize the database
        musculoRepository.saveAndFlush(musculo);
        int databaseSizeBeforeUpdate = musculoRepository.findAll().size();

        // Update the musculo
        Musculo updatedMusculo = musculoRepository.findOne(musculo.getId());
        // Disconnect from session so that the updates on updatedMusculo are not directly saved in db
        em.detach(updatedMusculo);
        updatedMusculo
            .nombreMusculo(UPDATED_NOMBRE_MUSCULO)
            .estadoMusculo(UPDATED_ESTADO_MUSCULO);
        MusculoDTO musculoDTO = musculoMapper.toDto(updatedMusculo);

        restMusculoMockMvc.perform(put("/api/musculos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(musculoDTO)))
            .andExpect(status().isOk());

        // Validate the Musculo in the database
        List<Musculo> musculoList = musculoRepository.findAll();
        assertThat(musculoList).hasSize(databaseSizeBeforeUpdate);
        Musculo testMusculo = musculoList.get(musculoList.size() - 1);
        assertThat(testMusculo.getNombreMusculo()).isEqualTo(UPDATED_NOMBRE_MUSCULO);
        assertThat(testMusculo.isEstadoMusculo()).isEqualTo(UPDATED_ESTADO_MUSCULO);
    }

    @Test
    @Transactional
    public void updateNonExistingMusculo() throws Exception {
        int databaseSizeBeforeUpdate = musculoRepository.findAll().size();

        // Create the Musculo
        MusculoDTO musculoDTO = musculoMapper.toDto(musculo);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restMusculoMockMvc.perform(put("/api/musculos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(musculoDTO)))
            .andExpect(status().isCreated());

        // Validate the Musculo in the database
        List<Musculo> musculoList = musculoRepository.findAll();
        assertThat(musculoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteMusculo() throws Exception {
        // Initialize the database
        musculoRepository.saveAndFlush(musculo);
        int databaseSizeBeforeDelete = musculoRepository.findAll().size();

        // Get the musculo
        restMusculoMockMvc.perform(delete("/api/musculos/{id}", musculo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Musculo> musculoList = musculoRepository.findAll();
        assertThat(musculoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Musculo.class);
        Musculo musculo1 = new Musculo();
        musculo1.setId(1L);
        Musculo musculo2 = new Musculo();
        musculo2.setId(musculo1.getId());
        assertThat(musculo1).isEqualTo(musculo2);
        musculo2.setId(2L);
        assertThat(musculo1).isNotEqualTo(musculo2);
        musculo1.setId(null);
        assertThat(musculo1).isNotEqualTo(musculo2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MusculoDTO.class);
        MusculoDTO musculoDTO1 = new MusculoDTO();
        musculoDTO1.setId(1L);
        MusculoDTO musculoDTO2 = new MusculoDTO();
        assertThat(musculoDTO1).isNotEqualTo(musculoDTO2);
        musculoDTO2.setId(musculoDTO1.getId());
        assertThat(musculoDTO1).isEqualTo(musculoDTO2);
        musculoDTO2.setId(2L);
        assertThat(musculoDTO1).isNotEqualTo(musculoDTO2);
        musculoDTO1.setId(null);
        assertThat(musculoDTO1).isNotEqualTo(musculoDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(musculoMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(musculoMapper.fromId(null)).isNull();
    }
}
