package com.test.gymtest.web.rest;

import com.test.gymtest.TestGymApp;

import com.test.gymtest.domain.ParametroConfiguracion;
import com.test.gymtest.repository.ParametroConfiguracionRepository;
import com.test.gymtest.service.ParametroConfiguracionService;
import com.test.gymtest.service.dto.ParametroConfiguracionDTO;
import com.test.gymtest.service.mapper.ParametroConfiguracionMapper;
import com.test.gymtest.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.test.gymtest.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ParametroConfiguracionResource REST controller.
 *
 * @see ParametroConfiguracionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestGymApp.class)
public class ParametroConfiguracionResourceIntTest {

    private static final String DEFAULT_NOMBRE_PARAMETRO = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE_PARAMETRO = "BBBBBBBBBB";

    private static final String DEFAULT_VALOR_PARAMETRO = "AAAAAAAAAA";
    private static final String UPDATED_VALOR_PARAMETRO = "BBBBBBBBBB";

    private static final String DEFAULT_TIPO_DE_DATO = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_DE_DATO = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ESTADO_PARAMETRO = false;
    private static final Boolean UPDATED_ESTADO_PARAMETRO = true;

    @Autowired
    private ParametroConfiguracionRepository parametroConfiguracionRepository;

    @Autowired
    private ParametroConfiguracionMapper parametroConfiguracionMapper;

    @Autowired
    private ParametroConfiguracionService parametroConfiguracionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restParametroConfiguracionMockMvc;

    private ParametroConfiguracion parametroConfiguracion;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ParametroConfiguracionResource parametroConfiguracionResource = new ParametroConfiguracionResource(parametroConfiguracionService);
        this.restParametroConfiguracionMockMvc = MockMvcBuilders.standaloneSetup(parametroConfiguracionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ParametroConfiguracion createEntity(EntityManager em) {
        ParametroConfiguracion parametroConfiguracion = new ParametroConfiguracion()
            .nombreParametro(DEFAULT_NOMBRE_PARAMETRO)
            .valorParametro(DEFAULT_VALOR_PARAMETRO)
            .tipoDeDato(DEFAULT_TIPO_DE_DATO)
            .estadoParametro(DEFAULT_ESTADO_PARAMETRO);
        return parametroConfiguracion;
    }

    @Before
    public void initTest() {
        parametroConfiguracion = createEntity(em);
    }

    @Test
    @Transactional
    public void createParametroConfiguracion() throws Exception {
        int databaseSizeBeforeCreate = parametroConfiguracionRepository.findAll().size();

        // Create the ParametroConfiguracion
        ParametroConfiguracionDTO parametroConfiguracionDTO = parametroConfiguracionMapper.toDto(parametroConfiguracion);
        restParametroConfiguracionMockMvc.perform(post("/api/parametro-configuracions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parametroConfiguracionDTO)))
            .andExpect(status().isCreated());

        // Validate the ParametroConfiguracion in the database
        List<ParametroConfiguracion> parametroConfiguracionList = parametroConfiguracionRepository.findAll();
        assertThat(parametroConfiguracionList).hasSize(databaseSizeBeforeCreate + 1);
        ParametroConfiguracion testParametroConfiguracion = parametroConfiguracionList.get(parametroConfiguracionList.size() - 1);
        assertThat(testParametroConfiguracion.getNombreParametro()).isEqualTo(DEFAULT_NOMBRE_PARAMETRO);
        assertThat(testParametroConfiguracion.getValorParametro()).isEqualTo(DEFAULT_VALOR_PARAMETRO);
        assertThat(testParametroConfiguracion.getTipoDeDato()).isEqualTo(DEFAULT_TIPO_DE_DATO);
        assertThat(testParametroConfiguracion.isEstadoParametro()).isEqualTo(DEFAULT_ESTADO_PARAMETRO);
    }

    @Test
    @Transactional
    public void createParametroConfiguracionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = parametroConfiguracionRepository.findAll().size();

        // Create the ParametroConfiguracion with an existing ID
        parametroConfiguracion.setId(1L);
        ParametroConfiguracionDTO parametroConfiguracionDTO = parametroConfiguracionMapper.toDto(parametroConfiguracion);

        // An entity with an existing ID cannot be created, so this API call must fail
        restParametroConfiguracionMockMvc.perform(post("/api/parametro-configuracions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parametroConfiguracionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ParametroConfiguracion in the database
        List<ParametroConfiguracion> parametroConfiguracionList = parametroConfiguracionRepository.findAll();
        assertThat(parametroConfiguracionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNombreParametroIsRequired() throws Exception {
        int databaseSizeBeforeTest = parametroConfiguracionRepository.findAll().size();
        // set the field null
        parametroConfiguracion.setNombreParametro(null);

        // Create the ParametroConfiguracion, which fails.
        ParametroConfiguracionDTO parametroConfiguracionDTO = parametroConfiguracionMapper.toDto(parametroConfiguracion);

        restParametroConfiguracionMockMvc.perform(post("/api/parametro-configuracions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parametroConfiguracionDTO)))
            .andExpect(status().isBadRequest());

        List<ParametroConfiguracion> parametroConfiguracionList = parametroConfiguracionRepository.findAll();
        assertThat(parametroConfiguracionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValorParametroIsRequired() throws Exception {
        int databaseSizeBeforeTest = parametroConfiguracionRepository.findAll().size();
        // set the field null
        parametroConfiguracion.setValorParametro(null);

        // Create the ParametroConfiguracion, which fails.
        ParametroConfiguracionDTO parametroConfiguracionDTO = parametroConfiguracionMapper.toDto(parametroConfiguracion);

        restParametroConfiguracionMockMvc.perform(post("/api/parametro-configuracions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parametroConfiguracionDTO)))
            .andExpect(status().isBadRequest());

        List<ParametroConfiguracion> parametroConfiguracionList = parametroConfiguracionRepository.findAll();
        assertThat(parametroConfiguracionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTipoDeDatoIsRequired() throws Exception {
        int databaseSizeBeforeTest = parametroConfiguracionRepository.findAll().size();
        // set the field null
        parametroConfiguracion.setTipoDeDato(null);

        // Create the ParametroConfiguracion, which fails.
        ParametroConfiguracionDTO parametroConfiguracionDTO = parametroConfiguracionMapper.toDto(parametroConfiguracion);

        restParametroConfiguracionMockMvc.perform(post("/api/parametro-configuracions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parametroConfiguracionDTO)))
            .andExpect(status().isBadRequest());

        List<ParametroConfiguracion> parametroConfiguracionList = parametroConfiguracionRepository.findAll();
        assertThat(parametroConfiguracionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEstadoParametroIsRequired() throws Exception {
        int databaseSizeBeforeTest = parametroConfiguracionRepository.findAll().size();
        // set the field null
        parametroConfiguracion.setEstadoParametro(null);

        // Create the ParametroConfiguracion, which fails.
        ParametroConfiguracionDTO parametroConfiguracionDTO = parametroConfiguracionMapper.toDto(parametroConfiguracion);

        restParametroConfiguracionMockMvc.perform(post("/api/parametro-configuracions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parametroConfiguracionDTO)))
            .andExpect(status().isBadRequest());

        List<ParametroConfiguracion> parametroConfiguracionList = parametroConfiguracionRepository.findAll();
        assertThat(parametroConfiguracionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllParametroConfiguracions() throws Exception {
        // Initialize the database
        parametroConfiguracionRepository.saveAndFlush(parametroConfiguracion);

        // Get all the parametroConfiguracionList
        restParametroConfiguracionMockMvc.perform(get("/api/parametro-configuracions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(parametroConfiguracion.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombreParametro").value(hasItem(DEFAULT_NOMBRE_PARAMETRO.toString())))
            .andExpect(jsonPath("$.[*].valorParametro").value(hasItem(DEFAULT_VALOR_PARAMETRO.toString())))
            .andExpect(jsonPath("$.[*].tipoDeDato").value(hasItem(DEFAULT_TIPO_DE_DATO.toString())))
            .andExpect(jsonPath("$.[*].estadoParametro").value(hasItem(DEFAULT_ESTADO_PARAMETRO.booleanValue())));
    }

    @Test
    @Transactional
    public void getParametroConfiguracion() throws Exception {
        // Initialize the database
        parametroConfiguracionRepository.saveAndFlush(parametroConfiguracion);

        // Get the parametroConfiguracion
        restParametroConfiguracionMockMvc.perform(get("/api/parametro-configuracions/{id}", parametroConfiguracion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(parametroConfiguracion.getId().intValue()))
            .andExpect(jsonPath("$.nombreParametro").value(DEFAULT_NOMBRE_PARAMETRO.toString()))
            .andExpect(jsonPath("$.valorParametro").value(DEFAULT_VALOR_PARAMETRO.toString()))
            .andExpect(jsonPath("$.tipoDeDato").value(DEFAULT_TIPO_DE_DATO.toString()))
            .andExpect(jsonPath("$.estadoParametro").value(DEFAULT_ESTADO_PARAMETRO.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingParametroConfiguracion() throws Exception {
        // Get the parametroConfiguracion
        restParametroConfiguracionMockMvc.perform(get("/api/parametro-configuracions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateParametroConfiguracion() throws Exception {
        // Initialize the database
        parametroConfiguracionRepository.saveAndFlush(parametroConfiguracion);
        int databaseSizeBeforeUpdate = parametroConfiguracionRepository.findAll().size();

        // Update the parametroConfiguracion
        ParametroConfiguracion updatedParametroConfiguracion = parametroConfiguracionRepository.findOne(parametroConfiguracion.getId());
        // Disconnect from session so that the updates on updatedParametroConfiguracion are not directly saved in db
        em.detach(updatedParametroConfiguracion);
        updatedParametroConfiguracion
            .nombreParametro(UPDATED_NOMBRE_PARAMETRO)
            .valorParametro(UPDATED_VALOR_PARAMETRO)
            .tipoDeDato(UPDATED_TIPO_DE_DATO)
            .estadoParametro(UPDATED_ESTADO_PARAMETRO);
        ParametroConfiguracionDTO parametroConfiguracionDTO = parametroConfiguracionMapper.toDto(updatedParametroConfiguracion);

        restParametroConfiguracionMockMvc.perform(put("/api/parametro-configuracions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parametroConfiguracionDTO)))
            .andExpect(status().isOk());

        // Validate the ParametroConfiguracion in the database
        List<ParametroConfiguracion> parametroConfiguracionList = parametroConfiguracionRepository.findAll();
        assertThat(parametroConfiguracionList).hasSize(databaseSizeBeforeUpdate);
        ParametroConfiguracion testParametroConfiguracion = parametroConfiguracionList.get(parametroConfiguracionList.size() - 1);
        assertThat(testParametroConfiguracion.getNombreParametro()).isEqualTo(UPDATED_NOMBRE_PARAMETRO);
        assertThat(testParametroConfiguracion.getValorParametro()).isEqualTo(UPDATED_VALOR_PARAMETRO);
        assertThat(testParametroConfiguracion.getTipoDeDato()).isEqualTo(UPDATED_TIPO_DE_DATO);
        assertThat(testParametroConfiguracion.isEstadoParametro()).isEqualTo(UPDATED_ESTADO_PARAMETRO);
    }

    @Test
    @Transactional
    public void updateNonExistingParametroConfiguracion() throws Exception {
        int databaseSizeBeforeUpdate = parametroConfiguracionRepository.findAll().size();

        // Create the ParametroConfiguracion
        ParametroConfiguracionDTO parametroConfiguracionDTO = parametroConfiguracionMapper.toDto(parametroConfiguracion);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restParametroConfiguracionMockMvc.perform(put("/api/parametro-configuracions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(parametroConfiguracionDTO)))
            .andExpect(status().isCreated());

        // Validate the ParametroConfiguracion in the database
        List<ParametroConfiguracion> parametroConfiguracionList = parametroConfiguracionRepository.findAll();
        assertThat(parametroConfiguracionList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteParametroConfiguracion() throws Exception {
        // Initialize the database
        parametroConfiguracionRepository.saveAndFlush(parametroConfiguracion);
        int databaseSizeBeforeDelete = parametroConfiguracionRepository.findAll().size();

        // Get the parametroConfiguracion
        restParametroConfiguracionMockMvc.perform(delete("/api/parametro-configuracions/{id}", parametroConfiguracion.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ParametroConfiguracion> parametroConfiguracionList = parametroConfiguracionRepository.findAll();
        assertThat(parametroConfiguracionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ParametroConfiguracion.class);
        ParametroConfiguracion parametroConfiguracion1 = new ParametroConfiguracion();
        parametroConfiguracion1.setId(1L);
        ParametroConfiguracion parametroConfiguracion2 = new ParametroConfiguracion();
        parametroConfiguracion2.setId(parametroConfiguracion1.getId());
        assertThat(parametroConfiguracion1).isEqualTo(parametroConfiguracion2);
        parametroConfiguracion2.setId(2L);
        assertThat(parametroConfiguracion1).isNotEqualTo(parametroConfiguracion2);
        parametroConfiguracion1.setId(null);
        assertThat(parametroConfiguracion1).isNotEqualTo(parametroConfiguracion2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ParametroConfiguracionDTO.class);
        ParametroConfiguracionDTO parametroConfiguracionDTO1 = new ParametroConfiguracionDTO();
        parametroConfiguracionDTO1.setId(1L);
        ParametroConfiguracionDTO parametroConfiguracionDTO2 = new ParametroConfiguracionDTO();
        assertThat(parametroConfiguracionDTO1).isNotEqualTo(parametroConfiguracionDTO2);
        parametroConfiguracionDTO2.setId(parametroConfiguracionDTO1.getId());
        assertThat(parametroConfiguracionDTO1).isEqualTo(parametroConfiguracionDTO2);
        parametroConfiguracionDTO2.setId(2L);
        assertThat(parametroConfiguracionDTO1).isNotEqualTo(parametroConfiguracionDTO2);
        parametroConfiguracionDTO1.setId(null);
        assertThat(parametroConfiguracionDTO1).isNotEqualTo(parametroConfiguracionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(parametroConfiguracionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(parametroConfiguracionMapper.fromId(null)).isNull();
    }
}
