package com.test.gymtest.web.rest;

import com.test.gymtest.TestGymApp;

import com.test.gymtest.domain.Rutina;
import com.test.gymtest.repository.RutinaRepository;
import com.test.gymtest.service.RutinaService;
import com.test.gymtest.service.dto.RutinaDTO;
import com.test.gymtest.service.mapper.RutinaMapper;
import com.test.gymtest.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.test.gymtest.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RutinaResource REST controller.
 *
 * @see RutinaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestGymApp.class)
public class RutinaResourceIntTest {

    private static final Integer DEFAULT_USUARIO_CREADOR_RUTINA = 1;
    private static final Integer UPDATED_USUARIO_CREADOR_RUTINA = 2;

    private static final String DEFAULT_NOMBRE_RUTINA = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE_RUTINA = "BBBBBBBBBB";

    private static final Integer DEFAULT_CANT_DIAS_RUTINA = 1;
    private static final Integer UPDATED_CANT_DIAS_RUTINA = 2;

    private static final Boolean DEFAULT_ESTADO_RUTINA = false;
    private static final Boolean UPDATED_ESTADO_RUTINA = true;

    @Autowired
    private RutinaRepository rutinaRepository;

    @Autowired
    private RutinaMapper rutinaMapper;

    @Autowired
    private RutinaService rutinaService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRutinaMockMvc;

    private Rutina rutina;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RutinaResource rutinaResource = new RutinaResource(rutinaService);
        this.restRutinaMockMvc = MockMvcBuilders.standaloneSetup(rutinaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Rutina createEntity(EntityManager em) {
        Rutina rutina = new Rutina()
            .usuarioCreadorRutina(DEFAULT_USUARIO_CREADOR_RUTINA)
            .nombreRutina(DEFAULT_NOMBRE_RUTINA)
            .cantDiasRutina(DEFAULT_CANT_DIAS_RUTINA)
            .estadoRutina(DEFAULT_ESTADO_RUTINA);
        return rutina;
    }

    @Before
    public void initTest() {
        rutina = createEntity(em);
    }

    @Test
    @Transactional
    public void createRutina() throws Exception {
        int databaseSizeBeforeCreate = rutinaRepository.findAll().size();

        // Create the Rutina
        RutinaDTO rutinaDTO = rutinaMapper.toDto(rutina);
        restRutinaMockMvc.perform(post("/api/rutinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rutinaDTO)))
            .andExpect(status().isCreated());

        // Validate the Rutina in the database
        List<Rutina> rutinaList = rutinaRepository.findAll();
        assertThat(rutinaList).hasSize(databaseSizeBeforeCreate + 1);
        Rutina testRutina = rutinaList.get(rutinaList.size() - 1);
        assertThat(testRutina.getUsuarioCreadorRutina()).isEqualTo(DEFAULT_USUARIO_CREADOR_RUTINA);
        assertThat(testRutina.getNombreRutina()).isEqualTo(DEFAULT_NOMBRE_RUTINA);
        assertThat(testRutina.getCantDiasRutina()).isEqualTo(DEFAULT_CANT_DIAS_RUTINA);
        assertThat(testRutina.isEstadoRutina()).isEqualTo(DEFAULT_ESTADO_RUTINA);
    }

    @Test
    @Transactional
    public void createRutinaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = rutinaRepository.findAll().size();

        // Create the Rutina with an existing ID
        rutina.setId(1L);
        RutinaDTO rutinaDTO = rutinaMapper.toDto(rutina);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRutinaMockMvc.perform(post("/api/rutinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rutinaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Rutina in the database
        List<Rutina> rutinaList = rutinaRepository.findAll();
        assertThat(rutinaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNombreRutinaIsRequired() throws Exception {
        int databaseSizeBeforeTest = rutinaRepository.findAll().size();
        // set the field null
        rutina.setNombreRutina(null);

        // Create the Rutina, which fails.
        RutinaDTO rutinaDTO = rutinaMapper.toDto(rutina);

        restRutinaMockMvc.perform(post("/api/rutinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rutinaDTO)))
            .andExpect(status().isBadRequest());

        List<Rutina> rutinaList = rutinaRepository.findAll();
        assertThat(rutinaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEstadoRutinaIsRequired() throws Exception {
        int databaseSizeBeforeTest = rutinaRepository.findAll().size();
        // set the field null
        rutina.setEstadoRutina(null);

        // Create the Rutina, which fails.
        RutinaDTO rutinaDTO = rutinaMapper.toDto(rutina);

        restRutinaMockMvc.perform(post("/api/rutinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rutinaDTO)))
            .andExpect(status().isBadRequest());

        List<Rutina> rutinaList = rutinaRepository.findAll();
        assertThat(rutinaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRutinas() throws Exception {
        // Initialize the database
        rutinaRepository.saveAndFlush(rutina);

        // Get all the rutinaList
        restRutinaMockMvc.perform(get("/api/rutinas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(rutina.getId().intValue())))
            .andExpect(jsonPath("$.[*].usuarioCreadorRutina").value(hasItem(DEFAULT_USUARIO_CREADOR_RUTINA)))
            .andExpect(jsonPath("$.[*].nombreRutina").value(hasItem(DEFAULT_NOMBRE_RUTINA.toString())))
            .andExpect(jsonPath("$.[*].cantDiasRutina").value(hasItem(DEFAULT_CANT_DIAS_RUTINA)))
            .andExpect(jsonPath("$.[*].estadoRutina").value(hasItem(DEFAULT_ESTADO_RUTINA.booleanValue())));
    }

    @Test
    @Transactional
    public void getRutina() throws Exception {
        // Initialize the database
        rutinaRepository.saveAndFlush(rutina);

        // Get the rutina
        restRutinaMockMvc.perform(get("/api/rutinas/{id}", rutina.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(rutina.getId().intValue()))
            .andExpect(jsonPath("$.usuarioCreadorRutina").value(DEFAULT_USUARIO_CREADOR_RUTINA))
            .andExpect(jsonPath("$.nombreRutina").value(DEFAULT_NOMBRE_RUTINA.toString()))
            .andExpect(jsonPath("$.cantDiasRutina").value(DEFAULT_CANT_DIAS_RUTINA))
            .andExpect(jsonPath("$.estadoRutina").value(DEFAULT_ESTADO_RUTINA.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingRutina() throws Exception {
        // Get the rutina
        restRutinaMockMvc.perform(get("/api/rutinas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRutina() throws Exception {
        // Initialize the database
        rutinaRepository.saveAndFlush(rutina);
        int databaseSizeBeforeUpdate = rutinaRepository.findAll().size();

        // Update the rutina
        Rutina updatedRutina = rutinaRepository.findOne(rutina.getId());
        // Disconnect from session so that the updates on updatedRutina are not directly saved in db
        em.detach(updatedRutina);
        updatedRutina
            .usuarioCreadorRutina(UPDATED_USUARIO_CREADOR_RUTINA)
            .nombreRutina(UPDATED_NOMBRE_RUTINA)
            .cantDiasRutina(UPDATED_CANT_DIAS_RUTINA)
            .estadoRutina(UPDATED_ESTADO_RUTINA);
        RutinaDTO rutinaDTO = rutinaMapper.toDto(updatedRutina);

        restRutinaMockMvc.perform(put("/api/rutinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rutinaDTO)))
            .andExpect(status().isOk());

        // Validate the Rutina in the database
        List<Rutina> rutinaList = rutinaRepository.findAll();
        assertThat(rutinaList).hasSize(databaseSizeBeforeUpdate);
        Rutina testRutina = rutinaList.get(rutinaList.size() - 1);
        assertThat(testRutina.getUsuarioCreadorRutina()).isEqualTo(UPDATED_USUARIO_CREADOR_RUTINA);
        assertThat(testRutina.getNombreRutina()).isEqualTo(UPDATED_NOMBRE_RUTINA);
        assertThat(testRutina.getCantDiasRutina()).isEqualTo(UPDATED_CANT_DIAS_RUTINA);
        assertThat(testRutina.isEstadoRutina()).isEqualTo(UPDATED_ESTADO_RUTINA);
    }

    @Test
    @Transactional
    public void updateNonExistingRutina() throws Exception {
        int databaseSizeBeforeUpdate = rutinaRepository.findAll().size();

        // Create the Rutina
        RutinaDTO rutinaDTO = rutinaMapper.toDto(rutina);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restRutinaMockMvc.perform(put("/api/rutinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(rutinaDTO)))
            .andExpect(status().isCreated());

        // Validate the Rutina in the database
        List<Rutina> rutinaList = rutinaRepository.findAll();
        assertThat(rutinaList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteRutina() throws Exception {
        // Initialize the database
        rutinaRepository.saveAndFlush(rutina);
        int databaseSizeBeforeDelete = rutinaRepository.findAll().size();

        // Get the rutina
        restRutinaMockMvc.perform(delete("/api/rutinas/{id}", rutina.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Rutina> rutinaList = rutinaRepository.findAll();
        assertThat(rutinaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Rutina.class);
        Rutina rutina1 = new Rutina();
        rutina1.setId(1L);
        Rutina rutina2 = new Rutina();
        rutina2.setId(rutina1.getId());
        assertThat(rutina1).isEqualTo(rutina2);
        rutina2.setId(2L);
        assertThat(rutina1).isNotEqualTo(rutina2);
        rutina1.setId(null);
        assertThat(rutina1).isNotEqualTo(rutina2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RutinaDTO.class);
        RutinaDTO rutinaDTO1 = new RutinaDTO();
        rutinaDTO1.setId(1L);
        RutinaDTO rutinaDTO2 = new RutinaDTO();
        assertThat(rutinaDTO1).isNotEqualTo(rutinaDTO2);
        rutinaDTO2.setId(rutinaDTO1.getId());
        assertThat(rutinaDTO1).isEqualTo(rutinaDTO2);
        rutinaDTO2.setId(2L);
        assertThat(rutinaDTO1).isNotEqualTo(rutinaDTO2);
        rutinaDTO1.setId(null);
        assertThat(rutinaDTO1).isNotEqualTo(rutinaDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(rutinaMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(rutinaMapper.fromId(null)).isNull();
    }
}
