package com.test.gymtest.web.rest;

import com.test.gymtest.TestGymApp;

import com.test.gymtest.domain.TipoParametro;
import com.test.gymtest.repository.TipoParametroRepository;
import com.test.gymtest.service.TipoParametroService;
import com.test.gymtest.service.dto.TipoParametroDTO;
import com.test.gymtest.service.mapper.TipoParametroMapper;
import com.test.gymtest.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.test.gymtest.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TipoParametroResource REST controller.
 *
 * @see TipoParametroResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestGymApp.class)
public class TipoParametroResourceIntTest {

    private static final String DEFAULT_NOMBRE_TIPO_PARAMETRO = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE_TIPO_PARAMETRO = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ESTADO_TIPO_PARAMETRO = false;
    private static final Boolean UPDATED_ESTADO_TIPO_PARAMETRO = true;

    @Autowired
    private TipoParametroRepository tipoParametroRepository;

    @Autowired
    private TipoParametroMapper tipoParametroMapper;

    @Autowired
    private TipoParametroService tipoParametroService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTipoParametroMockMvc;

    private TipoParametro tipoParametro;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipoParametroResource tipoParametroResource = new TipoParametroResource(tipoParametroService);
        this.restTipoParametroMockMvc = MockMvcBuilders.standaloneSetup(tipoParametroResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TipoParametro createEntity(EntityManager em) {
        TipoParametro tipoParametro = new TipoParametro()
            .nombreTipoParametro(DEFAULT_NOMBRE_TIPO_PARAMETRO)
            .estadoTipoParametro(DEFAULT_ESTADO_TIPO_PARAMETRO);
        return tipoParametro;
    }

    @Before
    public void initTest() {
        tipoParametro = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipoParametro() throws Exception {
        int databaseSizeBeforeCreate = tipoParametroRepository.findAll().size();

        // Create the TipoParametro
        TipoParametroDTO tipoParametroDTO = tipoParametroMapper.toDto(tipoParametro);
        restTipoParametroMockMvc.perform(post("/api/tipo-parametros")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoParametroDTO)))
            .andExpect(status().isCreated());

        // Validate the TipoParametro in the database
        List<TipoParametro> tipoParametroList = tipoParametroRepository.findAll();
        assertThat(tipoParametroList).hasSize(databaseSizeBeforeCreate + 1);
        TipoParametro testTipoParametro = tipoParametroList.get(tipoParametroList.size() - 1);
        assertThat(testTipoParametro.getNombreTipoParametro()).isEqualTo(DEFAULT_NOMBRE_TIPO_PARAMETRO);
        assertThat(testTipoParametro.isEstadoTipoParametro()).isEqualTo(DEFAULT_ESTADO_TIPO_PARAMETRO);
    }

    @Test
    @Transactional
    public void createTipoParametroWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipoParametroRepository.findAll().size();

        // Create the TipoParametro with an existing ID
        tipoParametro.setId(1L);
        TipoParametroDTO tipoParametroDTO = tipoParametroMapper.toDto(tipoParametro);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipoParametroMockMvc.perform(post("/api/tipo-parametros")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoParametroDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TipoParametro in the database
        List<TipoParametro> tipoParametroList = tipoParametroRepository.findAll();
        assertThat(tipoParametroList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNombreTipoParametroIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipoParametroRepository.findAll().size();
        // set the field null
        tipoParametro.setNombreTipoParametro(null);

        // Create the TipoParametro, which fails.
        TipoParametroDTO tipoParametroDTO = tipoParametroMapper.toDto(tipoParametro);

        restTipoParametroMockMvc.perform(post("/api/tipo-parametros")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoParametroDTO)))
            .andExpect(status().isBadRequest());

        List<TipoParametro> tipoParametroList = tipoParametroRepository.findAll();
        assertThat(tipoParametroList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEstadoTipoParametroIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipoParametroRepository.findAll().size();
        // set the field null
        tipoParametro.setEstadoTipoParametro(null);

        // Create the TipoParametro, which fails.
        TipoParametroDTO tipoParametroDTO = tipoParametroMapper.toDto(tipoParametro);

        restTipoParametroMockMvc.perform(post("/api/tipo-parametros")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoParametroDTO)))
            .andExpect(status().isBadRequest());

        List<TipoParametro> tipoParametroList = tipoParametroRepository.findAll();
        assertThat(tipoParametroList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTipoParametros() throws Exception {
        // Initialize the database
        tipoParametroRepository.saveAndFlush(tipoParametro);

        // Get all the tipoParametroList
        restTipoParametroMockMvc.perform(get("/api/tipo-parametros?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipoParametro.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombreTipoParametro").value(hasItem(DEFAULT_NOMBRE_TIPO_PARAMETRO.toString())))
            .andExpect(jsonPath("$.[*].estadoTipoParametro").value(hasItem(DEFAULT_ESTADO_TIPO_PARAMETRO.booleanValue())));
    }

    @Test
    @Transactional
    public void getTipoParametro() throws Exception {
        // Initialize the database
        tipoParametroRepository.saveAndFlush(tipoParametro);

        // Get the tipoParametro
        restTipoParametroMockMvc.perform(get("/api/tipo-parametros/{id}", tipoParametro.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipoParametro.getId().intValue()))
            .andExpect(jsonPath("$.nombreTipoParametro").value(DEFAULT_NOMBRE_TIPO_PARAMETRO.toString()))
            .andExpect(jsonPath("$.estadoTipoParametro").value(DEFAULT_ESTADO_TIPO_PARAMETRO.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingTipoParametro() throws Exception {
        // Get the tipoParametro
        restTipoParametroMockMvc.perform(get("/api/tipo-parametros/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipoParametro() throws Exception {
        // Initialize the database
        tipoParametroRepository.saveAndFlush(tipoParametro);
        int databaseSizeBeforeUpdate = tipoParametroRepository.findAll().size();

        // Update the tipoParametro
        TipoParametro updatedTipoParametro = tipoParametroRepository.findOne(tipoParametro.getId());
        // Disconnect from session so that the updates on updatedTipoParametro are not directly saved in db
        em.detach(updatedTipoParametro);
        updatedTipoParametro
            .nombreTipoParametro(UPDATED_NOMBRE_TIPO_PARAMETRO)
            .estadoTipoParametro(UPDATED_ESTADO_TIPO_PARAMETRO);
        TipoParametroDTO tipoParametroDTO = tipoParametroMapper.toDto(updatedTipoParametro);

        restTipoParametroMockMvc.perform(put("/api/tipo-parametros")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoParametroDTO)))
            .andExpect(status().isOk());

        // Validate the TipoParametro in the database
        List<TipoParametro> tipoParametroList = tipoParametroRepository.findAll();
        assertThat(tipoParametroList).hasSize(databaseSizeBeforeUpdate);
        TipoParametro testTipoParametro = tipoParametroList.get(tipoParametroList.size() - 1);
        assertThat(testTipoParametro.getNombreTipoParametro()).isEqualTo(UPDATED_NOMBRE_TIPO_PARAMETRO);
        assertThat(testTipoParametro.isEstadoTipoParametro()).isEqualTo(UPDATED_ESTADO_TIPO_PARAMETRO);
    }

    @Test
    @Transactional
    public void updateNonExistingTipoParametro() throws Exception {
        int databaseSizeBeforeUpdate = tipoParametroRepository.findAll().size();

        // Create the TipoParametro
        TipoParametroDTO tipoParametroDTO = tipoParametroMapper.toDto(tipoParametro);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTipoParametroMockMvc.perform(put("/api/tipo-parametros")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipoParametroDTO)))
            .andExpect(status().isCreated());

        // Validate the TipoParametro in the database
        List<TipoParametro> tipoParametroList = tipoParametroRepository.findAll();
        assertThat(tipoParametroList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTipoParametro() throws Exception {
        // Initialize the database
        tipoParametroRepository.saveAndFlush(tipoParametro);
        int databaseSizeBeforeDelete = tipoParametroRepository.findAll().size();

        // Get the tipoParametro
        restTipoParametroMockMvc.perform(delete("/api/tipo-parametros/{id}", tipoParametro.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TipoParametro> tipoParametroList = tipoParametroRepository.findAll();
        assertThat(tipoParametroList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipoParametro.class);
        TipoParametro tipoParametro1 = new TipoParametro();
        tipoParametro1.setId(1L);
        TipoParametro tipoParametro2 = new TipoParametro();
        tipoParametro2.setId(tipoParametro1.getId());
        assertThat(tipoParametro1).isEqualTo(tipoParametro2);
        tipoParametro2.setId(2L);
        assertThat(tipoParametro1).isNotEqualTo(tipoParametro2);
        tipoParametro1.setId(null);
        assertThat(tipoParametro1).isNotEqualTo(tipoParametro2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TipoParametroDTO.class);
        TipoParametroDTO tipoParametroDTO1 = new TipoParametroDTO();
        tipoParametroDTO1.setId(1L);
        TipoParametroDTO tipoParametroDTO2 = new TipoParametroDTO();
        assertThat(tipoParametroDTO1).isNotEqualTo(tipoParametroDTO2);
        tipoParametroDTO2.setId(tipoParametroDTO1.getId());
        assertThat(tipoParametroDTO1).isEqualTo(tipoParametroDTO2);
        tipoParametroDTO2.setId(2L);
        assertThat(tipoParametroDTO1).isNotEqualTo(tipoParametroDTO2);
        tipoParametroDTO1.setId(null);
        assertThat(tipoParametroDTO1).isNotEqualTo(tipoParametroDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(tipoParametroMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(tipoParametroMapper.fromId(null)).isNull();
    }
}
