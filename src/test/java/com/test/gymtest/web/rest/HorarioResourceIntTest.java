package com.test.gymtest.web.rest;

import com.test.gymtest.TestGymApp;

import com.test.gymtest.domain.Horario;
import com.test.gymtest.repository.HorarioRepository;
import com.test.gymtest.service.HorarioService;
import com.test.gymtest.service.dto.HorarioDTO;
import com.test.gymtest.service.mapper.HorarioMapper;
import com.test.gymtest.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.test.gymtest.web.rest.TestUtil.sameInstant;
import static com.test.gymtest.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the HorarioResource REST controller.
 *
 * @see HorarioResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestGymApp.class)
public class HorarioResourceIntTest {

    private static final String DEFAULT_DIA_ENTRENADOR = "A";
    private static final String UPDATED_DIA_ENTRENADOR = "B";

    private static final Boolean DEFAULT_DISPONIBILIDAD_ENTRENADOR = false;
    private static final Boolean UPDATED_DISPONIBILIDAD_ENTRENADOR = true;

    private static final ZonedDateTime DEFAULT_HORARIO_INICIO_HORARIO = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_HORARIO_INICIO_HORARIO = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_HORARIO_FIN_HORARIO = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_HORARIO_FIN_HORARIO = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_ESTADO_HORARIO = false;
    private static final Boolean UPDATED_ESTADO_HORARIO = true;

    @Autowired
    private HorarioRepository horarioRepository;

    @Autowired
    private HorarioMapper horarioMapper;

    @Autowired
    private HorarioService horarioService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restHorarioMockMvc;

    private Horario horario;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final HorarioResource horarioResource = new HorarioResource(horarioService);
        this.restHorarioMockMvc = MockMvcBuilders.standaloneSetup(horarioResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Horario createEntity(EntityManager em) {
        Horario horario = new Horario()
            .diaEntrenador(DEFAULT_DIA_ENTRENADOR)
            .disponibilidadEntrenador(DEFAULT_DISPONIBILIDAD_ENTRENADOR)
            .horarioInicioHorario(DEFAULT_HORARIO_INICIO_HORARIO)
            .horarioFinHorario(DEFAULT_HORARIO_FIN_HORARIO)
            .estadoHorario(DEFAULT_ESTADO_HORARIO);
        return horario;
    }

    @Before
    public void initTest() {
        horario = createEntity(em);
    }

    @Test
    @Transactional
    public void createHorario() throws Exception {
        int databaseSizeBeforeCreate = horarioRepository.findAll().size();

        // Create the Horario
        HorarioDTO horarioDTO = horarioMapper.toDto(horario);
        restHorarioMockMvc.perform(post("/api/horarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(horarioDTO)))
            .andExpect(status().isCreated());

        // Validate the Horario in the database
        List<Horario> horarioList = horarioRepository.findAll();
        assertThat(horarioList).hasSize(databaseSizeBeforeCreate + 1);
        Horario testHorario = horarioList.get(horarioList.size() - 1);
        assertThat(testHorario.getDiaEntrenador()).isEqualTo(DEFAULT_DIA_ENTRENADOR);
        assertThat(testHorario.isDisponibilidadEntrenador()).isEqualTo(DEFAULT_DISPONIBILIDAD_ENTRENADOR);
        assertThat(testHorario.getHorarioInicioHorario()).isEqualTo(DEFAULT_HORARIO_INICIO_HORARIO);
        assertThat(testHorario.getHorarioFinHorario()).isEqualTo(DEFAULT_HORARIO_FIN_HORARIO);
        assertThat(testHorario.isEstadoHorario()).isEqualTo(DEFAULT_ESTADO_HORARIO);
    }

    @Test
    @Transactional
    public void createHorarioWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = horarioRepository.findAll().size();

        // Create the Horario with an existing ID
        horario.setId(1L);
        HorarioDTO horarioDTO = horarioMapper.toDto(horario);

        // An entity with an existing ID cannot be created, so this API call must fail
        restHorarioMockMvc.perform(post("/api/horarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(horarioDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Horario in the database
        List<Horario> horarioList = horarioRepository.findAll();
        assertThat(horarioList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDiaEntrenadorIsRequired() throws Exception {
        int databaseSizeBeforeTest = horarioRepository.findAll().size();
        // set the field null
        horario.setDiaEntrenador(null);

        // Create the Horario, which fails.
        HorarioDTO horarioDTO = horarioMapper.toDto(horario);

        restHorarioMockMvc.perform(post("/api/horarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(horarioDTO)))
            .andExpect(status().isBadRequest());

        List<Horario> horarioList = horarioRepository.findAll();
        assertThat(horarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDisponibilidadEntrenadorIsRequired() throws Exception {
        int databaseSizeBeforeTest = horarioRepository.findAll().size();
        // set the field null
        horario.setDisponibilidadEntrenador(null);

        // Create the Horario, which fails.
        HorarioDTO horarioDTO = horarioMapper.toDto(horario);

        restHorarioMockMvc.perform(post("/api/horarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(horarioDTO)))
            .andExpect(status().isBadRequest());

        List<Horario> horarioList = horarioRepository.findAll();
        assertThat(horarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkHorarioInicioHorarioIsRequired() throws Exception {
        int databaseSizeBeforeTest = horarioRepository.findAll().size();
        // set the field null
        horario.setHorarioInicioHorario(null);

        // Create the Horario, which fails.
        HorarioDTO horarioDTO = horarioMapper.toDto(horario);

        restHorarioMockMvc.perform(post("/api/horarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(horarioDTO)))
            .andExpect(status().isBadRequest());

        List<Horario> horarioList = horarioRepository.findAll();
        assertThat(horarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkHorarioFinHorarioIsRequired() throws Exception {
        int databaseSizeBeforeTest = horarioRepository.findAll().size();
        // set the field null
        horario.setHorarioFinHorario(null);

        // Create the Horario, which fails.
        HorarioDTO horarioDTO = horarioMapper.toDto(horario);

        restHorarioMockMvc.perform(post("/api/horarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(horarioDTO)))
            .andExpect(status().isBadRequest());

        List<Horario> horarioList = horarioRepository.findAll();
        assertThat(horarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEstadoHorarioIsRequired() throws Exception {
        int databaseSizeBeforeTest = horarioRepository.findAll().size();
        // set the field null
        horario.setEstadoHorario(null);

        // Create the Horario, which fails.
        HorarioDTO horarioDTO = horarioMapper.toDto(horario);

        restHorarioMockMvc.perform(post("/api/horarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(horarioDTO)))
            .andExpect(status().isBadRequest());

        List<Horario> horarioList = horarioRepository.findAll();
        assertThat(horarioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllHorarios() throws Exception {
        // Initialize the database
        horarioRepository.saveAndFlush(horario);

        // Get all the horarioList
        restHorarioMockMvc.perform(get("/api/horarios?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(horario.getId().intValue())))
            .andExpect(jsonPath("$.[*].diaEntrenador").value(hasItem(DEFAULT_DIA_ENTRENADOR.toString())))
            .andExpect(jsonPath("$.[*].disponibilidadEntrenador").value(hasItem(DEFAULT_DISPONIBILIDAD_ENTRENADOR.booleanValue())))
            .andExpect(jsonPath("$.[*].horarioInicioHorario").value(hasItem(sameInstant(DEFAULT_HORARIO_INICIO_HORARIO))))
            .andExpect(jsonPath("$.[*].horarioFinHorario").value(hasItem(sameInstant(DEFAULT_HORARIO_FIN_HORARIO))))
            .andExpect(jsonPath("$.[*].estadoHorario").value(hasItem(DEFAULT_ESTADO_HORARIO.booleanValue())));
    }

    @Test
    @Transactional
    public void getHorario() throws Exception {
        // Initialize the database
        horarioRepository.saveAndFlush(horario);

        // Get the horario
        restHorarioMockMvc.perform(get("/api/horarios/{id}", horario.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(horario.getId().intValue()))
            .andExpect(jsonPath("$.diaEntrenador").value(DEFAULT_DIA_ENTRENADOR.toString()))
            .andExpect(jsonPath("$.disponibilidadEntrenador").value(DEFAULT_DISPONIBILIDAD_ENTRENADOR.booleanValue()))
            .andExpect(jsonPath("$.horarioInicioHorario").value(sameInstant(DEFAULT_HORARIO_INICIO_HORARIO)))
            .andExpect(jsonPath("$.horarioFinHorario").value(sameInstant(DEFAULT_HORARIO_FIN_HORARIO)))
            .andExpect(jsonPath("$.estadoHorario").value(DEFAULT_ESTADO_HORARIO.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingHorario() throws Exception {
        // Get the horario
        restHorarioMockMvc.perform(get("/api/horarios/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateHorario() throws Exception {
        // Initialize the database
        horarioRepository.saveAndFlush(horario);
        int databaseSizeBeforeUpdate = horarioRepository.findAll().size();

        // Update the horario
        Horario updatedHorario = horarioRepository.findOne(horario.getId());
        // Disconnect from session so that the updates on updatedHorario are not directly saved in db
        em.detach(updatedHorario);
        updatedHorario
            .diaEntrenador(UPDATED_DIA_ENTRENADOR)
            .disponibilidadEntrenador(UPDATED_DISPONIBILIDAD_ENTRENADOR)
            .horarioInicioHorario(UPDATED_HORARIO_INICIO_HORARIO)
            .horarioFinHorario(UPDATED_HORARIO_FIN_HORARIO)
            .estadoHorario(UPDATED_ESTADO_HORARIO);
        HorarioDTO horarioDTO = horarioMapper.toDto(updatedHorario);

        restHorarioMockMvc.perform(put("/api/horarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(horarioDTO)))
            .andExpect(status().isOk());

        // Validate the Horario in the database
        List<Horario> horarioList = horarioRepository.findAll();
        assertThat(horarioList).hasSize(databaseSizeBeforeUpdate);
        Horario testHorario = horarioList.get(horarioList.size() - 1);
        assertThat(testHorario.getDiaEntrenador()).isEqualTo(UPDATED_DIA_ENTRENADOR);
        assertThat(testHorario.isDisponibilidadEntrenador()).isEqualTo(UPDATED_DISPONIBILIDAD_ENTRENADOR);
        assertThat(testHorario.getHorarioInicioHorario()).isEqualTo(UPDATED_HORARIO_INICIO_HORARIO);
        assertThat(testHorario.getHorarioFinHorario()).isEqualTo(UPDATED_HORARIO_FIN_HORARIO);
        assertThat(testHorario.isEstadoHorario()).isEqualTo(UPDATED_ESTADO_HORARIO);
    }

    @Test
    @Transactional
    public void updateNonExistingHorario() throws Exception {
        int databaseSizeBeforeUpdate = horarioRepository.findAll().size();

        // Create the Horario
        HorarioDTO horarioDTO = horarioMapper.toDto(horario);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restHorarioMockMvc.perform(put("/api/horarios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(horarioDTO)))
            .andExpect(status().isCreated());

        // Validate the Horario in the database
        List<Horario> horarioList = horarioRepository.findAll();
        assertThat(horarioList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteHorario() throws Exception {
        // Initialize the database
        horarioRepository.saveAndFlush(horario);
        int databaseSizeBeforeDelete = horarioRepository.findAll().size();

        // Get the horario
        restHorarioMockMvc.perform(delete("/api/horarios/{id}", horario.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Horario> horarioList = horarioRepository.findAll();
        assertThat(horarioList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Horario.class);
        Horario horario1 = new Horario();
        horario1.setId(1L);
        Horario horario2 = new Horario();
        horario2.setId(horario1.getId());
        assertThat(horario1).isEqualTo(horario2);
        horario2.setId(2L);
        assertThat(horario1).isNotEqualTo(horario2);
        horario1.setId(null);
        assertThat(horario1).isNotEqualTo(horario2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(HorarioDTO.class);
        HorarioDTO horarioDTO1 = new HorarioDTO();
        horarioDTO1.setId(1L);
        HorarioDTO horarioDTO2 = new HorarioDTO();
        assertThat(horarioDTO1).isNotEqualTo(horarioDTO2);
        horarioDTO2.setId(horarioDTO1.getId());
        assertThat(horarioDTO1).isEqualTo(horarioDTO2);
        horarioDTO2.setId(2L);
        assertThat(horarioDTO1).isNotEqualTo(horarioDTO2);
        horarioDTO1.setId(null);
        assertThat(horarioDTO1).isNotEqualTo(horarioDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(horarioMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(horarioMapper.fromId(null)).isNull();
    }
}
