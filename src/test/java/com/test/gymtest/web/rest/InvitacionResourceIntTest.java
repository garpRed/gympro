package com.test.gymtest.web.rest;

import com.test.gymtest.TestGymApp;

import com.test.gymtest.domain.Invitacion;
import com.test.gymtest.repository.InvitacionRepository;
import com.test.gymtest.service.InvitacionService;
import com.test.gymtest.service.dto.InvitacionDTO;
import com.test.gymtest.service.mapper.InvitacionMapper;
import com.test.gymtest.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.test.gymtest.web.rest.TestUtil.sameInstant;
import static com.test.gymtest.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the InvitacionResource REST controller.
 *
 * @see InvitacionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestGymApp.class)
public class InvitacionResourceIntTest {

    private static final ZonedDateTime DEFAULT_HORARIO_INVITACION = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_HORARIO_INVITACION = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_ESTADO_INVITACION = false;
    private static final Boolean UPDATED_ESTADO_INVITACION = true;

    @Autowired
    private InvitacionRepository invitacionRepository;

    @Autowired
    private InvitacionMapper invitacionMapper;

    @Autowired
    private InvitacionService invitacionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restInvitacionMockMvc;

    private Invitacion invitacion;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final InvitacionResource invitacionResource = new InvitacionResource(invitacionService);
        this.restInvitacionMockMvc = MockMvcBuilders.standaloneSetup(invitacionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Invitacion createEntity(EntityManager em) {
        Invitacion invitacion = new Invitacion()
            .horarioInvitacion(DEFAULT_HORARIO_INVITACION)
            .estadoInvitacion(DEFAULT_ESTADO_INVITACION);
        return invitacion;
    }

    @Before
    public void initTest() {
        invitacion = createEntity(em);
    }

    @Test
    @Transactional
    public void createInvitacion() throws Exception {
        int databaseSizeBeforeCreate = invitacionRepository.findAll().size();

        // Create the Invitacion
        InvitacionDTO invitacionDTO = invitacionMapper.toDto(invitacion);
        restInvitacionMockMvc.perform(post("/api/invitacions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invitacionDTO)))
            .andExpect(status().isCreated());

        // Validate the Invitacion in the database
        List<Invitacion> invitacionList = invitacionRepository.findAll();
        assertThat(invitacionList).hasSize(databaseSizeBeforeCreate + 1);
        Invitacion testInvitacion = invitacionList.get(invitacionList.size() - 1);
        assertThat(testInvitacion.getHorarioInvitacion()).isEqualTo(DEFAULT_HORARIO_INVITACION);
        assertThat(testInvitacion.isEstadoInvitacion()).isEqualTo(DEFAULT_ESTADO_INVITACION);
    }

    @Test
    @Transactional
    public void createInvitacionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = invitacionRepository.findAll().size();

        // Create the Invitacion with an existing ID
        invitacion.setId(1L);
        InvitacionDTO invitacionDTO = invitacionMapper.toDto(invitacion);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInvitacionMockMvc.perform(post("/api/invitacions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invitacionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Invitacion in the database
        List<Invitacion> invitacionList = invitacionRepository.findAll();
        assertThat(invitacionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkHorarioInvitacionIsRequired() throws Exception {
        int databaseSizeBeforeTest = invitacionRepository.findAll().size();
        // set the field null
        invitacion.setHorarioInvitacion(null);

        // Create the Invitacion, which fails.
        InvitacionDTO invitacionDTO = invitacionMapper.toDto(invitacion);

        restInvitacionMockMvc.perform(post("/api/invitacions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invitacionDTO)))
            .andExpect(status().isBadRequest());

        List<Invitacion> invitacionList = invitacionRepository.findAll();
        assertThat(invitacionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEstadoInvitacionIsRequired() throws Exception {
        int databaseSizeBeforeTest = invitacionRepository.findAll().size();
        // set the field null
        invitacion.setEstadoInvitacion(null);

        // Create the Invitacion, which fails.
        InvitacionDTO invitacionDTO = invitacionMapper.toDto(invitacion);

        restInvitacionMockMvc.perform(post("/api/invitacions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invitacionDTO)))
            .andExpect(status().isBadRequest());

        List<Invitacion> invitacionList = invitacionRepository.findAll();
        assertThat(invitacionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllInvitacions() throws Exception {
        // Initialize the database
        invitacionRepository.saveAndFlush(invitacion);

        // Get all the invitacionList
        restInvitacionMockMvc.perform(get("/api/invitacions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(invitacion.getId().intValue())))
            .andExpect(jsonPath("$.[*].horarioInvitacion").value(hasItem(sameInstant(DEFAULT_HORARIO_INVITACION))))
            .andExpect(jsonPath("$.[*].estadoInvitacion").value(hasItem(DEFAULT_ESTADO_INVITACION.booleanValue())));
    }

    @Test
    @Transactional
    public void getInvitacion() throws Exception {
        // Initialize the database
        invitacionRepository.saveAndFlush(invitacion);

        // Get the invitacion
        restInvitacionMockMvc.perform(get("/api/invitacions/{id}", invitacion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(invitacion.getId().intValue()))
            .andExpect(jsonPath("$.horarioInvitacion").value(sameInstant(DEFAULT_HORARIO_INVITACION)))
            .andExpect(jsonPath("$.estadoInvitacion").value(DEFAULT_ESTADO_INVITACION.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingInvitacion() throws Exception {
        // Get the invitacion
        restInvitacionMockMvc.perform(get("/api/invitacions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInvitacion() throws Exception {
        // Initialize the database
        invitacionRepository.saveAndFlush(invitacion);
        int databaseSizeBeforeUpdate = invitacionRepository.findAll().size();

        // Update the invitacion
        Invitacion updatedInvitacion = invitacionRepository.findOne(invitacion.getId());
        // Disconnect from session so that the updates on updatedInvitacion are not directly saved in db
        em.detach(updatedInvitacion);
        updatedInvitacion
            .horarioInvitacion(UPDATED_HORARIO_INVITACION)
            .estadoInvitacion(UPDATED_ESTADO_INVITACION);
        InvitacionDTO invitacionDTO = invitacionMapper.toDto(updatedInvitacion);

        restInvitacionMockMvc.perform(put("/api/invitacions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invitacionDTO)))
            .andExpect(status().isOk());

        // Validate the Invitacion in the database
        List<Invitacion> invitacionList = invitacionRepository.findAll();
        assertThat(invitacionList).hasSize(databaseSizeBeforeUpdate);
        Invitacion testInvitacion = invitacionList.get(invitacionList.size() - 1);
        assertThat(testInvitacion.getHorarioInvitacion()).isEqualTo(UPDATED_HORARIO_INVITACION);
        assertThat(testInvitacion.isEstadoInvitacion()).isEqualTo(UPDATED_ESTADO_INVITACION);
    }

    @Test
    @Transactional
    public void updateNonExistingInvitacion() throws Exception {
        int databaseSizeBeforeUpdate = invitacionRepository.findAll().size();

        // Create the Invitacion
        InvitacionDTO invitacionDTO = invitacionMapper.toDto(invitacion);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restInvitacionMockMvc.perform(put("/api/invitacions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invitacionDTO)))
            .andExpect(status().isCreated());

        // Validate the Invitacion in the database
        List<Invitacion> invitacionList = invitacionRepository.findAll();
        assertThat(invitacionList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteInvitacion() throws Exception {
        // Initialize the database
        invitacionRepository.saveAndFlush(invitacion);
        int databaseSizeBeforeDelete = invitacionRepository.findAll().size();

        // Get the invitacion
        restInvitacionMockMvc.perform(delete("/api/invitacions/{id}", invitacion.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Invitacion> invitacionList = invitacionRepository.findAll();
        assertThat(invitacionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Invitacion.class);
        Invitacion invitacion1 = new Invitacion();
        invitacion1.setId(1L);
        Invitacion invitacion2 = new Invitacion();
        invitacion2.setId(invitacion1.getId());
        assertThat(invitacion1).isEqualTo(invitacion2);
        invitacion2.setId(2L);
        assertThat(invitacion1).isNotEqualTo(invitacion2);
        invitacion1.setId(null);
        assertThat(invitacion1).isNotEqualTo(invitacion2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(InvitacionDTO.class);
        InvitacionDTO invitacionDTO1 = new InvitacionDTO();
        invitacionDTO1.setId(1L);
        InvitacionDTO invitacionDTO2 = new InvitacionDTO();
        assertThat(invitacionDTO1).isNotEqualTo(invitacionDTO2);
        invitacionDTO2.setId(invitacionDTO1.getId());
        assertThat(invitacionDTO1).isEqualTo(invitacionDTO2);
        invitacionDTO2.setId(2L);
        assertThat(invitacionDTO1).isNotEqualTo(invitacionDTO2);
        invitacionDTO1.setId(null);
        assertThat(invitacionDTO1).isNotEqualTo(invitacionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(invitacionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(invitacionMapper.fromId(null)).isNull();
    }
}
