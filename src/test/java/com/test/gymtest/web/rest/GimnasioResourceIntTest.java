package com.test.gymtest.web.rest;

import com.test.gymtest.TestGymApp;

import com.test.gymtest.domain.Gimnasio;
import com.test.gymtest.repository.GimnasioRepository;
import com.test.gymtest.service.GimnasioService;
import com.test.gymtest.service.dto.GimnasioDTO;
import com.test.gymtest.service.mapper.GimnasioMapper;
import com.test.gymtest.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.test.gymtest.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the GimnasioResource REST controller.
 *
 * @see GimnasioResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestGymApp.class)
public class GimnasioResourceIntTest {

    private static final String DEFAULT_NOMBRE_GIMNASIO = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE_GIMNASIO = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ESTADO_GIMNASIO = false;
    private static final Boolean UPDATED_ESTADO_GIMNASIO = true;

    @Autowired
    private GimnasioRepository gimnasioRepository;

    @Autowired
    private GimnasioMapper gimnasioMapper;

    @Autowired
    private GimnasioService gimnasioService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restGimnasioMockMvc;

    private Gimnasio gimnasio;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final GimnasioResource gimnasioResource = new GimnasioResource(gimnasioService);
        this.restGimnasioMockMvc = MockMvcBuilders.standaloneSetup(gimnasioResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Gimnasio createEntity(EntityManager em) {
        Gimnasio gimnasio = new Gimnasio()
            .nombreGimnasio(DEFAULT_NOMBRE_GIMNASIO)
            .estadoGimnasio(DEFAULT_ESTADO_GIMNASIO);
        return gimnasio;
    }

    @Before
    public void initTest() {
        gimnasio = createEntity(em);
    }

    @Test
    @Transactional
    public void createGimnasio() throws Exception {
        int databaseSizeBeforeCreate = gimnasioRepository.findAll().size();

        // Create the Gimnasio
        GimnasioDTO gimnasioDTO = gimnasioMapper.toDto(gimnasio);
        restGimnasioMockMvc.perform(post("/api/gimnasios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(gimnasioDTO)))
            .andExpect(status().isCreated());

        // Validate the Gimnasio in the database
        List<Gimnasio> gimnasioList = gimnasioRepository.findAll();
        assertThat(gimnasioList).hasSize(databaseSizeBeforeCreate + 1);
        Gimnasio testGimnasio = gimnasioList.get(gimnasioList.size() - 1);
        assertThat(testGimnasio.getNombreGimnasio()).isEqualTo(DEFAULT_NOMBRE_GIMNASIO);
        assertThat(testGimnasio.isEstadoGimnasio()).isEqualTo(DEFAULT_ESTADO_GIMNASIO);
    }

    @Test
    @Transactional
    public void createGimnasioWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = gimnasioRepository.findAll().size();

        // Create the Gimnasio with an existing ID
        gimnasio.setId(1L);
        GimnasioDTO gimnasioDTO = gimnasioMapper.toDto(gimnasio);

        // An entity with an existing ID cannot be created, so this API call must fail
        restGimnasioMockMvc.perform(post("/api/gimnasios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(gimnasioDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Gimnasio in the database
        List<Gimnasio> gimnasioList = gimnasioRepository.findAll();
        assertThat(gimnasioList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNombreGimnasioIsRequired() throws Exception {
        int databaseSizeBeforeTest = gimnasioRepository.findAll().size();
        // set the field null
        gimnasio.setNombreGimnasio(null);

        // Create the Gimnasio, which fails.
        GimnasioDTO gimnasioDTO = gimnasioMapper.toDto(gimnasio);

        restGimnasioMockMvc.perform(post("/api/gimnasios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(gimnasioDTO)))
            .andExpect(status().isBadRequest());

        List<Gimnasio> gimnasioList = gimnasioRepository.findAll();
        assertThat(gimnasioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEstadoGimnasioIsRequired() throws Exception {
        int databaseSizeBeforeTest = gimnasioRepository.findAll().size();
        // set the field null
        gimnasio.setEstadoGimnasio(null);

        // Create the Gimnasio, which fails.
        GimnasioDTO gimnasioDTO = gimnasioMapper.toDto(gimnasio);

        restGimnasioMockMvc.perform(post("/api/gimnasios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(gimnasioDTO)))
            .andExpect(status().isBadRequest());

        List<Gimnasio> gimnasioList = gimnasioRepository.findAll();
        assertThat(gimnasioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllGimnasios() throws Exception {
        // Initialize the database
        gimnasioRepository.saveAndFlush(gimnasio);

        // Get all the gimnasioList
        restGimnasioMockMvc.perform(get("/api/gimnasios?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(gimnasio.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombreGimnasio").value(hasItem(DEFAULT_NOMBRE_GIMNASIO.toString())))
            .andExpect(jsonPath("$.[*].estadoGimnasio").value(hasItem(DEFAULT_ESTADO_GIMNASIO.booleanValue())));
    }

    @Test
    @Transactional
    public void getGimnasio() throws Exception {
        // Initialize the database
        gimnasioRepository.saveAndFlush(gimnasio);

        // Get the gimnasio
        restGimnasioMockMvc.perform(get("/api/gimnasios/{id}", gimnasio.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(gimnasio.getId().intValue()))
            .andExpect(jsonPath("$.nombreGimnasio").value(DEFAULT_NOMBRE_GIMNASIO.toString()))
            .andExpect(jsonPath("$.estadoGimnasio").value(DEFAULT_ESTADO_GIMNASIO.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingGimnasio() throws Exception {
        // Get the gimnasio
        restGimnasioMockMvc.perform(get("/api/gimnasios/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGimnasio() throws Exception {
        // Initialize the database
        gimnasioRepository.saveAndFlush(gimnasio);
        int databaseSizeBeforeUpdate = gimnasioRepository.findAll().size();

        // Update the gimnasio
        Gimnasio updatedGimnasio = gimnasioRepository.findOne(gimnasio.getId());
        // Disconnect from session so that the updates on updatedGimnasio are not directly saved in db
        em.detach(updatedGimnasio);
        updatedGimnasio
            .nombreGimnasio(UPDATED_NOMBRE_GIMNASIO)
            .estadoGimnasio(UPDATED_ESTADO_GIMNASIO);
        GimnasioDTO gimnasioDTO = gimnasioMapper.toDto(updatedGimnasio);

        restGimnasioMockMvc.perform(put("/api/gimnasios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(gimnasioDTO)))
            .andExpect(status().isOk());

        // Validate the Gimnasio in the database
        List<Gimnasio> gimnasioList = gimnasioRepository.findAll();
        assertThat(gimnasioList).hasSize(databaseSizeBeforeUpdate);
        Gimnasio testGimnasio = gimnasioList.get(gimnasioList.size() - 1);
        assertThat(testGimnasio.getNombreGimnasio()).isEqualTo(UPDATED_NOMBRE_GIMNASIO);
        assertThat(testGimnasio.isEstadoGimnasio()).isEqualTo(UPDATED_ESTADO_GIMNASIO);
    }

    @Test
    @Transactional
    public void updateNonExistingGimnasio() throws Exception {
        int databaseSizeBeforeUpdate = gimnasioRepository.findAll().size();

        // Create the Gimnasio
        GimnasioDTO gimnasioDTO = gimnasioMapper.toDto(gimnasio);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restGimnasioMockMvc.perform(put("/api/gimnasios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(gimnasioDTO)))
            .andExpect(status().isCreated());

        // Validate the Gimnasio in the database
        List<Gimnasio> gimnasioList = gimnasioRepository.findAll();
        assertThat(gimnasioList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteGimnasio() throws Exception {
        // Initialize the database
        gimnasioRepository.saveAndFlush(gimnasio);
        int databaseSizeBeforeDelete = gimnasioRepository.findAll().size();

        // Get the gimnasio
        restGimnasioMockMvc.perform(delete("/api/gimnasios/{id}", gimnasio.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Gimnasio> gimnasioList = gimnasioRepository.findAll();
        assertThat(gimnasioList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Gimnasio.class);
        Gimnasio gimnasio1 = new Gimnasio();
        gimnasio1.setId(1L);
        Gimnasio gimnasio2 = new Gimnasio();
        gimnasio2.setId(gimnasio1.getId());
        assertThat(gimnasio1).isEqualTo(gimnasio2);
        gimnasio2.setId(2L);
        assertThat(gimnasio1).isNotEqualTo(gimnasio2);
        gimnasio1.setId(null);
        assertThat(gimnasio1).isNotEqualTo(gimnasio2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(GimnasioDTO.class);
        GimnasioDTO gimnasioDTO1 = new GimnasioDTO();
        gimnasioDTO1.setId(1L);
        GimnasioDTO gimnasioDTO2 = new GimnasioDTO();
        assertThat(gimnasioDTO1).isNotEqualTo(gimnasioDTO2);
        gimnasioDTO2.setId(gimnasioDTO1.getId());
        assertThat(gimnasioDTO1).isEqualTo(gimnasioDTO2);
        gimnasioDTO2.setId(2L);
        assertThat(gimnasioDTO1).isNotEqualTo(gimnasioDTO2);
        gimnasioDTO1.setId(null);
        assertThat(gimnasioDTO1).isNotEqualTo(gimnasioDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(gimnasioMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(gimnasioMapper.fromId(null)).isNull();
    }
}
