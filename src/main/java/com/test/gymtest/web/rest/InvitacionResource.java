package com.test.gymtest.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.test.gymtest.service.InvitacionService;
import com.test.gymtest.web.rest.errors.BadRequestAlertException;
import com.test.gymtest.web.rest.util.HeaderUtil;
import com.test.gymtest.web.rest.util.PaginationUtil;
import com.test.gymtest.service.dto.InvitacionDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Invitacion.
 */
@RestController
@RequestMapping("/api")
public class InvitacionResource {

    private final Logger log = LoggerFactory.getLogger(InvitacionResource.class);

    private static final String ENTITY_NAME = "invitacion";

    private final InvitacionService invitacionService;

    public InvitacionResource(InvitacionService invitacionService) {
        this.invitacionService = invitacionService;
    }

    /**
     * POST  /invitacions : Create a new invitacion.
     *
     * @param invitacionDTO the invitacionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new invitacionDTO, or with status 400 (Bad Request) if the invitacion has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/invitacions")
    @Timed
    public ResponseEntity<InvitacionDTO> createInvitacion(@Valid @RequestBody InvitacionDTO invitacionDTO) throws URISyntaxException {
        log.debug("REST request to save Invitacion : {}", invitacionDTO);
        if (invitacionDTO.getId() != null) {
            throw new BadRequestAlertException("A new invitacion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        InvitacionDTO result = invitacionService.save(invitacionDTO);
        return ResponseEntity.created(new URI("/api/invitacions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /invitacions : Updates an existing invitacion.
     *
     * @param invitacionDTO the invitacionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated invitacionDTO,
     * or with status 400 (Bad Request) if the invitacionDTO is not valid,
     * or with status 500 (Internal Server Error) if the invitacionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/invitacions")
    @Timed
    public ResponseEntity<InvitacionDTO> updateInvitacion(@Valid @RequestBody InvitacionDTO invitacionDTO) throws URISyntaxException {
        log.debug("REST request to update Invitacion : {}", invitacionDTO);
        if (invitacionDTO.getId() == null) {
            return createInvitacion(invitacionDTO);
        }
        InvitacionDTO result = invitacionService.save(invitacionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, invitacionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /invitacions : get all the invitacions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of invitacions in body
     */
    @GetMapping("/invitacions")
    @Timed
    public ResponseEntity<List<InvitacionDTO>> getAllInvitacions(Pageable pageable) {
        log.debug("REST request to get a page of Invitacions");
        Page<InvitacionDTO> page = invitacionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/invitacions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /invitacions/:id : get the "id" invitacion.
     *
     * @param id the id of the invitacionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the invitacionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/invitacions/{id}")
    @Timed
    public ResponseEntity<InvitacionDTO> getInvitacion(@PathVariable Long id) {
        log.debug("REST request to get Invitacion : {}", id);
        InvitacionDTO invitacionDTO = invitacionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(invitacionDTO));
    }

    /**
     * DELETE  /invitacions/:id : delete the "id" invitacion.
     *
     * @param id the id of the invitacionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/invitacions/{id}")
    @Timed
    public ResponseEntity<Void> deleteInvitacion(@PathVariable Long id) {
        log.debug("REST request to delete Invitacion : {}", id);
        invitacionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
