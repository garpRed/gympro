package com.test.gymtest.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.test.gymtest.service.EjercicioXRutinaService;
import com.test.gymtest.web.rest.errors.BadRequestAlertException;
import com.test.gymtest.web.rest.util.HeaderUtil;
import com.test.gymtest.web.rest.util.PaginationUtil;
import com.test.gymtest.service.dto.EjercicioXRutinaDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing EjercicioXRutina.
 */
@RestController
@RequestMapping("/api")
public class EjercicioXRutinaResource {

    private final Logger log = LoggerFactory.getLogger(EjercicioXRutinaResource.class);

    private static final String ENTITY_NAME = "ejercicioXRutina";

    private final EjercicioXRutinaService ejercicioXRutinaService;

    public EjercicioXRutinaResource(EjercicioXRutinaService ejercicioXRutinaService) {
        this.ejercicioXRutinaService = ejercicioXRutinaService;
    }

    /**
     * POST  /ejercicio-x-rutinas : Create a new ejercicioXRutina.
     *
     * @param ejercicioXRutinaDTO the ejercicioXRutinaDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new ejercicioXRutinaDTO, or with status 400 (Bad Request) if the ejercicioXRutina has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/ejercicio-x-rutinas")
    @Timed
    public ResponseEntity<EjercicioXRutinaDTO> createEjercicioXRutina(@Valid @RequestBody EjercicioXRutinaDTO ejercicioXRutinaDTO) throws URISyntaxException {
        log.debug("REST request to save EjercicioXRutina : {}", ejercicioXRutinaDTO);
        if (ejercicioXRutinaDTO.getId() != null) {
            throw new BadRequestAlertException("A new ejercicioXRutina cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EjercicioXRutinaDTO result = ejercicioXRutinaService.save(ejercicioXRutinaDTO);
        return ResponseEntity.created(new URI("/api/ejercicio-x-rutinas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /ejercicio-x-rutinas : Updates an existing ejercicioXRutina.
     *
     * @param ejercicioXRutinaDTO the ejercicioXRutinaDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated ejercicioXRutinaDTO,
     * or with status 400 (Bad Request) if the ejercicioXRutinaDTO is not valid,
     * or with status 500 (Internal Server Error) if the ejercicioXRutinaDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/ejercicio-x-rutinas")
    @Timed
    public ResponseEntity<EjercicioXRutinaDTO> updateEjercicioXRutina(@Valid @RequestBody EjercicioXRutinaDTO ejercicioXRutinaDTO) throws URISyntaxException {
        log.debug("REST request to update EjercicioXRutina : {}", ejercicioXRutinaDTO);
        if (ejercicioXRutinaDTO.getId() == null) {
            return createEjercicioXRutina(ejercicioXRutinaDTO);
        }
        EjercicioXRutinaDTO result = ejercicioXRutinaService.save(ejercicioXRutinaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ejercicioXRutinaDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /ejercicio-x-rutinas : get all the ejercicioXRutinas.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of ejercicioXRutinas in body
     */
    @GetMapping("/ejercicio-x-rutinas")
    @Timed
    public ResponseEntity<List<EjercicioXRutinaDTO>> getAllEjercicioXRutinas(Pageable pageable) {
        log.debug("REST request to get a page of EjercicioXRutinas");
        Page<EjercicioXRutinaDTO> page = ejercicioXRutinaService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/ejercicio-x-rutinas");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /ejercicio-x-rutinas/:id : get the "id" ejercicioXRutina.
     *
     * @param id the id of the ejercicioXRutinaDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the ejercicioXRutinaDTO, or with status 404 (Not Found)
     */
    @GetMapping("/ejercicio-x-rutinas/{id}")
    @Timed
    public ResponseEntity<EjercicioXRutinaDTO> getEjercicioXRutina(@PathVariable Long id) {
        log.debug("REST request to get EjercicioXRutina : {}", id);
        EjercicioXRutinaDTO ejercicioXRutinaDTO = ejercicioXRutinaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(ejercicioXRutinaDTO));
    }

    /**
     * DELETE  /ejercicio-x-rutinas/:id : delete the "id" ejercicioXRutina.
     *
     * @param id the id of the ejercicioXRutinaDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/ejercicio-x-rutinas/{id}")
    @Timed
    public ResponseEntity<Void> deleteEjercicioXRutina(@PathVariable Long id) {
        log.debug("REST request to delete EjercicioXRutina : {}", id);
        ejercicioXRutinaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
