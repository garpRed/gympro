package com.test.gymtest.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.test.gymtest.service.SubRutinaService;
import com.test.gymtest.web.rest.errors.BadRequestAlertException;
import com.test.gymtest.web.rest.util.HeaderUtil;
import com.test.gymtest.web.rest.util.PaginationUtil;
import com.test.gymtest.service.dto.SubRutinaDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SubRutina.
 */
@RestController
@RequestMapping("/api")
public class SubRutinaResource {

    private final Logger log = LoggerFactory.getLogger(SubRutinaResource.class);

    private static final String ENTITY_NAME = "subRutina";

    private final SubRutinaService subRutinaService;

    public SubRutinaResource(SubRutinaService subRutinaService) {
        this.subRutinaService = subRutinaService;
    }

    /**
     * POST  /sub-rutinas : Create a new subRutina.
     *
     * @param subRutinaDTO the subRutinaDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new subRutinaDTO, or with status 400 (Bad Request) if the subRutina has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sub-rutinas")
    @Timed
    public ResponseEntity<SubRutinaDTO> createSubRutina(@Valid @RequestBody SubRutinaDTO subRutinaDTO) throws URISyntaxException {
        log.debug("REST request to save SubRutina : {}", subRutinaDTO);
        if (subRutinaDTO.getId() != null) {
            throw new BadRequestAlertException("A new subRutina cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SubRutinaDTO result = subRutinaService.save(subRutinaDTO);
        return ResponseEntity.created(new URI("/api/sub-rutinas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /sub-rutinas : Updates an existing subRutina.
     *
     * @param subRutinaDTO the subRutinaDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated subRutinaDTO,
     * or with status 400 (Bad Request) if the subRutinaDTO is not valid,
     * or with status 500 (Internal Server Error) if the subRutinaDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sub-rutinas")
    @Timed
    public ResponseEntity<SubRutinaDTO> updateSubRutina(@Valid @RequestBody SubRutinaDTO subRutinaDTO) throws URISyntaxException {
        log.debug("REST request to update SubRutina : {}", subRutinaDTO);
        if (subRutinaDTO.getId() == null) {
            return createSubRutina(subRutinaDTO);
        }
        SubRutinaDTO result = subRutinaService.save(subRutinaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, subRutinaDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /sub-rutinas : get all the subRutinas.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of subRutinas in body
     */
    @GetMapping("/sub-rutinas")
    @Timed
    public ResponseEntity<List<SubRutinaDTO>> getAllSubRutinas(Pageable pageable) {
        log.debug("REST request to get a page of SubRutinas");
        Page<SubRutinaDTO> page = subRutinaService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sub-rutinas");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /sub-rutinas/:id : get the "id" subRutina.
     *
     * @param id the id of the subRutinaDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the subRutinaDTO, or with status 404 (Not Found)
     */
    @GetMapping("/sub-rutinas/{id}")
    @Timed
    public ResponseEntity<SubRutinaDTO> getSubRutina(@PathVariable Long id) {
        log.debug("REST request to get SubRutina : {}", id);
        SubRutinaDTO subRutinaDTO = subRutinaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(subRutinaDTO));
    }

    /**
     * DELETE  /sub-rutinas/:id : delete the "id" subRutina.
     *
     * @param id the id of the subRutinaDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sub-rutinas/{id}")
    @Timed
    public ResponseEntity<Void> deleteSubRutina(@PathVariable Long id) {
        log.debug("REST request to delete SubRutina : {}", id);
        subRutinaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
