package com.test.gymtest.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.test.gymtest.service.MultimediaService;
import com.test.gymtest.web.rest.errors.BadRequestAlertException;
import com.test.gymtest.web.rest.util.HeaderUtil;
import com.test.gymtest.web.rest.util.PaginationUtil;
import com.test.gymtest.service.dto.MultimediaDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Multimedia.
 */
@RestController
@RequestMapping("/api")
public class MultimediaResource {

    private final Logger log = LoggerFactory.getLogger(MultimediaResource.class);

    private static final String ENTITY_NAME = "multimedia";

    private final MultimediaService multimediaService;

    public MultimediaResource(MultimediaService multimediaService) {
        this.multimediaService = multimediaService;
    }

    /**
     * POST  /multimedias : Create a new multimedia.
     *
     * @param multimediaDTO the multimediaDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new multimediaDTO, or with status 400 (Bad Request) if the multimedia has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/multimedias")
    @Timed
    public ResponseEntity<MultimediaDTO> createMultimedia(@Valid @RequestBody MultimediaDTO multimediaDTO) throws URISyntaxException {
        log.debug("REST request to save Multimedia : {}", multimediaDTO);
        if (multimediaDTO.getId() != null) {
            throw new BadRequestAlertException("A new multimedia cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MultimediaDTO result = multimediaService.save(multimediaDTO);
        return ResponseEntity.created(new URI("/api/multimedias/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /multimedias : Updates an existing multimedia.
     *
     * @param multimediaDTO the multimediaDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated multimediaDTO,
     * or with status 400 (Bad Request) if the multimediaDTO is not valid,
     * or with status 500 (Internal Server Error) if the multimediaDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/multimedias")
    @Timed
    public ResponseEntity<MultimediaDTO> updateMultimedia(@Valid @RequestBody MultimediaDTO multimediaDTO) throws URISyntaxException {
        log.debug("REST request to update Multimedia : {}", multimediaDTO);
        if (multimediaDTO.getId() == null) {
            return createMultimedia(multimediaDTO);
        }
        MultimediaDTO result = multimediaService.save(multimediaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, multimediaDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /multimedias : get all the multimedias.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of multimedias in body
     */
    @GetMapping("/multimedias")
    @Timed
    public ResponseEntity<List<MultimediaDTO>> getAllMultimedias(Pageable pageable) {
        log.debug("REST request to get a page of Multimedias");
        Page<MultimediaDTO> page = multimediaService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/multimedias");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /multimedias/:id : get the "id" multimedia.
     *
     * @param id the id of the multimediaDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the multimediaDTO, or with status 404 (Not Found)
     */
    @GetMapping("/multimedias/{id}")
    @Timed
    public ResponseEntity<MultimediaDTO> getMultimedia(@PathVariable Long id) {
        log.debug("REST request to get Multimedia : {}", id);
        MultimediaDTO multimediaDTO = multimediaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(multimediaDTO));
    }

    /**
     * DELETE  /multimedias/:id : delete the "id" multimedia.
     *
     * @param id the id of the multimediaDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/multimedias/{id}")
    @Timed
    public ResponseEntity<Void> deleteMultimedia(@PathVariable Long id) {
        log.debug("REST request to delete Multimedia : {}", id);
        multimediaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
