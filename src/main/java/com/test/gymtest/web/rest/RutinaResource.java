package com.test.gymtest.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.test.gymtest.service.RutinaService;
import com.test.gymtest.web.rest.errors.BadRequestAlertException;
import com.test.gymtest.web.rest.util.HeaderUtil;
import com.test.gymtest.web.rest.util.PaginationUtil;
import com.test.gymtest.service.dto.RutinaDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Rutina.
 */
@RestController
@RequestMapping("/api")
public class RutinaResource {

    private final Logger log = LoggerFactory.getLogger(RutinaResource.class);

    private static final String ENTITY_NAME = "rutina";

    private final RutinaService rutinaService;

    public RutinaResource(RutinaService rutinaService) {
        this.rutinaService = rutinaService;
    }

    /**
     * POST  /rutinas : Create a new rutina.
     *
     * @param rutinaDTO the rutinaDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new rutinaDTO, or with status 400 (Bad Request) if the rutina has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rutinas")
    @Timed
    public ResponseEntity<RutinaDTO> createRutina(@Valid @RequestBody RutinaDTO rutinaDTO) throws URISyntaxException {
        log.debug("REST request to save Rutina : {}", rutinaDTO);
        if (rutinaDTO.getId() != null) {
            throw new BadRequestAlertException("A new rutina cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RutinaDTO result = rutinaService.save(rutinaDTO);
        return ResponseEntity.created(new URI("/api/rutinas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /rutinas : Updates an existing rutina.
     *
     * @param rutinaDTO the rutinaDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated rutinaDTO,
     * or with status 400 (Bad Request) if the rutinaDTO is not valid,
     * or with status 500 (Internal Server Error) if the rutinaDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/rutinas")
    @Timed
    public ResponseEntity<RutinaDTO> updateRutina(@Valid @RequestBody RutinaDTO rutinaDTO) throws URISyntaxException {
        log.debug("REST request to update Rutina : {}", rutinaDTO);
        if (rutinaDTO.getId() == null) {
            return createRutina(rutinaDTO);
        }
        RutinaDTO result = rutinaService.save(rutinaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, rutinaDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /rutinas : get all the rutinas.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of rutinas in body
     */
    @GetMapping("/rutinas")
    @Timed
    public ResponseEntity<List<RutinaDTO>> getAllRutinas(Pageable pageable) {
        log.debug("REST request to get a page of Rutinas");
        Page<RutinaDTO> page = rutinaService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/rutinas");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /rutinas/:id : get the "id" rutina.
     *
     * @param id the id of the rutinaDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the rutinaDTO, or with status 404 (Not Found)
     */
    @GetMapping("/rutinas/{id}")
    @Timed
    public ResponseEntity<RutinaDTO> getRutina(@PathVariable Long id) {
        log.debug("REST request to get Rutina : {}", id);
        RutinaDTO rutinaDTO = rutinaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(rutinaDTO));
    }

    /**
     * DELETE  /rutinas/:id : delete the "id" rutina.
     *
     * @param id the id of the rutinaDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/rutinas/{id}")
    @Timed
    public ResponseEntity<Void> deleteRutina(@PathVariable Long id) {
        log.debug("REST request to delete Rutina : {}", id);
        rutinaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
