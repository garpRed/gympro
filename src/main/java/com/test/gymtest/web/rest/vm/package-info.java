/**
 * View Models used by Spring MVC REST controllers.
 */
package com.test.gymtest.web.rest.vm;
