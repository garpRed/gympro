package com.test.gymtest.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.test.gymtest.service.ActivoService;
import com.test.gymtest.web.rest.errors.BadRequestAlertException;
import com.test.gymtest.web.rest.util.HeaderUtil;
import com.test.gymtest.web.rest.util.PaginationUtil;
import com.test.gymtest.service.dto.ActivoDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Activo.
 */
@RestController
@RequestMapping("/api")
public class ActivoResource {

    private final Logger log = LoggerFactory.getLogger(ActivoResource.class);

    private static final String ENTITY_NAME = "activo";

    private final ActivoService activoService;

    public ActivoResource(ActivoService activoService) {
        this.activoService = activoService;
    }

    /**
     * POST  /activos : Create a new activo.
     *
     * @param activoDTO the activoDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new activoDTO, or with status 400 (Bad Request) if the activo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/activos")
    @Timed
    public ResponseEntity<ActivoDTO> createActivo(@Valid @RequestBody ActivoDTO activoDTO) throws URISyntaxException {
        log.debug("REST request to save Activo : {}", activoDTO);
        if (activoDTO.getId() != null) {
            throw new BadRequestAlertException("A new activo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ActivoDTO result = activoService.save(activoDTO);
        return ResponseEntity.created(new URI("/api/activos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /activos : Updates an existing activo.
     *
     * @param activoDTO the activoDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated activoDTO,
     * or with status 400 (Bad Request) if the activoDTO is not valid,
     * or with status 500 (Internal Server Error) if the activoDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/activos")
    @Timed
    public ResponseEntity<ActivoDTO> updateActivo(@Valid @RequestBody ActivoDTO activoDTO) throws URISyntaxException {
        log.debug("REST request to update Activo : {}", activoDTO);
        if (activoDTO.getId() == null) {
            return createActivo(activoDTO);
        }
        ActivoDTO result = activoService.save(activoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, activoDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /activos : get all the activos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of activos in body
     */
    @GetMapping("/activos")
    @Timed
    public ResponseEntity<List<ActivoDTO>> getAllActivos(Pageable pageable) {
        log.debug("REST request to get a page of Activos");
        Page<ActivoDTO> page = activoService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/activos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /activos/:id : get the "id" activo.
     *
     * @param id the id of the activoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the activoDTO, or with status 404 (Not Found)
     */
    @GetMapping("/activos/{id}")
    @Timed
    public ResponseEntity<ActivoDTO> getActivo(@PathVariable Long id) {
        log.debug("REST request to get Activo : {}", id);
        ActivoDTO activoDTO = activoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(activoDTO));
    }

    /**
     * DELETE  /activos/:id : delete the "id" activo.
     *
     * @param id the id of the activoDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/activos/{id}")
    @Timed
    public ResponseEntity<Void> deleteActivo(@PathVariable Long id) {
        log.debug("REST request to delete Activo : {}", id);
        activoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
