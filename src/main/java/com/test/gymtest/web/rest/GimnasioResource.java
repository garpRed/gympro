package com.test.gymtest.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.test.gymtest.service.GimnasioService;
import com.test.gymtest.web.rest.errors.BadRequestAlertException;
import com.test.gymtest.web.rest.util.HeaderUtil;
import com.test.gymtest.web.rest.util.PaginationUtil;
import com.test.gymtest.service.dto.GimnasioDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Gimnasio.
 */
@RestController
@RequestMapping("/api")
public class GimnasioResource {

    private final Logger log = LoggerFactory.getLogger(GimnasioResource.class);

    private static final String ENTITY_NAME = "gimnasio";

    private final GimnasioService gimnasioService;

    public GimnasioResource(GimnasioService gimnasioService) {
        this.gimnasioService = gimnasioService;
    }

    /**
     * POST  /gimnasios : Create a new gimnasio.
     *
     * @param gimnasioDTO the gimnasioDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new gimnasioDTO, or with status 400 (Bad Request) if the gimnasio has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/gimnasios")
    @Timed
    public ResponseEntity<GimnasioDTO> createGimnasio(@Valid @RequestBody GimnasioDTO gimnasioDTO) throws URISyntaxException {
        log.debug("REST request to save Gimnasio : {}", gimnasioDTO);
        if (gimnasioDTO.getId() != null) {
            throw new BadRequestAlertException("A new gimnasio cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GimnasioDTO result = gimnasioService.save(gimnasioDTO);
        return ResponseEntity.created(new URI("/api/gimnasios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /gimnasios : Updates an existing gimnasio.
     *
     * @param gimnasioDTO the gimnasioDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated gimnasioDTO,
     * or with status 400 (Bad Request) if the gimnasioDTO is not valid,
     * or with status 500 (Internal Server Error) if the gimnasioDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/gimnasios")
    @Timed
    public ResponseEntity<GimnasioDTO> updateGimnasio(@Valid @RequestBody GimnasioDTO gimnasioDTO) throws URISyntaxException {
        log.debug("REST request to update Gimnasio : {}", gimnasioDTO);
        if (gimnasioDTO.getId() == null) {
            return createGimnasio(gimnasioDTO);
        }
        GimnasioDTO result = gimnasioService.save(gimnasioDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, gimnasioDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /gimnasios : get all the gimnasios.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of gimnasios in body
     */
    @GetMapping("/gimnasios")
    @Timed
    public ResponseEntity<List<GimnasioDTO>> getAllGimnasios(Pageable pageable) {
        log.debug("REST request to get a page of Gimnasios");
        Page<GimnasioDTO> page = gimnasioService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/gimnasios");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /gimnasios/:id : get the "id" gimnasio.
     *
     * @param id the id of the gimnasioDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the gimnasioDTO, or with status 404 (Not Found)
     */
    @GetMapping("/gimnasios/{id}")
    @Timed
    public ResponseEntity<GimnasioDTO> getGimnasio(@PathVariable Long id) {
        log.debug("REST request to get Gimnasio : {}", id);
        GimnasioDTO gimnasioDTO = gimnasioService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(gimnasioDTO));
    }

    /**
     * DELETE  /gimnasios/:id : delete the "id" gimnasio.
     *
     * @param id the id of the gimnasioDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/gimnasios/{id}")
    @Timed
    public ResponseEntity<Void> deleteGimnasio(@PathVariable Long id) {
        log.debug("REST request to delete Gimnasio : {}", id);
        gimnasioService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
