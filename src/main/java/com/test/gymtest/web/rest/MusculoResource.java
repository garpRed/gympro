package com.test.gymtest.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.test.gymtest.service.MusculoService;
import com.test.gymtest.web.rest.errors.BadRequestAlertException;
import com.test.gymtest.web.rest.util.HeaderUtil;
import com.test.gymtest.web.rest.util.PaginationUtil;
import com.test.gymtest.service.dto.MusculoDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Musculo.
 */
@RestController
@RequestMapping("/api")
public class MusculoResource {

    private final Logger log = LoggerFactory.getLogger(MusculoResource.class);

    private static final String ENTITY_NAME = "musculo";

    private final MusculoService musculoService;

    public MusculoResource(MusculoService musculoService) {
        this.musculoService = musculoService;
    }

    /**
     * POST  /musculos : Create a new musculo.
     *
     * @param musculoDTO the musculoDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new musculoDTO, or with status 400 (Bad Request) if the musculo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/musculos")
    @Timed
    public ResponseEntity<MusculoDTO> createMusculo(@Valid @RequestBody MusculoDTO musculoDTO) throws URISyntaxException {
        log.debug("REST request to save Musculo : {}", musculoDTO);
        if (musculoDTO.getId() != null) {
            throw new BadRequestAlertException("A new musculo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MusculoDTO result = musculoService.save(musculoDTO);
        return ResponseEntity.created(new URI("/api/musculos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /musculos : Updates an existing musculo.
     *
     * @param musculoDTO the musculoDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated musculoDTO,
     * or with status 400 (Bad Request) if the musculoDTO is not valid,
     * or with status 500 (Internal Server Error) if the musculoDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/musculos")
    @Timed
    public ResponseEntity<MusculoDTO> updateMusculo(@Valid @RequestBody MusculoDTO musculoDTO) throws URISyntaxException {
        log.debug("REST request to update Musculo : {}", musculoDTO);
        if (musculoDTO.getId() == null) {
            return createMusculo(musculoDTO);
        }
        MusculoDTO result = musculoService.save(musculoDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, musculoDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /musculos : get all the musculos.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of musculos in body
     */
    @GetMapping("/musculos")
    @Timed
    public ResponseEntity<List<MusculoDTO>> getAllMusculos(Pageable pageable) {
        log.debug("REST request to get a page of Musculos");
        Page<MusculoDTO> page = musculoService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/musculos");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /musculos/:id : get the "id" musculo.
     *
     * @param id the id of the musculoDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the musculoDTO, or with status 404 (Not Found)
     */
    @GetMapping("/musculos/{id}")
    @Timed
    public ResponseEntity<MusculoDTO> getMusculo(@PathVariable Long id) {
        log.debug("REST request to get Musculo : {}", id);
        MusculoDTO musculoDTO = musculoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(musculoDTO));
    }

    /**
     * DELETE  /musculos/:id : delete the "id" musculo.
     *
     * @param id the id of the musculoDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/musculos/{id}")
    @Timed
    public ResponseEntity<Void> deleteMusculo(@PathVariable Long id) {
        log.debug("REST request to delete Musculo : {}", id);
        musculoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
