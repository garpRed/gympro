package com.test.gymtest.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.test.gymtest.service.TipoParametroService;
import com.test.gymtest.web.rest.errors.BadRequestAlertException;
import com.test.gymtest.web.rest.util.HeaderUtil;
import com.test.gymtest.web.rest.util.PaginationUtil;
import com.test.gymtest.service.dto.TipoParametroDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TipoParametro.
 */
@RestController
@RequestMapping("/api")
public class TipoParametroResource {

    private final Logger log = LoggerFactory.getLogger(TipoParametroResource.class);

    private static final String ENTITY_NAME = "tipoParametro";

    private final TipoParametroService tipoParametroService;

    public TipoParametroResource(TipoParametroService tipoParametroService) {
        this.tipoParametroService = tipoParametroService;
    }

    /**
     * POST  /tipo-parametros : Create a new tipoParametro.
     *
     * @param tipoParametroDTO the tipoParametroDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tipoParametroDTO, or with status 400 (Bad Request) if the tipoParametro has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tipo-parametros")
    @Timed
    public ResponseEntity<TipoParametroDTO> createTipoParametro(@Valid @RequestBody TipoParametroDTO tipoParametroDTO) throws URISyntaxException {
        log.debug("REST request to save TipoParametro : {}", tipoParametroDTO);
        if (tipoParametroDTO.getId() != null) {
            throw new BadRequestAlertException("A new tipoParametro cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TipoParametroDTO result = tipoParametroService.save(tipoParametroDTO);
        return ResponseEntity.created(new URI("/api/tipo-parametros/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tipo-parametros : Updates an existing tipoParametro.
     *
     * @param tipoParametroDTO the tipoParametroDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tipoParametroDTO,
     * or with status 400 (Bad Request) if the tipoParametroDTO is not valid,
     * or with status 500 (Internal Server Error) if the tipoParametroDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tipo-parametros")
    @Timed
    public ResponseEntity<TipoParametroDTO> updateTipoParametro(@Valid @RequestBody TipoParametroDTO tipoParametroDTO) throws URISyntaxException {
        log.debug("REST request to update TipoParametro : {}", tipoParametroDTO);
        if (tipoParametroDTO.getId() == null) {
            return createTipoParametro(tipoParametroDTO);
        }
        TipoParametroDTO result = tipoParametroService.save(tipoParametroDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tipoParametroDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tipo-parametros : get all the tipoParametros.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of tipoParametros in body
     */
    @GetMapping("/tipo-parametros")
    @Timed
    public ResponseEntity<List<TipoParametroDTO>> getAllTipoParametros(Pageable pageable) {
        log.debug("REST request to get a page of TipoParametros");
        Page<TipoParametroDTO> page = tipoParametroService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/tipo-parametros");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /tipo-parametros/:id : get the "id" tipoParametro.
     *
     * @param id the id of the tipoParametroDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tipoParametroDTO, or with status 404 (Not Found)
     */
    @GetMapping("/tipo-parametros/{id}")
    @Timed
    public ResponseEntity<TipoParametroDTO> getTipoParametro(@PathVariable Long id) {
        log.debug("REST request to get TipoParametro : {}", id);
        TipoParametroDTO tipoParametroDTO = tipoParametroService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tipoParametroDTO));
    }

    /**
     * DELETE  /tipo-parametros/:id : delete the "id" tipoParametro.
     *
     * @param id the id of the tipoParametroDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tipo-parametros/{id}")
    @Timed
    public ResponseEntity<Void> deleteTipoParametro(@PathVariable Long id) {
        log.debug("REST request to delete TipoParametro : {}", id);
        tipoParametroService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
