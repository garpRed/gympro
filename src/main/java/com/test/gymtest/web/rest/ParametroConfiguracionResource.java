package com.test.gymtest.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.test.gymtest.service.ParametroConfiguracionService;
import com.test.gymtest.web.rest.errors.BadRequestAlertException;
import com.test.gymtest.web.rest.util.HeaderUtil;
import com.test.gymtest.web.rest.util.PaginationUtil;
import com.test.gymtest.service.dto.ParametroConfiguracionDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing ParametroConfiguracion.
 */
@RestController
@RequestMapping("/api")
public class ParametroConfiguracionResource {

    private final Logger log = LoggerFactory.getLogger(ParametroConfiguracionResource.class);

    private static final String ENTITY_NAME = "parametroConfiguracion";

    private final ParametroConfiguracionService parametroConfiguracionService;

    public ParametroConfiguracionResource(ParametroConfiguracionService parametroConfiguracionService) {
        this.parametroConfiguracionService = parametroConfiguracionService;
    }

    /**
     * POST  /parametro-configuracions : Create a new parametroConfiguracion.
     *
     * @param parametroConfiguracionDTO the parametroConfiguracionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new parametroConfiguracionDTO, or with status 400 (Bad Request) if the parametroConfiguracion has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/parametro-configuracions")
    @Timed
    public ResponseEntity<ParametroConfiguracionDTO> createParametroConfiguracion(@Valid @RequestBody ParametroConfiguracionDTO parametroConfiguracionDTO) throws URISyntaxException {
        log.debug("REST request to save ParametroConfiguracion : {}", parametroConfiguracionDTO);
        if (parametroConfiguracionDTO.getId() != null) {
            throw new BadRequestAlertException("A new parametroConfiguracion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ParametroConfiguracionDTO result = parametroConfiguracionService.save(parametroConfiguracionDTO);
        return ResponseEntity.created(new URI("/api/parametro-configuracions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /parametro-configuracions : Updates an existing parametroConfiguracion.
     *
     * @param parametroConfiguracionDTO the parametroConfiguracionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated parametroConfiguracionDTO,
     * or with status 400 (Bad Request) if the parametroConfiguracionDTO is not valid,
     * or with status 500 (Internal Server Error) if the parametroConfiguracionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/parametro-configuracions")
    @Timed
    public ResponseEntity<ParametroConfiguracionDTO> updateParametroConfiguracion(@Valid @RequestBody ParametroConfiguracionDTO parametroConfiguracionDTO) throws URISyntaxException {
        log.debug("REST request to update ParametroConfiguracion : {}", parametroConfiguracionDTO);
        if (parametroConfiguracionDTO.getId() == null) {
            return createParametroConfiguracion(parametroConfiguracionDTO);
        }
        ParametroConfiguracionDTO result = parametroConfiguracionService.save(parametroConfiguracionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, parametroConfiguracionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /parametro-configuracions : get all the parametroConfiguracions.
     *
     * @param pageable the pagination information
     * @param filter the filter of the request
     * @return the ResponseEntity with status 200 (OK) and the list of parametroConfiguracions in body
     */
    @GetMapping("/parametro-configuracions")
    @Timed
    public ResponseEntity<List<ParametroConfiguracionDTO>> getAllParametroConfiguracions(Pageable pageable, @RequestParam(required = false) String filter) {
        if ("tipomultimedia-is-null".equals(filter)) {
            log.debug("REST request to get all ParametroConfiguracions where tipoMultimedia is null");
            return new ResponseEntity<>(parametroConfiguracionService.findAllWhereTipoMultimediaIsNull(),
                    HttpStatus.OK);
        }
        if ("zonamusculo-is-null".equals(filter)) {
            log.debug("REST request to get all ParametroConfiguracions where zonaMusculo is null");
            return new ResponseEntity<>(parametroConfiguracionService.findAllWhereZonaMusculoIsNull(),
                    HttpStatus.OK);
        }
        log.debug("REST request to get a page of ParametroConfiguracions");
        Page<ParametroConfiguracionDTO> page = parametroConfiguracionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/parametro-configuracions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /parametro-configuracions/:id : get the "id" parametroConfiguracion.
     *
     * @param id the id of the parametroConfiguracionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the parametroConfiguracionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/parametro-configuracions/{id}")
    @Timed
    public ResponseEntity<ParametroConfiguracionDTO> getParametroConfiguracion(@PathVariable Long id) {
        log.debug("REST request to get ParametroConfiguracion : {}", id);
        ParametroConfiguracionDTO parametroConfiguracionDTO = parametroConfiguracionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(parametroConfiguracionDTO));
    }

    /**
     * DELETE  /parametro-configuracions/:id : delete the "id" parametroConfiguracion.
     *
     * @param id the id of the parametroConfiguracionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/parametro-configuracions/{id}")
    @Timed
    public ResponseEntity<Void> deleteParametroConfiguracion(@PathVariable Long id) {
        log.debug("REST request to delete ParametroConfiguracion : {}", id);
        parametroConfiguracionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
