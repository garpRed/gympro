package com.test.gymtest.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.test.gymtest.service.RegistroActividadRutinaService;
import com.test.gymtest.web.rest.errors.BadRequestAlertException;
import com.test.gymtest.web.rest.util.HeaderUtil;
import com.test.gymtest.web.rest.util.PaginationUtil;
import com.test.gymtest.service.dto.RegistroActividadRutinaDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing RegistroActividadRutina.
 */
@RestController
@RequestMapping("/api")
public class RegistroActividadRutinaResource {

    private final Logger log = LoggerFactory.getLogger(RegistroActividadRutinaResource.class);

    private static final String ENTITY_NAME = "registroActividadRutina";

    private final RegistroActividadRutinaService registroActividadRutinaService;

    public RegistroActividadRutinaResource(RegistroActividadRutinaService registroActividadRutinaService) {
        this.registroActividadRutinaService = registroActividadRutinaService;
    }

    /**
     * POST  /registro-actividad-rutinas : Create a new registroActividadRutina.
     *
     * @param registroActividadRutinaDTO the registroActividadRutinaDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new registroActividadRutinaDTO, or with status 400 (Bad Request) if the registroActividadRutina has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/registro-actividad-rutinas")
    @Timed
    public ResponseEntity<RegistroActividadRutinaDTO> createRegistroActividadRutina(@Valid @RequestBody RegistroActividadRutinaDTO registroActividadRutinaDTO) throws URISyntaxException {
        log.debug("REST request to save RegistroActividadRutina : {}", registroActividadRutinaDTO);
        if (registroActividadRutinaDTO.getId() != null) {
            throw new BadRequestAlertException("A new registroActividadRutina cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegistroActividadRutinaDTO result = registroActividadRutinaService.save(registroActividadRutinaDTO);
        return ResponseEntity.created(new URI("/api/registro-actividad-rutinas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /registro-actividad-rutinas : Updates an existing registroActividadRutina.
     *
     * @param registroActividadRutinaDTO the registroActividadRutinaDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated registroActividadRutinaDTO,
     * or with status 400 (Bad Request) if the registroActividadRutinaDTO is not valid,
     * or with status 500 (Internal Server Error) if the registroActividadRutinaDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/registro-actividad-rutinas")
    @Timed
    public ResponseEntity<RegistroActividadRutinaDTO> updateRegistroActividadRutina(@Valid @RequestBody RegistroActividadRutinaDTO registroActividadRutinaDTO) throws URISyntaxException {
        log.debug("REST request to update RegistroActividadRutina : {}", registroActividadRutinaDTO);
        if (registroActividadRutinaDTO.getId() == null) {
            return createRegistroActividadRutina(registroActividadRutinaDTO);
        }
        RegistroActividadRutinaDTO result = registroActividadRutinaService.save(registroActividadRutinaDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, registroActividadRutinaDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /registro-actividad-rutinas : get all the registroActividadRutinas.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of registroActividadRutinas in body
     */
    @GetMapping("/registro-actividad-rutinas")
    @Timed
    public ResponseEntity<List<RegistroActividadRutinaDTO>> getAllRegistroActividadRutinas(Pageable pageable) {
        log.debug("REST request to get a page of RegistroActividadRutinas");
        Page<RegistroActividadRutinaDTO> page = registroActividadRutinaService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/registro-actividad-rutinas");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /registro-actividad-rutinas/:id : get the "id" registroActividadRutina.
     *
     * @param id the id of the registroActividadRutinaDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the registroActividadRutinaDTO, or with status 404 (Not Found)
     */
    @GetMapping("/registro-actividad-rutinas/{id}")
    @Timed
    public ResponseEntity<RegistroActividadRutinaDTO> getRegistroActividadRutina(@PathVariable Long id) {
        log.debug("REST request to get RegistroActividadRutina : {}", id);
        RegistroActividadRutinaDTO registroActividadRutinaDTO = registroActividadRutinaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(registroActividadRutinaDTO));
    }

    /**
     * DELETE  /registro-actividad-rutinas/:id : delete the "id" registroActividadRutina.
     *
     * @param id the id of the registroActividadRutinaDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/registro-actividad-rutinas/{id}")
    @Timed
    public ResponseEntity<Void> deleteRegistroActividadRutina(@PathVariable Long id) {
        log.debug("REST request to delete RegistroActividadRutina : {}", id);
        registroActividadRutinaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
