package com.test.gymtest.repository;

import com.test.gymtest.domain.Invitacion;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Invitacion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InvitacionRepository extends JpaRepository<Invitacion, Long> {

}
