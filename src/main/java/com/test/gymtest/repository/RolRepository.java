package com.test.gymtest.repository;

import com.test.gymtest.domain.Rol;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Rol entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RolRepository extends JpaRepository<Rol, Long> {
    @Query("select distinct rol from Rol rol left join fetch rol.rolXPermisos")
    List<Rol> findAllWithEagerRelationships();

    @Query("select rol from Rol rol left join fetch rol.rolXPermisos where rol.id =:id")
    Rol findOneWithEagerRelationships(@Param("id") Long id);

}
