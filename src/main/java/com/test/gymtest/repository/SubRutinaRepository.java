package com.test.gymtest.repository;

import com.test.gymtest.domain.SubRutina;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the SubRutina entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SubRutinaRepository extends JpaRepository<SubRutina, Long> {
    @Query("select distinct sub_rutina from SubRutina sub_rutina left join fetch sub_rutina.subrutinaEjercicios")
    List<SubRutina> findAllWithEagerRelationships();

    @Query("select sub_rutina from SubRutina sub_rutina left join fetch sub_rutina.subrutinaEjercicios where sub_rutina.id =:id")
    SubRutina findOneWithEagerRelationships(@Param("id") Long id);

}
