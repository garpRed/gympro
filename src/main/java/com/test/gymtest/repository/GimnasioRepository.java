package com.test.gymtest.repository;

import com.test.gymtest.domain.Gimnasio;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Gimnasio entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GimnasioRepository extends JpaRepository<Gimnasio, Long> {

}
