package com.test.gymtest.repository;

import com.test.gymtest.domain.Musculo;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Musculo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MusculoRepository extends JpaRepository<Musculo, Long> {

}
