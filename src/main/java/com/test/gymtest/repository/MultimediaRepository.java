package com.test.gymtest.repository;

import com.test.gymtest.domain.Multimedia;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Multimedia entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MultimediaRepository extends JpaRepository<Multimedia, Long> {

}
