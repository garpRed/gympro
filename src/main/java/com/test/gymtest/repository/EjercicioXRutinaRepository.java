package com.test.gymtest.repository;

import com.test.gymtest.domain.EjercicioXRutina;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the EjercicioXRutina entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EjercicioXRutinaRepository extends JpaRepository<EjercicioXRutina, Long> {

}
