package com.test.gymtest.repository;

import com.test.gymtest.domain.Horario;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Horario entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HorarioRepository extends JpaRepository<Horario, Long> {
    @Query("select distinct horario from Horario horario left join fetch horario.horariosParametros")
    List<Horario> findAllWithEagerRelationships();

    @Query("select horario from Horario horario left join fetch horario.horariosParametros where horario.id =:id")
    Horario findOneWithEagerRelationships(@Param("id") Long id);

}
