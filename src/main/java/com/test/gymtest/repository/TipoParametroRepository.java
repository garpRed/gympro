package com.test.gymtest.repository;

import com.test.gymtest.domain.TipoParametro;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the TipoParametro entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipoParametroRepository extends JpaRepository<TipoParametro, Long> {

}
