package com.test.gymtest.repository;

import com.test.gymtest.domain.Activo;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Activo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ActivoRepository extends JpaRepository<Activo, Long> {

}
