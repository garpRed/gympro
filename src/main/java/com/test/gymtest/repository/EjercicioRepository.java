package com.test.gymtest.repository;

import com.test.gymtest.domain.Ejercicio;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Ejercicio entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EjercicioRepository extends JpaRepository<Ejercicio, Long> {
    @Query("select distinct ejercicio from Ejercicio ejercicio left join fetch ejercicio.ejercicioActivos left join fetch ejercicio.ejercicioMusculos")
    List<Ejercicio> findAllWithEagerRelationships();

    @Query("select ejercicio from Ejercicio ejercicio left join fetch ejercicio.ejercicioActivos left join fetch ejercicio.ejercicioMusculos where ejercicio.id =:id")
    Ejercicio findOneWithEagerRelationships(@Param("id") Long id);

}
