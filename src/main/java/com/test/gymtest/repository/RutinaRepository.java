package com.test.gymtest.repository;

import com.test.gymtest.domain.Rutina;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Rutina entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RutinaRepository extends JpaRepository<Rutina, Long> {

}
