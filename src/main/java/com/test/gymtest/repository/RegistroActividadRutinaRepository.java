package com.test.gymtest.repository;

import com.test.gymtest.domain.RegistroActividadRutina;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the RegistroActividadRutina entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegistroActividadRutinaRepository extends JpaRepository<RegistroActividadRutina, Long> {

}
