package com.test.gymtest.repository;

import com.test.gymtest.domain.ParametroConfiguracion;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ParametroConfiguracion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ParametroConfiguracionRepository extends JpaRepository<ParametroConfiguracion, Long> {

}
