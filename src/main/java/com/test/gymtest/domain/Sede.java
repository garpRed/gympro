package com.test.gymtest.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * Class Sede.
 * @author Bulletproof.
 */
@ApiModel(description = "Class Sede. @author Bulletproof.")
@Entity
@Table(name = "sede")
public class Sede implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 999)
    @Column(name = "nombre_sede", length = 999, nullable = false)
    private String nombreSede;

    @NotNull
    @Size(max = 999)
    @Column(name = "direccion_sede", length = 999, nullable = false)
    private String direccionSede;

    @NotNull
    @Column(name = "estado_sede", nullable = false)
    private Boolean estadoSede;

    @OneToMany(mappedBy = "sede")
    @JsonIgnore
    private Set<Activo> activosSedes = new HashSet<>();

    @OneToMany(mappedBy = "sede")
    @JsonIgnore
    private Set<Ejercicio> ejerciciosSedes = new HashSet<>();

    @ManyToOne
    private Gimnasio gimnasio;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreSede() {
        return nombreSede;
    }

    public Sede nombreSede(String nombreSede) {
        this.nombreSede = nombreSede;
        return this;
    }

    public void setNombreSede(String nombreSede) {
        this.nombreSede = nombreSede;
    }

    public String getDireccionSede() {
        return direccionSede;
    }

    public Sede direccionSede(String direccionSede) {
        this.direccionSede = direccionSede;
        return this;
    }

    public void setDireccionSede(String direccionSede) {
        this.direccionSede = direccionSede;
    }

    public Boolean isEstadoSede() {
        return estadoSede;
    }

    public Sede estadoSede(Boolean estadoSede) {
        this.estadoSede = estadoSede;
        return this;
    }

    public void setEstadoSede(Boolean estadoSede) {
        this.estadoSede = estadoSede;
    }

    public Set<Activo> getActivosSedes() {
        return activosSedes;
    }

    public Sede activosSedes(Set<Activo> activos) {
        this.activosSedes = activos;
        return this;
    }

    public Sede addActivosSede(Activo activo) {
        this.activosSedes.add(activo);
        activo.setSede(this);
        return this;
    }

    public Sede removeActivosSede(Activo activo) {
        this.activosSedes.remove(activo);
        activo.setSede(null);
        return this;
    }

    public void setActivosSedes(Set<Activo> activos) {
        this.activosSedes = activos;
    }

    public Set<Ejercicio> getEjerciciosSedes() {
        return ejerciciosSedes;
    }

    public Sede ejerciciosSedes(Set<Ejercicio> ejercicios) {
        this.ejerciciosSedes = ejercicios;
        return this;
    }

    public Sede addEjerciciosSede(Ejercicio ejercicio) {
        this.ejerciciosSedes.add(ejercicio);
        ejercicio.setSede(this);
        return this;
    }

    public Sede removeEjerciciosSede(Ejercicio ejercicio) {
        this.ejerciciosSedes.remove(ejercicio);
        ejercicio.setSede(null);
        return this;
    }

    public void setEjerciciosSedes(Set<Ejercicio> ejercicios) {
        this.ejerciciosSedes = ejercicios;
    }

    public Gimnasio getGimnasio() {
        return gimnasio;
    }

    public Sede gimnasio(Gimnasio gimnasio) {
        this.gimnasio = gimnasio;
        return this;
    }

    public void setGimnasio(Gimnasio gimnasio) {
        this.gimnasio = gimnasio;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Sede sede = (Sede) o;
        if (sede.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sede.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Sede{" +
            "id=" + getId() +
            ", nombreSede='" + getNombreSede() + "'" +
            ", direccionSede='" + getDireccionSede() + "'" +
            ", estadoSede='" + isEstadoSede() + "'" +
            "}";
    }
}
