package com.test.gymtest.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * Class Usuario.
 * @author Bulletproof.
 */
@ApiModel(description = "Class Usuario. @author Bulletproof.")
@Entity
@Table(name = "usuario")
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 15)
    @Column(name = "nombre_usuario", length = 15, nullable = false)
    private String nombreUsuario;

    @NotNull
    @Size(max = 15)
    @Column(name = "apellido_usuario", length = 15, nullable = false)
    private String apellidoUsuario;

    @NotNull
    @Size(max = 999)
    @Column(name = "direccion", length = 999, nullable = false)
    private String direccion;

    @Column(name = "fecha_nacimiento")
    private ZonedDateTime fechaNacimiento;

    @NotNull
    @Size(max = 15)
    @Column(name = "telefono", length = 15, nullable = false)
    private String telefono;

    @NotNull
    @Size(min = 5, max = 20)
    @Column(name = "identificacion", length = 20, nullable = false)
    private String identificacion;

    @NotNull
    @Size(max = 30)
    @Column(name = "correo_electronico", length = 30, nullable = false)
    private String correoElectronico;

    @NotNull
    @Min(value = 1)
    @Max(value = 31)
    @Column(name = "dia_pago", nullable = false)
    private Integer diaPago;

    @Column(name = "lesion")
    private Boolean lesion;

    @Size(max = 999)
    @Column(name = "desc_lesion", length = 999)
    private String descLesion;

    @NotNull
    @Column(name = "estado", nullable = false)
    private Boolean estado;

    @NotNull
    @Column(name = "contrasenna", nullable = false)
    private String contrasenna;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    @OneToOne
    @JoinColumn(unique = true)
    private Sede sede;

    @OneToOne
    @JoinColumn(unique = true)
    private Rol rol;

    @ManyToOne
    private Usuario usuario;

    @OneToMany(mappedBy = "usuario")
    @JsonIgnore
    private Set<Usuario> entrenadorUsuarios = new HashSet<>();

    @OneToMany(mappedBy = "usuario")
    @JsonIgnore
    private Set<RegistroActividadRutina> registrosActividads = new HashSet<>();

    @OneToMany(mappedBy = "usuario")
    @JsonIgnore
    private Set<Solicitud> solicitudesUsuarios = new HashSet<>();

    @OneToMany(mappedBy = "usuario")
    @JsonIgnore
    private Set<Invitacion> invitacionesUsuarios = new HashSet<>();

    @OneToOne(mappedBy = "aceptaInvitacion")
    @JsonIgnore
    private Invitacion aceptaInvitacion;

    @ManyToOne
    private Rutina rutina;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public Usuario nombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
        return this;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getApellidoUsuario() {
        return apellidoUsuario;
    }

    public Usuario apellidoUsuario(String apellidoUsuario) {
        this.apellidoUsuario = apellidoUsuario;
        return this;
    }

    public void setApellidoUsuario(String apellidoUsuario) {
        this.apellidoUsuario = apellidoUsuario;
    }

    public String getDireccion() {
        return direccion;
    }

    public Usuario direccion(String direccion) {
        this.direccion = direccion;
        return this;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public ZonedDateTime getFechaNacimiento() {
        return fechaNacimiento;
    }

    public Usuario fechaNacimiento(ZonedDateTime fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
        return this;
    }

    public void setFechaNacimiento(ZonedDateTime fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getTelefono() {
        return telefono;
    }

    public Usuario telefono(String telefono) {
        this.telefono = telefono;
        return this;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public Usuario identificacion(String identificacion) {
        this.identificacion = identificacion;
        return this;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public Usuario correoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
        return this;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public Integer getDiaPago() {
        return diaPago;
    }

    public Usuario diaPago(Integer diaPago) {
        this.diaPago = diaPago;
        return this;
    }

    public void setDiaPago(Integer diaPago) {
        this.diaPago = diaPago;
    }

    public Boolean isLesion() {
        return lesion;
    }

    public Usuario lesion(Boolean lesion) {
        this.lesion = lesion;
        return this;
    }

    public void setLesion(Boolean lesion) {
        this.lesion = lesion;
    }

    public String getDescLesion() {
        return descLesion;
    }

    public Usuario descLesion(String descLesion) {
        this.descLesion = descLesion;
        return this;
    }

    public void setDescLesion(String descLesion) {
        this.descLesion = descLesion;
    }

    public Boolean isEstado() {
        return estado;
    }

    public Usuario estado(Boolean estado) {
        this.estado = estado;
        return this;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public String getContrasenna() {
        return contrasenna;
    }

    public Usuario contrasenna(String contrasenna) {
        this.contrasenna = contrasenna;
        return this;
    }

    public void setContrasenna(String contrasenna) {
        this.contrasenna = contrasenna;
    }

    public User getUser() {
        return user;
    }

    public Usuario user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Sede getSede() {
        return sede;
    }

    public Usuario sede(Sede sede) {
        this.sede = sede;
        return this;
    }

    public void setSede(Sede sede) {
        this.sede = sede;
    }

    public Rol getRol() {
        return rol;
    }

    public Usuario rol(Rol rol) {
        this.rol = rol;
        return this;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public Usuario usuario(Usuario usuario) {
        this.usuario = usuario;
        return this;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Set<Usuario> getEntrenadorUsuarios() {
        return entrenadorUsuarios;
    }

    public Usuario entrenadorUsuarios(Set<Usuario> usuarios) {
        this.entrenadorUsuarios = usuarios;
        return this;
    }

    public Usuario addEntrenadorUsuario(Usuario usuario) {
        this.entrenadorUsuarios.add(usuario);
        usuario.setUsuario(this);
        return this;
    }

    public Usuario removeEntrenadorUsuario(Usuario usuario) {
        this.entrenadorUsuarios.remove(usuario);
        usuario.setUsuario(null);
        return this;
    }

    public void setEntrenadorUsuarios(Set<Usuario> usuarios) {
        this.entrenadorUsuarios = usuarios;
    }

    public Set<RegistroActividadRutina> getRegistrosActividads() {
        return registrosActividads;
    }

    public Usuario registrosActividads(Set<RegistroActividadRutina> registroActividadRutinas) {
        this.registrosActividads = registroActividadRutinas;
        return this;
    }

    public Usuario addRegistrosActividad(RegistroActividadRutina registroActividadRutina) {
        this.registrosActividads.add(registroActividadRutina);
        registroActividadRutina.setUsuario(this);
        return this;
    }

    public Usuario removeRegistrosActividad(RegistroActividadRutina registroActividadRutina) {
        this.registrosActividads.remove(registroActividadRutina);
        registroActividadRutina.setUsuario(null);
        return this;
    }

    public void setRegistrosActividads(Set<RegistroActividadRutina> registroActividadRutinas) {
        this.registrosActividads = registroActividadRutinas;
    }

    public Set<Solicitud> getSolicitudesUsuarios() {
        return solicitudesUsuarios;
    }

    public Usuario solicitudesUsuarios(Set<Solicitud> solicituds) {
        this.solicitudesUsuarios = solicituds;
        return this;
    }

    public Usuario addSolicitudesUsuario(Solicitud solicitud) {
        this.solicitudesUsuarios.add(solicitud);
        solicitud.setUsuario(this);
        return this;
    }

    public Usuario removeSolicitudesUsuario(Solicitud solicitud) {
        this.solicitudesUsuarios.remove(solicitud);
        solicitud.setUsuario(null);
        return this;
    }

    public void setSolicitudesUsuarios(Set<Solicitud> solicituds) {
        this.solicitudesUsuarios = solicituds;
    }

    public Set<Invitacion> getInvitacionesUsuarios() {
        return invitacionesUsuarios;
    }

    public Usuario invitacionesUsuarios(Set<Invitacion> invitacions) {
        this.invitacionesUsuarios = invitacions;
        return this;
    }

    public Usuario addInvitacionesUsuario(Invitacion invitacion) {
        this.invitacionesUsuarios.add(invitacion);
        invitacion.setUsuario(this);
        return this;
    }

    public Usuario removeInvitacionesUsuario(Invitacion invitacion) {
        this.invitacionesUsuarios.remove(invitacion);
        invitacion.setUsuario(null);
        return this;
    }

    public void setInvitacionesUsuarios(Set<Invitacion> invitacions) {
        this.invitacionesUsuarios = invitacions;
    }

    public Invitacion getAceptaInvitacion() {
        return aceptaInvitacion;
    }

    public Usuario aceptaInvitacion(Invitacion invitacion) {
        this.aceptaInvitacion = invitacion;
        return this;
    }

    public void setAceptaInvitacion(Invitacion invitacion) {
        this.aceptaInvitacion = invitacion;
    }

    public Rutina getRutina() {
        return rutina;
    }

    public Usuario rutina(Rutina rutina) {
        this.rutina = rutina;
        return this;
    }

    public void setRutina(Rutina rutina) {
        this.rutina = rutina;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Usuario usuario = (Usuario) o;
        if (usuario.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), usuario.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Usuario{" +
            "id=" + getId() +
            ", nombreUsuario='" + getNombreUsuario() + "'" +
            ", apellidoUsuario='" + getApellidoUsuario() + "'" +
            ", direccion='" + getDireccion() + "'" +
            ", fechaNacimiento='" + getFechaNacimiento() + "'" +
            ", telefono='" + getTelefono() + "'" +
            ", identificacion='" + getIdentificacion() + "'" +
            ", correoElectronico='" + getCorreoElectronico() + "'" +
            ", diaPago=" + getDiaPago() +
            ", lesion='" + isLesion() + "'" +
            ", descLesion='" + getDescLesion() + "'" +
            ", estado='" + isEstado() + "'" +
            ", contrasenna='" + getContrasenna() + "'" +
            "}";
    }
}
