package com.test.gymtest.domain;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * Class Rol.
 * @author Bulletproof.
 */
@ApiModel(description = "Class Rol. @author Bulletproof.")
@Entity
@Table(name = "rol")
public class Rol implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 15)
    @Column(name = "nombre_rol", length = 15, nullable = false)
    private String nombreRol;

    @NotNull
    @Column(name = "estado_rol", nullable = false)
    private Boolean estadoRol;

    @ManyToMany
    @JoinTable(name = "rol_rolxpermiso",
               joinColumns = @JoinColumn(name="rols_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="rolxpermisos_id", referencedColumnName="id"))
    private Set<Permiso> rolXPermisos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreRol() {
        return nombreRol;
    }

    public Rol nombreRol(String nombreRol) {
        this.nombreRol = nombreRol;
        return this;
    }

    public void setNombreRol(String nombreRol) {
        this.nombreRol = nombreRol;
    }

    public Boolean isEstadoRol() {
        return estadoRol;
    }

    public Rol estadoRol(Boolean estadoRol) {
        this.estadoRol = estadoRol;
        return this;
    }

    public void setEstadoRol(Boolean estadoRol) {
        this.estadoRol = estadoRol;
    }

    public Set<Permiso> getRolXPermisos() {
        return rolXPermisos;
    }

    public Rol rolXPermisos(Set<Permiso> permisos) {
        this.rolXPermisos = permisos;
        return this;
    }

    public Rol addRolXPermiso(Permiso permiso) {
        this.rolXPermisos.add(permiso);
        return this;
    }

    public Rol removeRolXPermiso(Permiso permiso) {
        this.rolXPermisos.remove(permiso);
        return this;
    }

    public void setRolXPermisos(Set<Permiso> permisos) {
        this.rolXPermisos = permisos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Rol rol = (Rol) o;
        if (rol.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), rol.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Rol{" +
            "id=" + getId() +
            ", nombreRol='" + getNombreRol() + "'" +
            ", estadoRol='" + isEstadoRol() + "'" +
            "}";
    }
}
