package com.test.gymtest.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * Class ParametroConfiguracion.
 * @author Bulletproof.
 */
@ApiModel(description = "Class ParametroConfiguracion. @author Bulletproof.")
@Entity
@Table(name = "parametro_configuracion")
public class ParametroConfiguracion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 999)
    @Column(name = "nombre_parametro", length = 999, nullable = false)
    private String nombreParametro;

    @NotNull
    @Size(max = 999)
    @Column(name = "valor_parametro", length = 999, nullable = false)
    private String valorParametro;

    @NotNull
    @Size(max = 50)
    @Column(name = "tipo_de_dato", length = 50, nullable = false)
    private String tipoDeDato;

    @NotNull
    @Column(name = "estado_parametro", nullable = false)
    private Boolean estadoParametro;

    @OneToOne
    @JoinColumn(unique = true)
    private TipoParametro tipoParametro;

    @OneToOne(mappedBy = "tipoMultimedia")
    @JsonIgnore
    private Multimedia tipoMultimedia;

    @OneToOne(mappedBy = "zonaMusculo")
    @JsonIgnore
    private Musculo zonaMusculo;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreParametro() {
        return nombreParametro;
    }

    public ParametroConfiguracion nombreParametro(String nombreParametro) {
        this.nombreParametro = nombreParametro;
        return this;
    }

    public void setNombreParametro(String nombreParametro) {
        this.nombreParametro = nombreParametro;
    }

    public String getValorParametro() {
        return valorParametro;
    }

    public ParametroConfiguracion valorParametro(String valorParametro) {
        this.valorParametro = valorParametro;
        return this;
    }

    public void setValorParametro(String valorParametro) {
        this.valorParametro = valorParametro;
    }

    public String getTipoDeDato() {
        return tipoDeDato;
    }

    public ParametroConfiguracion tipoDeDato(String tipoDeDato) {
        this.tipoDeDato = tipoDeDato;
        return this;
    }

    public void setTipoDeDato(String tipoDeDato) {
        this.tipoDeDato = tipoDeDato;
    }

    public Boolean isEstadoParametro() {
        return estadoParametro;
    }

    public ParametroConfiguracion estadoParametro(Boolean estadoParametro) {
        this.estadoParametro = estadoParametro;
        return this;
    }

    public void setEstadoParametro(Boolean estadoParametro) {
        this.estadoParametro = estadoParametro;
    }

    public TipoParametro getTipoParametro() {
        return tipoParametro;
    }

    public ParametroConfiguracion tipoParametro(TipoParametro tipoParametro) {
        this.tipoParametro = tipoParametro;
        return this;
    }

    public void setTipoParametro(TipoParametro tipoParametro) {
        this.tipoParametro = tipoParametro;
    }

    public Multimedia getTipoMultimedia() {
        return tipoMultimedia;
    }

    public ParametroConfiguracion tipoMultimedia(Multimedia multimedia) {
        this.tipoMultimedia = multimedia;
        return this;
    }

    public void setTipoMultimedia(Multimedia multimedia) {
        this.tipoMultimedia = multimedia;
    }

    public Musculo getZonaMusculo() {
        return zonaMusculo;
    }

    public ParametroConfiguracion zonaMusculo(Musculo musculo) {
        this.zonaMusculo = musculo;
        return this;
    }

    public void setZonaMusculo(Musculo musculo) {
        this.zonaMusculo = musculo;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ParametroConfiguracion parametroConfiguracion = (ParametroConfiguracion) o;
        if (parametroConfiguracion.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), parametroConfiguracion.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ParametroConfiguracion{" +
            "id=" + getId() +
            ", nombreParametro='" + getNombreParametro() + "'" +
            ", valorParametro='" + getValorParametro() + "'" +
            ", tipoDeDato='" + getTipoDeDato() + "'" +
            ", estadoParametro='" + isEstadoParametro() + "'" +
            "}";
    }
}
