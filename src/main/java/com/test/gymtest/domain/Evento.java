package com.test.gymtest.domain;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * Class Evento.
 * @author Bulletproof.
 */
@ApiModel(description = "Class Evento. @author Bulletproof.")
@Entity
@Table(name = "evento")
public class Evento implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 999)
    @Column(name = "nombre_evento", length = 999, nullable = false)
    private String nombreEvento;

    @NotNull
    @Size(max = 999)
    @Column(name = "descripcion_evento", length = 999, nullable = false)
    private String descripcionEvento;

    @NotNull
    @Column(name = "fecha_inicio", nullable = false)
    private ZonedDateTime fechaInicio;

    @NotNull
    @Column(name = "fecha_fin", nullable = false)
    private ZonedDateTime fechaFin;

    @NotNull
    @Column(name = "hora_inicio_evento", nullable = false)
    private ZonedDateTime horaInicioEvento;

    @NotNull
    @Column(name = "hora_fin_evento", nullable = false)
    private ZonedDateTime horaFinEvento;

    @NotNull
    @Column(name = "estado_evento", nullable = false)
    private Boolean estadoEvento;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreEvento() {
        return nombreEvento;
    }

    public Evento nombreEvento(String nombreEvento) {
        this.nombreEvento = nombreEvento;
        return this;
    }

    public void setNombreEvento(String nombreEvento) {
        this.nombreEvento = nombreEvento;
    }

    public String getDescripcionEvento() {
        return descripcionEvento;
    }

    public Evento descripcionEvento(String descripcionEvento) {
        this.descripcionEvento = descripcionEvento;
        return this;
    }

    public void setDescripcionEvento(String descripcionEvento) {
        this.descripcionEvento = descripcionEvento;
    }

    public ZonedDateTime getFechaInicio() {
        return fechaInicio;
    }

    public Evento fechaInicio(ZonedDateTime fechaInicio) {
        this.fechaInicio = fechaInicio;
        return this;
    }

    public void setFechaInicio(ZonedDateTime fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public ZonedDateTime getFechaFin() {
        return fechaFin;
    }

    public Evento fechaFin(ZonedDateTime fechaFin) {
        this.fechaFin = fechaFin;
        return this;
    }

    public void setFechaFin(ZonedDateTime fechaFin) {
        this.fechaFin = fechaFin;
    }

    public ZonedDateTime getHoraInicioEvento() {
        return horaInicioEvento;
    }

    public Evento horaInicioEvento(ZonedDateTime horaInicioEvento) {
        this.horaInicioEvento = horaInicioEvento;
        return this;
    }

    public void setHoraInicioEvento(ZonedDateTime horaInicioEvento) {
        this.horaInicioEvento = horaInicioEvento;
    }

    public ZonedDateTime getHoraFinEvento() {
        return horaFinEvento;
    }

    public Evento horaFinEvento(ZonedDateTime horaFinEvento) {
        this.horaFinEvento = horaFinEvento;
        return this;
    }

    public void setHoraFinEvento(ZonedDateTime horaFinEvento) {
        this.horaFinEvento = horaFinEvento;
    }

    public Boolean isEstadoEvento() {
        return estadoEvento;
    }

    public Evento estadoEvento(Boolean estadoEvento) {
        this.estadoEvento = estadoEvento;
        return this;
    }

    public void setEstadoEvento(Boolean estadoEvento) {
        this.estadoEvento = estadoEvento;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Evento evento = (Evento) o;
        if (evento.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), evento.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Evento{" +
            "id=" + getId() +
            ", nombreEvento='" + getNombreEvento() + "'" +
            ", descripcionEvento='" + getDescripcionEvento() + "'" +
            ", fechaInicio='" + getFechaInicio() + "'" +
            ", fechaFin='" + getFechaFin() + "'" +
            ", horaInicioEvento='" + getHoraInicioEvento() + "'" +
            ", horaFinEvento='" + getHoraFinEvento() + "'" +
            ", estadoEvento='" + isEstadoEvento() + "'" +
            "}";
    }
}
