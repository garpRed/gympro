package com.test.gymtest.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * Class Gimnasio.
 * @author Bulletproof.
 */
@ApiModel(description = "Class Gimnasio. @author Bulletproof.")
@Entity
@Table(name = "gimnasio")
public class Gimnasio implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 999)
    @Column(name = "nombre_gimnasio", length = 999, nullable = false)
    private String nombreGimnasio;

    @NotNull
    @Column(name = "estado_gimnasio", nullable = false)
    private Boolean estadoGimnasio;

    @OneToMany(mappedBy = "gimnasio")
    @JsonIgnore
    private Set<Sede> gimnasioSedes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreGimnasio() {
        return nombreGimnasio;
    }

    public Gimnasio nombreGimnasio(String nombreGimnasio) {
        this.nombreGimnasio = nombreGimnasio;
        return this;
    }

    public void setNombreGimnasio(String nombreGimnasio) {
        this.nombreGimnasio = nombreGimnasio;
    }

    public Boolean isEstadoGimnasio() {
        return estadoGimnasio;
    }

    public Gimnasio estadoGimnasio(Boolean estadoGimnasio) {
        this.estadoGimnasio = estadoGimnasio;
        return this;
    }

    public void setEstadoGimnasio(Boolean estadoGimnasio) {
        this.estadoGimnasio = estadoGimnasio;
    }

    public Set<Sede> getGimnasioSedes() {
        return gimnasioSedes;
    }

    public Gimnasio gimnasioSedes(Set<Sede> sedes) {
        this.gimnasioSedes = sedes;
        return this;
    }

    public Gimnasio addGimnasioSedes(Sede sede) {
        this.gimnasioSedes.add(sede);
        sede.setGimnasio(this);
        return this;
    }

    public Gimnasio removeGimnasioSedes(Sede sede) {
        this.gimnasioSedes.remove(sede);
        sede.setGimnasio(null);
        return this;
    }

    public void setGimnasioSedes(Set<Sede> sedes) {
        this.gimnasioSedes = sedes;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Gimnasio gimnasio = (Gimnasio) o;
        if (gimnasio.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), gimnasio.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Gimnasio{" +
            "id=" + getId() +
            ", nombreGimnasio='" + getNombreGimnasio() + "'" +
            ", estadoGimnasio='" + isEstadoGimnasio() + "'" +
            "}";
    }
}
