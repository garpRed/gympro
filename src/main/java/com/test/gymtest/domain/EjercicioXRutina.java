package com.test.gymtest.domain;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * Class EjercicioXRutina.
 * @author Bulletproof.
 */
@ApiModel(description = "Class EjercicioXRutina. @author Bulletproof.")
@Entity
@Table(name = "ejercicio_x_rutina")
public class EjercicioXRutina implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Max(value = 500)
    @Column(name = "peso")
    private Integer peso;

    @Max(value = 500)
    @Column(name = "reps")
    private Integer reps;

    @Max(value = 500)
    @Column(name = "sets")
    private Integer sets;

    @Size(max = 20)
    @Column(name = "ajuste", length = 20)
    private String ajuste;

    @Size(max = 10)
    @Column(name = "tiempo", length = 10)
    private String tiempo;

    @Size(max = 50)
    @Column(name = "commentario", length = 50)
    private String commentario;

    @NotNull
    @Column(name = "estado_ejercicio_rutina", nullable = false)
    private Boolean estadoEjercicioRutina;

    @OneToOne
    @JoinColumn(unique = true)
    private SubRutina ejercicioXRutina;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPeso() {
        return peso;
    }

    public EjercicioXRutina peso(Integer peso) {
        this.peso = peso;
        return this;
    }

    public void setPeso(Integer peso) {
        this.peso = peso;
    }

    public Integer getReps() {
        return reps;
    }

    public EjercicioXRutina reps(Integer reps) {
        this.reps = reps;
        return this;
    }

    public void setReps(Integer reps) {
        this.reps = reps;
    }

    public Integer getSets() {
        return sets;
    }

    public EjercicioXRutina sets(Integer sets) {
        this.sets = sets;
        return this;
    }

    public void setSets(Integer sets) {
        this.sets = sets;
    }

    public String getAjuste() {
        return ajuste;
    }

    public EjercicioXRutina ajuste(String ajuste) {
        this.ajuste = ajuste;
        return this;
    }

    public void setAjuste(String ajuste) {
        this.ajuste = ajuste;
    }

    public String getTiempo() {
        return tiempo;
    }

    public EjercicioXRutina tiempo(String tiempo) {
        this.tiempo = tiempo;
        return this;
    }

    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }

    public String getCommentario() {
        return commentario;
    }

    public EjercicioXRutina commentario(String commentario) {
        this.commentario = commentario;
        return this;
    }

    public void setCommentario(String commentario) {
        this.commentario = commentario;
    }

    public Boolean isEstadoEjercicioRutina() {
        return estadoEjercicioRutina;
    }

    public EjercicioXRutina estadoEjercicioRutina(Boolean estadoEjercicioRutina) {
        this.estadoEjercicioRutina = estadoEjercicioRutina;
        return this;
    }

    public void setEstadoEjercicioRutina(Boolean estadoEjercicioRutina) {
        this.estadoEjercicioRutina = estadoEjercicioRutina;
    }

    public SubRutina getEjercicioXRutina() {
        return ejercicioXRutina;
    }

    public EjercicioXRutina ejercicioXRutina(SubRutina subRutina) {
        this.ejercicioXRutina = subRutina;
        return this;
    }

    public void setEjercicioXRutina(SubRutina subRutina) {
        this.ejercicioXRutina = subRutina;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EjercicioXRutina ejercicioXRutina = (EjercicioXRutina) o;
        if (ejercicioXRutina.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), ejercicioXRutina.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EjercicioXRutina{" +
            "id=" + getId() +
            ", peso=" + getPeso() +
            ", reps=" + getReps() +
            ", sets=" + getSets() +
            ", ajuste='" + getAjuste() + "'" +
            ", tiempo='" + getTiempo() + "'" +
            ", commentario='" + getCommentario() + "'" +
            ", estadoEjercicioRutina='" + isEstadoEjercicioRutina() + "'" +
            "}";
    }
}
