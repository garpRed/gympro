package com.test.gymtest.domain;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * Class Activo.
 * @author Bulletproof.
 */
@ApiModel(description = "Class Activo. @author Bulletproof.")
@Entity
@Table(name = "activo")
public class Activo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 999)
    @Column(name = "nombre_activo", length = 999, nullable = false)
    private String nombreActivo;

    @NotNull
    @Max(value = 500)
    @Column(name = "cant_activos", nullable = false)
    private Integer cantActivos;

    @NotNull
    @Column(name = "estado_activo", nullable = false)
    private Boolean estadoActivo;

    @ManyToOne
    private Sede sede;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreActivo() {
        return nombreActivo;
    }

    public Activo nombreActivo(String nombreActivo) {
        this.nombreActivo = nombreActivo;
        return this;
    }

    public void setNombreActivo(String nombreActivo) {
        this.nombreActivo = nombreActivo;
    }

    public Integer getCantActivos() {
        return cantActivos;
    }

    public Activo cantActivos(Integer cantActivos) {
        this.cantActivos = cantActivos;
        return this;
    }

    public void setCantActivos(Integer cantActivos) {
        this.cantActivos = cantActivos;
    }

    public Boolean isEstadoActivo() {
        return estadoActivo;
    }

    public Activo estadoActivo(Boolean estadoActivo) {
        this.estadoActivo = estadoActivo;
        return this;
    }

    public void setEstadoActivo(Boolean estadoActivo) {
        this.estadoActivo = estadoActivo;
    }

    public Sede getSede() {
        return sede;
    }

    public Activo sede(Sede sede) {
        this.sede = sede;
        return this;
    }

    public void setSede(Sede sede) {
        this.sede = sede;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Activo activo = (Activo) o;
        if (activo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), activo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Activo{" +
            "id=" + getId() +
            ", nombreActivo='" + getNombreActivo() + "'" +
            ", cantActivos=" + getCantActivos() +
            ", estadoActivo='" + isEstadoActivo() + "'" +
            "}";
    }
}
