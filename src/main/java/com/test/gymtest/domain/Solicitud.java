package com.test.gymtest.domain;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * Class Solicitud.
 * @author Bulletproof.
 */
@ApiModel(description = "Class Solicitud. @author Bulletproof.")
@Entity
@Table(name = "solicitud")
public class Solicitud implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 999)
    @Column(name = "descripcion_solicitud", length = 999)
    private String descripcionSolicitud;

    @NotNull
    @Column(name = "fecha_solicitud", nullable = false)
    private ZonedDateTime fechaSolicitud;

    @NotNull
    @Column(name = "hora_inicio", nullable = false)
    private ZonedDateTime horaInicio;

    @NotNull
    @Column(name = "hora_fin", nullable = false)
    private ZonedDateTime horaFin;

    @ManyToOne
    private Usuario usuario;

    @OneToOne
    @JoinColumn(unique = true)
    private Usuario entrenadorSolicitud;

    @OneToOne
    @JoinColumn(unique = true)
    private ParametroConfiguracion tipoSolicitud;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcionSolicitud() {
        return descripcionSolicitud;
    }

    public Solicitud descripcionSolicitud(String descripcionSolicitud) {
        this.descripcionSolicitud = descripcionSolicitud;
        return this;
    }

    public void setDescripcionSolicitud(String descripcionSolicitud) {
        this.descripcionSolicitud = descripcionSolicitud;
    }

    public ZonedDateTime getFechaSolicitud() {
        return fechaSolicitud;
    }

    public Solicitud fechaSolicitud(ZonedDateTime fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
        return this;
    }

    public void setFechaSolicitud(ZonedDateTime fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public ZonedDateTime getHoraInicio() {
        return horaInicio;
    }

    public Solicitud horaInicio(ZonedDateTime horaInicio) {
        this.horaInicio = horaInicio;
        return this;
    }

    public void setHoraInicio(ZonedDateTime horaInicio) {
        this.horaInicio = horaInicio;
    }

    public ZonedDateTime getHoraFin() {
        return horaFin;
    }

    public Solicitud horaFin(ZonedDateTime horaFin) {
        this.horaFin = horaFin;
        return this;
    }

    public void setHoraFin(ZonedDateTime horaFin) {
        this.horaFin = horaFin;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public Solicitud usuario(Usuario usuario) {
        this.usuario = usuario;
        return this;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Usuario getEntrenadorSolicitud() {
        return entrenadorSolicitud;
    }

    public Solicitud entrenadorSolicitud(Usuario usuario) {
        this.entrenadorSolicitud = usuario;
        return this;
    }

    public void setEntrenadorSolicitud(Usuario usuario) {
        this.entrenadorSolicitud = usuario;
    }

    public ParametroConfiguracion getTipoSolicitud() {
        return tipoSolicitud;
    }

    public Solicitud tipoSolicitud(ParametroConfiguracion parametroConfiguracion) {
        this.tipoSolicitud = parametroConfiguracion;
        return this;
    }

    public void setTipoSolicitud(ParametroConfiguracion parametroConfiguracion) {
        this.tipoSolicitud = parametroConfiguracion;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Solicitud solicitud = (Solicitud) o;
        if (solicitud.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), solicitud.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Solicitud{" +
            "id=" + getId() +
            ", descripcionSolicitud='" + getDescripcionSolicitud() + "'" +
            ", fechaSolicitud='" + getFechaSolicitud() + "'" +
            ", horaInicio='" + getHoraInicio() + "'" +
            ", horaFin='" + getHoraFin() + "'" +
            "}";
    }
}
