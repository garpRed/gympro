package com.test.gymtest.domain;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * Class Permiso.
 * @author Bulletproof.
 */
@ApiModel(description = "Class Permiso. @author Bulletproof.")
@Entity
@Table(name = "permiso")
public class Permiso implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 999)
    @Column(name = "nombre_permiso", length = 999, nullable = false)
    private String nombrePermiso;

    @NotNull
    @Column(name = "estado_permiso", nullable = false)
    private Boolean estadoPermiso;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombrePermiso() {
        return nombrePermiso;
    }

    public Permiso nombrePermiso(String nombrePermiso) {
        this.nombrePermiso = nombrePermiso;
        return this;
    }

    public void setNombrePermiso(String nombrePermiso) {
        this.nombrePermiso = nombrePermiso;
    }

    public Boolean isEstadoPermiso() {
        return estadoPermiso;
    }

    public Permiso estadoPermiso(Boolean estadoPermiso) {
        this.estadoPermiso = estadoPermiso;
        return this;
    }

    public void setEstadoPermiso(Boolean estadoPermiso) {
        this.estadoPermiso = estadoPermiso;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Permiso permiso = (Permiso) o;
        if (permiso.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), permiso.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Permiso{" +
            "id=" + getId() +
            ", nombrePermiso='" + getNombrePermiso() + "'" +
            ", estadoPermiso='" + isEstadoPermiso() + "'" +
            "}";
    }
}
