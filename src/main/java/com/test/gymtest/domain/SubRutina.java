package com.test.gymtest.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * Class SubRutina.
 * @author Bulletproof.
 */
@ApiModel(description = "Class SubRutina. @author Bulletproof.")
@Entity
@Table(name = "sub_rutina")
public class SubRutina implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 999)
    @Column(name = "nombre_sub_rutina", length = 999, nullable = false)
    private String nombreSubRutina;

    @Min(value = 1)
    @Max(value = 500)
    @Column(name = "cant_series")
    private Integer cantSeries;

    @Min(value = 1)
    @Max(value = 500)
    @Column(name = "cant_repeticiones")
    private Integer cantRepeticiones;

    @Size(max = 999)
    @Column(name = "comentario_sub_rutina", length = 999)
    private String comentarioSubRutina;

    @NotNull
    @Column(name = "estado_subrutina", nullable = false)
    private Boolean estadoSubrutina;

    @ManyToOne
    private Rutina rutina;

    @OneToMany(mappedBy = "subRutina")
    @JsonIgnore
    private Set<RegistroActividadRutina> subrutinaRegistros = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "sub_rutina_subrutina_ejercicios",
               joinColumns = @JoinColumn(name="sub_rutinas_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="subrutina_ejercicios_id", referencedColumnName="id"))
    private Set<Ejercicio> subrutinaEjercicios = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreSubRutina() {
        return nombreSubRutina;
    }

    public SubRutina nombreSubRutina(String nombreSubRutina) {
        this.nombreSubRutina = nombreSubRutina;
        return this;
    }

    public void setNombreSubRutina(String nombreSubRutina) {
        this.nombreSubRutina = nombreSubRutina;
    }

    public Integer getCantSeries() {
        return cantSeries;
    }

    public SubRutina cantSeries(Integer cantSeries) {
        this.cantSeries = cantSeries;
        return this;
    }

    public void setCantSeries(Integer cantSeries) {
        this.cantSeries = cantSeries;
    }

    public Integer getCantRepeticiones() {
        return cantRepeticiones;
    }

    public SubRutina cantRepeticiones(Integer cantRepeticiones) {
        this.cantRepeticiones = cantRepeticiones;
        return this;
    }

    public void setCantRepeticiones(Integer cantRepeticiones) {
        this.cantRepeticiones = cantRepeticiones;
    }

    public String getComentarioSubRutina() {
        return comentarioSubRutina;
    }

    public SubRutina comentarioSubRutina(String comentarioSubRutina) {
        this.comentarioSubRutina = comentarioSubRutina;
        return this;
    }

    public void setComentarioSubRutina(String comentarioSubRutina) {
        this.comentarioSubRutina = comentarioSubRutina;
    }

    public Boolean isEstadoSubrutina() {
        return estadoSubrutina;
    }

    public SubRutina estadoSubrutina(Boolean estadoSubrutina) {
        this.estadoSubrutina = estadoSubrutina;
        return this;
    }

    public void setEstadoSubrutina(Boolean estadoSubrutina) {
        this.estadoSubrutina = estadoSubrutina;
    }

    public Rutina getRutina() {
        return rutina;
    }

    public SubRutina rutina(Rutina rutina) {
        this.rutina = rutina;
        return this;
    }

    public void setRutina(Rutina rutina) {
        this.rutina = rutina;
    }

    public Set<RegistroActividadRutina> getSubrutinaRegistros() {
        return subrutinaRegistros;
    }

    public SubRutina subrutinaRegistros(Set<RegistroActividadRutina> registroActividadRutinas) {
        this.subrutinaRegistros = registroActividadRutinas;
        return this;
    }

    public SubRutina addSubrutinaRegistro(RegistroActividadRutina registroActividadRutina) {
        this.subrutinaRegistros.add(registroActividadRutina);
        registroActividadRutina.setSubRutina(this);
        return this;
    }

    public SubRutina removeSubrutinaRegistro(RegistroActividadRutina registroActividadRutina) {
        this.subrutinaRegistros.remove(registroActividadRutina);
        registroActividadRutina.setSubRutina(null);
        return this;
    }

    public void setSubrutinaRegistros(Set<RegistroActividadRutina> registroActividadRutinas) {
        this.subrutinaRegistros = registroActividadRutinas;
    }

    public Set<Ejercicio> getSubrutinaEjercicios() {
        return subrutinaEjercicios;
    }

    public SubRutina subrutinaEjercicios(Set<Ejercicio> ejercicios) {
        this.subrutinaEjercicios = ejercicios;
        return this;
    }

    public SubRutina addSubrutinaEjercicios(Ejercicio ejercicio) {
        this.subrutinaEjercicios.add(ejercicio);
        return this;
    }

    public SubRutina removeSubrutinaEjercicios(Ejercicio ejercicio) {
        this.subrutinaEjercicios.remove(ejercicio);
        return this;
    }

    public void setSubrutinaEjercicios(Set<Ejercicio> ejercicios) {
        this.subrutinaEjercicios = ejercicios;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SubRutina subRutina = (SubRutina) o;
        if (subRutina.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), subRutina.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SubRutina{" +
            "id=" + getId() +
            ", nombreSubRutina='" + getNombreSubRutina() + "'" +
            ", cantSeries=" + getCantSeries() +
            ", cantRepeticiones=" + getCantRepeticiones() +
            ", comentarioSubRutina='" + getComentarioSubRutina() + "'" +
            ", estadoSubrutina='" + isEstadoSubrutina() + "'" +
            "}";
    }
}
