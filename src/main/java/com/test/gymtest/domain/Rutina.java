package com.test.gymtest.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * Class Rutina.
 * @author Bulletproof.
 */
@ApiModel(description = "Class Rutina. @author Bulletproof.")
@Entity
@Table(name = "rutina")
public class Rutina implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "usuario_creador_rutina")
    private Integer usuarioCreadorRutina;

    @NotNull
    @Size(max = 999)
    @Column(name = "nombre_rutina", length = 999, nullable = false)
    private String nombreRutina;

    @Min(value = 1)
    @Max(value = 500)
    @Column(name = "cant_dias_rutina")
    private Integer cantDiasRutina;

    @NotNull
    @Column(name = "estado_rutina", nullable = false)
    private Boolean estadoRutina;

    @OneToMany(mappedBy = "rutina")
    @JsonIgnore
    private Set<SubRutina> subrutinaRutinas = new HashSet<>();

    @OneToMany(mappedBy = "rutina")
    @JsonIgnore
    private Set<Usuario> rutinaUsuarios = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getUsuarioCreadorRutina() {
        return usuarioCreadorRutina;
    }

    public Rutina usuarioCreadorRutina(Integer usuarioCreadorRutina) {
        this.usuarioCreadorRutina = usuarioCreadorRutina;
        return this;
    }

    public void setUsuarioCreadorRutina(Integer usuarioCreadorRutina) {
        this.usuarioCreadorRutina = usuarioCreadorRutina;
    }

    public String getNombreRutina() {
        return nombreRutina;
    }

    public Rutina nombreRutina(String nombreRutina) {
        this.nombreRutina = nombreRutina;
        return this;
    }

    public void setNombreRutina(String nombreRutina) {
        this.nombreRutina = nombreRutina;
    }

    public Integer getCantDiasRutina() {
        return cantDiasRutina;
    }

    public Rutina cantDiasRutina(Integer cantDiasRutina) {
        this.cantDiasRutina = cantDiasRutina;
        return this;
    }

    public void setCantDiasRutina(Integer cantDiasRutina) {
        this.cantDiasRutina = cantDiasRutina;
    }

    public Boolean isEstadoRutina() {
        return estadoRutina;
    }

    public Rutina estadoRutina(Boolean estadoRutina) {
        this.estadoRutina = estadoRutina;
        return this;
    }

    public void setEstadoRutina(Boolean estadoRutina) {
        this.estadoRutina = estadoRutina;
    }

    public Set<SubRutina> getSubrutinaRutinas() {
        return subrutinaRutinas;
    }

    public Rutina subrutinaRutinas(Set<SubRutina> subRutinas) {
        this.subrutinaRutinas = subRutinas;
        return this;
    }

    public Rutina addSubrutinaRutina(SubRutina subRutina) {
        this.subrutinaRutinas.add(subRutina);
        subRutina.setRutina(this);
        return this;
    }

    public Rutina removeSubrutinaRutina(SubRutina subRutina) {
        this.subrutinaRutinas.remove(subRutina);
        subRutina.setRutina(null);
        return this;
    }

    public void setSubrutinaRutinas(Set<SubRutina> subRutinas) {
        this.subrutinaRutinas = subRutinas;
    }

    public Set<Usuario> getRutinaUsuarios() {
        return rutinaUsuarios;
    }

    public Rutina rutinaUsuarios(Set<Usuario> usuarios) {
        this.rutinaUsuarios = usuarios;
        return this;
    }

    public Rutina addRutinaUsuario(Usuario usuario) {
        this.rutinaUsuarios.add(usuario);
        usuario.setRutina(this);
        return this;
    }

    public Rutina removeRutinaUsuario(Usuario usuario) {
        this.rutinaUsuarios.remove(usuario);
        usuario.setRutina(null);
        return this;
    }

    public void setRutinaUsuarios(Set<Usuario> usuarios) {
        this.rutinaUsuarios = usuarios;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Rutina rutina = (Rutina) o;
        if (rutina.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), rutina.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Rutina{" +
            "id=" + getId() +
            ", usuarioCreadorRutina=" + getUsuarioCreadorRutina() +
            ", nombreRutina='" + getNombreRutina() + "'" +
            ", cantDiasRutina=" + getCantDiasRutina() +
            ", estadoRutina='" + isEstadoRutina() + "'" +
            "}";
    }
}
