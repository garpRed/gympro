package com.test.gymtest.domain;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * Class Horario.
 * @author Bulletproof.
 */
@ApiModel(description = "Class Horario. @author Bulletproof.")
@Entity
@Table(name = "horario")
public class Horario implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 1)
    @Column(name = "dia_entrenador", length = 1, nullable = false)
    private String diaEntrenador;

    @NotNull
    @Column(name = "disponibilidad_entrenador", nullable = false)
    private Boolean disponibilidadEntrenador;

    @NotNull
    @Column(name = "horario_inicio_horario", nullable = false)
    private ZonedDateTime horarioInicioHorario;

    @NotNull
    @Column(name = "horario_fin_horario", nullable = false)
    private ZonedDateTime horarioFinHorario;

    @NotNull
    @Column(name = "estado_horario", nullable = false)
    private Boolean estadoHorario;

    @OneToOne
    @JoinColumn(unique = true)
    private Usuario horarioUsuario;

    @ManyToMany
    @JoinTable(name = "horario_horarios_parametro",
               joinColumns = @JoinColumn(name="horarios_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="horarios_parametros_id", referencedColumnName="id"))
    private Set<ParametroConfiguracion> horariosParametros = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDiaEntrenador() {
        return diaEntrenador;
    }

    public Horario diaEntrenador(String diaEntrenador) {
        this.diaEntrenador = diaEntrenador;
        return this;
    }

    public void setDiaEntrenador(String diaEntrenador) {
        this.diaEntrenador = diaEntrenador;
    }

    public Boolean isDisponibilidadEntrenador() {
        return disponibilidadEntrenador;
    }

    public Horario disponibilidadEntrenador(Boolean disponibilidadEntrenador) {
        this.disponibilidadEntrenador = disponibilidadEntrenador;
        return this;
    }

    public void setDisponibilidadEntrenador(Boolean disponibilidadEntrenador) {
        this.disponibilidadEntrenador = disponibilidadEntrenador;
    }

    public ZonedDateTime getHorarioInicioHorario() {
        return horarioInicioHorario;
    }

    public Horario horarioInicioHorario(ZonedDateTime horarioInicioHorario) {
        this.horarioInicioHorario = horarioInicioHorario;
        return this;
    }

    public void setHorarioInicioHorario(ZonedDateTime horarioInicioHorario) {
        this.horarioInicioHorario = horarioInicioHorario;
    }

    public ZonedDateTime getHorarioFinHorario() {
        return horarioFinHorario;
    }

    public Horario horarioFinHorario(ZonedDateTime horarioFinHorario) {
        this.horarioFinHorario = horarioFinHorario;
        return this;
    }

    public void setHorarioFinHorario(ZonedDateTime horarioFinHorario) {
        this.horarioFinHorario = horarioFinHorario;
    }

    public Boolean isEstadoHorario() {
        return estadoHorario;
    }

    public Horario estadoHorario(Boolean estadoHorario) {
        this.estadoHorario = estadoHorario;
        return this;
    }

    public void setEstadoHorario(Boolean estadoHorario) {
        this.estadoHorario = estadoHorario;
    }

    public Usuario getHorarioUsuario() {
        return horarioUsuario;
    }

    public Horario horarioUsuario(Usuario usuario) {
        this.horarioUsuario = usuario;
        return this;
    }

    public void setHorarioUsuario(Usuario usuario) {
        this.horarioUsuario = usuario;
    }

    public Set<ParametroConfiguracion> getHorariosParametros() {
        return horariosParametros;
    }

    public Horario horariosParametros(Set<ParametroConfiguracion> parametroConfiguracions) {
        this.horariosParametros = parametroConfiguracions;
        return this;
    }

    public Horario addHorariosParametro(ParametroConfiguracion parametroConfiguracion) {
        this.horariosParametros.add(parametroConfiguracion);
        return this;
    }

    public Horario removeHorariosParametro(ParametroConfiguracion parametroConfiguracion) {
        this.horariosParametros.remove(parametroConfiguracion);
        return this;
    }

    public void setHorariosParametros(Set<ParametroConfiguracion> parametroConfiguracions) {
        this.horariosParametros = parametroConfiguracions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Horario horario = (Horario) o;
        if (horario.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), horario.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Horario{" +
            "id=" + getId() +
            ", diaEntrenador='" + getDiaEntrenador() + "'" +
            ", disponibilidadEntrenador='" + isDisponibilidadEntrenador() + "'" +
            ", horarioInicioHorario='" + getHorarioInicioHorario() + "'" +
            ", horarioFinHorario='" + getHorarioFinHorario() + "'" +
            ", estadoHorario='" + isEstadoHorario() + "'" +
            "}";
    }
}
