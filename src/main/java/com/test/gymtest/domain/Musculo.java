package com.test.gymtest.domain;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * Class Musculo.
 * @author Bulletproof.
 */
@ApiModel(description = "Class Musculo. @author Bulletproof.")
@Entity
@Table(name = "musculo")
public class Musculo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 999)
    @Column(name = "nombre_musculo", length = 999, nullable = false)
    private String nombreMusculo;

    @NotNull
    @Column(name = "estado_musculo", nullable = false)
    private Boolean estadoMusculo;

    @OneToOne
    @JoinColumn(unique = true)
    private ParametroConfiguracion zonaMusculo;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreMusculo() {
        return nombreMusculo;
    }

    public Musculo nombreMusculo(String nombreMusculo) {
        this.nombreMusculo = nombreMusculo;
        return this;
    }

    public void setNombreMusculo(String nombreMusculo) {
        this.nombreMusculo = nombreMusculo;
    }

    public Boolean isEstadoMusculo() {
        return estadoMusculo;
    }

    public Musculo estadoMusculo(Boolean estadoMusculo) {
        this.estadoMusculo = estadoMusculo;
        return this;
    }

    public void setEstadoMusculo(Boolean estadoMusculo) {
        this.estadoMusculo = estadoMusculo;
    }

    public ParametroConfiguracion getZonaMusculo() {
        return zonaMusculo;
    }

    public Musculo zonaMusculo(ParametroConfiguracion parametroConfiguracion) {
        this.zonaMusculo = parametroConfiguracion;
        return this;
    }

    public void setZonaMusculo(ParametroConfiguracion parametroConfiguracion) {
        this.zonaMusculo = parametroConfiguracion;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Musculo musculo = (Musculo) o;
        if (musculo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), musculo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Musculo{" +
            "id=" + getId() +
            ", nombreMusculo='" + getNombreMusculo() + "'" +
            ", estadoMusculo='" + isEstadoMusculo() + "'" +
            "}";
    }
}
