package com.test.gymtest.domain;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * Class Multimedia.
 * @author Bulletproof.
 */
@ApiModel(description = "Class Multimedia. @author Bulletproof.")
@Entity
@Table(name = "multimedia")
public class Multimedia implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 999)
    @Column(name = "nombre_multimedia", length = 999, nullable = false)
    private String nombreMultimedia;

    @Column(name = "proposito_multimedia")
    private Integer propositoMultimedia;

    @NotNull
    @Column(name = "encode", nullable = false)
    private String encode;

    @OneToOne
    @JoinColumn(unique = true)
    private ParametroConfiguracion tipoMultimedia;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreMultimedia() {
        return nombreMultimedia;
    }

    public Multimedia nombreMultimedia(String nombreMultimedia) {
        this.nombreMultimedia = nombreMultimedia;
        return this;
    }

    public void setNombreMultimedia(String nombreMultimedia) {
        this.nombreMultimedia = nombreMultimedia;
    }

    public Integer getPropositoMultimedia() {
        return propositoMultimedia;
    }

    public Multimedia propositoMultimedia(Integer propositoMultimedia) {
        this.propositoMultimedia = propositoMultimedia;
        return this;
    }

    public void setPropositoMultimedia(Integer propositoMultimedia) {
        this.propositoMultimedia = propositoMultimedia;
    }

    public String getEncode() {
        return encode;
    }

    public Multimedia encode(String encode) {
        this.encode = encode;
        return this;
    }

    public void setEncode(String encode) {
        this.encode = encode;
    }

    public ParametroConfiguracion getTipoMultimedia() {
        return tipoMultimedia;
    }

    public Multimedia tipoMultimedia(ParametroConfiguracion parametroConfiguracion) {
        this.tipoMultimedia = parametroConfiguracion;
        return this;
    }

    public void setTipoMultimedia(ParametroConfiguracion parametroConfiguracion) {
        this.tipoMultimedia = parametroConfiguracion;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Multimedia multimedia = (Multimedia) o;
        if (multimedia.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), multimedia.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Multimedia{" +
            "id=" + getId() +
            ", nombreMultimedia='" + getNombreMultimedia() + "'" +
            ", propositoMultimedia=" + getPropositoMultimedia() +
            ", encode='" + getEncode() + "'" +
            "}";
    }
}
