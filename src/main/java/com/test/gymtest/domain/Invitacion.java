package com.test.gymtest.domain;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * Class Invitacion.
 * @author Bulletproof.
 */
@ApiModel(description = "Class Invitacion. @author Bulletproof.")
@Entity
@Table(name = "invitacion")
public class Invitacion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "horario_invitacion", nullable = false)
    private ZonedDateTime horarioInvitacion;

    @NotNull
    @Column(name = "estado_invitacion", nullable = false)
    private Boolean estadoInvitacion;

    @ManyToOne
    private Usuario usuario;

    @OneToOne
    @JoinColumn(unique = true)
    private Usuario aceptaInvitacion;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getHorarioInvitacion() {
        return horarioInvitacion;
    }

    public Invitacion horarioInvitacion(ZonedDateTime horarioInvitacion) {
        this.horarioInvitacion = horarioInvitacion;
        return this;
    }

    public void setHorarioInvitacion(ZonedDateTime horarioInvitacion) {
        this.horarioInvitacion = horarioInvitacion;
    }

    public Boolean isEstadoInvitacion() {
        return estadoInvitacion;
    }

    public Invitacion estadoInvitacion(Boolean estadoInvitacion) {
        this.estadoInvitacion = estadoInvitacion;
        return this;
    }

    public void setEstadoInvitacion(Boolean estadoInvitacion) {
        this.estadoInvitacion = estadoInvitacion;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public Invitacion usuario(Usuario usuario) {
        this.usuario = usuario;
        return this;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Usuario getAceptaInvitacion() {
        return aceptaInvitacion;
    }

    public Invitacion aceptaInvitacion(Usuario usuario) {
        this.aceptaInvitacion = usuario;
        return this;
    }

    public void setAceptaInvitacion(Usuario usuario) {
        this.aceptaInvitacion = usuario;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Invitacion invitacion = (Invitacion) o;
        if (invitacion.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), invitacion.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Invitacion{" +
            "id=" + getId() +
            ", horarioInvitacion='" + getHorarioInvitacion() + "'" +
            ", estadoInvitacion='" + isEstadoInvitacion() + "'" +
            "}";
    }
}
