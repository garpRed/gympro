package com.test.gymtest.domain;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * Class Ejercicio.
 * @author Bulletproof.
 */
@ApiModel(description = "Class Ejercicio. @author Bulletproof.")
@Entity
@Table(name = "ejercicio")
public class Ejercicio implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 999)
    @Column(name = "nombre_ejercicio", length = 999, nullable = false)
    private String nombreEjercicio;

    @NotNull
    @Column(name = "estado_ejercicio", nullable = false)
    private Boolean estadoEjercicio;

    @ManyToOne
    private Sede sede;

    @ManyToMany
    @JoinTable(name = "ejercicio_ejercicio_activos",
               joinColumns = @JoinColumn(name="ejercicios_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="ejercicio_activos_id", referencedColumnName="id"))
    private Set<Activo> ejercicioActivos = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "ejercicio_ejercicio_musculos",
               joinColumns = @JoinColumn(name="ejercicios_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="ejercicio_musculos_id", referencedColumnName="id"))
    private Set<Musculo> ejercicioMusculos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreEjercicio() {
        return nombreEjercicio;
    }

    public Ejercicio nombreEjercicio(String nombreEjercicio) {
        this.nombreEjercicio = nombreEjercicio;
        return this;
    }

    public void setNombreEjercicio(String nombreEjercicio) {
        this.nombreEjercicio = nombreEjercicio;
    }

    public Boolean isEstadoEjercicio() {
        return estadoEjercicio;
    }

    public Ejercicio estadoEjercicio(Boolean estadoEjercicio) {
        this.estadoEjercicio = estadoEjercicio;
        return this;
    }

    public void setEstadoEjercicio(Boolean estadoEjercicio) {
        this.estadoEjercicio = estadoEjercicio;
    }

    public Sede getSede() {
        return sede;
    }

    public Ejercicio sede(Sede sede) {
        this.sede = sede;
        return this;
    }

    public void setSede(Sede sede) {
        this.sede = sede;
    }

    public Set<Activo> getEjercicioActivos() {
        return ejercicioActivos;
    }

    public Ejercicio ejercicioActivos(Set<Activo> activos) {
        this.ejercicioActivos = activos;
        return this;
    }

    public Ejercicio addEjercicioActivos(Activo activo) {
        this.ejercicioActivos.add(activo);
        return this;
    }

    public Ejercicio removeEjercicioActivos(Activo activo) {
        this.ejercicioActivos.remove(activo);
        return this;
    }

    public void setEjercicioActivos(Set<Activo> activos) {
        this.ejercicioActivos = activos;
    }

    public Set<Musculo> getEjercicioMusculos() {
        return ejercicioMusculos;
    }

    public Ejercicio ejercicioMusculos(Set<Musculo> musculos) {
        this.ejercicioMusculos = musculos;
        return this;
    }

    public Ejercicio addEjercicioMusculos(Musculo musculo) {
        this.ejercicioMusculos.add(musculo);
        return this;
    }

    public Ejercicio removeEjercicioMusculos(Musculo musculo) {
        this.ejercicioMusculos.remove(musculo);
        return this;
    }

    public void setEjercicioMusculos(Set<Musculo> musculos) {
        this.ejercicioMusculos = musculos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Ejercicio ejercicio = (Ejercicio) o;
        if (ejercicio.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), ejercicio.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Ejercicio{" +
            "id=" + getId() +
            ", nombreEjercicio='" + getNombreEjercicio() + "'" +
            ", estadoEjercicio='" + isEstadoEjercicio() + "'" +
            "}";
    }
}
