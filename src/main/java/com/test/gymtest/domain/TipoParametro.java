package com.test.gymtest.domain;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * Class TipoParametro.
 * @author Bulletproof.
 */
@ApiModel(description = "Class TipoParametro. @author Bulletproof.")
@Entity
@Table(name = "tipo_parametro")
public class TipoParametro implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 999)
    @Column(name = "nombre_tipo_parametro", length = 999, nullable = false)
    private String nombreTipoParametro;

    @NotNull
    @Column(name = "estado_tipo_parametro", nullable = false)
    private Boolean estadoTipoParametro;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreTipoParametro() {
        return nombreTipoParametro;
    }

    public TipoParametro nombreTipoParametro(String nombreTipoParametro) {
        this.nombreTipoParametro = nombreTipoParametro;
        return this;
    }

    public void setNombreTipoParametro(String nombreTipoParametro) {
        this.nombreTipoParametro = nombreTipoParametro;
    }

    public Boolean isEstadoTipoParametro() {
        return estadoTipoParametro;
    }

    public TipoParametro estadoTipoParametro(Boolean estadoTipoParametro) {
        this.estadoTipoParametro = estadoTipoParametro;
        return this;
    }

    public void setEstadoTipoParametro(Boolean estadoTipoParametro) {
        this.estadoTipoParametro = estadoTipoParametro;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TipoParametro tipoParametro = (TipoParametro) o;
        if (tipoParametro.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tipoParametro.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TipoParametro{" +
            "id=" + getId() +
            ", nombreTipoParametro='" + getNombreTipoParametro() + "'" +
            ", estadoTipoParametro='" + isEstadoTipoParametro() + "'" +
            "}";
    }
}
