package com.test.gymtest.domain;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * Class RegistroActividadRutina.
 * @author Bulletproof.
 */
@ApiModel(description = "Class RegistroActividadRutina. @author Bulletproof.")
@Entity
@Table(name = "registro_actividad_rutina")
public class RegistroActividadRutina implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "fecha_asistencia", nullable = false)
    private ZonedDateTime fechaAsistencia;

    @NotNull
    @Column(name = "dia_asistencia", nullable = false)
    private Integer diaAsistencia;

    @NotNull
    @Column(name = "estado_registro_actividad", nullable = false)
    private Boolean estadoRegistroActividad;

    @ManyToOne
    private Usuario usuario;

    @ManyToOne
    private SubRutina subRutina;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getFechaAsistencia() {
        return fechaAsistencia;
    }

    public RegistroActividadRutina fechaAsistencia(ZonedDateTime fechaAsistencia) {
        this.fechaAsistencia = fechaAsistencia;
        return this;
    }

    public void setFechaAsistencia(ZonedDateTime fechaAsistencia) {
        this.fechaAsistencia = fechaAsistencia;
    }

    public Integer getDiaAsistencia() {
        return diaAsistencia;
    }

    public RegistroActividadRutina diaAsistencia(Integer diaAsistencia) {
        this.diaAsistencia = diaAsistencia;
        return this;
    }

    public void setDiaAsistencia(Integer diaAsistencia) {
        this.diaAsistencia = diaAsistencia;
    }

    public Boolean isEstadoRegistroActividad() {
        return estadoRegistroActividad;
    }

    public RegistroActividadRutina estadoRegistroActividad(Boolean estadoRegistroActividad) {
        this.estadoRegistroActividad = estadoRegistroActividad;
        return this;
    }

    public void setEstadoRegistroActividad(Boolean estadoRegistroActividad) {
        this.estadoRegistroActividad = estadoRegistroActividad;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public RegistroActividadRutina usuario(Usuario usuario) {
        this.usuario = usuario;
        return this;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public SubRutina getSubRutina() {
        return subRutina;
    }

    public RegistroActividadRutina subRutina(SubRutina subRutina) {
        this.subRutina = subRutina;
        return this;
    }

    public void setSubRutina(SubRutina subRutina) {
        this.subRutina = subRutina;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RegistroActividadRutina registroActividadRutina = (RegistroActividadRutina) o;
        if (registroActividadRutina.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), registroActividadRutina.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RegistroActividadRutina{" +
            "id=" + getId() +
            ", fechaAsistencia='" + getFechaAsistencia() + "'" +
            ", diaAsistencia=" + getDiaAsistencia() +
            ", estadoRegistroActividad='" + isEstadoRegistroActividad() + "'" +
            "}";
    }
}
