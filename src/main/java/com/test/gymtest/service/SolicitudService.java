package com.test.gymtest.service;

import com.test.gymtest.domain.Solicitud;
import com.test.gymtest.repository.SolicitudRepository;
import com.test.gymtest.service.dto.SolicitudDTO;
import com.test.gymtest.service.mapper.SolicitudMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Solicitud.
 */
@Service
@Transactional
public class SolicitudService {

    private final Logger log = LoggerFactory.getLogger(SolicitudService.class);

    private final SolicitudRepository solicitudRepository;

    private final SolicitudMapper solicitudMapper;

    public SolicitudService(SolicitudRepository solicitudRepository, SolicitudMapper solicitudMapper) {
        this.solicitudRepository = solicitudRepository;
        this.solicitudMapper = solicitudMapper;
    }

    /**
     * Save a solicitud.
     *
     * @param solicitudDTO the entity to save
     * @return the persisted entity
     */
    public SolicitudDTO save(SolicitudDTO solicitudDTO) {
        log.debug("Request to save Solicitud : {}", solicitudDTO);
        Solicitud solicitud = solicitudMapper.toEntity(solicitudDTO);
        solicitud = solicitudRepository.save(solicitud);
        return solicitudMapper.toDto(solicitud);
    }

    /**
     * Get all the solicituds.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SolicitudDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Solicituds");
        return solicitudRepository.findAll(pageable)
            .map(solicitudMapper::toDto);
    }

    /**
     * Get one solicitud by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public SolicitudDTO findOne(Long id) {
        log.debug("Request to get Solicitud : {}", id);
        Solicitud solicitud = solicitudRepository.findOne(id);
        return solicitudMapper.toDto(solicitud);
    }

    /**
     * Delete the solicitud by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Solicitud : {}", id);
        solicitudRepository.delete(id);
    }
}
