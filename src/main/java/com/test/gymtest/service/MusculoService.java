package com.test.gymtest.service;

import com.test.gymtest.domain.Musculo;
import com.test.gymtest.repository.MusculoRepository;
import com.test.gymtest.service.dto.MusculoDTO;
import com.test.gymtest.service.mapper.MusculoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Musculo.
 */
@Service
@Transactional
public class MusculoService {

    private final Logger log = LoggerFactory.getLogger(MusculoService.class);

    private final MusculoRepository musculoRepository;

    private final MusculoMapper musculoMapper;

    public MusculoService(MusculoRepository musculoRepository, MusculoMapper musculoMapper) {
        this.musculoRepository = musculoRepository;
        this.musculoMapper = musculoMapper;
    }

    /**
     * Save a musculo.
     *
     * @param musculoDTO the entity to save
     * @return the persisted entity
     */
    public MusculoDTO save(MusculoDTO musculoDTO) {
        log.debug("Request to save Musculo : {}", musculoDTO);
        Musculo musculo = musculoMapper.toEntity(musculoDTO);
        musculo = musculoRepository.save(musculo);
        return musculoMapper.toDto(musculo);
    }

    /**
     * Get all the musculos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MusculoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Musculos");
        return musculoRepository.findAll(pageable)
            .map(musculoMapper::toDto);
    }

    /**
     * Get one musculo by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public MusculoDTO findOne(Long id) {
        log.debug("Request to get Musculo : {}", id);
        Musculo musculo = musculoRepository.findOne(id);
        return musculoMapper.toDto(musculo);
    }

    /**
     * Delete the musculo by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Musculo : {}", id);
        musculoRepository.delete(id);
    }
}
