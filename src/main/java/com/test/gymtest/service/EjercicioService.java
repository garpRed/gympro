package com.test.gymtest.service;

import com.test.gymtest.domain.Ejercicio;
import com.test.gymtest.repository.EjercicioRepository;
import com.test.gymtest.service.dto.EjercicioDTO;
import com.test.gymtest.service.mapper.EjercicioMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Ejercicio.
 */
@Service
@Transactional
public class EjercicioService {

    private final Logger log = LoggerFactory.getLogger(EjercicioService.class);

    private final EjercicioRepository ejercicioRepository;

    private final EjercicioMapper ejercicioMapper;

    public EjercicioService(EjercicioRepository ejercicioRepository, EjercicioMapper ejercicioMapper) {
        this.ejercicioRepository = ejercicioRepository;
        this.ejercicioMapper = ejercicioMapper;
    }

    /**
     * Save a ejercicio.
     *
     * @param ejercicioDTO the entity to save
     * @return the persisted entity
     */
    public EjercicioDTO save(EjercicioDTO ejercicioDTO) {
        log.debug("Request to save Ejercicio : {}", ejercicioDTO);
        Ejercicio ejercicio = ejercicioMapper.toEntity(ejercicioDTO);
        ejercicio = ejercicioRepository.save(ejercicio);
        return ejercicioMapper.toDto(ejercicio);
    }

    /**
     * Get all the ejercicios.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<EjercicioDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Ejercicios");
        return ejercicioRepository.findAll(pageable)
            .map(ejercicioMapper::toDto);
    }

    /**
     * Get one ejercicio by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public EjercicioDTO findOne(Long id) {
        log.debug("Request to get Ejercicio : {}", id);
        Ejercicio ejercicio = ejercicioRepository.findOneWithEagerRelationships(id);
        return ejercicioMapper.toDto(ejercicio);
    }

    /**
     * Delete the ejercicio by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Ejercicio : {}", id);
        ejercicioRepository.delete(id);
    }
}
