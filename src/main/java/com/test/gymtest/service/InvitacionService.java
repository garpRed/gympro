package com.test.gymtest.service;

import com.test.gymtest.domain.Invitacion;
import com.test.gymtest.repository.InvitacionRepository;
import com.test.gymtest.service.dto.InvitacionDTO;
import com.test.gymtest.service.mapper.InvitacionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Invitacion.
 */
@Service
@Transactional
public class InvitacionService {

    private final Logger log = LoggerFactory.getLogger(InvitacionService.class);

    private final InvitacionRepository invitacionRepository;

    private final InvitacionMapper invitacionMapper;

    public InvitacionService(InvitacionRepository invitacionRepository, InvitacionMapper invitacionMapper) {
        this.invitacionRepository = invitacionRepository;
        this.invitacionMapper = invitacionMapper;
    }

    /**
     * Save a invitacion.
     *
     * @param invitacionDTO the entity to save
     * @return the persisted entity
     */
    public InvitacionDTO save(InvitacionDTO invitacionDTO) {
        log.debug("Request to save Invitacion : {}", invitacionDTO);
        Invitacion invitacion = invitacionMapper.toEntity(invitacionDTO);
        invitacion = invitacionRepository.save(invitacion);
        return invitacionMapper.toDto(invitacion);
    }

    /**
     * Get all the invitacions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<InvitacionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Invitacions");
        return invitacionRepository.findAll(pageable)
            .map(invitacionMapper::toDto);
    }

    /**
     * Get one invitacion by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public InvitacionDTO findOne(Long id) {
        log.debug("Request to get Invitacion : {}", id);
        Invitacion invitacion = invitacionRepository.findOne(id);
        return invitacionMapper.toDto(invitacion);
    }

    /**
     * Delete the invitacion by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Invitacion : {}", id);
        invitacionRepository.delete(id);
    }
}
