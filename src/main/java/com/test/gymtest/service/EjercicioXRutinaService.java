package com.test.gymtest.service;

import com.test.gymtest.domain.EjercicioXRutina;
import com.test.gymtest.repository.EjercicioXRutinaRepository;
import com.test.gymtest.service.dto.EjercicioXRutinaDTO;
import com.test.gymtest.service.mapper.EjercicioXRutinaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing EjercicioXRutina.
 */
@Service
@Transactional
public class EjercicioXRutinaService {

    private final Logger log = LoggerFactory.getLogger(EjercicioXRutinaService.class);

    private final EjercicioXRutinaRepository ejercicioXRutinaRepository;

    private final EjercicioXRutinaMapper ejercicioXRutinaMapper;

    public EjercicioXRutinaService(EjercicioXRutinaRepository ejercicioXRutinaRepository, EjercicioXRutinaMapper ejercicioXRutinaMapper) {
        this.ejercicioXRutinaRepository = ejercicioXRutinaRepository;
        this.ejercicioXRutinaMapper = ejercicioXRutinaMapper;
    }

    /**
     * Save a ejercicioXRutina.
     *
     * @param ejercicioXRutinaDTO the entity to save
     * @return the persisted entity
     */
    public EjercicioXRutinaDTO save(EjercicioXRutinaDTO ejercicioXRutinaDTO) {
        log.debug("Request to save EjercicioXRutina : {}", ejercicioXRutinaDTO);
        EjercicioXRutina ejercicioXRutina = ejercicioXRutinaMapper.toEntity(ejercicioXRutinaDTO);
        ejercicioXRutina = ejercicioXRutinaRepository.save(ejercicioXRutina);
        return ejercicioXRutinaMapper.toDto(ejercicioXRutina);
    }

    /**
     * Get all the ejercicioXRutinas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<EjercicioXRutinaDTO> findAll(Pageable pageable) {
        log.debug("Request to get all EjercicioXRutinas");
        return ejercicioXRutinaRepository.findAll(pageable)
            .map(ejercicioXRutinaMapper::toDto);
    }

    /**
     * Get one ejercicioXRutina by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public EjercicioXRutinaDTO findOne(Long id) {
        log.debug("Request to get EjercicioXRutina : {}", id);
        EjercicioXRutina ejercicioXRutina = ejercicioXRutinaRepository.findOne(id);
        return ejercicioXRutinaMapper.toDto(ejercicioXRutina);
    }

    /**
     * Delete the ejercicioXRutina by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete EjercicioXRutina : {}", id);
        ejercicioXRutinaRepository.delete(id);
    }
}
