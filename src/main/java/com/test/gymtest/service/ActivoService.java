package com.test.gymtest.service;

import com.test.gymtest.domain.Activo;
import com.test.gymtest.repository.ActivoRepository;
import com.test.gymtest.service.dto.ActivoDTO;
import com.test.gymtest.service.mapper.ActivoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Activo.
 */
@Service
@Transactional
public class ActivoService {

    private final Logger log = LoggerFactory.getLogger(ActivoService.class);

    private final ActivoRepository activoRepository;

    private final ActivoMapper activoMapper;

    public ActivoService(ActivoRepository activoRepository, ActivoMapper activoMapper) {
        this.activoRepository = activoRepository;
        this.activoMapper = activoMapper;
    }

    /**
     * Save a activo.
     *
     * @param activoDTO the entity to save
     * @return the persisted entity
     */
    public ActivoDTO save(ActivoDTO activoDTO) {
        log.debug("Request to save Activo : {}", activoDTO);
        Activo activo = activoMapper.toEntity(activoDTO);
        activo = activoRepository.save(activo);
        return activoMapper.toDto(activo);
    }

    /**
     * Get all the activos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ActivoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Activos");
        return activoRepository.findAll(pageable)
            .map(activoMapper::toDto);
    }

    /**
     * Get one activo by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ActivoDTO findOne(Long id) {
        log.debug("Request to get Activo : {}", id);
        Activo activo = activoRepository.findOne(id);
        return activoMapper.toDto(activo);
    }

    /**
     * Delete the activo by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Activo : {}", id);
        activoRepository.delete(id);
    }
}
