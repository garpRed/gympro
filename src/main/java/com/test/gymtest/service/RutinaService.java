package com.test.gymtest.service;

import com.test.gymtest.domain.Rutina;
import com.test.gymtest.repository.RutinaRepository;
import com.test.gymtest.service.dto.RutinaDTO;
import com.test.gymtest.service.mapper.RutinaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Rutina.
 */
@Service
@Transactional
public class RutinaService {

    private final Logger log = LoggerFactory.getLogger(RutinaService.class);

    private final RutinaRepository rutinaRepository;

    private final RutinaMapper rutinaMapper;

    public RutinaService(RutinaRepository rutinaRepository, RutinaMapper rutinaMapper) {
        this.rutinaRepository = rutinaRepository;
        this.rutinaMapper = rutinaMapper;
    }

    /**
     * Save a rutina.
     *
     * @param rutinaDTO the entity to save
     * @return the persisted entity
     */
    public RutinaDTO save(RutinaDTO rutinaDTO) {
        log.debug("Request to save Rutina : {}", rutinaDTO);
        Rutina rutina = rutinaMapper.toEntity(rutinaDTO);
        rutina = rutinaRepository.save(rutina);
        return rutinaMapper.toDto(rutina);
    }

    /**
     * Get all the rutinas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RutinaDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Rutinas");
        return rutinaRepository.findAll(pageable)
            .map(rutinaMapper::toDto);
    }

    /**
     * Get one rutina by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public RutinaDTO findOne(Long id) {
        log.debug("Request to get Rutina : {}", id);
        Rutina rutina = rutinaRepository.findOne(id);
        return rutinaMapper.toDto(rutina);
    }

    /**
     * Delete the rutina by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Rutina : {}", id);
        rutinaRepository.delete(id);
    }
}
