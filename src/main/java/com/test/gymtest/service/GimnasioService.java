package com.test.gymtest.service;

import com.test.gymtest.domain.Gimnasio;
import com.test.gymtest.repository.GimnasioRepository;
import com.test.gymtest.service.dto.GimnasioDTO;
import com.test.gymtest.service.mapper.GimnasioMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Gimnasio.
 */
@Service
@Transactional
public class GimnasioService {

    private final Logger log = LoggerFactory.getLogger(GimnasioService.class);

    private final GimnasioRepository gimnasioRepository;

    private final GimnasioMapper gimnasioMapper;

    public GimnasioService(GimnasioRepository gimnasioRepository, GimnasioMapper gimnasioMapper) {
        this.gimnasioRepository = gimnasioRepository;
        this.gimnasioMapper = gimnasioMapper;
    }

    /**
     * Save a gimnasio.
     *
     * @param gimnasioDTO the entity to save
     * @return the persisted entity
     */
    public GimnasioDTO save(GimnasioDTO gimnasioDTO) {
        log.debug("Request to save Gimnasio : {}", gimnasioDTO);
        Gimnasio gimnasio = gimnasioMapper.toEntity(gimnasioDTO);
        gimnasio = gimnasioRepository.save(gimnasio);
        return gimnasioMapper.toDto(gimnasio);
    }

    /**
     * Get all the gimnasios.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<GimnasioDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Gimnasios");
        return gimnasioRepository.findAll(pageable)
            .map(gimnasioMapper::toDto);
    }

    /**
     * Get one gimnasio by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public GimnasioDTO findOne(Long id) {
        log.debug("Request to get Gimnasio : {}", id);
        Gimnasio gimnasio = gimnasioRepository.findOne(id);
        return gimnasioMapper.toDto(gimnasio);
    }

    /**
     * Delete the gimnasio by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Gimnasio : {}", id);
        gimnasioRepository.delete(id);
    }
}
