package com.test.gymtest.service;

import com.test.gymtest.domain.TipoParametro;
import com.test.gymtest.repository.TipoParametroRepository;
import com.test.gymtest.service.dto.TipoParametroDTO;
import com.test.gymtest.service.mapper.TipoParametroMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing TipoParametro.
 */
@Service
@Transactional
public class TipoParametroService {

    private final Logger log = LoggerFactory.getLogger(TipoParametroService.class);

    private final TipoParametroRepository tipoParametroRepository;

    private final TipoParametroMapper tipoParametroMapper;

    public TipoParametroService(TipoParametroRepository tipoParametroRepository, TipoParametroMapper tipoParametroMapper) {
        this.tipoParametroRepository = tipoParametroRepository;
        this.tipoParametroMapper = tipoParametroMapper;
    }

    /**
     * Save a tipoParametro.
     *
     * @param tipoParametroDTO the entity to save
     * @return the persisted entity
     */
    public TipoParametroDTO save(TipoParametroDTO tipoParametroDTO) {
        log.debug("Request to save TipoParametro : {}", tipoParametroDTO);
        TipoParametro tipoParametro = tipoParametroMapper.toEntity(tipoParametroDTO);
        tipoParametro = tipoParametroRepository.save(tipoParametro);
        return tipoParametroMapper.toDto(tipoParametro);
    }

    /**
     * Get all the tipoParametros.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<TipoParametroDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TipoParametros");
        return tipoParametroRepository.findAll(pageable)
            .map(tipoParametroMapper::toDto);
    }

    /**
     * Get one tipoParametro by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public TipoParametroDTO findOne(Long id) {
        log.debug("Request to get TipoParametro : {}", id);
        TipoParametro tipoParametro = tipoParametroRepository.findOne(id);
        return tipoParametroMapper.toDto(tipoParametro);
    }

    /**
     * Delete the tipoParametro by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete TipoParametro : {}", id);
        tipoParametroRepository.delete(id);
    }
}
