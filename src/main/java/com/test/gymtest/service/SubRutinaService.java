package com.test.gymtest.service;

import com.test.gymtest.domain.SubRutina;
import com.test.gymtest.repository.SubRutinaRepository;
import com.test.gymtest.service.dto.SubRutinaDTO;
import com.test.gymtest.service.mapper.SubRutinaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing SubRutina.
 */
@Service
@Transactional
public class SubRutinaService {

    private final Logger log = LoggerFactory.getLogger(SubRutinaService.class);

    private final SubRutinaRepository subRutinaRepository;

    private final SubRutinaMapper subRutinaMapper;

    public SubRutinaService(SubRutinaRepository subRutinaRepository, SubRutinaMapper subRutinaMapper) {
        this.subRutinaRepository = subRutinaRepository;
        this.subRutinaMapper = subRutinaMapper;
    }

    /**
     * Save a subRutina.
     *
     * @param subRutinaDTO the entity to save
     * @return the persisted entity
     */
    public SubRutinaDTO save(SubRutinaDTO subRutinaDTO) {
        log.debug("Request to save SubRutina : {}", subRutinaDTO);
        SubRutina subRutina = subRutinaMapper.toEntity(subRutinaDTO);
        subRutina = subRutinaRepository.save(subRutina);
        return subRutinaMapper.toDto(subRutina);
    }

    /**
     * Get all the subRutinas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<SubRutinaDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SubRutinas");
        return subRutinaRepository.findAll(pageable)
            .map(subRutinaMapper::toDto);
    }

    /**
     * Get one subRutina by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public SubRutinaDTO findOne(Long id) {
        log.debug("Request to get SubRutina : {}", id);
        SubRutina subRutina = subRutinaRepository.findOneWithEagerRelationships(id);
        return subRutinaMapper.toDto(subRutina);
    }

    /**
     * Delete the subRutina by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete SubRutina : {}", id);
        subRutinaRepository.delete(id);
    }
}
