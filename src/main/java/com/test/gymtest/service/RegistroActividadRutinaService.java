package com.test.gymtest.service;

import com.test.gymtest.domain.RegistroActividadRutina;
import com.test.gymtest.repository.RegistroActividadRutinaRepository;
import com.test.gymtest.service.dto.RegistroActividadRutinaDTO;
import com.test.gymtest.service.mapper.RegistroActividadRutinaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing RegistroActividadRutina.
 */
@Service
@Transactional
public class RegistroActividadRutinaService {

    private final Logger log = LoggerFactory.getLogger(RegistroActividadRutinaService.class);

    private final RegistroActividadRutinaRepository registroActividadRutinaRepository;

    private final RegistroActividadRutinaMapper registroActividadRutinaMapper;

    public RegistroActividadRutinaService(RegistroActividadRutinaRepository registroActividadRutinaRepository, RegistroActividadRutinaMapper registroActividadRutinaMapper) {
        this.registroActividadRutinaRepository = registroActividadRutinaRepository;
        this.registroActividadRutinaMapper = registroActividadRutinaMapper;
    }

    /**
     * Save a registroActividadRutina.
     *
     * @param registroActividadRutinaDTO the entity to save
     * @return the persisted entity
     */
    public RegistroActividadRutinaDTO save(RegistroActividadRutinaDTO registroActividadRutinaDTO) {
        log.debug("Request to save RegistroActividadRutina : {}", registroActividadRutinaDTO);
        RegistroActividadRutina registroActividadRutina = registroActividadRutinaMapper.toEntity(registroActividadRutinaDTO);
        registroActividadRutina = registroActividadRutinaRepository.save(registroActividadRutina);
        return registroActividadRutinaMapper.toDto(registroActividadRutina);
    }

    /**
     * Get all the registroActividadRutinas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<RegistroActividadRutinaDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RegistroActividadRutinas");
        return registroActividadRutinaRepository.findAll(pageable)
            .map(registroActividadRutinaMapper::toDto);
    }

    /**
     * Get one registroActividadRutina by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public RegistroActividadRutinaDTO findOne(Long id) {
        log.debug("Request to get RegistroActividadRutina : {}", id);
        RegistroActividadRutina registroActividadRutina = registroActividadRutinaRepository.findOne(id);
        return registroActividadRutinaMapper.toDto(registroActividadRutina);
    }

    /**
     * Delete the registroActividadRutina by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete RegistroActividadRutina : {}", id);
        registroActividadRutinaRepository.delete(id);
    }
}
