package com.test.gymtest.service.dto;


import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the RegistroActividadRutina entity.
 */
public class RegistroActividadRutinaDTO implements Serializable {

    private Long id;

    @NotNull
    private ZonedDateTime fechaAsistencia;

    @NotNull
    private Integer diaAsistencia;

    @NotNull
    private Boolean estadoRegistroActividad;

    private Long usuarioId;

    private Long subRutinaId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getFechaAsistencia() {
        return fechaAsistencia;
    }

    public void setFechaAsistencia(ZonedDateTime fechaAsistencia) {
        this.fechaAsistencia = fechaAsistencia;
    }

    public Integer getDiaAsistencia() {
        return diaAsistencia;
    }

    public void setDiaAsistencia(Integer diaAsistencia) {
        this.diaAsistencia = diaAsistencia;
    }

    public Boolean isEstadoRegistroActividad() {
        return estadoRegistroActividad;
    }

    public void setEstadoRegistroActividad(Boolean estadoRegistroActividad) {
        this.estadoRegistroActividad = estadoRegistroActividad;
    }

    public Long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Long usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Long getSubRutinaId() {
        return subRutinaId;
    }

    public void setSubRutinaId(Long subRutinaId) {
        this.subRutinaId = subRutinaId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RegistroActividadRutinaDTO registroActividadRutinaDTO = (RegistroActividadRutinaDTO) o;
        if(registroActividadRutinaDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), registroActividadRutinaDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RegistroActividadRutinaDTO{" +
            "id=" + getId() +
            ", fechaAsistencia='" + getFechaAsistencia() + "'" +
            ", diaAsistencia=" + getDiaAsistencia() +
            ", estadoRegistroActividad='" + isEstadoRegistroActividad() + "'" +
            "}";
    }
}
