package com.test.gymtest.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Ejercicio entity.
 */
public class EjercicioDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 999)
    private String nombreEjercicio;

    @NotNull
    private Boolean estadoEjercicio;

    private Long sedeId;

    private Set<ActivoDTO> ejercicioActivos = new HashSet<>();

    private Set<MusculoDTO> ejercicioMusculos = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreEjercicio() {
        return nombreEjercicio;
    }

    public void setNombreEjercicio(String nombreEjercicio) {
        this.nombreEjercicio = nombreEjercicio;
    }

    public Boolean isEstadoEjercicio() {
        return estadoEjercicio;
    }

    public void setEstadoEjercicio(Boolean estadoEjercicio) {
        this.estadoEjercicio = estadoEjercicio;
    }

    public Long getSedeId() {
        return sedeId;
    }

    public void setSedeId(Long sedeId) {
        this.sedeId = sedeId;
    }

    public Set<ActivoDTO> getEjercicioActivos() {
        return ejercicioActivos;
    }

    public void setEjercicioActivos(Set<ActivoDTO> activos) {
        this.ejercicioActivos = activos;
    }

    public Set<MusculoDTO> getEjercicioMusculos() {
        return ejercicioMusculos;
    }

    public void setEjercicioMusculos(Set<MusculoDTO> musculos) {
        this.ejercicioMusculos = musculos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EjercicioDTO ejercicioDTO = (EjercicioDTO) o;
        if(ejercicioDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), ejercicioDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EjercicioDTO{" +
            "id=" + getId() +
            ", nombreEjercicio='" + getNombreEjercicio() + "'" +
            ", estadoEjercicio='" + isEstadoEjercicio() + "'" +
            "}";
    }
}
