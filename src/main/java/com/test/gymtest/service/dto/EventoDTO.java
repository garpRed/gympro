package com.test.gymtest.service.dto;


import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Evento entity.
 */
public class EventoDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 999)
    private String nombreEvento;

    @NotNull
    @Size(max = 999)
    private String descripcionEvento;

    @NotNull
    private ZonedDateTime fechaInicio;

    @NotNull
    private ZonedDateTime fechaFin;

    @NotNull
    private ZonedDateTime horaInicioEvento;

    @NotNull
    private ZonedDateTime horaFinEvento;

    @NotNull
    private Boolean estadoEvento;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreEvento() {
        return nombreEvento;
    }

    public void setNombreEvento(String nombreEvento) {
        this.nombreEvento = nombreEvento;
    }

    public String getDescripcionEvento() {
        return descripcionEvento;
    }

    public void setDescripcionEvento(String descripcionEvento) {
        this.descripcionEvento = descripcionEvento;
    }

    public ZonedDateTime getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(ZonedDateTime fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public ZonedDateTime getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(ZonedDateTime fechaFin) {
        this.fechaFin = fechaFin;
    }

    public ZonedDateTime getHoraInicioEvento() {
        return horaInicioEvento;
    }

    public void setHoraInicioEvento(ZonedDateTime horaInicioEvento) {
        this.horaInicioEvento = horaInicioEvento;
    }

    public ZonedDateTime getHoraFinEvento() {
        return horaFinEvento;
    }

    public void setHoraFinEvento(ZonedDateTime horaFinEvento) {
        this.horaFinEvento = horaFinEvento;
    }

    public Boolean isEstadoEvento() {
        return estadoEvento;
    }

    public void setEstadoEvento(Boolean estadoEvento) {
        this.estadoEvento = estadoEvento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EventoDTO eventoDTO = (EventoDTO) o;
        if(eventoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), eventoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EventoDTO{" +
            "id=" + getId() +
            ", nombreEvento='" + getNombreEvento() + "'" +
            ", descripcionEvento='" + getDescripcionEvento() + "'" +
            ", fechaInicio='" + getFechaInicio() + "'" +
            ", fechaFin='" + getFechaFin() + "'" +
            ", horaInicioEvento='" + getHoraInicioEvento() + "'" +
            ", horaFinEvento='" + getHoraFinEvento() + "'" +
            ", estadoEvento='" + isEstadoEvento() + "'" +
            "}";
    }
}
