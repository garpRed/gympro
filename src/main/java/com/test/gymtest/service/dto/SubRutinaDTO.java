package com.test.gymtest.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the SubRutina entity.
 */
public class SubRutinaDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 999)
    private String nombreSubRutina;

    @Min(value = 1)
    @Max(value = 500)
    private Integer cantSeries;

    @Min(value = 1)
    @Max(value = 500)
    private Integer cantRepeticiones;

    @Size(max = 999)
    private String comentarioSubRutina;

    @NotNull
    private Boolean estadoSubrutina;

    private Long rutinaId;

    private Set<EjercicioDTO> subrutinaEjercicios = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreSubRutina() {
        return nombreSubRutina;
    }

    public void setNombreSubRutina(String nombreSubRutina) {
        this.nombreSubRutina = nombreSubRutina;
    }

    public Integer getCantSeries() {
        return cantSeries;
    }

    public void setCantSeries(Integer cantSeries) {
        this.cantSeries = cantSeries;
    }

    public Integer getCantRepeticiones() {
        return cantRepeticiones;
    }

    public void setCantRepeticiones(Integer cantRepeticiones) {
        this.cantRepeticiones = cantRepeticiones;
    }

    public String getComentarioSubRutina() {
        return comentarioSubRutina;
    }

    public void setComentarioSubRutina(String comentarioSubRutina) {
        this.comentarioSubRutina = comentarioSubRutina;
    }

    public Boolean isEstadoSubrutina() {
        return estadoSubrutina;
    }

    public void setEstadoSubrutina(Boolean estadoSubrutina) {
        this.estadoSubrutina = estadoSubrutina;
    }

    public Long getRutinaId() {
        return rutinaId;
    }

    public void setRutinaId(Long rutinaId) {
        this.rutinaId = rutinaId;
    }

    public Set<EjercicioDTO> getSubrutinaEjercicios() {
        return subrutinaEjercicios;
    }

    public void setSubrutinaEjercicios(Set<EjercicioDTO> ejercicios) {
        this.subrutinaEjercicios = ejercicios;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SubRutinaDTO subRutinaDTO = (SubRutinaDTO) o;
        if(subRutinaDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), subRutinaDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SubRutinaDTO{" +
            "id=" + getId() +
            ", nombreSubRutina='" + getNombreSubRutina() + "'" +
            ", cantSeries=" + getCantSeries() +
            ", cantRepeticiones=" + getCantRepeticiones() +
            ", comentarioSubRutina='" + getComentarioSubRutina() + "'" +
            ", estadoSubrutina='" + isEstadoSubrutina() + "'" +
            "}";
    }
}
