package com.test.gymtest.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Activo entity.
 */
public class ActivoDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 999)
    private String nombreActivo;

    @NotNull
    @Max(value = 500)
    private Integer cantActivos;

    @NotNull
    private Boolean estadoActivo;

    private Long sedeId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreActivo() {
        return nombreActivo;
    }

    public void setNombreActivo(String nombreActivo) {
        this.nombreActivo = nombreActivo;
    }

    public Integer getCantActivos() {
        return cantActivos;
    }

    public void setCantActivos(Integer cantActivos) {
        this.cantActivos = cantActivos;
    }

    public Boolean isEstadoActivo() {
        return estadoActivo;
    }

    public void setEstadoActivo(Boolean estadoActivo) {
        this.estadoActivo = estadoActivo;
    }

    public Long getSedeId() {
        return sedeId;
    }

    public void setSedeId(Long sedeId) {
        this.sedeId = sedeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ActivoDTO activoDTO = (ActivoDTO) o;
        if(activoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), activoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ActivoDTO{" +
            "id=" + getId() +
            ", nombreActivo='" + getNombreActivo() + "'" +
            ", cantActivos=" + getCantActivos() +
            ", estadoActivo='" + isEstadoActivo() + "'" +
            "}";
    }
}
