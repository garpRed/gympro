package com.test.gymtest.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Rutina entity.
 */
public class RutinaDTO implements Serializable {

    private Long id;

    private Integer usuarioCreadorRutina;

    @NotNull
    @Size(max = 999)
    private String nombreRutina;

    @Min(value = 1)
    @Max(value = 500)
    private Integer cantDiasRutina;

    @NotNull
    private Boolean estadoRutina;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getUsuarioCreadorRutina() {
        return usuarioCreadorRutina;
    }

    public void setUsuarioCreadorRutina(Integer usuarioCreadorRutina) {
        this.usuarioCreadorRutina = usuarioCreadorRutina;
    }

    public String getNombreRutina() {
        return nombreRutina;
    }

    public void setNombreRutina(String nombreRutina) {
        this.nombreRutina = nombreRutina;
    }

    public Integer getCantDiasRutina() {
        return cantDiasRutina;
    }

    public void setCantDiasRutina(Integer cantDiasRutina) {
        this.cantDiasRutina = cantDiasRutina;
    }

    public Boolean isEstadoRutina() {
        return estadoRutina;
    }

    public void setEstadoRutina(Boolean estadoRutina) {
        this.estadoRutina = estadoRutina;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RutinaDTO rutinaDTO = (RutinaDTO) o;
        if(rutinaDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), rutinaDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RutinaDTO{" +
            "id=" + getId() +
            ", usuarioCreadorRutina=" + getUsuarioCreadorRutina() +
            ", nombreRutina='" + getNombreRutina() + "'" +
            ", cantDiasRutina=" + getCantDiasRutina() +
            ", estadoRutina='" + isEstadoRutina() + "'" +
            "}";
    }
}
