package com.test.gymtest.service.dto;


import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Horario entity.
 */
public class HorarioDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 1)
    private String diaEntrenador;

    @NotNull
    private Boolean disponibilidadEntrenador;

    @NotNull
    private ZonedDateTime horarioInicioHorario;

    @NotNull
    private ZonedDateTime horarioFinHorario;

    @NotNull
    private Boolean estadoHorario;

    private Long horarioUsuarioId;

    private Set<ParametroConfiguracionDTO> horariosParametros = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDiaEntrenador() {
        return diaEntrenador;
    }

    public void setDiaEntrenador(String diaEntrenador) {
        this.diaEntrenador = diaEntrenador;
    }

    public Boolean isDisponibilidadEntrenador() {
        return disponibilidadEntrenador;
    }

    public void setDisponibilidadEntrenador(Boolean disponibilidadEntrenador) {
        this.disponibilidadEntrenador = disponibilidadEntrenador;
    }

    public ZonedDateTime getHorarioInicioHorario() {
        return horarioInicioHorario;
    }

    public void setHorarioInicioHorario(ZonedDateTime horarioInicioHorario) {
        this.horarioInicioHorario = horarioInicioHorario;
    }

    public ZonedDateTime getHorarioFinHorario() {
        return horarioFinHorario;
    }

    public void setHorarioFinHorario(ZonedDateTime horarioFinHorario) {
        this.horarioFinHorario = horarioFinHorario;
    }

    public Boolean isEstadoHorario() {
        return estadoHorario;
    }

    public void setEstadoHorario(Boolean estadoHorario) {
        this.estadoHorario = estadoHorario;
    }

    public Long getHorarioUsuarioId() {
        return horarioUsuarioId;
    }

    public void setHorarioUsuarioId(Long usuarioId) {
        this.horarioUsuarioId = usuarioId;
    }

    public Set<ParametroConfiguracionDTO> getHorariosParametros() {
        return horariosParametros;
    }

    public void setHorariosParametros(Set<ParametroConfiguracionDTO> parametroConfiguracions) {
        this.horariosParametros = parametroConfiguracions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        HorarioDTO horarioDTO = (HorarioDTO) o;
        if(horarioDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), horarioDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "HorarioDTO{" +
            "id=" + getId() +
            ", diaEntrenador='" + getDiaEntrenador() + "'" +
            ", disponibilidadEntrenador='" + isDisponibilidadEntrenador() + "'" +
            ", horarioInicioHorario='" + getHorarioInicioHorario() + "'" +
            ", horarioFinHorario='" + getHorarioFinHorario() + "'" +
            ", estadoHorario='" + isEstadoHorario() + "'" +
            "}";
    }
}
