package com.test.gymtest.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the TipoParametro entity.
 */
public class TipoParametroDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 999)
    private String nombreTipoParametro;

    @NotNull
    private Boolean estadoTipoParametro;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreTipoParametro() {
        return nombreTipoParametro;
    }

    public void setNombreTipoParametro(String nombreTipoParametro) {
        this.nombreTipoParametro = nombreTipoParametro;
    }

    public Boolean isEstadoTipoParametro() {
        return estadoTipoParametro;
    }

    public void setEstadoTipoParametro(Boolean estadoTipoParametro) {
        this.estadoTipoParametro = estadoTipoParametro;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TipoParametroDTO tipoParametroDTO = (TipoParametroDTO) o;
        if(tipoParametroDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tipoParametroDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TipoParametroDTO{" +
            "id=" + getId() +
            ", nombreTipoParametro='" + getNombreTipoParametro() + "'" +
            ", estadoTipoParametro='" + isEstadoTipoParametro() + "'" +
            "}";
    }
}
