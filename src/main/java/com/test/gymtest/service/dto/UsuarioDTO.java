package com.test.gymtest.service.dto;


import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Usuario entity.
 */
public class UsuarioDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 15)
    private String nombreUsuario;

    @NotNull
    @Size(max = 15)
    private String apellidoUsuario;

    @NotNull
    @Size(max = 999)
    private String direccion;

    private ZonedDateTime fechaNacimiento;

    @NotNull
    @Size(max = 15)
    private String telefono;

    @NotNull
    @Size(min = 5, max = 20)
    private String identificacion;

    @NotNull
    @Size(max = 30)
    private String correoElectronico;

    @NotNull
    @Min(value = 1)
    @Max(value = 31)
    private Integer diaPago;

    private Boolean lesion;

    @Size(max = 999)
    private String descLesion;

    @NotNull
    private Boolean estado;

    @NotNull
    private String contrasenna;

    private Long userId;

    private Long sedeId;

    private Long rolId;

    private Long usuarioId;

    private Long rutinaId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getApellidoUsuario() {
        return apellidoUsuario;
    }

    public void setApellidoUsuario(String apellidoUsuario) {
        this.apellidoUsuario = apellidoUsuario;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public ZonedDateTime getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(ZonedDateTime fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public Integer getDiaPago() {
        return diaPago;
    }

    public void setDiaPago(Integer diaPago) {
        this.diaPago = diaPago;
    }

    public Boolean isLesion() {
        return lesion;
    }

    public void setLesion(Boolean lesion) {
        this.lesion = lesion;
    }

    public String getDescLesion() {
        return descLesion;
    }

    public void setDescLesion(String descLesion) {
        this.descLesion = descLesion;
    }

    public Boolean isEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public String getContrasenna() {
        return contrasenna;
    }

    public void setContrasenna(String contrasenna) {
        this.contrasenna = contrasenna;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getSedeId() {
        return sedeId;
    }

    public void setSedeId(Long sedeId) {
        this.sedeId = sedeId;
    }

    public Long getRolId() {
        return rolId;
    }

    public void setRolId(Long rolId) {
        this.rolId = rolId;
    }

    public Long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Long usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Long getRutinaId() {
        return rutinaId;
    }

    public void setRutinaId(Long rutinaId) {
        this.rutinaId = rutinaId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UsuarioDTO usuarioDTO = (UsuarioDTO) o;
        if(usuarioDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), usuarioDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UsuarioDTO{" +
            "id=" + getId() +
            ", nombreUsuario='" + getNombreUsuario() + "'" +
            ", apellidoUsuario='" + getApellidoUsuario() + "'" +
            ", direccion='" + getDireccion() + "'" +
            ", fechaNacimiento='" + getFechaNacimiento() + "'" +
            ", telefono='" + getTelefono() + "'" +
            ", identificacion='" + getIdentificacion() + "'" +
            ", correoElectronico='" + getCorreoElectronico() + "'" +
            ", diaPago=" + getDiaPago() +
            ", lesion='" + isLesion() + "'" +
            ", descLesion='" + getDescLesion() + "'" +
            ", estado='" + isEstado() + "'" +
            ", contrasenna='" + getContrasenna() + "'" +
            "}";
    }
}
