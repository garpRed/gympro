package com.test.gymtest.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the ParametroConfiguracion entity.
 */
public class ParametroConfiguracionDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 999)
    private String nombreParametro;

    @NotNull
    @Size(max = 999)
    private String valorParametro;

    @NotNull
    @Size(max = 50)
    private String tipoDeDato;

    @NotNull
    private Boolean estadoParametro;

    private Long tipoParametroId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreParametro() {
        return nombreParametro;
    }

    public void setNombreParametro(String nombreParametro) {
        this.nombreParametro = nombreParametro;
    }

    public String getValorParametro() {
        return valorParametro;
    }

    public void setValorParametro(String valorParametro) {
        this.valorParametro = valorParametro;
    }

    public String getTipoDeDato() {
        return tipoDeDato;
    }

    public void setTipoDeDato(String tipoDeDato) {
        this.tipoDeDato = tipoDeDato;
    }

    public Boolean isEstadoParametro() {
        return estadoParametro;
    }

    public void setEstadoParametro(Boolean estadoParametro) {
        this.estadoParametro = estadoParametro;
    }

    public Long getTipoParametroId() {
        return tipoParametroId;
    }

    public void setTipoParametroId(Long tipoParametroId) {
        this.tipoParametroId = tipoParametroId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ParametroConfiguracionDTO parametroConfiguracionDTO = (ParametroConfiguracionDTO) o;
        if(parametroConfiguracionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), parametroConfiguracionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ParametroConfiguracionDTO{" +
            "id=" + getId() +
            ", nombreParametro='" + getNombreParametro() + "'" +
            ", valorParametro='" + getValorParametro() + "'" +
            ", tipoDeDato='" + getTipoDeDato() + "'" +
            ", estadoParametro='" + isEstadoParametro() + "'" +
            "}";
    }
}
