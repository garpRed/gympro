package com.test.gymtest.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Multimedia entity.
 */
public class MultimediaDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 999)
    private String nombreMultimedia;

    private Integer propositoMultimedia;

    @NotNull
    private String encode;

    private Long tipoMultimediaId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreMultimedia() {
        return nombreMultimedia;
    }

    public void setNombreMultimedia(String nombreMultimedia) {
        this.nombreMultimedia = nombreMultimedia;
    }

    public Integer getPropositoMultimedia() {
        return propositoMultimedia;
    }

    public void setPropositoMultimedia(Integer propositoMultimedia) {
        this.propositoMultimedia = propositoMultimedia;
    }

    public String getEncode() {
        return encode;
    }

    public void setEncode(String encode) {
        this.encode = encode;
    }

    public Long getTipoMultimediaId() {
        return tipoMultimediaId;
    }

    public void setTipoMultimediaId(Long parametroConfiguracionId) {
        this.tipoMultimediaId = parametroConfiguracionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MultimediaDTO multimediaDTO = (MultimediaDTO) o;
        if(multimediaDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), multimediaDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MultimediaDTO{" +
            "id=" + getId() +
            ", nombreMultimedia='" + getNombreMultimedia() + "'" +
            ", propositoMultimedia=" + getPropositoMultimedia() +
            ", encode='" + getEncode() + "'" +
            "}";
    }
}
