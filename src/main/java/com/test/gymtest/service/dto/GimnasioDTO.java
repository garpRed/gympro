package com.test.gymtest.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Gimnasio entity.
 */
public class GimnasioDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 999)
    private String nombreGimnasio;

    @NotNull
    private Boolean estadoGimnasio;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreGimnasio() {
        return nombreGimnasio;
    }

    public void setNombreGimnasio(String nombreGimnasio) {
        this.nombreGimnasio = nombreGimnasio;
    }

    public Boolean isEstadoGimnasio() {
        return estadoGimnasio;
    }

    public void setEstadoGimnasio(Boolean estadoGimnasio) {
        this.estadoGimnasio = estadoGimnasio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GimnasioDTO gimnasioDTO = (GimnasioDTO) o;
        if(gimnasioDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), gimnasioDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "GimnasioDTO{" +
            "id=" + getId() +
            ", nombreGimnasio='" + getNombreGimnasio() + "'" +
            ", estadoGimnasio='" + isEstadoGimnasio() + "'" +
            "}";
    }
}
