package com.test.gymtest.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Rol entity.
 */
public class RolDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 15)
    private String nombreRol;

    @NotNull
    private Boolean estadoRol;

    private Set<PermisoDTO> rolXPermisos = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreRol() {
        return nombreRol;
    }

    public void setNombreRol(String nombreRol) {
        this.nombreRol = nombreRol;
    }

    public Boolean isEstadoRol() {
        return estadoRol;
    }

    public void setEstadoRol(Boolean estadoRol) {
        this.estadoRol = estadoRol;
    }

    public Set<PermisoDTO> getRolXPermisos() {
        return rolXPermisos;
    }

    public void setRolXPermisos(Set<PermisoDTO> permisos) {
        this.rolXPermisos = permisos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RolDTO rolDTO = (RolDTO) o;
        if(rolDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), rolDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RolDTO{" +
            "id=" + getId() +
            ", nombreRol='" + getNombreRol() + "'" +
            ", estadoRol='" + isEstadoRol() + "'" +
            "}";
    }
}
