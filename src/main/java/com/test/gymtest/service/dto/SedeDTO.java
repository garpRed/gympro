package com.test.gymtest.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Sede entity.
 */
public class SedeDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 999)
    private String nombreSede;

    @NotNull
    @Size(max = 999)
    private String direccionSede;

    @NotNull
    private Boolean estadoSede;

    private Long gimnasioId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreSede() {
        return nombreSede;
    }

    public void setNombreSede(String nombreSede) {
        this.nombreSede = nombreSede;
    }

    public String getDireccionSede() {
        return direccionSede;
    }

    public void setDireccionSede(String direccionSede) {
        this.direccionSede = direccionSede;
    }

    public Boolean isEstadoSede() {
        return estadoSede;
    }

    public void setEstadoSede(Boolean estadoSede) {
        this.estadoSede = estadoSede;
    }

    public Long getGimnasioId() {
        return gimnasioId;
    }

    public void setGimnasioId(Long gimnasioId) {
        this.gimnasioId = gimnasioId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SedeDTO sedeDTO = (SedeDTO) o;
        if(sedeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sedeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SedeDTO{" +
            "id=" + getId() +
            ", nombreSede='" + getNombreSede() + "'" +
            ", direccionSede='" + getDireccionSede() + "'" +
            ", estadoSede='" + isEstadoSede() + "'" +
            "}";
    }
}
