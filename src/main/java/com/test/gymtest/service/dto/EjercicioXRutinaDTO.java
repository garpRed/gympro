package com.test.gymtest.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the EjercicioXRutina entity.
 */
public class EjercicioXRutinaDTO implements Serializable {

    private Long id;

    @Max(value = 500)
    private Integer peso;

    @Max(value = 500)
    private Integer reps;

    @Max(value = 500)
    private Integer sets;

    @Size(max = 20)
    private String ajuste;

    @Size(max = 10)
    private String tiempo;

    @Size(max = 50)
    private String commentario;

    @NotNull
    private Boolean estadoEjercicioRutina;

    private Long ejercicioXRutinaId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPeso() {
        return peso;
    }

    public void setPeso(Integer peso) {
        this.peso = peso;
    }

    public Integer getReps() {
        return reps;
    }

    public void setReps(Integer reps) {
        this.reps = reps;
    }

    public Integer getSets() {
        return sets;
    }

    public void setSets(Integer sets) {
        this.sets = sets;
    }

    public String getAjuste() {
        return ajuste;
    }

    public void setAjuste(String ajuste) {
        this.ajuste = ajuste;
    }

    public String getTiempo() {
        return tiempo;
    }

    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }

    public String getCommentario() {
        return commentario;
    }

    public void setCommentario(String commentario) {
        this.commentario = commentario;
    }

    public Boolean isEstadoEjercicioRutina() {
        return estadoEjercicioRutina;
    }

    public void setEstadoEjercicioRutina(Boolean estadoEjercicioRutina) {
        this.estadoEjercicioRutina = estadoEjercicioRutina;
    }

    public Long getEjercicioXRutinaId() {
        return ejercicioXRutinaId;
    }

    public void setEjercicioXRutinaId(Long subRutinaId) {
        this.ejercicioXRutinaId = subRutinaId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EjercicioXRutinaDTO ejercicioXRutinaDTO = (EjercicioXRutinaDTO) o;
        if(ejercicioXRutinaDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), ejercicioXRutinaDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EjercicioXRutinaDTO{" +
            "id=" + getId() +
            ", peso=" + getPeso() +
            ", reps=" + getReps() +
            ", sets=" + getSets() +
            ", ajuste='" + getAjuste() + "'" +
            ", tiempo='" + getTiempo() + "'" +
            ", commentario='" + getCommentario() + "'" +
            ", estadoEjercicioRutina='" + isEstadoEjercicioRutina() + "'" +
            "}";
    }
}
