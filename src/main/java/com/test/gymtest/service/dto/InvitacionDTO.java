package com.test.gymtest.service.dto;


import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Invitacion entity.
 */
public class InvitacionDTO implements Serializable {

    private Long id;

    @NotNull
    private ZonedDateTime horarioInvitacion;

    @NotNull
    private Boolean estadoInvitacion;

    private Long usuarioId;

    private Long aceptaInvitacionId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getHorarioInvitacion() {
        return horarioInvitacion;
    }

    public void setHorarioInvitacion(ZonedDateTime horarioInvitacion) {
        this.horarioInvitacion = horarioInvitacion;
    }

    public Boolean isEstadoInvitacion() {
        return estadoInvitacion;
    }

    public void setEstadoInvitacion(Boolean estadoInvitacion) {
        this.estadoInvitacion = estadoInvitacion;
    }

    public Long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Long usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Long getAceptaInvitacionId() {
        return aceptaInvitacionId;
    }

    public void setAceptaInvitacionId(Long usuarioId) {
        this.aceptaInvitacionId = usuarioId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InvitacionDTO invitacionDTO = (InvitacionDTO) o;
        if(invitacionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), invitacionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "InvitacionDTO{" +
            "id=" + getId() +
            ", horarioInvitacion='" + getHorarioInvitacion() + "'" +
            ", estadoInvitacion='" + isEstadoInvitacion() + "'" +
            "}";
    }
}
