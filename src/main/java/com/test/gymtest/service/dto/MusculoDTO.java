package com.test.gymtest.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Musculo entity.
 */
public class MusculoDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 999)
    private String nombreMusculo;

    @NotNull
    private Boolean estadoMusculo;

    private Long zonaMusculoId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreMusculo() {
        return nombreMusculo;
    }

    public void setNombreMusculo(String nombreMusculo) {
        this.nombreMusculo = nombreMusculo;
    }

    public Boolean isEstadoMusculo() {
        return estadoMusculo;
    }

    public void setEstadoMusculo(Boolean estadoMusculo) {
        this.estadoMusculo = estadoMusculo;
    }

    public Long getZonaMusculoId() {
        return zonaMusculoId;
    }

    public void setZonaMusculoId(Long parametroConfiguracionId) {
        this.zonaMusculoId = parametroConfiguracionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MusculoDTO musculoDTO = (MusculoDTO) o;
        if(musculoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), musculoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "MusculoDTO{" +
            "id=" + getId() +
            ", nombreMusculo='" + getNombreMusculo() + "'" +
            ", estadoMusculo='" + isEstadoMusculo() + "'" +
            "}";
    }
}
