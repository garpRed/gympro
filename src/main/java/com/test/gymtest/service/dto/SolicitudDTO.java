package com.test.gymtest.service.dto;


import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Solicitud entity.
 */
public class SolicitudDTO implements Serializable {

    private Long id;

    @Size(max = 999)
    private String descripcionSolicitud;

    @NotNull
    private ZonedDateTime fechaSolicitud;

    @NotNull
    private ZonedDateTime horaInicio;

    @NotNull
    private ZonedDateTime horaFin;

    private Long usuarioId;

    private Long entrenadorSolicitudId;

    private Long tipoSolicitudId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcionSolicitud() {
        return descripcionSolicitud;
    }

    public void setDescripcionSolicitud(String descripcionSolicitud) {
        this.descripcionSolicitud = descripcionSolicitud;
    }

    public ZonedDateTime getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(ZonedDateTime fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public ZonedDateTime getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(ZonedDateTime horaInicio) {
        this.horaInicio = horaInicio;
    }

    public ZonedDateTime getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(ZonedDateTime horaFin) {
        this.horaFin = horaFin;
    }

    public Long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Long usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Long getEntrenadorSolicitudId() {
        return entrenadorSolicitudId;
    }

    public void setEntrenadorSolicitudId(Long usuarioId) {
        this.entrenadorSolicitudId = usuarioId;
    }

    public Long getTipoSolicitudId() {
        return tipoSolicitudId;
    }

    public void setTipoSolicitudId(Long parametroConfiguracionId) {
        this.tipoSolicitudId = parametroConfiguracionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SolicitudDTO solicitudDTO = (SolicitudDTO) o;
        if(solicitudDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), solicitudDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SolicitudDTO{" +
            "id=" + getId() +
            ", descripcionSolicitud='" + getDescripcionSolicitud() + "'" +
            ", fechaSolicitud='" + getFechaSolicitud() + "'" +
            ", horaInicio='" + getHoraInicio() + "'" +
            ", horaFin='" + getHoraFin() + "'" +
            "}";
    }
}
