package com.test.gymtest.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Permiso entity.
 */
public class PermisoDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 999)
    private String nombrePermiso;

    @NotNull
    private Boolean estadoPermiso;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombrePermiso() {
        return nombrePermiso;
    }

    public void setNombrePermiso(String nombrePermiso) {
        this.nombrePermiso = nombrePermiso;
    }

    public Boolean isEstadoPermiso() {
        return estadoPermiso;
    }

    public void setEstadoPermiso(Boolean estadoPermiso) {
        this.estadoPermiso = estadoPermiso;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PermisoDTO permisoDTO = (PermisoDTO) o;
        if(permisoDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), permisoDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PermisoDTO{" +
            "id=" + getId() +
            ", nombrePermiso='" + getNombrePermiso() + "'" +
            ", estadoPermiso='" + isEstadoPermiso() + "'" +
            "}";
    }
}
