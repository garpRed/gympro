package com.test.gymtest.service.mapper;

import com.test.gymtest.domain.*;
import com.test.gymtest.service.dto.ActivoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Activo and its DTO ActivoDTO.
 */
@Mapper(componentModel = "spring", uses = {SedeMapper.class})
public interface ActivoMapper extends EntityMapper<ActivoDTO, Activo> {

    @Mapping(source = "sede.id", target = "sedeId")
    ActivoDTO toDto(Activo activo);

    @Mapping(source = "sedeId", target = "sede")
    Activo toEntity(ActivoDTO activoDTO);

    default Activo fromId(Long id) {
        if (id == null) {
            return null;
        }
        Activo activo = new Activo();
        activo.setId(id);
        return activo;
    }
}
