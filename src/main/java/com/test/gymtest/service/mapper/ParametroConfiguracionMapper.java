package com.test.gymtest.service.mapper;

import com.test.gymtest.domain.*;
import com.test.gymtest.service.dto.ParametroConfiguracionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ParametroConfiguracion and its DTO ParametroConfiguracionDTO.
 */
@Mapper(componentModel = "spring", uses = {TipoParametroMapper.class})
public interface ParametroConfiguracionMapper extends EntityMapper<ParametroConfiguracionDTO, ParametroConfiguracion> {

    @Mapping(source = "tipoParametro.id", target = "tipoParametroId")
    ParametroConfiguracionDTO toDto(ParametroConfiguracion parametroConfiguracion);

    @Mapping(source = "tipoParametroId", target = "tipoParametro")
    @Mapping(target = "tipoMultimedia", ignore = true)
    @Mapping(target = "zonaMusculo", ignore = true)
    ParametroConfiguracion toEntity(ParametroConfiguracionDTO parametroConfiguracionDTO);

    default ParametroConfiguracion fromId(Long id) {
        if (id == null) {
            return null;
        }
        ParametroConfiguracion parametroConfiguracion = new ParametroConfiguracion();
        parametroConfiguracion.setId(id);
        return parametroConfiguracion;
    }
}
