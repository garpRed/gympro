package com.test.gymtest.service.mapper;

import com.test.gymtest.domain.*;
import com.test.gymtest.service.dto.TipoParametroDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity TipoParametro and its DTO TipoParametroDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TipoParametroMapper extends EntityMapper<TipoParametroDTO, TipoParametro> {



    default TipoParametro fromId(Long id) {
        if (id == null) {
            return null;
        }
        TipoParametro tipoParametro = new TipoParametro();
        tipoParametro.setId(id);
        return tipoParametro;
    }
}
