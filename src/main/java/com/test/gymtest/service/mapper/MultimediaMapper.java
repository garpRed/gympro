package com.test.gymtest.service.mapper;

import com.test.gymtest.domain.*;
import com.test.gymtest.service.dto.MultimediaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Multimedia and its DTO MultimediaDTO.
 */
@Mapper(componentModel = "spring", uses = {ParametroConfiguracionMapper.class})
public interface MultimediaMapper extends EntityMapper<MultimediaDTO, Multimedia> {

    @Mapping(source = "tipoMultimedia.id", target = "tipoMultimediaId")
    MultimediaDTO toDto(Multimedia multimedia);

    @Mapping(source = "tipoMultimediaId", target = "tipoMultimedia")
    Multimedia toEntity(MultimediaDTO multimediaDTO);

    default Multimedia fromId(Long id) {
        if (id == null) {
            return null;
        }
        Multimedia multimedia = new Multimedia();
        multimedia.setId(id);
        return multimedia;
    }
}
