package com.test.gymtest.service.mapper;

import com.test.gymtest.domain.*;
import com.test.gymtest.service.dto.UsuarioDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Usuario and its DTO UsuarioDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, SedeMapper.class, RolMapper.class, RutinaMapper.class})
public interface UsuarioMapper extends EntityMapper<UsuarioDTO, Usuario> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "sede.id", target = "sedeId")
    @Mapping(source = "rol.id", target = "rolId")
    @Mapping(source = "usuario.id", target = "usuarioId")
    @Mapping(source = "rutina.id", target = "rutinaId")
    UsuarioDTO toDto(Usuario usuario);

    @Mapping(source = "userId", target = "user")
    @Mapping(source = "sedeId", target = "sede")
    @Mapping(source = "rolId", target = "rol")
    @Mapping(source = "usuarioId", target = "usuario")
    @Mapping(target = "entrenadorUsuarios", ignore = true)
    @Mapping(target = "registrosActividads", ignore = true)
    @Mapping(target = "solicitudesUsuarios", ignore = true)
    @Mapping(target = "invitacionesUsuarios", ignore = true)
    @Mapping(target = "aceptaInvitacion", ignore = true)
    @Mapping(source = "rutinaId", target = "rutina")
    Usuario toEntity(UsuarioDTO usuarioDTO);

    default Usuario fromId(Long id) {
        if (id == null) {
            return null;
        }
        Usuario usuario = new Usuario();
        usuario.setId(id);
        return usuario;
    }
}
