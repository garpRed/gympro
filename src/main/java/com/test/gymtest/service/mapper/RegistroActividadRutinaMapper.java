package com.test.gymtest.service.mapper;

import com.test.gymtest.domain.*;
import com.test.gymtest.service.dto.RegistroActividadRutinaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity RegistroActividadRutina and its DTO RegistroActividadRutinaDTO.
 */
@Mapper(componentModel = "spring", uses = {UsuarioMapper.class, SubRutinaMapper.class})
public interface RegistroActividadRutinaMapper extends EntityMapper<RegistroActividadRutinaDTO, RegistroActividadRutina> {

    @Mapping(source = "usuario.id", target = "usuarioId")
    @Mapping(source = "subRutina.id", target = "subRutinaId")
    RegistroActividadRutinaDTO toDto(RegistroActividadRutina registroActividadRutina);

    @Mapping(source = "usuarioId", target = "usuario")
    @Mapping(source = "subRutinaId", target = "subRutina")
    RegistroActividadRutina toEntity(RegistroActividadRutinaDTO registroActividadRutinaDTO);

    default RegistroActividadRutina fromId(Long id) {
        if (id == null) {
            return null;
        }
        RegistroActividadRutina registroActividadRutina = new RegistroActividadRutina();
        registroActividadRutina.setId(id);
        return registroActividadRutina;
    }
}
