package com.test.gymtest.service.mapper;

import com.test.gymtest.domain.*;
import com.test.gymtest.service.dto.RutinaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Rutina and its DTO RutinaDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RutinaMapper extends EntityMapper<RutinaDTO, Rutina> {


    @Mapping(target = "subrutinaRutinas", ignore = true)
    @Mapping(target = "rutinaUsuarios", ignore = true)
    Rutina toEntity(RutinaDTO rutinaDTO);

    default Rutina fromId(Long id) {
        if (id == null) {
            return null;
        }
        Rutina rutina = new Rutina();
        rutina.setId(id);
        return rutina;
    }
}
