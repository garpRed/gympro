package com.test.gymtest.service.mapper;

import com.test.gymtest.domain.*;
import com.test.gymtest.service.dto.GimnasioDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Gimnasio and its DTO GimnasioDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface GimnasioMapper extends EntityMapper<GimnasioDTO, Gimnasio> {


    @Mapping(target = "gimnasioSedes", ignore = true)
    Gimnasio toEntity(GimnasioDTO gimnasioDTO);

    default Gimnasio fromId(Long id) {
        if (id == null) {
            return null;
        }
        Gimnasio gimnasio = new Gimnasio();
        gimnasio.setId(id);
        return gimnasio;
    }
}
