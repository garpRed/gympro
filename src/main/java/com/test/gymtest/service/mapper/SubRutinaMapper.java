package com.test.gymtest.service.mapper;

import com.test.gymtest.domain.*;
import com.test.gymtest.service.dto.SubRutinaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SubRutina and its DTO SubRutinaDTO.
 */
@Mapper(componentModel = "spring", uses = {RutinaMapper.class, EjercicioMapper.class})
public interface SubRutinaMapper extends EntityMapper<SubRutinaDTO, SubRutina> {

    @Mapping(source = "rutina.id", target = "rutinaId")
    SubRutinaDTO toDto(SubRutina subRutina);

    @Mapping(source = "rutinaId", target = "rutina")
    @Mapping(target = "subrutinaRegistros", ignore = true)
    SubRutina toEntity(SubRutinaDTO subRutinaDTO);

    default SubRutina fromId(Long id) {
        if (id == null) {
            return null;
        }
        SubRutina subRutina = new SubRutina();
        subRutina.setId(id);
        return subRutina;
    }
}
