package com.test.gymtest.service.mapper;

import com.test.gymtest.domain.*;
import com.test.gymtest.service.dto.SolicitudDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Solicitud and its DTO SolicitudDTO.
 */
@Mapper(componentModel = "spring", uses = {UsuarioMapper.class, ParametroConfiguracionMapper.class})
public interface SolicitudMapper extends EntityMapper<SolicitudDTO, Solicitud> {

    @Mapping(source = "usuario.id", target = "usuarioId")
    @Mapping(source = "entrenadorSolicitud.id", target = "entrenadorSolicitudId")
    @Mapping(source = "tipoSolicitud.id", target = "tipoSolicitudId")
    SolicitudDTO toDto(Solicitud solicitud);

    @Mapping(source = "usuarioId", target = "usuario")
    @Mapping(source = "entrenadorSolicitudId", target = "entrenadorSolicitud")
    @Mapping(source = "tipoSolicitudId", target = "tipoSolicitud")
    Solicitud toEntity(SolicitudDTO solicitudDTO);

    default Solicitud fromId(Long id) {
        if (id == null) {
            return null;
        }
        Solicitud solicitud = new Solicitud();
        solicitud.setId(id);
        return solicitud;
    }
}
