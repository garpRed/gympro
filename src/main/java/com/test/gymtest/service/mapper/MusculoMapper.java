package com.test.gymtest.service.mapper;

import com.test.gymtest.domain.*;
import com.test.gymtest.service.dto.MusculoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Musculo and its DTO MusculoDTO.
 */
@Mapper(componentModel = "spring", uses = {ParametroConfiguracionMapper.class})
public interface MusculoMapper extends EntityMapper<MusculoDTO, Musculo> {

    @Mapping(source = "zonaMusculo.id", target = "zonaMusculoId")
    MusculoDTO toDto(Musculo musculo);

    @Mapping(source = "zonaMusculoId", target = "zonaMusculo")
    Musculo toEntity(MusculoDTO musculoDTO);

    default Musculo fromId(Long id) {
        if (id == null) {
            return null;
        }
        Musculo musculo = new Musculo();
        musculo.setId(id);
        return musculo;
    }
}
