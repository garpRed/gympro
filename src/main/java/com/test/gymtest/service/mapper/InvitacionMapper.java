package com.test.gymtest.service.mapper;

import com.test.gymtest.domain.*;
import com.test.gymtest.service.dto.InvitacionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Invitacion and its DTO InvitacionDTO.
 */
@Mapper(componentModel = "spring", uses = {UsuarioMapper.class})
public interface InvitacionMapper extends EntityMapper<InvitacionDTO, Invitacion> {

    @Mapping(source = "usuario.id", target = "usuarioId")
    @Mapping(source = "aceptaInvitacion.id", target = "aceptaInvitacionId")
    InvitacionDTO toDto(Invitacion invitacion);

    @Mapping(source = "usuarioId", target = "usuario")
    @Mapping(source = "aceptaInvitacionId", target = "aceptaInvitacion")
    Invitacion toEntity(InvitacionDTO invitacionDTO);

    default Invitacion fromId(Long id) {
        if (id == null) {
            return null;
        }
        Invitacion invitacion = new Invitacion();
        invitacion.setId(id);
        return invitacion;
    }
}
