package com.test.gymtest.service.mapper;

import com.test.gymtest.domain.*;
import com.test.gymtest.service.dto.EjercicioXRutinaDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity EjercicioXRutina and its DTO EjercicioXRutinaDTO.
 */
@Mapper(componentModel = "spring", uses = {SubRutinaMapper.class})
public interface EjercicioXRutinaMapper extends EntityMapper<EjercicioXRutinaDTO, EjercicioXRutina> {

    @Mapping(source = "ejercicioXRutina.id", target = "ejercicioXRutinaId")
    EjercicioXRutinaDTO toDto(EjercicioXRutina ejercicioXRutina);

    @Mapping(source = "ejercicioXRutinaId", target = "ejercicioXRutina")
    EjercicioXRutina toEntity(EjercicioXRutinaDTO ejercicioXRutinaDTO);

    default EjercicioXRutina fromId(Long id) {
        if (id == null) {
            return null;
        }
        EjercicioXRutina ejercicioXRutina = new EjercicioXRutina();
        ejercicioXRutina.setId(id);
        return ejercicioXRutina;
    }
}
