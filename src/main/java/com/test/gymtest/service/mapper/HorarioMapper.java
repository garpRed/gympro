package com.test.gymtest.service.mapper;

import com.test.gymtest.domain.*;
import com.test.gymtest.service.dto.HorarioDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Horario and its DTO HorarioDTO.
 */
@Mapper(componentModel = "spring", uses = {UsuarioMapper.class, ParametroConfiguracionMapper.class})
public interface HorarioMapper extends EntityMapper<HorarioDTO, Horario> {

    @Mapping(source = "horarioUsuario.id", target = "horarioUsuarioId")
    HorarioDTO toDto(Horario horario);

    @Mapping(source = "horarioUsuarioId", target = "horarioUsuario")
    Horario toEntity(HorarioDTO horarioDTO);

    default Horario fromId(Long id) {
        if (id == null) {
            return null;
        }
        Horario horario = new Horario();
        horario.setId(id);
        return horario;
    }
}
