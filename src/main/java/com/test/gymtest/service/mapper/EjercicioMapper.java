package com.test.gymtest.service.mapper;

import com.test.gymtest.domain.*;
import com.test.gymtest.service.dto.EjercicioDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Ejercicio and its DTO EjercicioDTO.
 */
@Mapper(componentModel = "spring", uses = {SedeMapper.class, ActivoMapper.class, MusculoMapper.class})
public interface EjercicioMapper extends EntityMapper<EjercicioDTO, Ejercicio> {

    @Mapping(source = "sede.id", target = "sedeId")
    EjercicioDTO toDto(Ejercicio ejercicio);

    @Mapping(source = "sedeId", target = "sede")
    Ejercicio toEntity(EjercicioDTO ejercicioDTO);

    default Ejercicio fromId(Long id) {
        if (id == null) {
            return null;
        }
        Ejercicio ejercicio = new Ejercicio();
        ejercicio.setId(id);
        return ejercicio;
    }
}
