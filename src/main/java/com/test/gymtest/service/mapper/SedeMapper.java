package com.test.gymtest.service.mapper;

import com.test.gymtest.domain.*;
import com.test.gymtest.service.dto.SedeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Sede and its DTO SedeDTO.
 */
@Mapper(componentModel = "spring", uses = {GimnasioMapper.class})
public interface SedeMapper extends EntityMapper<SedeDTO, Sede> {

    @Mapping(source = "gimnasio.id", target = "gimnasioId")
    SedeDTO toDto(Sede sede);

    @Mapping(target = "activosSedes", ignore = true)
    @Mapping(target = "ejerciciosSedes", ignore = true)
    @Mapping(source = "gimnasioId", target = "gimnasio")
    Sede toEntity(SedeDTO sedeDTO);

    default Sede fromId(Long id) {
        if (id == null) {
            return null;
        }
        Sede sede = new Sede();
        sede.setId(id);
        return sede;
    }
}
