package com.test.gymtest.service;

import com.test.gymtest.domain.Multimedia;
import com.test.gymtest.repository.MultimediaRepository;
import com.test.gymtest.service.dto.MultimediaDTO;
import com.test.gymtest.service.mapper.MultimediaMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Multimedia.
 */
@Service
@Transactional
public class MultimediaService {

    private final Logger log = LoggerFactory.getLogger(MultimediaService.class);

    private final MultimediaRepository multimediaRepository;

    private final MultimediaMapper multimediaMapper;

    public MultimediaService(MultimediaRepository multimediaRepository, MultimediaMapper multimediaMapper) {
        this.multimediaRepository = multimediaRepository;
        this.multimediaMapper = multimediaMapper;
    }

    /**
     * Save a multimedia.
     *
     * @param multimediaDTO the entity to save
     * @return the persisted entity
     */
    public MultimediaDTO save(MultimediaDTO multimediaDTO) {
        log.debug("Request to save Multimedia : {}", multimediaDTO);
        Multimedia multimedia = multimediaMapper.toEntity(multimediaDTO);
        multimedia = multimediaRepository.save(multimedia);
        return multimediaMapper.toDto(multimedia);
    }

    /**
     * Get all the multimedias.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<MultimediaDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Multimedias");
        return multimediaRepository.findAll(pageable)
            .map(multimediaMapper::toDto);
    }

    /**
     * Get one multimedia by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public MultimediaDTO findOne(Long id) {
        log.debug("Request to get Multimedia : {}", id);
        Multimedia multimedia = multimediaRepository.findOne(id);
        return multimediaMapper.toDto(multimedia);
    }

    /**
     * Delete the multimedia by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Multimedia : {}", id);
        multimediaRepository.delete(id);
    }
}
