package com.test.gymtest.service;

import com.test.gymtest.domain.ParametroConfiguracion;
import com.test.gymtest.repository.ParametroConfiguracionRepository;
import com.test.gymtest.service.dto.ParametroConfiguracionDTO;
import com.test.gymtest.service.mapper.ParametroConfiguracionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing ParametroConfiguracion.
 */
@Service
@Transactional
public class ParametroConfiguracionService {

    private final Logger log = LoggerFactory.getLogger(ParametroConfiguracionService.class);

    private final ParametroConfiguracionRepository parametroConfiguracionRepository;

    private final ParametroConfiguracionMapper parametroConfiguracionMapper;

    public ParametroConfiguracionService(ParametroConfiguracionRepository parametroConfiguracionRepository, ParametroConfiguracionMapper parametroConfiguracionMapper) {
        this.parametroConfiguracionRepository = parametroConfiguracionRepository;
        this.parametroConfiguracionMapper = parametroConfiguracionMapper;
    }

    /**
     * Save a parametroConfiguracion.
     *
     * @param parametroConfiguracionDTO the entity to save
     * @return the persisted entity
     */
    public ParametroConfiguracionDTO save(ParametroConfiguracionDTO parametroConfiguracionDTO) {
        log.debug("Request to save ParametroConfiguracion : {}", parametroConfiguracionDTO);
        ParametroConfiguracion parametroConfiguracion = parametroConfiguracionMapper.toEntity(parametroConfiguracionDTO);
        parametroConfiguracion = parametroConfiguracionRepository.save(parametroConfiguracion);
        return parametroConfiguracionMapper.toDto(parametroConfiguracion);
    }

    /**
     * Get all the parametroConfiguracions.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ParametroConfiguracionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ParametroConfiguracions");
        return parametroConfiguracionRepository.findAll(pageable)
            .map(parametroConfiguracionMapper::toDto);
    }


    /**
     *  get all the parametroConfiguracions where TipoMultimedia is null.
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<ParametroConfiguracionDTO> findAllWhereTipoMultimediaIsNull() {
        log.debug("Request to get all parametroConfiguracions where TipoMultimedia is null");
        return StreamSupport
            .stream(parametroConfiguracionRepository.findAll().spliterator(), false)
            .filter(parametroConfiguracion -> parametroConfiguracion.getTipoMultimedia() == null)
            .map(parametroConfiguracionMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     *  get all the parametroConfiguracions where ZonaMusculo is null.
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public List<ParametroConfiguracionDTO> findAllWhereZonaMusculoIsNull() {
        log.debug("Request to get all parametroConfiguracions where ZonaMusculo is null");
        return StreamSupport
            .stream(parametroConfiguracionRepository.findAll().spliterator(), false)
            .filter(parametroConfiguracion -> parametroConfiguracion.getZonaMusculo() == null)
            .map(parametroConfiguracionMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one parametroConfiguracion by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public ParametroConfiguracionDTO findOne(Long id) {
        log.debug("Request to get ParametroConfiguracion : {}", id);
        ParametroConfiguracion parametroConfiguracion = parametroConfiguracionRepository.findOne(id);
        return parametroConfiguracionMapper.toDto(parametroConfiguracion);
    }

    /**
     * Delete the parametroConfiguracion by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ParametroConfiguracion : {}", id);
        parametroConfiguracionRepository.delete(id);
    }
}
