import { BaseEntity } from './../../shared';

export class Permiso implements BaseEntity {
    constructor(
        public id?: number,
        public nombrePermiso?: string,
        public estadoPermiso?: boolean,
    ) {
        this.estadoPermiso = false;
    }
}
