import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { EjercicioXRutina } from './ejercicio-x-rutina.model';
import { EjercicioXRutinaService } from './ejercicio-x-rutina.service';

@Component({
    selector: 'jhi-ejercicio-x-rutina-detail',
    templateUrl: './ejercicio-x-rutina-detail.component.html'
})
export class EjercicioXRutinaDetailComponent implements OnInit, OnDestroy {

    ejercicioXRutina: EjercicioXRutina;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private ejercicioXRutinaService: EjercicioXRutinaService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInEjercicioXRutinas();
    }

    load(id) {
        this.ejercicioXRutinaService.find(id)
            .subscribe((ejercicioXRutinaResponse: HttpResponse<EjercicioXRutina>) => {
                this.ejercicioXRutina = ejercicioXRutinaResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInEjercicioXRutinas() {
        this.eventSubscriber = this.eventManager.subscribe(
            'ejercicioXRutinaListModification',
            (response) => this.load(this.ejercicioXRutina.id)
        );
    }
}
