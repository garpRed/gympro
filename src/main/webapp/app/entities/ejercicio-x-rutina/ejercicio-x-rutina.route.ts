import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { EjercicioXRutinaComponent } from './ejercicio-x-rutina.component';
import { EjercicioXRutinaDetailComponent } from './ejercicio-x-rutina-detail.component';
import { EjercicioXRutinaPopupComponent } from './ejercicio-x-rutina-dialog.component';
import { EjercicioXRutinaDeletePopupComponent } from './ejercicio-x-rutina-delete-dialog.component';

@Injectable()
export class EjercicioXRutinaResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const ejercicioXRutinaRoute: Routes = [
    {
        path: 'ejercicio-x-rutina',
        component: EjercicioXRutinaComponent,
        resolve: {
            'pagingParams': EjercicioXRutinaResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'EjercicioXRutinas'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'ejercicio-x-rutina/:id',
        component: EjercicioXRutinaDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'EjercicioXRutinas'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const ejercicioXRutinaPopupRoute: Routes = [
    {
        path: 'ejercicio-x-rutina-new',
        component: EjercicioXRutinaPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'EjercicioXRutinas'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'ejercicio-x-rutina/:id/edit',
        component: EjercicioXRutinaPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'EjercicioXRutinas'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'ejercicio-x-rutina/:id/delete',
        component: EjercicioXRutinaDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'EjercicioXRutinas'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
