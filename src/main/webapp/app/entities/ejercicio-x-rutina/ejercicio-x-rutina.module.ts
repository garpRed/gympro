import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestGymSharedModule } from '../../shared';
import {
    EjercicioXRutinaService,
    EjercicioXRutinaPopupService,
    EjercicioXRutinaComponent,
    EjercicioXRutinaDetailComponent,
    EjercicioXRutinaDialogComponent,
    EjercicioXRutinaPopupComponent,
    EjercicioXRutinaDeletePopupComponent,
    EjercicioXRutinaDeleteDialogComponent,
    ejercicioXRutinaRoute,
    ejercicioXRutinaPopupRoute,
    EjercicioXRutinaResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...ejercicioXRutinaRoute,
    ...ejercicioXRutinaPopupRoute,
];

@NgModule({
    imports: [
        TestGymSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        EjercicioXRutinaComponent,
        EjercicioXRutinaDetailComponent,
        EjercicioXRutinaDialogComponent,
        EjercicioXRutinaDeleteDialogComponent,
        EjercicioXRutinaPopupComponent,
        EjercicioXRutinaDeletePopupComponent,
    ],
    entryComponents: [
        EjercicioXRutinaComponent,
        EjercicioXRutinaDialogComponent,
        EjercicioXRutinaPopupComponent,
        EjercicioXRutinaDeleteDialogComponent,
        EjercicioXRutinaDeletePopupComponent,
    ],
    providers: [
        EjercicioXRutinaService,
        EjercicioXRutinaPopupService,
        EjercicioXRutinaResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestGymEjercicioXRutinaModule {}
