import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { EjercicioXRutina } from './ejercicio-x-rutina.model';
import { EjercicioXRutinaPopupService } from './ejercicio-x-rutina-popup.service';
import { EjercicioXRutinaService } from './ejercicio-x-rutina.service';

@Component({
    selector: 'jhi-ejercicio-x-rutina-delete-dialog',
    templateUrl: './ejercicio-x-rutina-delete-dialog.component.html'
})
export class EjercicioXRutinaDeleteDialogComponent {

    ejercicioXRutina: EjercicioXRutina;

    constructor(
        private ejercicioXRutinaService: EjercicioXRutinaService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.ejercicioXRutinaService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'ejercicioXRutinaListModification',
                content: 'Deleted an ejercicioXRutina'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-ejercicio-x-rutina-delete-popup',
    template: ''
})
export class EjercicioXRutinaDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private ejercicioXRutinaPopupService: EjercicioXRutinaPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.ejercicioXRutinaPopupService
                .open(EjercicioXRutinaDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
