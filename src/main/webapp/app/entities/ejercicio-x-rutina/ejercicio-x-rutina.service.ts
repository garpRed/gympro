import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { EjercicioXRutina } from './ejercicio-x-rutina.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<EjercicioXRutina>;

@Injectable()
export class EjercicioXRutinaService {

    private resourceUrl =  SERVER_API_URL + 'api/ejercicio-x-rutinas';

    constructor(private http: HttpClient) { }

    create(ejercicioXRutina: EjercicioXRutina): Observable<EntityResponseType> {
        const copy = this.convert(ejercicioXRutina);
        return this.http.post<EjercicioXRutina>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(ejercicioXRutina: EjercicioXRutina): Observable<EntityResponseType> {
        const copy = this.convert(ejercicioXRutina);
        return this.http.put<EjercicioXRutina>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<EjercicioXRutina>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<EjercicioXRutina[]>> {
        const options = createRequestOption(req);
        return this.http.get<EjercicioXRutina[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<EjercicioXRutina[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: EjercicioXRutina = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<EjercicioXRutina[]>): HttpResponse<EjercicioXRutina[]> {
        const jsonResponse: EjercicioXRutina[] = res.body;
        const body: EjercicioXRutina[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to EjercicioXRutina.
     */
    private convertItemFromServer(ejercicioXRutina: EjercicioXRutina): EjercicioXRutina {
        const copy: EjercicioXRutina = Object.assign({}, ejercicioXRutina);
        return copy;
    }

    /**
     * Convert a EjercicioXRutina to a JSON which can be sent to the server.
     */
    private convert(ejercicioXRutina: EjercicioXRutina): EjercicioXRutina {
        const copy: EjercicioXRutina = Object.assign({}, ejercicioXRutina);
        return copy;
    }
}
