import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { EjercicioXRutina } from './ejercicio-x-rutina.model';
import { EjercicioXRutinaPopupService } from './ejercicio-x-rutina-popup.service';
import { EjercicioXRutinaService } from './ejercicio-x-rutina.service';
import { SubRutina, SubRutinaService } from '../sub-rutina';

@Component({
    selector: 'jhi-ejercicio-x-rutina-dialog',
    templateUrl: './ejercicio-x-rutina-dialog.component.html'
})
export class EjercicioXRutinaDialogComponent implements OnInit {

    ejercicioXRutina: EjercicioXRutina;
    isSaving: boolean;

    ejercicioxrutinas: SubRutina[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private ejercicioXRutinaService: EjercicioXRutinaService,
        private subRutinaService: SubRutinaService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.subRutinaService
            .query({filter: 'ejercicioxrutina-is-null'})
            .subscribe((res: HttpResponse<SubRutina[]>) => {
                if (!this.ejercicioXRutina.ejercicioXRutinaId) {
                    this.ejercicioxrutinas = res.body;
                } else {
                    this.subRutinaService
                        .find(this.ejercicioXRutina.ejercicioXRutinaId)
                        .subscribe((subRes: HttpResponse<SubRutina>) => {
                            this.ejercicioxrutinas = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.ejercicioXRutina.id !== undefined) {
            this.subscribeToSaveResponse(
                this.ejercicioXRutinaService.update(this.ejercicioXRutina));
        } else {
            this.subscribeToSaveResponse(
                this.ejercicioXRutinaService.create(this.ejercicioXRutina));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<EjercicioXRutina>>) {
        result.subscribe((res: HttpResponse<EjercicioXRutina>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: EjercicioXRutina) {
        this.eventManager.broadcast({ name: 'ejercicioXRutinaListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackSubRutinaById(index: number, item: SubRutina) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-ejercicio-x-rutina-popup',
    template: ''
})
export class EjercicioXRutinaPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private ejercicioXRutinaPopupService: EjercicioXRutinaPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.ejercicioXRutinaPopupService
                    .open(EjercicioXRutinaDialogComponent as Component, params['id']);
            } else {
                this.ejercicioXRutinaPopupService
                    .open(EjercicioXRutinaDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
