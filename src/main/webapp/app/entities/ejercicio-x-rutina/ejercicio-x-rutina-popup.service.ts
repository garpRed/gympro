import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { EjercicioXRutina } from './ejercicio-x-rutina.model';
import { EjercicioXRutinaService } from './ejercicio-x-rutina.service';

@Injectable()
export class EjercicioXRutinaPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private ejercicioXRutinaService: EjercicioXRutinaService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.ejercicioXRutinaService.find(id)
                    .subscribe((ejercicioXRutinaResponse: HttpResponse<EjercicioXRutina>) => {
                        const ejercicioXRutina: EjercicioXRutina = ejercicioXRutinaResponse.body;
                        this.ngbModalRef = this.ejercicioXRutinaModalRef(component, ejercicioXRutina);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.ejercicioXRutinaModalRef(component, new EjercicioXRutina());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    ejercicioXRutinaModalRef(component: Component, ejercicioXRutina: EjercicioXRutina): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.ejercicioXRutina = ejercicioXRutina;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
