import { BaseEntity } from './../../shared';

export class EjercicioXRutina implements BaseEntity {
    constructor(
        public id?: number,
        public peso?: number,
        public reps?: number,
        public sets?: number,
        public ajuste?: string,
        public tiempo?: string,
        public commentario?: string,
        public estadoEjercicioRutina?: boolean,
        public ejercicioXRutinaId?: number,
    ) {
        this.estadoEjercicioRutina = false;
    }
}
