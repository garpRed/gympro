export * from './ejercicio-x-rutina.model';
export * from './ejercicio-x-rutina-popup.service';
export * from './ejercicio-x-rutina.service';
export * from './ejercicio-x-rutina-dialog.component';
export * from './ejercicio-x-rutina-delete-dialog.component';
export * from './ejercicio-x-rutina-detail.component';
export * from './ejercicio-x-rutina.component';
export * from './ejercicio-x-rutina.route';
