import { BaseEntity } from './../../shared';

export class Rol implements BaseEntity {
    constructor(
        public id?: number,
        public nombreRol?: string,
        public estadoRol?: boolean,
        public rolXPermisos?: BaseEntity[],
    ) {
        this.estadoRol = false;
    }
}
