export * from './rol.model';
export * from './rol-popup.service';
export * from './rol.service';
export * from './rol-dialog.component';
export * from './rol-delete-dialog.component';
export * from './rol-detail.component';
export * from './rol.component';
export * from './rol.route';
