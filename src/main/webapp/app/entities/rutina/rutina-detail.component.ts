import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Rutina } from './rutina.model';
import { RutinaService } from './rutina.service';

@Component({
    selector: 'jhi-rutina-detail',
    templateUrl: './rutina-detail.component.html'
})
export class RutinaDetailComponent implements OnInit, OnDestroy {

    rutina: Rutina;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private rutinaService: RutinaService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInRutinas();
    }

    load(id) {
        this.rutinaService.find(id)
            .subscribe((rutinaResponse: HttpResponse<Rutina>) => {
                this.rutina = rutinaResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInRutinas() {
        this.eventSubscriber = this.eventManager.subscribe(
            'rutinaListModification',
            (response) => this.load(this.rutina.id)
        );
    }
}
