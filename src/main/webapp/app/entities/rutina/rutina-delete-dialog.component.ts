import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Rutina } from './rutina.model';
import { RutinaPopupService } from './rutina-popup.service';
import { RutinaService } from './rutina.service';

@Component({
    selector: 'jhi-rutina-delete-dialog',
    templateUrl: './rutina-delete-dialog.component.html'
})
export class RutinaDeleteDialogComponent {

    rutina: Rutina;

    constructor(
        private rutinaService: RutinaService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.rutinaService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'rutinaListModification',
                content: 'Deleted an rutina'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-rutina-delete-popup',
    template: ''
})
export class RutinaDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private rutinaPopupService: RutinaPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.rutinaPopupService
                .open(RutinaDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
