import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Rutina } from './rutina.model';
import { RutinaPopupService } from './rutina-popup.service';
import { RutinaService } from './rutina.service';

@Component({
    selector: 'jhi-rutina-dialog',
    templateUrl: './rutina-dialog.component.html'
})
export class RutinaDialogComponent implements OnInit {

    rutina: Rutina;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private rutinaService: RutinaService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.rutina.id !== undefined) {
            this.subscribeToSaveResponse(
                this.rutinaService.update(this.rutina));
        } else {
            this.subscribeToSaveResponse(
                this.rutinaService.create(this.rutina));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Rutina>>) {
        result.subscribe((res: HttpResponse<Rutina>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Rutina) {
        this.eventManager.broadcast({ name: 'rutinaListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-rutina-popup',
    template: ''
})
export class RutinaPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private rutinaPopupService: RutinaPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.rutinaPopupService
                    .open(RutinaDialogComponent as Component, params['id']);
            } else {
                this.rutinaPopupService
                    .open(RutinaDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
