import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { RutinaComponent } from './rutina.component';
import { RutinaDetailComponent } from './rutina-detail.component';
import { RutinaPopupComponent } from './rutina-dialog.component';
import { RutinaDeletePopupComponent } from './rutina-delete-dialog.component';

@Injectable()
export class RutinaResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const rutinaRoute: Routes = [
    {
        path: 'rutina',
        component: RutinaComponent,
        resolve: {
            'pagingParams': RutinaResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Rutinas'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'rutina/:id',
        component: RutinaDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Rutinas'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const rutinaPopupRoute: Routes = [
    {
        path: 'rutina-new',
        component: RutinaPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Rutinas'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'rutina/:id/edit',
        component: RutinaPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Rutinas'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'rutina/:id/delete',
        component: RutinaDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Rutinas'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
