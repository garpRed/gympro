import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestGymSharedModule } from '../../shared';
import {
    RutinaService,
    RutinaPopupService,
    RutinaComponent,
    RutinaDetailComponent,
    RutinaDialogComponent,
    RutinaPopupComponent,
    RutinaDeletePopupComponent,
    RutinaDeleteDialogComponent,
    rutinaRoute,
    rutinaPopupRoute,
    RutinaResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...rutinaRoute,
    ...rutinaPopupRoute,
];

@NgModule({
    imports: [
        TestGymSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        RutinaComponent,
        RutinaDetailComponent,
        RutinaDialogComponent,
        RutinaDeleteDialogComponent,
        RutinaPopupComponent,
        RutinaDeletePopupComponent,
    ],
    entryComponents: [
        RutinaComponent,
        RutinaDialogComponent,
        RutinaPopupComponent,
        RutinaDeleteDialogComponent,
        RutinaDeletePopupComponent,
    ],
    providers: [
        RutinaService,
        RutinaPopupService,
        RutinaResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestGymRutinaModule {}
