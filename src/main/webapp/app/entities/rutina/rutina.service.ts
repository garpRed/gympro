import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Rutina } from './rutina.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Rutina>;

@Injectable()
export class RutinaService {

    private resourceUrl =  SERVER_API_URL + 'api/rutinas';

    constructor(private http: HttpClient) { }

    create(rutina: Rutina): Observable<EntityResponseType> {
        const copy = this.convert(rutina);
        return this.http.post<Rutina>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(rutina: Rutina): Observable<EntityResponseType> {
        const copy = this.convert(rutina);
        return this.http.put<Rutina>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Rutina>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Rutina[]>> {
        const options = createRequestOption(req);
        return this.http.get<Rutina[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Rutina[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Rutina = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Rutina[]>): HttpResponse<Rutina[]> {
        const jsonResponse: Rutina[] = res.body;
        const body: Rutina[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Rutina.
     */
    private convertItemFromServer(rutina: Rutina): Rutina {
        const copy: Rutina = Object.assign({}, rutina);
        return copy;
    }

    /**
     * Convert a Rutina to a JSON which can be sent to the server.
     */
    private convert(rutina: Rutina): Rutina {
        const copy: Rutina = Object.assign({}, rutina);
        return copy;
    }
}
