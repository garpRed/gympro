import { BaseEntity } from './../../shared';

export class Rutina implements BaseEntity {
    constructor(
        public id?: number,
        public usuarioCreadorRutina?: number,
        public nombreRutina?: string,
        public cantDiasRutina?: number,
        public estadoRutina?: boolean,
        public subrutinaRutinas?: BaseEntity[],
        public rutinaUsuarios?: BaseEntity[],
    ) {
        this.estadoRutina = false;
    }
}
