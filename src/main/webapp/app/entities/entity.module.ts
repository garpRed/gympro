import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { TestGymUsuarioModule } from './usuario/usuario.module';
import { TestGymSedeModule } from './sede/sede.module';
import { TestGymGimnasioModule } from './gimnasio/gimnasio.module';
import { TestGymInvitacionModule } from './invitacion/invitacion.module';
import { TestGymHorarioModule } from './horario/horario.module';
import { TestGymRutinaModule } from './rutina/rutina.module';
import { TestGymSubRutinaModule } from './sub-rutina/sub-rutina.module';
import { TestGymEjercicioModule } from './ejercicio/ejercicio.module';
import { TestGymMultimediaModule } from './multimedia/multimedia.module';
import { TestGymActivoModule } from './activo/activo.module';
import { TestGymMusculoModule } from './musculo/musculo.module';
import { TestGymSolicitudModule } from './solicitud/solicitud.module';
import { TestGymEventoModule } from './evento/evento.module';
import { TestGymRolModule } from './rol/rol.module';
import { TestGymPermisoModule } from './permiso/permiso.module';
import { TestGymParametroConfiguracionModule } from './parametro-configuracion/parametro-configuracion.module';
import { TestGymTipoParametroModule } from './tipo-parametro/tipo-parametro.module';
import { TestGymEjercicioXRutinaModule } from './ejercicio-x-rutina/ejercicio-x-rutina.module';
import { TestGymRegistroActividadRutinaModule } from './registro-actividad-rutina/registro-actividad-rutina.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        TestGymUsuarioModule,
        TestGymSedeModule,
        TestGymGimnasioModule,
        TestGymInvitacionModule,
        TestGymHorarioModule,
        TestGymRutinaModule,
        TestGymSubRutinaModule,
        TestGymEjercicioModule,
        TestGymMultimediaModule,
        TestGymActivoModule,
        TestGymMusculoModule,
        TestGymSolicitudModule,
        TestGymEventoModule,
        TestGymRolModule,
        TestGymPermisoModule,
        TestGymParametroConfiguracionModule,
        TestGymTipoParametroModule,
        TestGymEjercicioXRutinaModule,
        TestGymRegistroActividadRutinaModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestGymEntityModule {}
