import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Activo } from './activo.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Activo>;

@Injectable()
export class ActivoService {

    private resourceUrl =  SERVER_API_URL + 'api/activos';

    constructor(private http: HttpClient) { }

    create(activo: Activo): Observable<EntityResponseType> {
        const copy = this.convert(activo);
        return this.http.post<Activo>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(activo: Activo): Observable<EntityResponseType> {
        const copy = this.convert(activo);
        return this.http.put<Activo>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Activo>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Activo[]>> {
        const options = createRequestOption(req);
        return this.http.get<Activo[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Activo[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Activo = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Activo[]>): HttpResponse<Activo[]> {
        const jsonResponse: Activo[] = res.body;
        const body: Activo[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Activo.
     */
    private convertItemFromServer(activo: Activo): Activo {
        const copy: Activo = Object.assign({}, activo);
        return copy;
    }

    /**
     * Convert a Activo to a JSON which can be sent to the server.
     */
    private convert(activo: Activo): Activo {
        const copy: Activo = Object.assign({}, activo);
        return copy;
    }
}
