import { BaseEntity } from './../../shared';

export class Activo implements BaseEntity {
    constructor(
        public id?: number,
        public nombreActivo?: string,
        public cantActivos?: number,
        public estadoActivo?: boolean,
        public sedeId?: number,
    ) {
        this.estadoActivo = false;
    }
}
