import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Activo } from './activo.model';
import { ActivoPopupService } from './activo-popup.service';
import { ActivoService } from './activo.service';

@Component({
    selector: 'jhi-activo-delete-dialog',
    templateUrl: './activo-delete-dialog.component.html'
})
export class ActivoDeleteDialogComponent {

    activo: Activo;

    constructor(
        private activoService: ActivoService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.activoService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'activoListModification',
                content: 'Deleted an activo'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-activo-delete-popup',
    template: ''
})
export class ActivoDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private activoPopupService: ActivoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.activoPopupService
                .open(ActivoDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
