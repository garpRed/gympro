import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Activo } from './activo.model';
import { ActivoService } from './activo.service';

@Component({
    selector: 'jhi-activo-detail',
    templateUrl: './activo-detail.component.html'
})
export class ActivoDetailComponent implements OnInit, OnDestroy {

    activo: Activo;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private activoService: ActivoService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInActivos();
    }

    load(id) {
        this.activoService.find(id)
            .subscribe((activoResponse: HttpResponse<Activo>) => {
                this.activo = activoResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInActivos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'activoListModification',
            (response) => this.load(this.activo.id)
        );
    }
}
