import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ActivoComponent } from './activo.component';
import { ActivoDetailComponent } from './activo-detail.component';
import { ActivoPopupComponent } from './activo-dialog.component';
import { ActivoDeletePopupComponent } from './activo-delete-dialog.component';

@Injectable()
export class ActivoResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const activoRoute: Routes = [
    {
        path: 'activo',
        component: ActivoComponent,
        resolve: {
            'pagingParams': ActivoResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Activos'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'activo/:id',
        component: ActivoDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Activos'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const activoPopupRoute: Routes = [
    {
        path: 'activo-new',
        component: ActivoPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Activos'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'activo/:id/edit',
        component: ActivoPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Activos'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'activo/:id/delete',
        component: ActivoDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Activos'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
