import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestGymSharedModule } from '../../shared';
import {
    ActivoService,
    ActivoPopupService,
    ActivoComponent,
    ActivoDetailComponent,
    ActivoDialogComponent,
    ActivoPopupComponent,
    ActivoDeletePopupComponent,
    ActivoDeleteDialogComponent,
    activoRoute,
    activoPopupRoute,
    ActivoResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...activoRoute,
    ...activoPopupRoute,
];

@NgModule({
    imports: [
        TestGymSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ActivoComponent,
        ActivoDetailComponent,
        ActivoDialogComponent,
        ActivoDeleteDialogComponent,
        ActivoPopupComponent,
        ActivoDeletePopupComponent,
    ],
    entryComponents: [
        ActivoComponent,
        ActivoDialogComponent,
        ActivoPopupComponent,
        ActivoDeleteDialogComponent,
        ActivoDeletePopupComponent,
    ],
    providers: [
        ActivoService,
        ActivoPopupService,
        ActivoResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestGymActivoModule {}
