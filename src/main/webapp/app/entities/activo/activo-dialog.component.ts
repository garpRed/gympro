import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Activo } from './activo.model';
import { ActivoPopupService } from './activo-popup.service';
import { ActivoService } from './activo.service';
import { Sede, SedeService } from '../sede';

@Component({
    selector: 'jhi-activo-dialog',
    templateUrl: './activo-dialog.component.html'
})
export class ActivoDialogComponent implements OnInit {

    activo: Activo;
    isSaving: boolean;

    sedes: Sede[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private activoService: ActivoService,
        private sedeService: SedeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.sedeService.query()
            .subscribe((res: HttpResponse<Sede[]>) => { this.sedes = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.activo.id !== undefined) {
            this.subscribeToSaveResponse(
                this.activoService.update(this.activo));
        } else {
            this.subscribeToSaveResponse(
                this.activoService.create(this.activo));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Activo>>) {
        result.subscribe((res: HttpResponse<Activo>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Activo) {
        this.eventManager.broadcast({ name: 'activoListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackSedeById(index: number, item: Sede) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-activo-popup',
    template: ''
})
export class ActivoPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private activoPopupService: ActivoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.activoPopupService
                    .open(ActivoDialogComponent as Component, params['id']);
            } else {
                this.activoPopupService
                    .open(ActivoDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
