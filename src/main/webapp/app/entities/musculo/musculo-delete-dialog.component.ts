import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Musculo } from './musculo.model';
import { MusculoPopupService } from './musculo-popup.service';
import { MusculoService } from './musculo.service';

@Component({
    selector: 'jhi-musculo-delete-dialog',
    templateUrl: './musculo-delete-dialog.component.html'
})
export class MusculoDeleteDialogComponent {

    musculo: Musculo;

    constructor(
        private musculoService: MusculoService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.musculoService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'musculoListModification',
                content: 'Deleted an musculo'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-musculo-delete-popup',
    template: ''
})
export class MusculoDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private musculoPopupService: MusculoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.musculoPopupService
                .open(MusculoDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
