import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { MusculoComponent } from './musculo.component';
import { MusculoDetailComponent } from './musculo-detail.component';
import { MusculoPopupComponent } from './musculo-dialog.component';
import { MusculoDeletePopupComponent } from './musculo-delete-dialog.component';

@Injectable()
export class MusculoResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const musculoRoute: Routes = [
    {
        path: 'musculo',
        component: MusculoComponent,
        resolve: {
            'pagingParams': MusculoResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Musculos'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'musculo/:id',
        component: MusculoDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Musculos'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const musculoPopupRoute: Routes = [
    {
        path: 'musculo-new',
        component: MusculoPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Musculos'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'musculo/:id/edit',
        component: MusculoPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Musculos'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'musculo/:id/delete',
        component: MusculoDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Musculos'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
