import { BaseEntity } from './../../shared';

export class Musculo implements BaseEntity {
    constructor(
        public id?: number,
        public nombreMusculo?: string,
        public estadoMusculo?: boolean,
        public zonaMusculoId?: number,
    ) {
        this.estadoMusculo = false;
    }
}
