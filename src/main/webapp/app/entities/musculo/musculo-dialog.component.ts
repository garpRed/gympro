import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Musculo } from './musculo.model';
import { MusculoPopupService } from './musculo-popup.service';
import { MusculoService } from './musculo.service';
import { ParametroConfiguracion, ParametroConfiguracionService } from '../parametro-configuracion';

@Component({
    selector: 'jhi-musculo-dialog',
    templateUrl: './musculo-dialog.component.html'
})
export class MusculoDialogComponent implements OnInit {

    musculo: Musculo;
    isSaving: boolean;

    zonamusculos: ParametroConfiguracion[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private musculoService: MusculoService,
        private parametroConfiguracionService: ParametroConfiguracionService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.parametroConfiguracionService
            .query({filter: 'zonamusculo-is-null'})
            .subscribe((res: HttpResponse<ParametroConfiguracion[]>) => {
                if (!this.musculo.zonaMusculoId) {
                    this.zonamusculos = res.body;
                } else {
                    this.parametroConfiguracionService
                        .find(this.musculo.zonaMusculoId)
                        .subscribe((subRes: HttpResponse<ParametroConfiguracion>) => {
                            this.zonamusculos = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.musculo.id !== undefined) {
            this.subscribeToSaveResponse(
                this.musculoService.update(this.musculo));
        } else {
            this.subscribeToSaveResponse(
                this.musculoService.create(this.musculo));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Musculo>>) {
        result.subscribe((res: HttpResponse<Musculo>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Musculo) {
        this.eventManager.broadcast({ name: 'musculoListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackParametroConfiguracionById(index: number, item: ParametroConfiguracion) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-musculo-popup',
    template: ''
})
export class MusculoPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private musculoPopupService: MusculoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.musculoPopupService
                    .open(MusculoDialogComponent as Component, params['id']);
            } else {
                this.musculoPopupService
                    .open(MusculoDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
