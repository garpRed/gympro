export * from './musculo.model';
export * from './musculo-popup.service';
export * from './musculo.service';
export * from './musculo-dialog.component';
export * from './musculo-delete-dialog.component';
export * from './musculo-detail.component';
export * from './musculo.component';
export * from './musculo.route';
