import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Musculo } from './musculo.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Musculo>;

@Injectable()
export class MusculoService {

    private resourceUrl =  SERVER_API_URL + 'api/musculos';

    constructor(private http: HttpClient) { }

    create(musculo: Musculo): Observable<EntityResponseType> {
        const copy = this.convert(musculo);
        return this.http.post<Musculo>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(musculo: Musculo): Observable<EntityResponseType> {
        const copy = this.convert(musculo);
        return this.http.put<Musculo>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Musculo>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Musculo[]>> {
        const options = createRequestOption(req);
        return this.http.get<Musculo[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Musculo[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Musculo = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Musculo[]>): HttpResponse<Musculo[]> {
        const jsonResponse: Musculo[] = res.body;
        const body: Musculo[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Musculo.
     */
    private convertItemFromServer(musculo: Musculo): Musculo {
        const copy: Musculo = Object.assign({}, musculo);
        return copy;
    }

    /**
     * Convert a Musculo to a JSON which can be sent to the server.
     */
    private convert(musculo: Musculo): Musculo {
        const copy: Musculo = Object.assign({}, musculo);
        return copy;
    }
}
