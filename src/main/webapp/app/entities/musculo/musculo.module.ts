import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestGymSharedModule } from '../../shared';
import {
    MusculoService,
    MusculoPopupService,
    MusculoComponent,
    MusculoDetailComponent,
    MusculoDialogComponent,
    MusculoPopupComponent,
    MusculoDeletePopupComponent,
    MusculoDeleteDialogComponent,
    musculoRoute,
    musculoPopupRoute,
    MusculoResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...musculoRoute,
    ...musculoPopupRoute,
];

@NgModule({
    imports: [
        TestGymSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        MusculoComponent,
        MusculoDetailComponent,
        MusculoDialogComponent,
        MusculoDeleteDialogComponent,
        MusculoPopupComponent,
        MusculoDeletePopupComponent,
    ],
    entryComponents: [
        MusculoComponent,
        MusculoDialogComponent,
        MusculoPopupComponent,
        MusculoDeleteDialogComponent,
        MusculoDeletePopupComponent,
    ],
    providers: [
        MusculoService,
        MusculoPopupService,
        MusculoResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestGymMusculoModule {}
