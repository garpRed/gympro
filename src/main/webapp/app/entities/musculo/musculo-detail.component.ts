import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Musculo } from './musculo.model';
import { MusculoService } from './musculo.service';

@Component({
    selector: 'jhi-musculo-detail',
    templateUrl: './musculo-detail.component.html'
})
export class MusculoDetailComponent implements OnInit, OnDestroy {

    musculo: Musculo;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private musculoService: MusculoService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInMusculos();
    }

    load(id) {
        this.musculoService.find(id)
            .subscribe((musculoResponse: HttpResponse<Musculo>) => {
                this.musculo = musculoResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInMusculos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'musculoListModification',
            (response) => this.load(this.musculo.id)
        );
    }
}
