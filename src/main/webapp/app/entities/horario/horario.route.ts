import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { HorarioComponent } from './horario.component';
import { HorarioDetailComponent } from './horario-detail.component';
import { HorarioPopupComponent } from './horario-dialog.component';
import { HorarioDeletePopupComponent } from './horario-delete-dialog.component';

@Injectable()
export class HorarioResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const horarioRoute: Routes = [
    {
        path: 'horario',
        component: HorarioComponent,
        resolve: {
            'pagingParams': HorarioResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Horarios'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'horario/:id',
        component: HorarioDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Horarios'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const horarioPopupRoute: Routes = [
    {
        path: 'horario-new',
        component: HorarioPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Horarios'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'horario/:id/edit',
        component: HorarioPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Horarios'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'horario/:id/delete',
        component: HorarioDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Horarios'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
