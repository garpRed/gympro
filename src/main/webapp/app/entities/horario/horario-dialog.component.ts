import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Horario } from './horario.model';
import { HorarioPopupService } from './horario-popup.service';
import { HorarioService } from './horario.service';
import { Usuario, UsuarioService } from '../usuario';
import { ParametroConfiguracion, ParametroConfiguracionService } from '../parametro-configuracion';

@Component({
    selector: 'jhi-horario-dialog',
    templateUrl: './horario-dialog.component.html'
})
export class HorarioDialogComponent implements OnInit {

    horario: Horario;
    isSaving: boolean;

    horariousuarios: Usuario[];

    parametroconfiguracions: ParametroConfiguracion[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private horarioService: HorarioService,
        private usuarioService: UsuarioService,
        private parametroConfiguracionService: ParametroConfiguracionService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.usuarioService
            .query({filter: 'horario-is-null'})
            .subscribe((res: HttpResponse<Usuario[]>) => {
                if (!this.horario.horarioUsuarioId) {
                    this.horariousuarios = res.body;
                } else {
                    this.usuarioService
                        .find(this.horario.horarioUsuarioId)
                        .subscribe((subRes: HttpResponse<Usuario>) => {
                            this.horariousuarios = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.parametroConfiguracionService.query()
            .subscribe((res: HttpResponse<ParametroConfiguracion[]>) => { this.parametroconfiguracions = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.horario.id !== undefined) {
            this.subscribeToSaveResponse(
                this.horarioService.update(this.horario));
        } else {
            this.subscribeToSaveResponse(
                this.horarioService.create(this.horario));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Horario>>) {
        result.subscribe((res: HttpResponse<Horario>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Horario) {
        this.eventManager.broadcast({ name: 'horarioListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackUsuarioById(index: number, item: Usuario) {
        return item.id;
    }

    trackParametroConfiguracionById(index: number, item: ParametroConfiguracion) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-horario-popup',
    template: ''
})
export class HorarioPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private horarioPopupService: HorarioPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.horarioPopupService
                    .open(HorarioDialogComponent as Component, params['id']);
            } else {
                this.horarioPopupService
                    .open(HorarioDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
