import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestGymSharedModule } from '../../shared';
import {
    HorarioService,
    HorarioPopupService,
    HorarioComponent,
    HorarioDetailComponent,
    HorarioDialogComponent,
    HorarioPopupComponent,
    HorarioDeletePopupComponent,
    HorarioDeleteDialogComponent,
    horarioRoute,
    horarioPopupRoute,
    HorarioResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...horarioRoute,
    ...horarioPopupRoute,
];

@NgModule({
    imports: [
        TestGymSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        HorarioComponent,
        HorarioDetailComponent,
        HorarioDialogComponent,
        HorarioDeleteDialogComponent,
        HorarioPopupComponent,
        HorarioDeletePopupComponent,
    ],
    entryComponents: [
        HorarioComponent,
        HorarioDialogComponent,
        HorarioPopupComponent,
        HorarioDeleteDialogComponent,
        HorarioDeletePopupComponent,
    ],
    providers: [
        HorarioService,
        HorarioPopupService,
        HorarioResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestGymHorarioModule {}
