import { BaseEntity } from './../../shared';

export class Horario implements BaseEntity {
    constructor(
        public id?: number,
        public diaEntrenador?: string,
        public disponibilidadEntrenador?: boolean,
        public horarioInicioHorario?: any,
        public horarioFinHorario?: any,
        public estadoHorario?: boolean,
        public horarioUsuarioId?: number,
        public horariosParametros?: BaseEntity[],
    ) {
        this.disponibilidadEntrenador = false;
        this.estadoHorario = false;
    }
}
