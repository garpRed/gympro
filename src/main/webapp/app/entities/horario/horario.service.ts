import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Horario } from './horario.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Horario>;

@Injectable()
export class HorarioService {

    private resourceUrl =  SERVER_API_URL + 'api/horarios';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(horario: Horario): Observable<EntityResponseType> {
        const copy = this.convert(horario);
        return this.http.post<Horario>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(horario: Horario): Observable<EntityResponseType> {
        const copy = this.convert(horario);
        return this.http.put<Horario>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Horario>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Horario[]>> {
        const options = createRequestOption(req);
        return this.http.get<Horario[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Horario[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Horario = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Horario[]>): HttpResponse<Horario[]> {
        const jsonResponse: Horario[] = res.body;
        const body: Horario[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Horario.
     */
    private convertItemFromServer(horario: Horario): Horario {
        const copy: Horario = Object.assign({}, horario);
        copy.horarioInicioHorario = this.dateUtils
            .convertDateTimeFromServer(horario.horarioInicioHorario);
        copy.horarioFinHorario = this.dateUtils
            .convertDateTimeFromServer(horario.horarioFinHorario);
        return copy;
    }

    /**
     * Convert a Horario to a JSON which can be sent to the server.
     */
    private convert(horario: Horario): Horario {
        const copy: Horario = Object.assign({}, horario);

        copy.horarioInicioHorario = this.dateUtils.toDate(horario.horarioInicioHorario);

        copy.horarioFinHorario = this.dateUtils.toDate(horario.horarioFinHorario);
        return copy;
    }
}
