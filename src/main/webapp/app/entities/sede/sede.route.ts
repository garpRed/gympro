import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { SedeComponent } from './sede.component';
import { SedeDetailComponent } from './sede-detail.component';
import { SedePopupComponent } from './sede-dialog.component';
import { SedeDeletePopupComponent } from './sede-delete-dialog.component';

@Injectable()
export class SedeResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const sedeRoute: Routes = [
    {
        path: 'sede',
        component: SedeComponent,
        resolve: {
            'pagingParams': SedeResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Sedes'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'sede/:id',
        component: SedeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Sedes'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const sedePopupRoute: Routes = [
    {
        path: 'sede-new',
        component: SedePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Sedes'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sede/:id/edit',
        component: SedePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Sedes'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sede/:id/delete',
        component: SedeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Sedes'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
