import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Sede } from './sede.model';
import { SedePopupService } from './sede-popup.service';
import { SedeService } from './sede.service';
import { Gimnasio, GimnasioService } from '../gimnasio';

@Component({
    selector: 'jhi-sede-dialog',
    templateUrl: './sede-dialog.component.html'
})
export class SedeDialogComponent implements OnInit {

    sede: Sede;
    isSaving: boolean;

    gimnasios: Gimnasio[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private sedeService: SedeService,
        private gimnasioService: GimnasioService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.gimnasioService.query()
            .subscribe((res: HttpResponse<Gimnasio[]>) => { this.gimnasios = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.sede.id !== undefined) {
            this.subscribeToSaveResponse(
                this.sedeService.update(this.sede));
        } else {
            this.subscribeToSaveResponse(
                this.sedeService.create(this.sede));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Sede>>) {
        result.subscribe((res: HttpResponse<Sede>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Sede) {
        this.eventManager.broadcast({ name: 'sedeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackGimnasioById(index: number, item: Gimnasio) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-sede-popup',
    template: ''
})
export class SedePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private sedePopupService: SedePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.sedePopupService
                    .open(SedeDialogComponent as Component, params['id']);
            } else {
                this.sedePopupService
                    .open(SedeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
