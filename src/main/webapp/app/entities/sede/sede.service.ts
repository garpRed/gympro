import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Sede } from './sede.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Sede>;

@Injectable()
export class SedeService {

    private resourceUrl =  SERVER_API_URL + 'api/sedes';

    constructor(private http: HttpClient) { }

    create(sede: Sede): Observable<EntityResponseType> {
        const copy = this.convert(sede);
        return this.http.post<Sede>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(sede: Sede): Observable<EntityResponseType> {
        const copy = this.convert(sede);
        return this.http.put<Sede>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Sede>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Sede[]>> {
        const options = createRequestOption(req);
        return this.http.get<Sede[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Sede[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Sede = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Sede[]>): HttpResponse<Sede[]> {
        const jsonResponse: Sede[] = res.body;
        const body: Sede[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Sede.
     */
    private convertItemFromServer(sede: Sede): Sede {
        const copy: Sede = Object.assign({}, sede);
        return copy;
    }

    /**
     * Convert a Sede to a JSON which can be sent to the server.
     */
    private convert(sede: Sede): Sede {
        const copy: Sede = Object.assign({}, sede);
        return copy;
    }
}
