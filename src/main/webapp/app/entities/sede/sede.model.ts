import { BaseEntity } from './../../shared';

export class Sede implements BaseEntity {
    constructor(
        public id?: number,
        public nombreSede?: string,
        public direccionSede?: string,
        public estadoSede?: boolean,
        public activosSedes?: BaseEntity[],
        public ejerciciosSedes?: BaseEntity[],
        public gimnasioId?: number,
    ) {
        this.estadoSede = false;
    }
}
