import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestGymSharedModule } from '../../shared';
import {
    SedeService,
    SedePopupService,
    SedeComponent,
    SedeDetailComponent,
    SedeDialogComponent,
    SedePopupComponent,
    SedeDeletePopupComponent,
    SedeDeleteDialogComponent,
    sedeRoute,
    sedePopupRoute,
    SedeResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...sedeRoute,
    ...sedePopupRoute,
];

@NgModule({
    imports: [
        TestGymSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        SedeComponent,
        SedeDetailComponent,
        SedeDialogComponent,
        SedeDeleteDialogComponent,
        SedePopupComponent,
        SedeDeletePopupComponent,
    ],
    entryComponents: [
        SedeComponent,
        SedeDialogComponent,
        SedePopupComponent,
        SedeDeleteDialogComponent,
        SedeDeletePopupComponent,
    ],
    providers: [
        SedeService,
        SedePopupService,
        SedeResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestGymSedeModule {}
