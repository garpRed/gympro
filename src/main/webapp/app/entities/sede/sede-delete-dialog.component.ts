import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Sede } from './sede.model';
import { SedePopupService } from './sede-popup.service';
import { SedeService } from './sede.service';

@Component({
    selector: 'jhi-sede-delete-dialog',
    templateUrl: './sede-delete-dialog.component.html'
})
export class SedeDeleteDialogComponent {

    sede: Sede;

    constructor(
        private sedeService: SedeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.sedeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'sedeListModification',
                content: 'Deleted an sede'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-sede-delete-popup',
    template: ''
})
export class SedeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private sedePopupService: SedePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.sedePopupService
                .open(SedeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
