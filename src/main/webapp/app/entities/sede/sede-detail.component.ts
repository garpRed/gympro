import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Sede } from './sede.model';
import { SedeService } from './sede.service';

@Component({
    selector: 'jhi-sede-detail',
    templateUrl: './sede-detail.component.html'
})
export class SedeDetailComponent implements OnInit, OnDestroy {

    sede: Sede;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private sedeService: SedeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSedes();
    }

    load(id) {
        this.sedeService.find(id)
            .subscribe((sedeResponse: HttpResponse<Sede>) => {
                this.sede = sedeResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSedes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'sedeListModification',
            (response) => this.load(this.sede.id)
        );
    }
}
