import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Invitacion } from './invitacion.model';
import { InvitacionPopupService } from './invitacion-popup.service';
import { InvitacionService } from './invitacion.service';

@Component({
    selector: 'jhi-invitacion-delete-dialog',
    templateUrl: './invitacion-delete-dialog.component.html'
})
export class InvitacionDeleteDialogComponent {

    invitacion: Invitacion;

    constructor(
        private invitacionService: InvitacionService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.invitacionService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'invitacionListModification',
                content: 'Deleted an invitacion'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-invitacion-delete-popup',
    template: ''
})
export class InvitacionDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private invitacionPopupService: InvitacionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.invitacionPopupService
                .open(InvitacionDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
