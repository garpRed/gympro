import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestGymSharedModule } from '../../shared';
import {
    InvitacionService,
    InvitacionPopupService,
    InvitacionComponent,
    InvitacionDetailComponent,
    InvitacionDialogComponent,
    InvitacionPopupComponent,
    InvitacionDeletePopupComponent,
    InvitacionDeleteDialogComponent,
    invitacionRoute,
    invitacionPopupRoute,
    InvitacionResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...invitacionRoute,
    ...invitacionPopupRoute,
];

@NgModule({
    imports: [
        TestGymSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        InvitacionComponent,
        InvitacionDetailComponent,
        InvitacionDialogComponent,
        InvitacionDeleteDialogComponent,
        InvitacionPopupComponent,
        InvitacionDeletePopupComponent,
    ],
    entryComponents: [
        InvitacionComponent,
        InvitacionDialogComponent,
        InvitacionPopupComponent,
        InvitacionDeleteDialogComponent,
        InvitacionDeletePopupComponent,
    ],
    providers: [
        InvitacionService,
        InvitacionPopupService,
        InvitacionResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestGymInvitacionModule {}
