import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { InvitacionComponent } from './invitacion.component';
import { InvitacionDetailComponent } from './invitacion-detail.component';
import { InvitacionPopupComponent } from './invitacion-dialog.component';
import { InvitacionDeletePopupComponent } from './invitacion-delete-dialog.component';

@Injectable()
export class InvitacionResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const invitacionRoute: Routes = [
    {
        path: 'invitacion',
        component: InvitacionComponent,
        resolve: {
            'pagingParams': InvitacionResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Invitacions'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'invitacion/:id',
        component: InvitacionDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Invitacions'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const invitacionPopupRoute: Routes = [
    {
        path: 'invitacion-new',
        component: InvitacionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Invitacions'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'invitacion/:id/edit',
        component: InvitacionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Invitacions'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'invitacion/:id/delete',
        component: InvitacionDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Invitacions'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
