import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Invitacion } from './invitacion.model';
import { InvitacionService } from './invitacion.service';

@Component({
    selector: 'jhi-invitacion-detail',
    templateUrl: './invitacion-detail.component.html'
})
export class InvitacionDetailComponent implements OnInit, OnDestroy {

    invitacion: Invitacion;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private invitacionService: InvitacionService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInInvitacions();
    }

    load(id) {
        this.invitacionService.find(id)
            .subscribe((invitacionResponse: HttpResponse<Invitacion>) => {
                this.invitacion = invitacionResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInInvitacions() {
        this.eventSubscriber = this.eventManager.subscribe(
            'invitacionListModification',
            (response) => this.load(this.invitacion.id)
        );
    }
}
