import { BaseEntity } from './../../shared';

export class Invitacion implements BaseEntity {
    constructor(
        public id?: number,
        public horarioInvitacion?: any,
        public estadoInvitacion?: boolean,
        public usuarioId?: number,
        public aceptaInvitacionId?: number,
    ) {
        this.estadoInvitacion = false;
    }
}
