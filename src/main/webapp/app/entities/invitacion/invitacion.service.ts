import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Invitacion } from './invitacion.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Invitacion>;

@Injectable()
export class InvitacionService {

    private resourceUrl =  SERVER_API_URL + 'api/invitacions';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(invitacion: Invitacion): Observable<EntityResponseType> {
        const copy = this.convert(invitacion);
        return this.http.post<Invitacion>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(invitacion: Invitacion): Observable<EntityResponseType> {
        const copy = this.convert(invitacion);
        return this.http.put<Invitacion>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Invitacion>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Invitacion[]>> {
        const options = createRequestOption(req);
        return this.http.get<Invitacion[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Invitacion[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Invitacion = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Invitacion[]>): HttpResponse<Invitacion[]> {
        const jsonResponse: Invitacion[] = res.body;
        const body: Invitacion[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Invitacion.
     */
    private convertItemFromServer(invitacion: Invitacion): Invitacion {
        const copy: Invitacion = Object.assign({}, invitacion);
        copy.horarioInvitacion = this.dateUtils
            .convertDateTimeFromServer(invitacion.horarioInvitacion);
        return copy;
    }

    /**
     * Convert a Invitacion to a JSON which can be sent to the server.
     */
    private convert(invitacion: Invitacion): Invitacion {
        const copy: Invitacion = Object.assign({}, invitacion);

        copy.horarioInvitacion = this.dateUtils.toDate(invitacion.horarioInvitacion);
        return copy;
    }
}
