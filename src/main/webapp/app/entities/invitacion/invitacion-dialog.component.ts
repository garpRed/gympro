import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Invitacion } from './invitacion.model';
import { InvitacionPopupService } from './invitacion-popup.service';
import { InvitacionService } from './invitacion.service';
import { Usuario, UsuarioService } from '../usuario';

@Component({
    selector: 'jhi-invitacion-dialog',
    templateUrl: './invitacion-dialog.component.html'
})
export class InvitacionDialogComponent implements OnInit {

    invitacion: Invitacion;
    isSaving: boolean;

    usuarios: Usuario[];

    aceptainvitacions: Usuario[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private invitacionService: InvitacionService,
        private usuarioService: UsuarioService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.usuarioService.query()
            .subscribe((res: HttpResponse<Usuario[]>) => { this.usuarios = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.usuarioService
            .query({filter: 'aceptainvitacion-is-null'})
            .subscribe((res: HttpResponse<Usuario[]>) => {
                if (!this.invitacion.aceptaInvitacionId) {
                    this.aceptainvitacions = res.body;
                } else {
                    this.usuarioService
                        .find(this.invitacion.aceptaInvitacionId)
                        .subscribe((subRes: HttpResponse<Usuario>) => {
                            this.aceptainvitacions = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.invitacion.id !== undefined) {
            this.subscribeToSaveResponse(
                this.invitacionService.update(this.invitacion));
        } else {
            this.subscribeToSaveResponse(
                this.invitacionService.create(this.invitacion));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Invitacion>>) {
        result.subscribe((res: HttpResponse<Invitacion>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Invitacion) {
        this.eventManager.broadcast({ name: 'invitacionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackUsuarioById(index: number, item: Usuario) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-invitacion-popup',
    template: ''
})
export class InvitacionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private invitacionPopupService: InvitacionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.invitacionPopupService
                    .open(InvitacionDialogComponent as Component, params['id']);
            } else {
                this.invitacionPopupService
                    .open(InvitacionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
