import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Invitacion } from './invitacion.model';
import { InvitacionService } from './invitacion.service';

@Injectable()
export class InvitacionPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private invitacionService: InvitacionService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.invitacionService.find(id)
                    .subscribe((invitacionResponse: HttpResponse<Invitacion>) => {
                        const invitacion: Invitacion = invitacionResponse.body;
                        invitacion.horarioInvitacion = this.datePipe
                            .transform(invitacion.horarioInvitacion, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.invitacionModalRef(component, invitacion);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.invitacionModalRef(component, new Invitacion());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    invitacionModalRef(component: Component, invitacion: Invitacion): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.invitacion = invitacion;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
