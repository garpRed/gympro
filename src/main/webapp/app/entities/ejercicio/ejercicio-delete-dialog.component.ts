import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Ejercicio } from './ejercicio.model';
import { EjercicioPopupService } from './ejercicio-popup.service';
import { EjercicioService } from './ejercicio.service';

@Component({
    selector: 'jhi-ejercicio-delete-dialog',
    templateUrl: './ejercicio-delete-dialog.component.html'
})
export class EjercicioDeleteDialogComponent {

    ejercicio: Ejercicio;

    constructor(
        private ejercicioService: EjercicioService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.ejercicioService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'ejercicioListModification',
                content: 'Deleted an ejercicio'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-ejercicio-delete-popup',
    template: ''
})
export class EjercicioDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private ejercicioPopupService: EjercicioPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.ejercicioPopupService
                .open(EjercicioDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
