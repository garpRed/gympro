import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Ejercicio } from './ejercicio.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Ejercicio>;

@Injectable()
export class EjercicioService {

    private resourceUrl =  SERVER_API_URL + 'api/ejercicios';

    constructor(private http: HttpClient) { }

    create(ejercicio: Ejercicio): Observable<EntityResponseType> {
        const copy = this.convert(ejercicio);
        return this.http.post<Ejercicio>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(ejercicio: Ejercicio): Observable<EntityResponseType> {
        const copy = this.convert(ejercicio);
        return this.http.put<Ejercicio>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Ejercicio>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Ejercicio[]>> {
        const options = createRequestOption(req);
        return this.http.get<Ejercicio[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Ejercicio[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Ejercicio = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Ejercicio[]>): HttpResponse<Ejercicio[]> {
        const jsonResponse: Ejercicio[] = res.body;
        const body: Ejercicio[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Ejercicio.
     */
    private convertItemFromServer(ejercicio: Ejercicio): Ejercicio {
        const copy: Ejercicio = Object.assign({}, ejercicio);
        return copy;
    }

    /**
     * Convert a Ejercicio to a JSON which can be sent to the server.
     */
    private convert(ejercicio: Ejercicio): Ejercicio {
        const copy: Ejercicio = Object.assign({}, ejercicio);
        return copy;
    }
}
