import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Ejercicio } from './ejercicio.model';
import { EjercicioService } from './ejercicio.service';

@Component({
    selector: 'jhi-ejercicio-detail',
    templateUrl: './ejercicio-detail.component.html'
})
export class EjercicioDetailComponent implements OnInit, OnDestroy {

    ejercicio: Ejercicio;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private ejercicioService: EjercicioService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInEjercicios();
    }

    load(id) {
        this.ejercicioService.find(id)
            .subscribe((ejercicioResponse: HttpResponse<Ejercicio>) => {
                this.ejercicio = ejercicioResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInEjercicios() {
        this.eventSubscriber = this.eventManager.subscribe(
            'ejercicioListModification',
            (response) => this.load(this.ejercicio.id)
        );
    }
}
