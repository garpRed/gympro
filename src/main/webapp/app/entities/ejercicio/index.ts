export * from './ejercicio.model';
export * from './ejercicio-popup.service';
export * from './ejercicio.service';
export * from './ejercicio-dialog.component';
export * from './ejercicio-delete-dialog.component';
export * from './ejercicio-detail.component';
export * from './ejercicio.component';
export * from './ejercicio.route';
