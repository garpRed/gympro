import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { EjercicioComponent } from './ejercicio.component';
import { EjercicioDetailComponent } from './ejercicio-detail.component';
import { EjercicioPopupComponent } from './ejercicio-dialog.component';
import { EjercicioDeletePopupComponent } from './ejercicio-delete-dialog.component';

@Injectable()
export class EjercicioResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const ejercicioRoute: Routes = [
    {
        path: 'ejercicio',
        component: EjercicioComponent,
        resolve: {
            'pagingParams': EjercicioResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Ejercicios'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'ejercicio/:id',
        component: EjercicioDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Ejercicios'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const ejercicioPopupRoute: Routes = [
    {
        path: 'ejercicio-new',
        component: EjercicioPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Ejercicios'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'ejercicio/:id/edit',
        component: EjercicioPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Ejercicios'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'ejercicio/:id/delete',
        component: EjercicioDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Ejercicios'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
