import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestGymSharedModule } from '../../shared';
import {
    EjercicioService,
    EjercicioPopupService,
    EjercicioComponent,
    EjercicioDetailComponent,
    EjercicioDialogComponent,
    EjercicioPopupComponent,
    EjercicioDeletePopupComponent,
    EjercicioDeleteDialogComponent,
    ejercicioRoute,
    ejercicioPopupRoute,
    EjercicioResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...ejercicioRoute,
    ...ejercicioPopupRoute,
];

@NgModule({
    imports: [
        TestGymSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        EjercicioComponent,
        EjercicioDetailComponent,
        EjercicioDialogComponent,
        EjercicioDeleteDialogComponent,
        EjercicioPopupComponent,
        EjercicioDeletePopupComponent,
    ],
    entryComponents: [
        EjercicioComponent,
        EjercicioDialogComponent,
        EjercicioPopupComponent,
        EjercicioDeleteDialogComponent,
        EjercicioDeletePopupComponent,
    ],
    providers: [
        EjercicioService,
        EjercicioPopupService,
        EjercicioResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestGymEjercicioModule {}
