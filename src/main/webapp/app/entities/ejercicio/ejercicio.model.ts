import { BaseEntity } from './../../shared';

export class Ejercicio implements BaseEntity {
    constructor(
        public id?: number,
        public nombreEjercicio?: string,
        public estadoEjercicio?: boolean,
        public sedeId?: number,
        public ejercicioActivos?: BaseEntity[],
        public ejercicioMusculos?: BaseEntity[],
    ) {
        this.estadoEjercicio = false;
    }
}
