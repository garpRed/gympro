import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Ejercicio } from './ejercicio.model';
import { EjercicioPopupService } from './ejercicio-popup.service';
import { EjercicioService } from './ejercicio.service';
import { Sede, SedeService } from '../sede';
import { Activo, ActivoService } from '../activo';
import { Musculo, MusculoService } from '../musculo';

@Component({
    selector: 'jhi-ejercicio-dialog',
    templateUrl: './ejercicio-dialog.component.html'
})
export class EjercicioDialogComponent implements OnInit {

    ejercicio: Ejercicio;
    isSaving: boolean;

    sedes: Sede[];

    activos: Activo[];

    musculos: Musculo[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private ejercicioService: EjercicioService,
        private sedeService: SedeService,
        private activoService: ActivoService,
        private musculoService: MusculoService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.sedeService.query()
            .subscribe((res: HttpResponse<Sede[]>) => { this.sedes = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.activoService.query()
            .subscribe((res: HttpResponse<Activo[]>) => { this.activos = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.musculoService.query()
            .subscribe((res: HttpResponse<Musculo[]>) => { this.musculos = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.ejercicio.id !== undefined) {
            this.subscribeToSaveResponse(
                this.ejercicioService.update(this.ejercicio));
        } else {
            this.subscribeToSaveResponse(
                this.ejercicioService.create(this.ejercicio));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Ejercicio>>) {
        result.subscribe((res: HttpResponse<Ejercicio>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Ejercicio) {
        this.eventManager.broadcast({ name: 'ejercicioListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackSedeById(index: number, item: Sede) {
        return item.id;
    }

    trackActivoById(index: number, item: Activo) {
        return item.id;
    }

    trackMusculoById(index: number, item: Musculo) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-ejercicio-popup',
    template: ''
})
export class EjercicioPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private ejercicioPopupService: EjercicioPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.ejercicioPopupService
                    .open(EjercicioDialogComponent as Component, params['id']);
            } else {
                this.ejercicioPopupService
                    .open(EjercicioDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
