import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { RegistroActividadRutina } from './registro-actividad-rutina.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<RegistroActividadRutina>;

@Injectable()
export class RegistroActividadRutinaService {

    private resourceUrl =  SERVER_API_URL + 'api/registro-actividad-rutinas';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(registroActividadRutina: RegistroActividadRutina): Observable<EntityResponseType> {
        const copy = this.convert(registroActividadRutina);
        return this.http.post<RegistroActividadRutina>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(registroActividadRutina: RegistroActividadRutina): Observable<EntityResponseType> {
        const copy = this.convert(registroActividadRutina);
        return this.http.put<RegistroActividadRutina>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<RegistroActividadRutina>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<RegistroActividadRutina[]>> {
        const options = createRequestOption(req);
        return this.http.get<RegistroActividadRutina[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<RegistroActividadRutina[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: RegistroActividadRutina = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<RegistroActividadRutina[]>): HttpResponse<RegistroActividadRutina[]> {
        const jsonResponse: RegistroActividadRutina[] = res.body;
        const body: RegistroActividadRutina[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to RegistroActividadRutina.
     */
    private convertItemFromServer(registroActividadRutina: RegistroActividadRutina): RegistroActividadRutina {
        const copy: RegistroActividadRutina = Object.assign({}, registroActividadRutina);
        copy.fechaAsistencia = this.dateUtils
            .convertDateTimeFromServer(registroActividadRutina.fechaAsistencia);
        return copy;
    }

    /**
     * Convert a RegistroActividadRutina to a JSON which can be sent to the server.
     */
    private convert(registroActividadRutina: RegistroActividadRutina): RegistroActividadRutina {
        const copy: RegistroActividadRutina = Object.assign({}, registroActividadRutina);

        copy.fechaAsistencia = this.dateUtils.toDate(registroActividadRutina.fechaAsistencia);
        return copy;
    }
}
