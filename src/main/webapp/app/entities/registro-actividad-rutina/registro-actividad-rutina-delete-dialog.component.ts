import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { RegistroActividadRutina } from './registro-actividad-rutina.model';
import { RegistroActividadRutinaPopupService } from './registro-actividad-rutina-popup.service';
import { RegistroActividadRutinaService } from './registro-actividad-rutina.service';

@Component({
    selector: 'jhi-registro-actividad-rutina-delete-dialog',
    templateUrl: './registro-actividad-rutina-delete-dialog.component.html'
})
export class RegistroActividadRutinaDeleteDialogComponent {

    registroActividadRutina: RegistroActividadRutina;

    constructor(
        private registroActividadRutinaService: RegistroActividadRutinaService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.registroActividadRutinaService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'registroActividadRutinaListModification',
                content: 'Deleted an registroActividadRutina'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-registro-actividad-rutina-delete-popup',
    template: ''
})
export class RegistroActividadRutinaDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private registroActividadRutinaPopupService: RegistroActividadRutinaPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.registroActividadRutinaPopupService
                .open(RegistroActividadRutinaDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
