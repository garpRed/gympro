import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { RegistroActividadRutina } from './registro-actividad-rutina.model';
import { RegistroActividadRutinaPopupService } from './registro-actividad-rutina-popup.service';
import { RegistroActividadRutinaService } from './registro-actividad-rutina.service';
import { Usuario, UsuarioService } from '../usuario';
import { SubRutina, SubRutinaService } from '../sub-rutina';

@Component({
    selector: 'jhi-registro-actividad-rutina-dialog',
    templateUrl: './registro-actividad-rutina-dialog.component.html'
})
export class RegistroActividadRutinaDialogComponent implements OnInit {

    registroActividadRutina: RegistroActividadRutina;
    isSaving: boolean;

    usuarios: Usuario[];

    subrutinas: SubRutina[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private registroActividadRutinaService: RegistroActividadRutinaService,
        private usuarioService: UsuarioService,
        private subRutinaService: SubRutinaService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.usuarioService.query()
            .subscribe((res: HttpResponse<Usuario[]>) => { this.usuarios = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.subRutinaService.query()
            .subscribe((res: HttpResponse<SubRutina[]>) => { this.subrutinas = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.registroActividadRutina.id !== undefined) {
            this.subscribeToSaveResponse(
                this.registroActividadRutinaService.update(this.registroActividadRutina));
        } else {
            this.subscribeToSaveResponse(
                this.registroActividadRutinaService.create(this.registroActividadRutina));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<RegistroActividadRutina>>) {
        result.subscribe((res: HttpResponse<RegistroActividadRutina>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: RegistroActividadRutina) {
        this.eventManager.broadcast({ name: 'registroActividadRutinaListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackUsuarioById(index: number, item: Usuario) {
        return item.id;
    }

    trackSubRutinaById(index: number, item: SubRutina) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-registro-actividad-rutina-popup',
    template: ''
})
export class RegistroActividadRutinaPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private registroActividadRutinaPopupService: RegistroActividadRutinaPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.registroActividadRutinaPopupService
                    .open(RegistroActividadRutinaDialogComponent as Component, params['id']);
            } else {
                this.registroActividadRutinaPopupService
                    .open(RegistroActividadRutinaDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
