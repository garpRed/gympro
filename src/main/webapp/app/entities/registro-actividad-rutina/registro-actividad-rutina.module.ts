import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestGymSharedModule } from '../../shared';
import {
    RegistroActividadRutinaService,
    RegistroActividadRutinaPopupService,
    RegistroActividadRutinaComponent,
    RegistroActividadRutinaDetailComponent,
    RegistroActividadRutinaDialogComponent,
    RegistroActividadRutinaPopupComponent,
    RegistroActividadRutinaDeletePopupComponent,
    RegistroActividadRutinaDeleteDialogComponent,
    registroActividadRutinaRoute,
    registroActividadRutinaPopupRoute,
    RegistroActividadRutinaResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...registroActividadRutinaRoute,
    ...registroActividadRutinaPopupRoute,
];

@NgModule({
    imports: [
        TestGymSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        RegistroActividadRutinaComponent,
        RegistroActividadRutinaDetailComponent,
        RegistroActividadRutinaDialogComponent,
        RegistroActividadRutinaDeleteDialogComponent,
        RegistroActividadRutinaPopupComponent,
        RegistroActividadRutinaDeletePopupComponent,
    ],
    entryComponents: [
        RegistroActividadRutinaComponent,
        RegistroActividadRutinaDialogComponent,
        RegistroActividadRutinaPopupComponent,
        RegistroActividadRutinaDeleteDialogComponent,
        RegistroActividadRutinaDeletePopupComponent,
    ],
    providers: [
        RegistroActividadRutinaService,
        RegistroActividadRutinaPopupService,
        RegistroActividadRutinaResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestGymRegistroActividadRutinaModule {}
