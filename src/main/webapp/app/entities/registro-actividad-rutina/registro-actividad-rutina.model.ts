import { BaseEntity } from './../../shared';

export class RegistroActividadRutina implements BaseEntity {
    constructor(
        public id?: number,
        public fechaAsistencia?: any,
        public diaAsistencia?: number,
        public estadoRegistroActividad?: boolean,
        public usuarioId?: number,
        public subRutinaId?: number,
    ) {
        this.estadoRegistroActividad = false;
    }
}
