import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { RegistroActividadRutinaComponent } from './registro-actividad-rutina.component';
import { RegistroActividadRutinaDetailComponent } from './registro-actividad-rutina-detail.component';
import { RegistroActividadRutinaPopupComponent } from './registro-actividad-rutina-dialog.component';
import { RegistroActividadRutinaDeletePopupComponent } from './registro-actividad-rutina-delete-dialog.component';

@Injectable()
export class RegistroActividadRutinaResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const registroActividadRutinaRoute: Routes = [
    {
        path: 'registro-actividad-rutina',
        component: RegistroActividadRutinaComponent,
        resolve: {
            'pagingParams': RegistroActividadRutinaResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'RegistroActividadRutinas'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'registro-actividad-rutina/:id',
        component: RegistroActividadRutinaDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'RegistroActividadRutinas'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const registroActividadRutinaPopupRoute: Routes = [
    {
        path: 'registro-actividad-rutina-new',
        component: RegistroActividadRutinaPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'RegistroActividadRutinas'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'registro-actividad-rutina/:id/edit',
        component: RegistroActividadRutinaPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'RegistroActividadRutinas'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'registro-actividad-rutina/:id/delete',
        component: RegistroActividadRutinaDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'RegistroActividadRutinas'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
