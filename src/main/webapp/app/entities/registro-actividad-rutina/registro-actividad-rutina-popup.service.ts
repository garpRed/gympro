import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { RegistroActividadRutina } from './registro-actividad-rutina.model';
import { RegistroActividadRutinaService } from './registro-actividad-rutina.service';

@Injectable()
export class RegistroActividadRutinaPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private registroActividadRutinaService: RegistroActividadRutinaService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.registroActividadRutinaService.find(id)
                    .subscribe((registroActividadRutinaResponse: HttpResponse<RegistroActividadRutina>) => {
                        const registroActividadRutina: RegistroActividadRutina = registroActividadRutinaResponse.body;
                        registroActividadRutina.fechaAsistencia = this.datePipe
                            .transform(registroActividadRutina.fechaAsistencia, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.registroActividadRutinaModalRef(component, registroActividadRutina);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.registroActividadRutinaModalRef(component, new RegistroActividadRutina());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    registroActividadRutinaModalRef(component: Component, registroActividadRutina: RegistroActividadRutina): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.registroActividadRutina = registroActividadRutina;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
