import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { RegistroActividadRutina } from './registro-actividad-rutina.model';
import { RegistroActividadRutinaService } from './registro-actividad-rutina.service';

@Component({
    selector: 'jhi-registro-actividad-rutina-detail',
    templateUrl: './registro-actividad-rutina-detail.component.html'
})
export class RegistroActividadRutinaDetailComponent implements OnInit, OnDestroy {

    registroActividadRutina: RegistroActividadRutina;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private registroActividadRutinaService: RegistroActividadRutinaService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInRegistroActividadRutinas();
    }

    load(id) {
        this.registroActividadRutinaService.find(id)
            .subscribe((registroActividadRutinaResponse: HttpResponse<RegistroActividadRutina>) => {
                this.registroActividadRutina = registroActividadRutinaResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInRegistroActividadRutinas() {
        this.eventSubscriber = this.eventManager.subscribe(
            'registroActividadRutinaListModification',
            (response) => this.load(this.registroActividadRutina.id)
        );
    }
}
