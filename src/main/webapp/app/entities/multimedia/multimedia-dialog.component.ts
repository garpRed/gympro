import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Multimedia } from './multimedia.model';
import { MultimediaPopupService } from './multimedia-popup.service';
import { MultimediaService } from './multimedia.service';
import { ParametroConfiguracion, ParametroConfiguracionService } from '../parametro-configuracion';

@Component({
    selector: 'jhi-multimedia-dialog',
    templateUrl: './multimedia-dialog.component.html'
})
export class MultimediaDialogComponent implements OnInit {

    multimedia: Multimedia;
    isSaving: boolean;

    tipomultimedias: ParametroConfiguracion[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private multimediaService: MultimediaService,
        private parametroConfiguracionService: ParametroConfiguracionService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.parametroConfiguracionService
            .query({filter: 'tipomultimedia-is-null'})
            .subscribe((res: HttpResponse<ParametroConfiguracion[]>) => {
                if (!this.multimedia.tipoMultimediaId) {
                    this.tipomultimedias = res.body;
                } else {
                    this.parametroConfiguracionService
                        .find(this.multimedia.tipoMultimediaId)
                        .subscribe((subRes: HttpResponse<ParametroConfiguracion>) => {
                            this.tipomultimedias = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.multimedia.id !== undefined) {
            this.subscribeToSaveResponse(
                this.multimediaService.update(this.multimedia));
        } else {
            this.subscribeToSaveResponse(
                this.multimediaService.create(this.multimedia));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Multimedia>>) {
        result.subscribe((res: HttpResponse<Multimedia>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Multimedia) {
        this.eventManager.broadcast({ name: 'multimediaListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackParametroConfiguracionById(index: number, item: ParametroConfiguracion) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-multimedia-popup',
    template: ''
})
export class MultimediaPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private multimediaPopupService: MultimediaPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.multimediaPopupService
                    .open(MultimediaDialogComponent as Component, params['id']);
            } else {
                this.multimediaPopupService
                    .open(MultimediaDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
