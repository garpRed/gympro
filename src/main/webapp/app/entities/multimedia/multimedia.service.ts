import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Multimedia } from './multimedia.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Multimedia>;

@Injectable()
export class MultimediaService {

    private resourceUrl =  SERVER_API_URL + 'api/multimedias';

    constructor(private http: HttpClient) { }

    create(multimedia: Multimedia): Observable<EntityResponseType> {
        const copy = this.convert(multimedia);
        return this.http.post<Multimedia>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(multimedia: Multimedia): Observable<EntityResponseType> {
        const copy = this.convert(multimedia);
        return this.http.put<Multimedia>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Multimedia>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Multimedia[]>> {
        const options = createRequestOption(req);
        return this.http.get<Multimedia[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Multimedia[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Multimedia = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Multimedia[]>): HttpResponse<Multimedia[]> {
        const jsonResponse: Multimedia[] = res.body;
        const body: Multimedia[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Multimedia.
     */
    private convertItemFromServer(multimedia: Multimedia): Multimedia {
        const copy: Multimedia = Object.assign({}, multimedia);
        return copy;
    }

    /**
     * Convert a Multimedia to a JSON which can be sent to the server.
     */
    private convert(multimedia: Multimedia): Multimedia {
        const copy: Multimedia = Object.assign({}, multimedia);
        return copy;
    }
}
