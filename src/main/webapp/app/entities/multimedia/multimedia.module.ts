import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestGymSharedModule } from '../../shared';
import {
    MultimediaService,
    MultimediaPopupService,
    MultimediaComponent,
    MultimediaDetailComponent,
    MultimediaDialogComponent,
    MultimediaPopupComponent,
    MultimediaDeletePopupComponent,
    MultimediaDeleteDialogComponent,
    multimediaRoute,
    multimediaPopupRoute,
    MultimediaResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...multimediaRoute,
    ...multimediaPopupRoute,
];

@NgModule({
    imports: [
        TestGymSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        MultimediaComponent,
        MultimediaDetailComponent,
        MultimediaDialogComponent,
        MultimediaDeleteDialogComponent,
        MultimediaPopupComponent,
        MultimediaDeletePopupComponent,
    ],
    entryComponents: [
        MultimediaComponent,
        MultimediaDialogComponent,
        MultimediaPopupComponent,
        MultimediaDeleteDialogComponent,
        MultimediaDeletePopupComponent,
    ],
    providers: [
        MultimediaService,
        MultimediaPopupService,
        MultimediaResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestGymMultimediaModule {}
