import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { MultimediaComponent } from './multimedia.component';
import { MultimediaDetailComponent } from './multimedia-detail.component';
import { MultimediaPopupComponent } from './multimedia-dialog.component';
import { MultimediaDeletePopupComponent } from './multimedia-delete-dialog.component';

@Injectable()
export class MultimediaResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const multimediaRoute: Routes = [
    {
        path: 'multimedia',
        component: MultimediaComponent,
        resolve: {
            'pagingParams': MultimediaResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Multimedias'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'multimedia/:id',
        component: MultimediaDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Multimedias'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const multimediaPopupRoute: Routes = [
    {
        path: 'multimedia-new',
        component: MultimediaPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Multimedias'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'multimedia/:id/edit',
        component: MultimediaPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Multimedias'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'multimedia/:id/delete',
        component: MultimediaDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Multimedias'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
