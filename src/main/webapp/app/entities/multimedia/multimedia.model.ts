import { BaseEntity } from './../../shared';

export class Multimedia implements BaseEntity {
    constructor(
        public id?: number,
        public nombreMultimedia?: string,
        public propositoMultimedia?: number,
        public encode?: string,
        public tipoMultimediaId?: number,
    ) {
    }
}
