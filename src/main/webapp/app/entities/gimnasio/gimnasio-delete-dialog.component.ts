import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Gimnasio } from './gimnasio.model';
import { GimnasioPopupService } from './gimnasio-popup.service';
import { GimnasioService } from './gimnasio.service';

@Component({
    selector: 'jhi-gimnasio-delete-dialog',
    templateUrl: './gimnasio-delete-dialog.component.html'
})
export class GimnasioDeleteDialogComponent {

    gimnasio: Gimnasio;

    constructor(
        private gimnasioService: GimnasioService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.gimnasioService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'gimnasioListModification',
                content: 'Deleted an gimnasio'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-gimnasio-delete-popup',
    template: ''
})
export class GimnasioDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private gimnasioPopupService: GimnasioPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.gimnasioPopupService
                .open(GimnasioDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
