import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { GimnasioComponent } from './gimnasio.component';
import { GimnasioDetailComponent } from './gimnasio-detail.component';
import { GimnasioPopupComponent } from './gimnasio-dialog.component';
import { GimnasioDeletePopupComponent } from './gimnasio-delete-dialog.component';

@Injectable()
export class GimnasioResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const gimnasioRoute: Routes = [
    {
        path: 'gimnasio',
        component: GimnasioComponent,
        resolve: {
            'pagingParams': GimnasioResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Gimnasios'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'gimnasio/:id',
        component: GimnasioDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Gimnasios'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const gimnasioPopupRoute: Routes = [
    {
        path: 'gimnasio-new',
        component: GimnasioPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Gimnasios'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'gimnasio/:id/edit',
        component: GimnasioPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Gimnasios'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'gimnasio/:id/delete',
        component: GimnasioDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Gimnasios'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
