import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Gimnasio } from './gimnasio.model';
import { GimnasioService } from './gimnasio.service';

@Component({
    selector: 'jhi-gimnasio-detail',
    templateUrl: './gimnasio-detail.component.html'
})
export class GimnasioDetailComponent implements OnInit, OnDestroy {

    gimnasio: Gimnasio;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private gimnasioService: GimnasioService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInGimnasios();
    }

    load(id) {
        this.gimnasioService.find(id)
            .subscribe((gimnasioResponse: HttpResponse<Gimnasio>) => {
                this.gimnasio = gimnasioResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInGimnasios() {
        this.eventSubscriber = this.eventManager.subscribe(
            'gimnasioListModification',
            (response) => this.load(this.gimnasio.id)
        );
    }
}
