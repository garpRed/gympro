import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Gimnasio } from './gimnasio.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Gimnasio>;

@Injectable()
export class GimnasioService {

    private resourceUrl =  SERVER_API_URL + 'api/gimnasios';

    constructor(private http: HttpClient) { }

    create(gimnasio: Gimnasio): Observable<EntityResponseType> {
        const copy = this.convert(gimnasio);
        return this.http.post<Gimnasio>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(gimnasio: Gimnasio): Observable<EntityResponseType> {
        const copy = this.convert(gimnasio);
        return this.http.put<Gimnasio>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Gimnasio>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Gimnasio[]>> {
        const options = createRequestOption(req);
        return this.http.get<Gimnasio[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Gimnasio[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Gimnasio = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Gimnasio[]>): HttpResponse<Gimnasio[]> {
        const jsonResponse: Gimnasio[] = res.body;
        const body: Gimnasio[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Gimnasio.
     */
    private convertItemFromServer(gimnasio: Gimnasio): Gimnasio {
        const copy: Gimnasio = Object.assign({}, gimnasio);
        return copy;
    }

    /**
     * Convert a Gimnasio to a JSON which can be sent to the server.
     */
    private convert(gimnasio: Gimnasio): Gimnasio {
        const copy: Gimnasio = Object.assign({}, gimnasio);
        return copy;
    }
}
