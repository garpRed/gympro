import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Gimnasio } from './gimnasio.model';
import { GimnasioPopupService } from './gimnasio-popup.service';
import { GimnasioService } from './gimnasio.service';

@Component({
    selector: 'jhi-gimnasio-dialog',
    templateUrl: './gimnasio-dialog.component.html'
})
export class GimnasioDialogComponent implements OnInit {

    gimnasio: Gimnasio;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private gimnasioService: GimnasioService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.gimnasio.id !== undefined) {
            this.subscribeToSaveResponse(
                this.gimnasioService.update(this.gimnasio));
        } else {
            this.subscribeToSaveResponse(
                this.gimnasioService.create(this.gimnasio));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Gimnasio>>) {
        result.subscribe((res: HttpResponse<Gimnasio>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Gimnasio) {
        this.eventManager.broadcast({ name: 'gimnasioListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-gimnasio-popup',
    template: ''
})
export class GimnasioPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private gimnasioPopupService: GimnasioPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.gimnasioPopupService
                    .open(GimnasioDialogComponent as Component, params['id']);
            } else {
                this.gimnasioPopupService
                    .open(GimnasioDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
