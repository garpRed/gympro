import { BaseEntity } from './../../shared';

export class Gimnasio implements BaseEntity {
    constructor(
        public id?: number,
        public nombreGimnasio?: string,
        public estadoGimnasio?: boolean,
        public gimnasioSedes?: BaseEntity[],
    ) {
        this.estadoGimnasio = false;
    }
}
