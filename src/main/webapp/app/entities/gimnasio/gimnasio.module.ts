import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestGymSharedModule } from '../../shared';
import {
    GimnasioService,
    GimnasioPopupService,
    GimnasioComponent,
    GimnasioDetailComponent,
    GimnasioDialogComponent,
    GimnasioPopupComponent,
    GimnasioDeletePopupComponent,
    GimnasioDeleteDialogComponent,
    gimnasioRoute,
    gimnasioPopupRoute,
    GimnasioResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...gimnasioRoute,
    ...gimnasioPopupRoute,
];

@NgModule({
    imports: [
        TestGymSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        GimnasioComponent,
        GimnasioDetailComponent,
        GimnasioDialogComponent,
        GimnasioDeleteDialogComponent,
        GimnasioPopupComponent,
        GimnasioDeletePopupComponent,
    ],
    entryComponents: [
        GimnasioComponent,
        GimnasioDialogComponent,
        GimnasioPopupComponent,
        GimnasioDeleteDialogComponent,
        GimnasioDeletePopupComponent,
    ],
    providers: [
        GimnasioService,
        GimnasioPopupService,
        GimnasioResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestGymGimnasioModule {}
