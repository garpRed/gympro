export * from './gimnasio.model';
export * from './gimnasio-popup.service';
export * from './gimnasio.service';
export * from './gimnasio-dialog.component';
export * from './gimnasio-delete-dialog.component';
export * from './gimnasio-detail.component';
export * from './gimnasio.component';
export * from './gimnasio.route';
