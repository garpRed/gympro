export * from './tipo-parametro.model';
export * from './tipo-parametro-popup.service';
export * from './tipo-parametro.service';
export * from './tipo-parametro-dialog.component';
export * from './tipo-parametro-delete-dialog.component';
export * from './tipo-parametro-detail.component';
export * from './tipo-parametro.component';
export * from './tipo-parametro.route';
