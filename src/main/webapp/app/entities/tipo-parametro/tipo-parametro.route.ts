import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { TipoParametroComponent } from './tipo-parametro.component';
import { TipoParametroDetailComponent } from './tipo-parametro-detail.component';
import { TipoParametroPopupComponent } from './tipo-parametro-dialog.component';
import { TipoParametroDeletePopupComponent } from './tipo-parametro-delete-dialog.component';

@Injectable()
export class TipoParametroResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const tipoParametroRoute: Routes = [
    {
        path: 'tipo-parametro',
        component: TipoParametroComponent,
        resolve: {
            'pagingParams': TipoParametroResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'TipoParametros'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'tipo-parametro/:id',
        component: TipoParametroDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'TipoParametros'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const tipoParametroPopupRoute: Routes = [
    {
        path: 'tipo-parametro-new',
        component: TipoParametroPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'TipoParametros'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'tipo-parametro/:id/edit',
        component: TipoParametroPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'TipoParametros'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'tipo-parametro/:id/delete',
        component: TipoParametroDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'TipoParametros'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
