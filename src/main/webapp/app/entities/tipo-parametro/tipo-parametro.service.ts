import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { TipoParametro } from './tipo-parametro.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<TipoParametro>;

@Injectable()
export class TipoParametroService {

    private resourceUrl =  SERVER_API_URL + 'api/tipo-parametros';

    constructor(private http: HttpClient) { }

    create(tipoParametro: TipoParametro): Observable<EntityResponseType> {
        const copy = this.convert(tipoParametro);
        return this.http.post<TipoParametro>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(tipoParametro: TipoParametro): Observable<EntityResponseType> {
        const copy = this.convert(tipoParametro);
        return this.http.put<TipoParametro>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<TipoParametro>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<TipoParametro[]>> {
        const options = createRequestOption(req);
        return this.http.get<TipoParametro[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<TipoParametro[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: TipoParametro = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<TipoParametro[]>): HttpResponse<TipoParametro[]> {
        const jsonResponse: TipoParametro[] = res.body;
        const body: TipoParametro[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to TipoParametro.
     */
    private convertItemFromServer(tipoParametro: TipoParametro): TipoParametro {
        const copy: TipoParametro = Object.assign({}, tipoParametro);
        return copy;
    }

    /**
     * Convert a TipoParametro to a JSON which can be sent to the server.
     */
    private convert(tipoParametro: TipoParametro): TipoParametro {
        const copy: TipoParametro = Object.assign({}, tipoParametro);
        return copy;
    }
}
