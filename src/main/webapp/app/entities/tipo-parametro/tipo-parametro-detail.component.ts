import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { TipoParametro } from './tipo-parametro.model';
import { TipoParametroService } from './tipo-parametro.service';

@Component({
    selector: 'jhi-tipo-parametro-detail',
    templateUrl: './tipo-parametro-detail.component.html'
})
export class TipoParametroDetailComponent implements OnInit, OnDestroy {

    tipoParametro: TipoParametro;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private tipoParametroService: TipoParametroService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTipoParametros();
    }

    load(id) {
        this.tipoParametroService.find(id)
            .subscribe((tipoParametroResponse: HttpResponse<TipoParametro>) => {
                this.tipoParametro = tipoParametroResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTipoParametros() {
        this.eventSubscriber = this.eventManager.subscribe(
            'tipoParametroListModification',
            (response) => this.load(this.tipoParametro.id)
        );
    }
}
