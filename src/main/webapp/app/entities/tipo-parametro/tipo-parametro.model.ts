import { BaseEntity } from './../../shared';

export class TipoParametro implements BaseEntity {
    constructor(
        public id?: number,
        public nombreTipoParametro?: string,
        public estadoTipoParametro?: boolean,
    ) {
        this.estadoTipoParametro = false;
    }
}
