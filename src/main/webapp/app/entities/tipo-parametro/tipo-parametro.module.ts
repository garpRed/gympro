import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestGymSharedModule } from '../../shared';
import {
    TipoParametroService,
    TipoParametroPopupService,
    TipoParametroComponent,
    TipoParametroDetailComponent,
    TipoParametroDialogComponent,
    TipoParametroPopupComponent,
    TipoParametroDeletePopupComponent,
    TipoParametroDeleteDialogComponent,
    tipoParametroRoute,
    tipoParametroPopupRoute,
    TipoParametroResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...tipoParametroRoute,
    ...tipoParametroPopupRoute,
];

@NgModule({
    imports: [
        TestGymSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        TipoParametroComponent,
        TipoParametroDetailComponent,
        TipoParametroDialogComponent,
        TipoParametroDeleteDialogComponent,
        TipoParametroPopupComponent,
        TipoParametroDeletePopupComponent,
    ],
    entryComponents: [
        TipoParametroComponent,
        TipoParametroDialogComponent,
        TipoParametroPopupComponent,
        TipoParametroDeleteDialogComponent,
        TipoParametroDeletePopupComponent,
    ],
    providers: [
        TipoParametroService,
        TipoParametroPopupService,
        TipoParametroResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestGymTipoParametroModule {}
