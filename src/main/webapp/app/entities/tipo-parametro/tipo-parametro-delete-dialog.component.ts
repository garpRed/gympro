import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TipoParametro } from './tipo-parametro.model';
import { TipoParametroPopupService } from './tipo-parametro-popup.service';
import { TipoParametroService } from './tipo-parametro.service';

@Component({
    selector: 'jhi-tipo-parametro-delete-dialog',
    templateUrl: './tipo-parametro-delete-dialog.component.html'
})
export class TipoParametroDeleteDialogComponent {

    tipoParametro: TipoParametro;

    constructor(
        private tipoParametroService: TipoParametroService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.tipoParametroService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'tipoParametroListModification',
                content: 'Deleted an tipoParametro'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-tipo-parametro-delete-popup',
    template: ''
})
export class TipoParametroDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tipoParametroPopupService: TipoParametroPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.tipoParametroPopupService
                .open(TipoParametroDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
