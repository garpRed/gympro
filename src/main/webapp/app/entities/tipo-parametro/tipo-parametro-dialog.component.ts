import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TipoParametro } from './tipo-parametro.model';
import { TipoParametroPopupService } from './tipo-parametro-popup.service';
import { TipoParametroService } from './tipo-parametro.service';

@Component({
    selector: 'jhi-tipo-parametro-dialog',
    templateUrl: './tipo-parametro-dialog.component.html'
})
export class TipoParametroDialogComponent implements OnInit {

    tipoParametro: TipoParametro;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private tipoParametroService: TipoParametroService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.tipoParametro.id !== undefined) {
            this.subscribeToSaveResponse(
                this.tipoParametroService.update(this.tipoParametro));
        } else {
            this.subscribeToSaveResponse(
                this.tipoParametroService.create(this.tipoParametro));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<TipoParametro>>) {
        result.subscribe((res: HttpResponse<TipoParametro>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: TipoParametro) {
        this.eventManager.broadcast({ name: 'tipoParametroListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-tipo-parametro-popup',
    template: ''
})
export class TipoParametroPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tipoParametroPopupService: TipoParametroPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.tipoParametroPopupService
                    .open(TipoParametroDialogComponent as Component, params['id']);
            } else {
                this.tipoParametroPopupService
                    .open(TipoParametroDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
