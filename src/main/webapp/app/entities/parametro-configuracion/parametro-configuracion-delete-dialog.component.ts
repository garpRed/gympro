import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ParametroConfiguracion } from './parametro-configuracion.model';
import { ParametroConfiguracionPopupService } from './parametro-configuracion-popup.service';
import { ParametroConfiguracionService } from './parametro-configuracion.service';

@Component({
    selector: 'jhi-parametro-configuracion-delete-dialog',
    templateUrl: './parametro-configuracion-delete-dialog.component.html'
})
export class ParametroConfiguracionDeleteDialogComponent {

    parametroConfiguracion: ParametroConfiguracion;

    constructor(
        private parametroConfiguracionService: ParametroConfiguracionService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.parametroConfiguracionService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'parametroConfiguracionListModification',
                content: 'Deleted an parametroConfiguracion'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-parametro-configuracion-delete-popup',
    template: ''
})
export class ParametroConfiguracionDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private parametroConfiguracionPopupService: ParametroConfiguracionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.parametroConfiguracionPopupService
                .open(ParametroConfiguracionDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
