import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ParametroConfiguracion } from './parametro-configuracion.model';
import { ParametroConfiguracionPopupService } from './parametro-configuracion-popup.service';
import { ParametroConfiguracionService } from './parametro-configuracion.service';
import { TipoParametro, TipoParametroService } from '../tipo-parametro';
import { Multimedia, MultimediaService } from '../multimedia';
import { Musculo, MusculoService } from '../musculo';

@Component({
    selector: 'jhi-parametro-configuracion-dialog',
    templateUrl: './parametro-configuracion-dialog.component.html'
})
export class ParametroConfiguracionDialogComponent implements OnInit {

    parametroConfiguracion: ParametroConfiguracion;
    isSaving: boolean;

    tipoparametros: TipoParametro[];

    multimedias: Multimedia[];

    musculos: Musculo[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private parametroConfiguracionService: ParametroConfiguracionService,
        private tipoParametroService: TipoParametroService,
        private multimediaService: MultimediaService,
        private musculoService: MusculoService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.tipoParametroService
            .query({filter: 'parametroconfiguracion-is-null'})
            .subscribe((res: HttpResponse<TipoParametro[]>) => {
                if (!this.parametroConfiguracion.tipoParametroId) {
                    this.tipoparametros = res.body;
                } else {
                    this.tipoParametroService
                        .find(this.parametroConfiguracion.tipoParametroId)
                        .subscribe((subRes: HttpResponse<TipoParametro>) => {
                            this.tipoparametros = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.multimediaService.query()
            .subscribe((res: HttpResponse<Multimedia[]>) => { this.multimedias = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.musculoService.query()
            .subscribe((res: HttpResponse<Musculo[]>) => { this.musculos = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.parametroConfiguracion.id !== undefined) {
            this.subscribeToSaveResponse(
                this.parametroConfiguracionService.update(this.parametroConfiguracion));
        } else {
            this.subscribeToSaveResponse(
                this.parametroConfiguracionService.create(this.parametroConfiguracion));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ParametroConfiguracion>>) {
        result.subscribe((res: HttpResponse<ParametroConfiguracion>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ParametroConfiguracion) {
        this.eventManager.broadcast({ name: 'parametroConfiguracionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackTipoParametroById(index: number, item: TipoParametro) {
        return item.id;
    }

    trackMultimediaById(index: number, item: Multimedia) {
        return item.id;
    }

    trackMusculoById(index: number, item: Musculo) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-parametro-configuracion-popup',
    template: ''
})
export class ParametroConfiguracionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private parametroConfiguracionPopupService: ParametroConfiguracionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.parametroConfiguracionPopupService
                    .open(ParametroConfiguracionDialogComponent as Component, params['id']);
            } else {
                this.parametroConfiguracionPopupService
                    .open(ParametroConfiguracionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
