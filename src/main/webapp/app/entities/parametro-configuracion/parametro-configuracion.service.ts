import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ParametroConfiguracion } from './parametro-configuracion.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<ParametroConfiguracion>;

@Injectable()
export class ParametroConfiguracionService {

    private resourceUrl =  SERVER_API_URL + 'api/parametro-configuracions';

    constructor(private http: HttpClient) { }

    create(parametroConfiguracion: ParametroConfiguracion): Observable<EntityResponseType> {
        const copy = this.convert(parametroConfiguracion);
        return this.http.post<ParametroConfiguracion>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(parametroConfiguracion: ParametroConfiguracion): Observable<EntityResponseType> {
        const copy = this.convert(parametroConfiguracion);
        return this.http.put<ParametroConfiguracion>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ParametroConfiguracion>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ParametroConfiguracion[]>> {
        const options = createRequestOption(req);
        return this.http.get<ParametroConfiguracion[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ParametroConfiguracion[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ParametroConfiguracion = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ParametroConfiguracion[]>): HttpResponse<ParametroConfiguracion[]> {
        const jsonResponse: ParametroConfiguracion[] = res.body;
        const body: ParametroConfiguracion[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ParametroConfiguracion.
     */
    private convertItemFromServer(parametroConfiguracion: ParametroConfiguracion): ParametroConfiguracion {
        const copy: ParametroConfiguracion = Object.assign({}, parametroConfiguracion);
        return copy;
    }

    /**
     * Convert a ParametroConfiguracion to a JSON which can be sent to the server.
     */
    private convert(parametroConfiguracion: ParametroConfiguracion): ParametroConfiguracion {
        const copy: ParametroConfiguracion = Object.assign({}, parametroConfiguracion);
        return copy;
    }
}
