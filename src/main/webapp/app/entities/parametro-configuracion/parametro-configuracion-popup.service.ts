import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { ParametroConfiguracion } from './parametro-configuracion.model';
import { ParametroConfiguracionService } from './parametro-configuracion.service';

@Injectable()
export class ParametroConfiguracionPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private parametroConfiguracionService: ParametroConfiguracionService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.parametroConfiguracionService.find(id)
                    .subscribe((parametroConfiguracionResponse: HttpResponse<ParametroConfiguracion>) => {
                        const parametroConfiguracion: ParametroConfiguracion = parametroConfiguracionResponse.body;
                        this.ngbModalRef = this.parametroConfiguracionModalRef(component, parametroConfiguracion);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.parametroConfiguracionModalRef(component, new ParametroConfiguracion());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    parametroConfiguracionModalRef(component: Component, parametroConfiguracion: ParametroConfiguracion): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.parametroConfiguracion = parametroConfiguracion;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
