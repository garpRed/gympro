import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { ParametroConfiguracionComponent } from './parametro-configuracion.component';
import { ParametroConfiguracionDetailComponent } from './parametro-configuracion-detail.component';
import { ParametroConfiguracionPopupComponent } from './parametro-configuracion-dialog.component';
import { ParametroConfiguracionDeletePopupComponent } from './parametro-configuracion-delete-dialog.component';

@Injectable()
export class ParametroConfiguracionResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const parametroConfiguracionRoute: Routes = [
    {
        path: 'parametro-configuracion',
        component: ParametroConfiguracionComponent,
        resolve: {
            'pagingParams': ParametroConfiguracionResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ParametroConfiguracions'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'parametro-configuracion/:id',
        component: ParametroConfiguracionDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ParametroConfiguracions'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const parametroConfiguracionPopupRoute: Routes = [
    {
        path: 'parametro-configuracion-new',
        component: ParametroConfiguracionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ParametroConfiguracions'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'parametro-configuracion/:id/edit',
        component: ParametroConfiguracionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ParametroConfiguracions'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'parametro-configuracion/:id/delete',
        component: ParametroConfiguracionDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'ParametroConfiguracions'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
