import { BaseEntity } from './../../shared';

export class ParametroConfiguracion implements BaseEntity {
    constructor(
        public id?: number,
        public nombreParametro?: string,
        public valorParametro?: string,
        public tipoDeDato?: string,
        public estadoParametro?: boolean,
        public tipoParametroId?: number,
        public tipoMultimediaId?: number,
        public zonaMusculoId?: number,
    ) {
        this.estadoParametro = false;
    }
}
