import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestGymSharedModule } from '../../shared';
import {
    ParametroConfiguracionService,
    ParametroConfiguracionPopupService,
    ParametroConfiguracionComponent,
    ParametroConfiguracionDetailComponent,
    ParametroConfiguracionDialogComponent,
    ParametroConfiguracionPopupComponent,
    ParametroConfiguracionDeletePopupComponent,
    ParametroConfiguracionDeleteDialogComponent,
    parametroConfiguracionRoute,
    parametroConfiguracionPopupRoute,
    ParametroConfiguracionResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...parametroConfiguracionRoute,
    ...parametroConfiguracionPopupRoute,
];

@NgModule({
    imports: [
        TestGymSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ParametroConfiguracionComponent,
        ParametroConfiguracionDetailComponent,
        ParametroConfiguracionDialogComponent,
        ParametroConfiguracionDeleteDialogComponent,
        ParametroConfiguracionPopupComponent,
        ParametroConfiguracionDeletePopupComponent,
    ],
    entryComponents: [
        ParametroConfiguracionComponent,
        ParametroConfiguracionDialogComponent,
        ParametroConfiguracionPopupComponent,
        ParametroConfiguracionDeleteDialogComponent,
        ParametroConfiguracionDeletePopupComponent,
    ],
    providers: [
        ParametroConfiguracionService,
        ParametroConfiguracionPopupService,
        ParametroConfiguracionResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestGymParametroConfiguracionModule {}
