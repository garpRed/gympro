import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { ParametroConfiguracion } from './parametro-configuracion.model';
import { ParametroConfiguracionService } from './parametro-configuracion.service';

@Component({
    selector: 'jhi-parametro-configuracion-detail',
    templateUrl: './parametro-configuracion-detail.component.html'
})
export class ParametroConfiguracionDetailComponent implements OnInit, OnDestroy {

    parametroConfiguracion: ParametroConfiguracion;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private parametroConfiguracionService: ParametroConfiguracionService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInParametroConfiguracions();
    }

    load(id) {
        this.parametroConfiguracionService.find(id)
            .subscribe((parametroConfiguracionResponse: HttpResponse<ParametroConfiguracion>) => {
                this.parametroConfiguracion = parametroConfiguracionResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInParametroConfiguracions() {
        this.eventSubscriber = this.eventManager.subscribe(
            'parametroConfiguracionListModification',
            (response) => this.load(this.parametroConfiguracion.id)
        );
    }
}
