export * from './parametro-configuracion.model';
export * from './parametro-configuracion-popup.service';
export * from './parametro-configuracion.service';
export * from './parametro-configuracion-dialog.component';
export * from './parametro-configuracion-delete-dialog.component';
export * from './parametro-configuracion-detail.component';
export * from './parametro-configuracion.component';
export * from './parametro-configuracion.route';
