import { BaseEntity } from './../../shared';

export class Solicitud implements BaseEntity {
    constructor(
        public id?: number,
        public descripcionSolicitud?: string,
        public fechaSolicitud?: any,
        public horaInicio?: any,
        public horaFin?: any,
        public usuarioId?: number,
        public entrenadorSolicitudId?: number,
        public tipoSolicitudId?: number,
    ) {
    }
}
