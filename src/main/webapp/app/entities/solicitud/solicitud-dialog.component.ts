import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Solicitud } from './solicitud.model';
import { SolicitudPopupService } from './solicitud-popup.service';
import { SolicitudService } from './solicitud.service';
import { Usuario, UsuarioService } from '../usuario';
import { ParametroConfiguracion, ParametroConfiguracionService } from '../parametro-configuracion';

@Component({
    selector: 'jhi-solicitud-dialog',
    templateUrl: './solicitud-dialog.component.html'
})
export class SolicitudDialogComponent implements OnInit {

    solicitud: Solicitud;
    isSaving: boolean;

    usuarios: Usuario[];

    entrenadorsolicituds: Usuario[];

    tiposolicituds: ParametroConfiguracion[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private solicitudService: SolicitudService,
        private usuarioService: UsuarioService,
        private parametroConfiguracionService: ParametroConfiguracionService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.usuarioService.query()
            .subscribe((res: HttpResponse<Usuario[]>) => { this.usuarios = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.usuarioService
            .query({filter: 'solicitud-is-null'})
            .subscribe((res: HttpResponse<Usuario[]>) => {
                if (!this.solicitud.entrenadorSolicitudId) {
                    this.entrenadorsolicituds = res.body;
                } else {
                    this.usuarioService
                        .find(this.solicitud.entrenadorSolicitudId)
                        .subscribe((subRes: HttpResponse<Usuario>) => {
                            this.entrenadorsolicituds = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.parametroConfiguracionService
            .query({filter: 'solicitud-is-null'})
            .subscribe((res: HttpResponse<ParametroConfiguracion[]>) => {
                if (!this.solicitud.tipoSolicitudId) {
                    this.tiposolicituds = res.body;
                } else {
                    this.parametroConfiguracionService
                        .find(this.solicitud.tipoSolicitudId)
                        .subscribe((subRes: HttpResponse<ParametroConfiguracion>) => {
                            this.tiposolicituds = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.solicitud.id !== undefined) {
            this.subscribeToSaveResponse(
                this.solicitudService.update(this.solicitud));
        } else {
            this.subscribeToSaveResponse(
                this.solicitudService.create(this.solicitud));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Solicitud>>) {
        result.subscribe((res: HttpResponse<Solicitud>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Solicitud) {
        this.eventManager.broadcast({ name: 'solicitudListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackUsuarioById(index: number, item: Usuario) {
        return item.id;
    }

    trackParametroConfiguracionById(index: number, item: ParametroConfiguracion) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-solicitud-popup',
    template: ''
})
export class SolicitudPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private solicitudPopupService: SolicitudPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.solicitudPopupService
                    .open(SolicitudDialogComponent as Component, params['id']);
            } else {
                this.solicitudPopupService
                    .open(SolicitudDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
