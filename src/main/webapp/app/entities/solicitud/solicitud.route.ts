import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { SolicitudComponent } from './solicitud.component';
import { SolicitudDetailComponent } from './solicitud-detail.component';
import { SolicitudPopupComponent } from './solicitud-dialog.component';
import { SolicitudDeletePopupComponent } from './solicitud-delete-dialog.component';

@Injectable()
export class SolicitudResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const solicitudRoute: Routes = [
    {
        path: 'solicitud',
        component: SolicitudComponent,
        resolve: {
            'pagingParams': SolicitudResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Solicituds'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'solicitud/:id',
        component: SolicitudDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Solicituds'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const solicitudPopupRoute: Routes = [
    {
        path: 'solicitud-new',
        component: SolicitudPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Solicituds'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'solicitud/:id/edit',
        component: SolicitudPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Solicituds'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'solicitud/:id/delete',
        component: SolicitudDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Solicituds'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
