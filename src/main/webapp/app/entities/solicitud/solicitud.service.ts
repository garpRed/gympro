import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Solicitud } from './solicitud.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Solicitud>;

@Injectable()
export class SolicitudService {

    private resourceUrl =  SERVER_API_URL + 'api/solicituds';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(solicitud: Solicitud): Observable<EntityResponseType> {
        const copy = this.convert(solicitud);
        return this.http.post<Solicitud>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(solicitud: Solicitud): Observable<EntityResponseType> {
        const copy = this.convert(solicitud);
        return this.http.put<Solicitud>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Solicitud>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Solicitud[]>> {
        const options = createRequestOption(req);
        return this.http.get<Solicitud[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Solicitud[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Solicitud = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Solicitud[]>): HttpResponse<Solicitud[]> {
        const jsonResponse: Solicitud[] = res.body;
        const body: Solicitud[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Solicitud.
     */
    private convertItemFromServer(solicitud: Solicitud): Solicitud {
        const copy: Solicitud = Object.assign({}, solicitud);
        copy.fechaSolicitud = this.dateUtils
            .convertDateTimeFromServer(solicitud.fechaSolicitud);
        copy.horaInicio = this.dateUtils
            .convertDateTimeFromServer(solicitud.horaInicio);
        copy.horaFin = this.dateUtils
            .convertDateTimeFromServer(solicitud.horaFin);
        return copy;
    }

    /**
     * Convert a Solicitud to a JSON which can be sent to the server.
     */
    private convert(solicitud: Solicitud): Solicitud {
        const copy: Solicitud = Object.assign({}, solicitud);

        copy.fechaSolicitud = this.dateUtils.toDate(solicitud.fechaSolicitud);

        copy.horaInicio = this.dateUtils.toDate(solicitud.horaInicio);

        copy.horaFin = this.dateUtils.toDate(solicitud.horaFin);
        return copy;
    }
}
