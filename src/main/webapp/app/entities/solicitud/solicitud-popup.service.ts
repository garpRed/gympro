import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Solicitud } from './solicitud.model';
import { SolicitudService } from './solicitud.service';

@Injectable()
export class SolicitudPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private solicitudService: SolicitudService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.solicitudService.find(id)
                    .subscribe((solicitudResponse: HttpResponse<Solicitud>) => {
                        const solicitud: Solicitud = solicitudResponse.body;
                        solicitud.fechaSolicitud = this.datePipe
                            .transform(solicitud.fechaSolicitud, 'yyyy-MM-ddTHH:mm:ss');
                        solicitud.horaInicio = this.datePipe
                            .transform(solicitud.horaInicio, 'yyyy-MM-ddTHH:mm:ss');
                        solicitud.horaFin = this.datePipe
                            .transform(solicitud.horaFin, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.solicitudModalRef(component, solicitud);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.solicitudModalRef(component, new Solicitud());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    solicitudModalRef(component: Component, solicitud: Solicitud): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.solicitud = solicitud;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
