import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestGymSharedModule } from '../../shared';
import {
    SolicitudService,
    SolicitudPopupService,
    SolicitudComponent,
    SolicitudDetailComponent,
    SolicitudDialogComponent,
    SolicitudPopupComponent,
    SolicitudDeletePopupComponent,
    SolicitudDeleteDialogComponent,
    solicitudRoute,
    solicitudPopupRoute,
    SolicitudResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...solicitudRoute,
    ...solicitudPopupRoute,
];

@NgModule({
    imports: [
        TestGymSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        SolicitudComponent,
        SolicitudDetailComponent,
        SolicitudDialogComponent,
        SolicitudDeleteDialogComponent,
        SolicitudPopupComponent,
        SolicitudDeletePopupComponent,
    ],
    entryComponents: [
        SolicitudComponent,
        SolicitudDialogComponent,
        SolicitudPopupComponent,
        SolicitudDeleteDialogComponent,
        SolicitudDeletePopupComponent,
    ],
    providers: [
        SolicitudService,
        SolicitudPopupService,
        SolicitudResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestGymSolicitudModule {}
