import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { SubRutina } from './sub-rutina.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<SubRutina>;

@Injectable()
export class SubRutinaService {

    private resourceUrl =  SERVER_API_URL + 'api/sub-rutinas';

    constructor(private http: HttpClient) { }

    create(subRutina: SubRutina): Observable<EntityResponseType> {
        const copy = this.convert(subRutina);
        return this.http.post<SubRutina>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(subRutina: SubRutina): Observable<EntityResponseType> {
        const copy = this.convert(subRutina);
        return this.http.put<SubRutina>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<SubRutina>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<SubRutina[]>> {
        const options = createRequestOption(req);
        return this.http.get<SubRutina[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<SubRutina[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: SubRutina = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<SubRutina[]>): HttpResponse<SubRutina[]> {
        const jsonResponse: SubRutina[] = res.body;
        const body: SubRutina[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to SubRutina.
     */
    private convertItemFromServer(subRutina: SubRutina): SubRutina {
        const copy: SubRutina = Object.assign({}, subRutina);
        return copy;
    }

    /**
     * Convert a SubRutina to a JSON which can be sent to the server.
     */
    private convert(subRutina: SubRutina): SubRutina {
        const copy: SubRutina = Object.assign({}, subRutina);
        return copy;
    }
}
