import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { SubRutinaComponent } from './sub-rutina.component';
import { SubRutinaDetailComponent } from './sub-rutina-detail.component';
import { SubRutinaPopupComponent } from './sub-rutina-dialog.component';
import { SubRutinaDeletePopupComponent } from './sub-rutina-delete-dialog.component';

@Injectable()
export class SubRutinaResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const subRutinaRoute: Routes = [
    {
        path: 'sub-rutina',
        component: SubRutinaComponent,
        resolve: {
            'pagingParams': SubRutinaResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'SubRutinas'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'sub-rutina/:id',
        component: SubRutinaDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'SubRutinas'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const subRutinaPopupRoute: Routes = [
    {
        path: 'sub-rutina-new',
        component: SubRutinaPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'SubRutinas'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sub-rutina/:id/edit',
        component: SubRutinaPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'SubRutinas'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sub-rutina/:id/delete',
        component: SubRutinaDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'SubRutinas'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
