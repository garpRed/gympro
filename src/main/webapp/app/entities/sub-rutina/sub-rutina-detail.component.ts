import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { SubRutina } from './sub-rutina.model';
import { SubRutinaService } from './sub-rutina.service';

@Component({
    selector: 'jhi-sub-rutina-detail',
    templateUrl: './sub-rutina-detail.component.html'
})
export class SubRutinaDetailComponent implements OnInit, OnDestroy {

    subRutina: SubRutina;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private subRutinaService: SubRutinaService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSubRutinas();
    }

    load(id) {
        this.subRutinaService.find(id)
            .subscribe((subRutinaResponse: HttpResponse<SubRutina>) => {
                this.subRutina = subRutinaResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSubRutinas() {
        this.eventSubscriber = this.eventManager.subscribe(
            'subRutinaListModification',
            (response) => this.load(this.subRutina.id)
        );
    }
}
