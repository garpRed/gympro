import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { SubRutina } from './sub-rutina.model';
import { SubRutinaPopupService } from './sub-rutina-popup.service';
import { SubRutinaService } from './sub-rutina.service';
import { Rutina, RutinaService } from '../rutina';
import { Ejercicio, EjercicioService } from '../ejercicio';

@Component({
    selector: 'jhi-sub-rutina-dialog',
    templateUrl: './sub-rutina-dialog.component.html'
})
export class SubRutinaDialogComponent implements OnInit {

    subRutina: SubRutina;
    isSaving: boolean;

    rutinas: Rutina[];

    ejercicios: Ejercicio[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private subRutinaService: SubRutinaService,
        private rutinaService: RutinaService,
        private ejercicioService: EjercicioService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.rutinaService.query()
            .subscribe((res: HttpResponse<Rutina[]>) => { this.rutinas = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.ejercicioService.query()
            .subscribe((res: HttpResponse<Ejercicio[]>) => { this.ejercicios = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.subRutina.id !== undefined) {
            this.subscribeToSaveResponse(
                this.subRutinaService.update(this.subRutina));
        } else {
            this.subscribeToSaveResponse(
                this.subRutinaService.create(this.subRutina));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<SubRutina>>) {
        result.subscribe((res: HttpResponse<SubRutina>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: SubRutina) {
        this.eventManager.broadcast({ name: 'subRutinaListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackRutinaById(index: number, item: Rutina) {
        return item.id;
    }

    trackEjercicioById(index: number, item: Ejercicio) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-sub-rutina-popup',
    template: ''
})
export class SubRutinaPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private subRutinaPopupService: SubRutinaPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.subRutinaPopupService
                    .open(SubRutinaDialogComponent as Component, params['id']);
            } else {
                this.subRutinaPopupService
                    .open(SubRutinaDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
