import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SubRutina } from './sub-rutina.model';
import { SubRutinaPopupService } from './sub-rutina-popup.service';
import { SubRutinaService } from './sub-rutina.service';

@Component({
    selector: 'jhi-sub-rutina-delete-dialog',
    templateUrl: './sub-rutina-delete-dialog.component.html'
})
export class SubRutinaDeleteDialogComponent {

    subRutina: SubRutina;

    constructor(
        private subRutinaService: SubRutinaService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.subRutinaService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'subRutinaListModification',
                content: 'Deleted an subRutina'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-sub-rutina-delete-popup',
    template: ''
})
export class SubRutinaDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private subRutinaPopupService: SubRutinaPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.subRutinaPopupService
                .open(SubRutinaDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
