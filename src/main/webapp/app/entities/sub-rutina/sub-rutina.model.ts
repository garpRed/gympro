import { BaseEntity } from './../../shared';

export class SubRutina implements BaseEntity {
    constructor(
        public id?: number,
        public nombreSubRutina?: string,
        public cantSeries?: number,
        public cantRepeticiones?: number,
        public comentarioSubRutina?: string,
        public estadoSubrutina?: boolean,
        public rutinaId?: number,
        public subrutinaRegistros?: BaseEntity[],
        public subrutinaEjercicios?: BaseEntity[],
    ) {
        this.estadoSubrutina = false;
    }
}
