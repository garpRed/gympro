import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TestGymSharedModule } from '../../shared';
import {
    SubRutinaService,
    SubRutinaPopupService,
    SubRutinaComponent,
    SubRutinaDetailComponent,
    SubRutinaDialogComponent,
    SubRutinaPopupComponent,
    SubRutinaDeletePopupComponent,
    SubRutinaDeleteDialogComponent,
    subRutinaRoute,
    subRutinaPopupRoute,
    SubRutinaResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...subRutinaRoute,
    ...subRutinaPopupRoute,
];

@NgModule({
    imports: [
        TestGymSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        SubRutinaComponent,
        SubRutinaDetailComponent,
        SubRutinaDialogComponent,
        SubRutinaDeleteDialogComponent,
        SubRutinaPopupComponent,
        SubRutinaDeletePopupComponent,
    ],
    entryComponents: [
        SubRutinaComponent,
        SubRutinaDialogComponent,
        SubRutinaPopupComponent,
        SubRutinaDeleteDialogComponent,
        SubRutinaDeletePopupComponent,
    ],
    providers: [
        SubRutinaService,
        SubRutinaPopupService,
        SubRutinaResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestGymSubRutinaModule {}
