import { BaseEntity } from './../../shared';

export class Usuario implements BaseEntity {
    constructor(
        public id?: number,
        public nombreUsuario?: string,
        public apellidoUsuario?: string,
        public direccion?: string,
        public fechaNacimiento?: any,
        public telefono?: string,
        public identificacion?: string,
        public correoElectronico?: string,
        public diaPago?: number,
        public lesion?: boolean,
        public descLesion?: string,
        public estado?: boolean,
        public contrasenna?: string,
        public userId?: number,
        public sedeId?: number,
        public rolId?: number,
        public usuarioId?: number,
        public entrenadorUsuarios?: BaseEntity[],
        public registrosActividads?: BaseEntity[],
        public solicitudesUsuarios?: BaseEntity[],
        public invitacionesUsuarios?: BaseEntity[],
        public aceptaInvitacionId?: number,
        public rutinaId?: number,
    ) {
        this.lesion = false;
        this.estado = false;
    }
}
