import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Usuario } from './usuario.model';
import { UsuarioPopupService } from './usuario-popup.service';
import { UsuarioService } from './usuario.service';
import { User, UserService } from '../../shared';
import { Sede, SedeService } from '../sede';
import { Rol, RolService } from '../rol';
import { Invitacion, InvitacionService } from '../invitacion';
import { Rutina, RutinaService } from '../rutina';

@Component({
    selector: 'jhi-usuario-dialog',
    templateUrl: './usuario-dialog.component.html'
})
export class UsuarioDialogComponent implements OnInit {

    usuario: Usuario;
    isSaving: boolean;

    users: User[];

    sedes: Sede[];

    rols: Rol[];

    usuarios: Usuario[];

    invitacions: Invitacion[];

    rutinas: Rutina[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private usuarioService: UsuarioService,
        private userService: UserService,
        private sedeService: SedeService,
        private rolService: RolService,
        private invitacionService: InvitacionService,
        private rutinaService: RutinaService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.userService.query()
            .subscribe((res: HttpResponse<User[]>) => { this.users = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.sedeService
            .query({filter: 'usuario-is-null'})
            .subscribe((res: HttpResponse<Sede[]>) => {
                if (!this.usuario.sedeId) {
                    this.sedes = res.body;
                } else {
                    this.sedeService
                        .find(this.usuario.sedeId)
                        .subscribe((subRes: HttpResponse<Sede>) => {
                            this.sedes = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.rolService
            .query({filter: 'usuario-is-null'})
            .subscribe((res: HttpResponse<Rol[]>) => {
                if (!this.usuario.rolId) {
                    this.rols = res.body;
                } else {
                    this.rolService
                        .find(this.usuario.rolId)
                        .subscribe((subRes: HttpResponse<Rol>) => {
                            this.rols = [subRes.body].concat(res.body);
                        }, (subRes: HttpErrorResponse) => this.onError(subRes.message));
                }
            }, (res: HttpErrorResponse) => this.onError(res.message));
        this.usuarioService.query()
            .subscribe((res: HttpResponse<Usuario[]>) => { this.usuarios = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.invitacionService.query()
            .subscribe((res: HttpResponse<Invitacion[]>) => { this.invitacions = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.rutinaService.query()
            .subscribe((res: HttpResponse<Rutina[]>) => { this.rutinas = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.usuario.id !== undefined) {
            this.subscribeToSaveResponse(
                this.usuarioService.update(this.usuario));
        } else {
            this.subscribeToSaveResponse(
                this.usuarioService.create(this.usuario));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Usuario>>) {
        result.subscribe((res: HttpResponse<Usuario>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Usuario) {
        this.eventManager.broadcast({ name: 'usuarioListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }

    trackSedeById(index: number, item: Sede) {
        return item.id;
    }

    trackRolById(index: number, item: Rol) {
        return item.id;
    }

    trackUsuarioById(index: number, item: Usuario) {
        return item.id;
    }

    trackInvitacionById(index: number, item: Invitacion) {
        return item.id;
    }

    trackRutinaById(index: number, item: Rutina) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-usuario-popup',
    template: ''
})
export class UsuarioPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private usuarioPopupService: UsuarioPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.usuarioPopupService
                    .open(UsuarioDialogComponent as Component, params['id']);
            } else {
                this.usuarioPopupService
                    .open(UsuarioDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
