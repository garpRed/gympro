import { BaseEntity } from './../../shared';

export class Evento implements BaseEntity {
    constructor(
        public id?: number,
        public nombreEvento?: string,
        public descripcionEvento?: string,
        public fechaInicio?: any,
        public fechaFin?: any,
        public horaInicioEvento?: any,
        public horaFinEvento?: any,
        public estadoEvento?: boolean,
    ) {
        this.estadoEvento = false;
    }
}
